//
//  Dialogs.swift
//  online
//
//  Created by KingOP on 24.10.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit
import Haneke
import DZNEmptyDataSet

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class Dialogs: UITableViewController, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, UIViewControllerPreviewingDelegate {
    
    var dialogs : [Dictionary<String, AnyObject>] = []
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var dialogIds : [Int] = []
    
    var isLoaded : Bool = false
    
    var dialogsLimit : Int = 30
    var currentOffset : Int = 0
    
    var loadingMoreView = UIView()
    var animationView = UIActivityIndicatorView()
    
    var loadMoreDialogs = true
    var allowLoadingMore = true
    var dialogsLoaded : Bool = false
    
    var needsReload : Bool = false
    
    var isReloading : Bool = false
    
    var selectedIndexPath : IndexPath? = nil
    
    var previewVC : Messages? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if traitCollection.forceTouchCapability == .available {
            registerForPreviewing(with: self, sourceView: self.tableView)
        }
        
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.activityIndicator.isHidden = true
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(Dialogs.reloadDialogs), for: UIControlEvents.valueChanged)
        self.refreshControl = refreshControl
        
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        self.loadingMoreView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 60))
        self.animationView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        
        self.loadingMoreView.addSubview(animationView)
        
        self.animationView.center = self.loadingMoreView.center
        
        self.tableView.tableFooterView = loadingMoreView
        self.tableView.tableFooterView!.isHidden = true
        
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        
        self.title = "Диалоги"
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        
        guard let indexPath = self.tableView.indexPathForRow(at: location) else {return nil}
        
        self.selectedIndexPath = indexPath
        
        print((indexPath as NSIndexPath).row)
        
        self.previewVC = nil
        
        self.previewVC = self.storyboard?.instantiateViewController(withIdentifier: "messagesVC") as? Messages
        
        self.previewVC!.dialogId = self.dialogs[indexPath.row]["id"] as? Int
        self.previewVC!.userName = ((self.tableView.cellForRow(at: indexPath) as! DialogCell).userLogin.text)!
        self.previewVC!.avatar = ((self.tableView.cellForRow(at: indexPath)! as! DialogCell).userAvatar.image)!
        
        self.previewVC!.members = self.dialogs[indexPath.row]["members"] as! [Dictionary<String, AnyObject>]
        self.previewVC!.creatorId = self.dialogs[indexPath.row]["master_user_id"] as? Int
        
        let photo = self.dialogs[indexPath.row]["photo"]
        
        if !(photo is NSNull) && photo != nil && photo as! String != "" {
            
            self.previewVC!.avatarPath = self.dialogs[indexPath.row]["photo"] as? String
        }
        
        self.previewVC!.fromDialogsList = true
        
        return self.previewVC!
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        
        let messagesVC = self.previewVC!
        show(messagesVC, sender: self)
        self.previewVC = nil
        self.selectedIndexPath = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !isUserAuthenticated() {
            let info = JSSAlertView().info(self, title: "Личные сообщения", text: "Обмен личными сообщениями с другими пользователями доступен только авторизованным пользователям", buttonText: "Войти", cancelButtonText: "Отмена")
            
            info.addAction({ () -> Void in
                self.performSegue(withIdentifier: "goToLoginFromPM", sender: self)
            })
        } else {
            if !self.isLoaded {
                self.loadDialogs()
            }
        }
        
        isDialogsListShown = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        isDialogsListShown = false
        super.viewDidDisappear(animated)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.isLoaded && !isUserAuthenticated() {
            self.dialogIds.removeAll()
            self.dialogs.removeAll()
            self.isLoaded = false
        }
        //UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        //mainContainer?.tabBar.items![1].badgeValue = nil
        self.tableView.reloadData()
        UIView.setAnimationsEnabled(true)
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dialogs.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : DialogCell? = tableView.dequeueReusableCell(withIdentifier: "dialog", for: indexPath) as? DialogCell
        
        if cell == nil {
            let nib : Array = Bundle.main.loadNibNamed("cell", owner: self, options: nil)!
            cell = nib[0] as? DialogCell
        }
        
        let dialog = self.dialogs[indexPath.row]
        var username : String = ""
        var avatar : Dictionary<String, AnyObject>? = nil
        
        let members : [Dictionary<String, AnyObject>] = dialog["members"] as! [Dictionary<String, AnyObject>]
        
        if members.count > 2 {
            if !(dialog["title"] is NSNull) && dialog["title"] != nil && dialog["title"] as! String != "" {
                username = dialog["title"] as! String
            } else {
                for user in members {
                    username += user["username"] as! String + ", "
                }
                
                username = username.truncate(username.length - 2)
            }
        } else {
            for user in members {
                if user["id"] as! Int != getUserId()! {
                    username = user["username"] as! String
                    
                    if !(user["photo"] is NSNull) && user["photo"] != nil && user["photo"] as! String != "" {
                        // у пользователя есть аватарка, сохраним ее во временную переменную
                        avatar = [
                            "id"    : user["id"] as! Int as AnyObject,
                            "photo" : user["photo"] as! String as AnyObject
                        ]
                    } else {
                        avatar = [
                            "id"    : user["id"] as! Int as AnyObject
                        ]
                    }
                    
                    break
                }
            }
        }
        
        self.dialogIds.append(dialog["id"] as! Int)
        
        cell?.userAvatar.image = maskedAvatarFromImage(UIImage(named: "nophoto.jpg")!)
        cell?.userLogin.text = username
        
        var messageText : String = "Нет сообщений"
        cell?.messageDate.text = ""
        
        if !(dialog["message"] is NSNull) && dialog["message"] != nil {
            messageText = ((dialog["message"] as! NSDictionary)["message"] as? String)!
            cell?.messageDate.text = stringDateFromTimestamp((dialog["message"] as! NSDictionary)["created_at"] as! Double)

            if !((dialog["message"] as! NSDictionary)["attachment"] is NSNull) && (dialog["message"] as! NSDictionary)["attachment"] != nil {
                if ((dialog["message"] as! NSDictionary)["attachment"] as! NSDictionary)["type"] as! String == "location" {
                    messageText = "Местоположение"
                } else {
                    messageText = "Изображение"
                }
            }
        }
        
        cell?.message.text = messageText
        cell?.message.sizeToFit()
        
        if Int(dialog["unread"] as! String) == 0 {
            cell?.unreadCount.text = ""
            cell?.backgroundColor = UIColor.white
            cell?.unreadCountBG.isHidden = true
        } else {
            let unreadCountInt : Int = Int(dialog["unread"] as! String)!
            /*UIApplication.sharedApplication().applicationIconBadgeNumber += unreadCountInt
            mainContainer?.tabBar.items![1].badgeValue = UIApplication.sharedApplication().applicationIconBadgeNumber.description*/
            cell?.unreadCount.text = unreadCountInt > 99 ? "99+" : dialog["unread"] as! String
            cell?.unreadCountBG.isHidden = false
            cell?.backgroundColor = UIColor(netHex: 0xd9e2ee)
        }
        
        // вот тут надо кэш аватарок и вот это все вывести
        if members.count > 2 {
            cell?.userAvatar.image = maskedAvatarFromImage(UIImage(named: "group.png")!)
            if !(dialog["photo"] is NSNull) && dialog["photo"] != nil && dialog["photo"] as! String != "" {
                
                
                let urlString = "\(getBackendDomainPath())\(dialog["photo"] as! String)"
                let imgURL = URL(string: urlString)
                
                if let img = avatarCache[urlString] {
                    cell?.userAvatar!.image = img
                    cell?.userAvatar!.tag = (indexPath.row + 1)
                }
                else {
                    // The image isn't cached, download the img data
                    // We should perform this in a background thread
                    let request: URLRequest = URLRequest(url: imgURL!)
                    let mainQueue = OperationQueue.main
                    NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
                        if error == nil {
                            
                            let res = response as! HTTPURLResponse
                            if res.statusCode != 404 {
                                // Convert the downloaded data in to a UIImage object
                                
                                if data != nil {
                                    let image : UIImage = maskedAvatarFromImage(UIImage(data: data!)!)
                                    // Store the image in to our cache
                                    avatarCache[urlString] = image
                                    
                                    // Update the cell
                                    DispatchQueue.main.async(execute: {
                                        if let cellToUpdate = tableView.cellForRow(at: indexPath) as? DialogCell {
                                            cellToUpdate.userAvatar!.image = image
                                            cellToUpdate.userAvatar!.tag = (indexPath.row + 1)
                                        }
                                    })
                                } else {
                                    DispatchQueue.main.async(execute: {
                                        if let cellToUpdate = tableView.cellForRow(at: indexPath) as? DialogCell {
                                            cell?.userAvatar?.image = maskedAvatarFromImage(UIImage(named: "group.png")!, original: true)
                                            cellToUpdate.userAvatar!.tag = (indexPath.row + 1)
                                        }
                                    })
                                }
                            } else {
                                DispatchQueue.main.async(execute: {
                                    if let cellToUpdate = tableView.cellForRow(at: indexPath) as? DialogCell {
                                        cell?.userAvatar?.image = maskedAvatarFromImage(UIImage(named: "group.png")!, original: true)
                                        cellToUpdate.userAvatar!.tag = (indexPath.row + 1)
                                    }
                                })
                            }
                        }
                        else {
                            DispatchQueue.main.async(execute: {
                                if let cellToUpdate = tableView.cellForRow(at: indexPath) as? DialogCell {
                                    cell?.userAvatar.image = maskedAvatarFromImage(UIImage(named: "group.png")!, original: true)
                                    cellToUpdate.userAvatar!.tag = (indexPath.row + 1)
                                }
                            })
                        }
                    })
                }
            } else {
                cell?.userAvatar.image = maskedAvatarFromImage(UIImage(named: "group.png")!)
                cell?.userAvatar.tag = indexPath.row + 1
            }
        } else {
            cell?.userAvatar!.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!, original: true)
            if avatar?.count > 1 {
                // есть аватарка
                let urlString = "\(getBackendDomainPath())\(avatar!["photo"] as! String)"
                let imgURL = URL(string: urlString)
                
                if let img = avatarCache[urlString] {
                    cell?.userAvatar!.image = img
                    cell?.userAvatar!.tag = (indexPath.row + 1)
                }
                else {
                    // The image isn't cached, download the img data
                    // We should perform this in a background thread
                    let request: URLRequest = URLRequest(url: imgURL!)
                    let mainQueue = OperationQueue.main
                    NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
                        if error == nil {
                            
                            let res = response as! HTTPURLResponse
                            if res.statusCode != 404 {
                                // Convert the downloaded data in to a UIImage object
                                
                                if data != nil {
                                    let image : UIImage = maskedAvatarFromImage(UIImage(data: data!)!)
                                    // Store the image in to our cache
                                    avatarCache[urlString] = image
                                    
                                    // Update the cell
                                    DispatchQueue.main.async(execute: {
                                        if let cellToUpdate = tableView.cellForRow(at: indexPath) as? DialogCell {
                                            cellToUpdate.userAvatar!.image = image
                                            cellToUpdate.userAvatar!.tag = (indexPath.row + 1)
                                        }
                                    })
                                } else {
                                    DispatchQueue.main.async(execute: {
                                        if let cellToUpdate = tableView.cellForRow(at: indexPath) as? DialogCell {
                                            cell?.userAvatar?.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!, original: true)
                                            cellToUpdate.userAvatar!.tag = (indexPath.row + 1)
                                        }
                                    })
                                }
                            } else {
                                DispatchQueue.main.async(execute: {
                                    if let cellToUpdate = tableView.cellForRow(at: indexPath) as? DialogCell {
                                        cell?.userAvatar?.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!, original: true)
                                        cellToUpdate.userAvatar!.tag = (indexPath.row + 1)
                                    }
                                })
                            }
                        }
                        else {
                            DispatchQueue.main.async(execute: {
                                if let cellToUpdate = tableView.cellForRow(at: indexPath) as? DialogCell {
                                    cell?.userAvatar.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!, original: true)
                                    cellToUpdate.userAvatar!.tag = (indexPath.row + 1)
                                }
                            })
                        }
                    })
                }
            } else {
                cell?.userAvatar.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!)
                cell?.userAvatar.tag = indexPath.row + 1
            }
        }
        
        return cell!

    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    func startActivityAnimating() {
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
    }
    
    func stopActivityAnimating() {
        self.activityIndicator.isHidden = true
        self.activityIndicator.stopAnimating()
    }
    
    func loadDialogs(_ noAnimation : Bool = false, append: Bool = false) {
        // загрузка списка диалогов
        let getDialogsRequest = getURLRequest("GET", url: getBackendApiPath() + "dialog?limit=\(self.dialogsLimit)&offset=\(self.currentOffset)", params: Dictionary<String, AnyObject>(), token: getUserToken())
        
        let loadDialogsTask = URLSession.shared.dataTask(with: getDialogsRequest, completionHandler: { (data, response, error) in
            
            self.isLoaded = true
            
            if (error != nil) {
                // сюда надо добавить обработку ошибок
                DispatchQueue.main.async(execute: {
                    self.stopActivityAnimating()
                    self.refreshControl?.endRefreshing()
                    
                    JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                })
            } else {
                let dialogsResponse = response as! HTTPURLResponse
                
                print(dialogsResponse.statusCode)
                
                if dialogsResponse.statusCode == 200 {
                    let dialogList = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [Dictionary<String, AnyObject>]
                    
                    if dialogList.count == 0 && !append {
                        self.allowLoadingMore = false
                        self.dialogs = dialogList
                    } else if dialogList.count == 0 && append {
                        self.currentOffset = self.dialogs.count
                        self.allowLoadingMore = false
                    } else {
                        
                        if !append {
                            self.dialogs.removeAll()
                            self.dialogIds.removeAll()
                            self.dialogs = dialogList
                            
                            if dialogList.count < self.dialogsLimit {
                                self.allowLoadingMore = false
                            }
                        } else {
                            if dialogList.count >= self.dialogsLimit {
                                self.dialogs.append(contentsOf: dialogList)
                                self.allowLoadingMore = true
                            } else if dialogList.count > 0 && dialogList.count < self.dialogsLimit {
                                self.dialogs.append(contentsOf: dialogList)
                                self.allowLoadingMore = false
                            } else if dialogList.count == 0 {
                                self.allowLoadingMore = false
                            }
                        }
                        
                        self.currentOffset = self.dialogs.count
                        self.loadMoreDialogs = false
                        self.isReloading = false
                    }
                    
                    self.isLoaded = true
                    
                    DispatchQueue.main.async(execute: {
                        dialogsController = self
                        
                        //UIApplication.sharedApplication().applicationIconBadgeNumber = 0
                        //mainContainer?.tabBar.items![1].badgeValue = nil
                        self.tableView.tableFooterView!.isHidden = true
                        self.tableView.reloadData()
                        self.tableView.reloadEmptyDataSet()
                        
                        self.stopActivityAnimating()
                        self.refreshControl?.endRefreshing()
                        
                        if !append {
                            updateNotificationCount()
                        }
                    })
                } else if dialogsResponse.statusCode == 500 {
                    DispatchQueue.main.async(execute: {
                        self.stopActivityAnimating()
                        self.refreshControl?.endRefreshing()
                        
                        JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                    })
                } else if dialogsResponse.statusCode == 401 {
                    error401Action(self)
                }
            }
        }) 
        
        if !noAnimation {
            self.startActivityAnimating()
        }
        
        loadDialogsTask.resume()
    }
    
    func reloadDialogs() {
        self.isLoaded = false
        self.dialogsLoaded = false
        self.allowLoadingMore = true
        self.currentOffset = 0
        //self.dialogs.removeAllObjects()
        //self.dialogIds.removeAll()
        //self.tableView.reloadData()
        self.loadDialogs(true)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndexPath = indexPath
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToDialogView" {
            
            /*if self.tableView.indexPathForSelectedRow != nil {
                self.selectedIndexPath = self.tableView.indexPathForSelectedRow
            }*/
            
            //if self.selectedIndexPath != nil {
            self.selectedIndexPath = self.tableView.indexPathForSelectedRow
            let indexPath = self.tableView.indexPathForSelectedRow
            let destVC : Messages = segue.destination as! Messages
            destVC.dialogId = self.dialogs[indexPath!.row]["id"] as? Int
            destVC.userName = ((self.tableView.cellForRow(at: indexPath!)! as! DialogCell).userLogin.text)!
            destVC.avatar = ((self.tableView.cellForRow(at: indexPath!)! as! DialogCell).userAvatar.image)!
            
            destVC.members = self.dialogs[indexPath!.row]["members"] as! [Dictionary<String, AnyObject>]
            destVC.creatorId = self.dialogs[indexPath!.row]["master_user_id"] as? Int
            
            let photo = self.dialogs[indexPath!.row]["photo"]
            
            if !(photo is NSNull) && photo != nil && photo as! String != "" {
                
                destVC.avatarPath = self.dialogs[indexPath!.row]["photo"] as? String
            }
            
            destVC.fromDialogsList = true
            //}
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            // удаление диалога
            // спросим подтверждения
            
            let confirmAlertController : UIAlertController = UIAlertController(title: "Подтверждение", message: "Вы точно хотите удалить выбранный диалог?", preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let deleteAction : UIAlertAction = UIAlertAction(title: "Удалить", style: UIAlertActionStyle.destructive, handler: { (UIAlertAction) -> Void in
                
                // удаляем диалог
                let deleteDialogRequest = getURLRequest("DELETE", url: getBackendApiPath() + "dialog/\(self.dialogs[indexPath.row]["id"] as! Int)", params: Dictionary<String, AnyObject>(), token: getUserToken())
                
                let deleteDialogTask = URLSession.shared.dataTask(with: deleteDialogRequest, completionHandler: {
                    (data, response, error) in
                    
                    if (error != nil) {
                        // сюда надо добавить обработку ошибок
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                        })
                    } else {
                        let res = response as! HTTPURLResponse
                        
                        print(res.statusCode)
                        
                        if res.statusCode == 204 {
                            // удаление на сервере прошло успешно
                            DispatchQueue.main.async(execute: {
                                SwiftSpinner.hide()
                                
                                // уменьшим количество непрочитанных на вкладке и бейдже
                                if Int(self.dialogs[indexPath.row]["unread"] as! String) > 0 {
                                    UIApplication.shared.applicationIconBadgeNumber -= Int(self.dialogs[indexPath.row]["unread"] as! String)!
                                    
                                    UIApplication.shared.applicationIconBadgeNumber = max(0, UIApplication.shared.applicationIconBadgeNumber)
                                    
                                    mainContainer?.tabBar.items![1].badgeValue = UIApplication.shared.applicationIconBadgeNumber > 0 ? UIApplication.shared.applicationIconBadgeNumber.description : nil
                                }
                                
                                self.dialogs.remove(at: indexPath.row)
                                self.dialogIds.remove(at: indexPath.row)
                                self.tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.left)
                                
                                if self.dialogs.count == 0 {
                                    self.tableView.reloadData()
                                }
                            })
                        } else if res.statusCode == 401 {
                            error401Action(self)
                        } else if res.statusCode == 403 {
                            let deleteResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary<String, AnyObject>
                            
                            DispatchQueue.main.async(execute: {
                                SwiftSpinner.hide()
                                
                                JSSAlertView().danger(self, title: "Ошибка", text: deleteResult!["message"] as? String, buttonText: "Закрыть")
                            })
                        } else if res.statusCode == 500 {
                            DispatchQueue.main.async(execute: {
                                SwiftSpinner.hide()
                                
                                JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                            })
                        }
                    }
                }) 
                
                SwiftSpinner.show("Удаление диалога...", animated: true, viewToDisplay: self.view)
                deleteDialogTask.resume()
            })
            
            confirmAlertController.addAction(deleteAction)
            
            // отмена
            let cancelAction : UIAlertAction = UIAlertAction(title: "Отмена", style: UIAlertActionStyle.cancel, handler: nil)
            
            confirmAlertController.addAction(cancelAction)
            
            self.present(confirmAlertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func markAllDialogsAsRead(_ sender: AnyObject) {
        // отметка всех диалогов прочитанными
        // спросим подтверждения
        
        let confirmAlertController : UIAlertController = UIAlertController(title: "Подтверждение", message: "Вы точно хотите отметить все диалоги прочитанными?", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let markAction : UIAlertAction = UIAlertAction(title: "Отметить прочитанными", style: UIAlertActionStyle.destructive, handler: { (UIAlertAction) -> Void in
            
            // удаляем диалог
            let markAllDialogsRequest = getURLRequest("POST", url: getBackendApiPath() + "dialog/read-all", params: Dictionary<String, AnyObject>(), token: getUserToken())
            
            let markAllDialogsTask = URLSession.shared.dataTask(with: markAllDialogsRequest, completionHandler: {
                (data, response, error) in
                
                if (error != nil) {
                    // сюда надо добавить обработку ошибок
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                    })
                } else {
                    let res = response as! HTTPURLResponse
                    
                    print(res.statusCode)
                    
                    if res.statusCode == 204 {
                        // отметка прошла успешно
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            
                            UIApplication.shared.applicationIconBadgeNumber = 0
                            
                            mainContainer?.tabBar.items![1].badgeValue = nil
                            
                            self.reloadDialogs()
                        })
                    } else if res.statusCode == 401 {
                        error401Action(self)
                    } else if res.statusCode == 500 {
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            
                            JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                        })
                    }
                }
            }) 
            
            SwiftSpinner.show("Сохранение...", animated: true, viewToDisplay: self.view)
            markAllDialogsTask.resume()
        })
        
        confirmAlertController.addAction(markAction)
        
        // отмена
        let cancelAction : UIAlertAction = UIAlertAction(title: "Отмена", style: UIAlertActionStyle.cancel, handler: nil)
        
        confirmAlertController.addAction(cancelAction)
        
        self.present(confirmAlertController, animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        let deltaOffset = maximumOffset - currentOffset
        
        if deltaOffset <= 0 {
            loadMore()
        }
    }
    
    func loadMore() {
        if (!self.loadMoreDialogs && self.allowLoadingMore && !self.isReloading && self.currentOffset >= self.dialogsLimit) {
            self.loadMoreDialogs = true
            self.isReloading = true
            self.animationView.startAnimating()
            self.tableView.tableFooterView!.isHidden = false
            loadMoreBegin("загрузка...",
                loadMoreEnd: {(x:Int) -> () in
            })
        }
    }
    
    func loadMoreBegin(_ newtext:String, loadMoreEnd:@escaping (Int) -> ()) {
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
            self.loadDialogs(true, append: true)
            
            DispatchQueue.main.async {
                loadMoreEnd(0)
            }
        }
    }
    
    // ----------- EMPTY DATASET --------------
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let string : String = "Диалогов нет"
        
        let attributes : [String : AnyObject] = [
            NSFontAttributeName : UIFont.boldSystemFont(ofSize: 32),
            NSForegroundColorAttributeName : UIColor.gray
        ]
        
        return NSAttributedString(string: string, attributes: attributes)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let string : String = "Вы еще ни с кем не заводили беседы"
        
        let paragraph : NSMutableParagraphStyle = NSMutableParagraphStyle()
        paragraph.lineBreakMode = .byWordWrapping
        paragraph.alignment = .center
        
        let attributes : [String : AnyObject] = [
            NSFontAttributeName : UIFont.systemFont(ofSize: 14),
            NSForegroundColorAttributeName : UIColor.lightGray,
            NSParagraphStyleAttributeName : paragraph
        ]
        
        return NSAttributedString(string: string, attributes: attributes)
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        if self.dialogs.count == 0 && self.isLoaded {
            return true
        }
        
        return false
    }
    
    func emptyDataSetShouldAllowTouch(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    // ----------- EMPTY DATASET --------------
}
