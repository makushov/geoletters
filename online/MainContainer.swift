//
//  MainContainer.swift
//  online
//
//  Created by KingOP on 04.11.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit

@objc class MainContainer: UITabBarController {
    
    let barItemsTitles : [String] = [
        "Карта",
        "Диалоги",
        "Избранные",
        "Настройки"
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var i : Int = 0
        
        for item in (self.tabBar.items as [UITabBarItem]!) {
            if let image = item.selectedImage {
                item.selectedImage = image.imageWithColor(UIColor(netHex: 0x3870A3)).withRenderingMode(.alwaysOriginal)
            }

            item.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor(netHex: 0x185996)], for:UIControlState.selected)
            
            item.title = self.barItemsTitles[i]
            
            i += 1
        }
        
        mainContainer = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBar.items![1].badgeValue = UIApplication.shared.applicationIconBadgeNumber == 0 ? nil : UIApplication.shared.applicationIconBadgeNumber.description
        UIView.setAnimationsEnabled(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .all
    }
}
