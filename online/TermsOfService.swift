//
//  TermsOfService.swift
//  Letters
//
//  Created by KingOP on 01.12.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit

class TermsOfService: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // тут мы загрузим правила
        self.webView.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        SwiftSpinner.show("Загрузка...")
        self.webView.loadRequest(URLRequest(url: URL(string: "\(getBackendApiPath())page?locale=ru&alias=tos&as_html=1")!))
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SwiftSpinner.hide()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func acceptAction(_ sender: AnyObject) {
        
        let confirmAlertController : UIAlertController = UIAlertController(title: "Подтверждение", message: "Нажимая \"Подтверждаю\" Вы соглашаетесь с правилами ресурса.", preferredStyle: .actionSheet)
        
        let confirmAction : UIAlertAction = UIAlertAction(title: "Подтверждаю", style: .default) { (UIAlertAction) -> Void in
            
            setTOSShown()
            
            let tutorialVC : TutorialPlaceholder = self.storyboard?.instantiateViewController(withIdentifier: "TutorialPlaceholder") as! TutorialPlaceholder
            
            self.present(tutorialVC, animated: true, completion: nil)
        }
        
        confirmAlertController.addAction(confirmAction)
        confirmAlertController.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        
        self.present(confirmAlertController, animated: true, completion: nil)
    }
}
