//
//  MyPins.swift
//  Letters
//
//  Created by KingOP on 16.11.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class MyPins: UITableViewController, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource, UIViewControllerPreviewingDelegate {
    
    var pins : [Dictionary<String, AnyObject>] = []
    var expiredPins : [Dictionary<String, AnyObject>] = []
    @IBOutlet weak var pinsTypeSwitcher: UISegmentedControl!
    var activeLoaded : Bool = false
    var expiredLoaded : Bool = false
    
    var pinsLimit : Int = 30
    var currentActiveOffset : Int = 0
    var currentExpiredOffset : Int = 0
    
    var totalActiveCount : Int = 0
    var totalExpiredCount : Int = 0
    
    var allowLoadingMoreActive : Bool = true
    var allowLoadingMoreExpired : Bool = true
    
    var isReloading : Bool = false
    
    var loadingMoreView : UIView = UIView()
    var animationView : UIActivityIndicatorView = UIActivityIndicatorView()
    
    var loadMoreActive : Bool = true
    var loadMoreExpired : Bool = true
    
    var pinToEdit : Dictionary<String, AnyObject>? = nil
    
    var indexToUpdate : Int? = nil
    
    var previewVC : PinDetailView? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(MyPins.reloadPins), for: UIControlEvents.valueChanged)
        self.refreshControl = refreshControl
        
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        self.loadingMoreView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 60))
        self.animationView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        
        self.loadingMoreView.addSubview(animationView)
        
        self.animationView.center = self.loadingMoreView.center
        
        self.tableView.tableFooterView = loadingMoreView
        self.tableView.tableFooterView!.isHidden = true
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        
        //self.tableView.registerNib(UINib(nibName: "PinCell", bundle: nil), forCellReuseIdentifier: "myPin")
        
        if traitCollection.forceTouchCapability == .available {
            registerForPreviewing(with: self, sourceView: self.tableView)
        }
        
        self.loadPins()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.tableView.indexPathForSelectedRow != nil {
            self.tableView.deselectRow(at: self.tableView.indexPathForSelectedRow!, animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 83//max(60, UITableViewAutomaticDimension)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "goToMyPinDetailView", sender: self)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "myPin", for: indexPath)
        
        cell.textLabel?.text = self.pinsTypeSwitcher.selectedSegmentIndex == 0 ? self.pins[indexPath.row]["status"] as? String : self.expiredPins[indexPath.row]["status"] as? String
        
        cell.detailTextLabel?.text = "\(stringDateFromTimestamp(self.pinsTypeSwitcher.selectedSegmentIndex == 0 ? self.pins[indexPath.row]["created_at"] as! Double : self.expiredPins[indexPath.row]["created_at"] as! Double))\nПонравилось: \(self.pinsTypeSwitcher.selectedSegmentIndex == 0 ? (self.pins[indexPath.row]["likes_count"] as! Int).description : (self.expiredPins[indexPath.row]["likes_count"] as! Int).description)\nКомментарии: \(self.pinsTypeSwitcher.selectedSegmentIndex == 0 ? (self.pins[indexPath.row]["comments_count"] as! Int).description : (self.expiredPins[indexPath.row]["comments_count"] as! Int).description)"
        
        
        var imageName : String = "pinIcon"
        
        if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
            if (self.pins[indexPath.row]["category_id"] as! Int) < pinsCategories.count {
                imageName = pinsCategories[self.pins[indexPath.row]["category_id"] as! Int].image
                
                if (self.pins[indexPath.row]["is_special"] as! Int) == 1 {
                    imageName = "\(imageName)Urgency"
                }
            }
        } else {
            if (self.expiredPins[indexPath.row]["category_id"] as! Int) < pinsCategories.count {
                imageName = pinsCategories[self.expiredPins[indexPath.row]["category_id"] as! Int].image
                
                if (self.expiredPins[indexPath.row]["is_special"] as! Int) == 1 {
                    imageName = "\(imageName)Urgency"
                }
            }
        }
        
        cell.imageView?.image = UIImage(named: imageName)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let editAction : UITableViewRowAction = UITableViewRowAction(style: .normal, title: "Изменить") { (action, indexPath) -> Void in
            
            self.pinToEdit = self.pinsTypeSwitcher.selectedSegmentIndex == 0 ? self.pins[indexPath.row] : self.expiredPins[indexPath.row]
            
            self.indexToUpdate = indexPath.row
            
            self.performSegue(withIdentifier: "goToPinEdit", sender: self)
        }
        
        let deleteAction : UITableViewRowAction = UITableViewRowAction(style: .default, title: "Удалить") { (action, indexPath) -> Void in
            
            // удаление метки
            // сначала спрочим подтверждения
            let confirmAlertController : UIAlertController = UIAlertController(title: "Подтверждение", message: "Удалить метку?", preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let deleteAction : UIAlertAction = UIAlertAction(title: "Удалить", style: UIAlertActionStyle.destructive, handler: { (UIAlertAction) -> Void in
                
                let id : Int = self.pinsTypeSwitcher.selectedSegmentIndex == 0 ? self.pins[indexPath.row]["id"] as! Int : self.expiredPins[indexPath.row]["id"] as! Int
                
                // удаление
                let deletePinQuery = getURLRequest("DELETE", url: getBackendApiPath() + "temprorary-status/\(id)", params: Dictionary<String, AnyObject>(), token: getUserToken()!)
                let deletePinTask = URLSession.shared.dataTask(with: deletePinQuery, completionHandler: {
                    (data, response, error) in
                    
                    if (error != nil) {
                        // сюда надо добавить обработку ошибок
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            
                            JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                        })
                    } else {
                        let res = response as! HTTPURLResponse
                        
                        print(res.statusCode)
                        
                        if res.statusCode == 204 {
                            // метка успешно удалена
                            
                            if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
                                self.pins.remove(at: indexPath.row)
                            } else {
                                self.expiredPins.remove(at: indexPath.row)
                            }
                            
                            DispatchQueue.main.async(execute: {
                                SwiftSpinner.hide()
                                
                                self.tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.left)
                                
                                if self.pinsTypeSwitcher.selectedSegmentIndex == 0 && self.pins.count == 0 {
                                    self.tableView.reloadData()
                                } else if self.pinsTypeSwitcher.selectedSegmentIndex == 1 && self.expiredPins.count == 0 {
                                    self.tableView.reloadData()
                                }
                            })
                        } else if res.statusCode == 401 {
                            error401Action(self)
                        } else if res.statusCode == 500 {
                            DispatchQueue.main.async(execute: {
                                SwiftSpinner.hide()
                                
                                JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                            })
                        }
                    }
                }) 
                
                SwiftSpinner.show("Удаление метки...", animated: true, viewToDisplay: self.view)
                deletePinTask.resume()
            })
            
            confirmAlertController.addAction(deleteAction)
            
            // отмена
            let cancelAction : UIAlertAction = UIAlertAction(title: "Отмена", style: UIAlertActionStyle.cancel, handler: nil)
            
            confirmAlertController.addAction(cancelAction)
            
            self.present(confirmAlertController, animated: true, completion: nil)
        }
        
        return [deleteAction, editAction]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pinsTypeSwitcher.selectedSegmentIndex == 0 ? self.pins.count : self.expiredPins.count
    }

    @IBAction func changePinsType(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            if !activeLoaded {
                self.loadPins()
            } else {
                self.tableView.reloadData()
            }
        } else {
            if !expiredLoaded {
                self.loadPins()
            } else {
                self.tableView.reloadData()
            }
        }
    }
    
    func reloadPins() {
        self.isReloading = true
        
        if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
            self.activeLoaded = false
            self.allowLoadingMoreActive = true
            self.currentActiveOffset = 0
            //self.pins.removeAllObjects()
            //self.tableView.reloadData()
            
            self.loadPins(true)
        } else {
            self.expiredLoaded = false
            self.allowLoadingMoreExpired = true
            self.currentExpiredOffset = 0
            //self.expiredPins.removeAllObjects()
            //self.tableView.reloadData()
            
            self.loadPins(true)
        }
    }
    
    func deletePinsByType(_ mode: String = "all") {
        var request : URLRequest
        
        if mode == "active" {
            request = getURLRequest("DELETE", url: getBackendApiPath() + "temprorary-status/active", params: Dictionary<String, AnyObject>(), token: getUserToken()!)
        } else if mode == "inactive" {
            request = getURLRequest("DELETE", url: getBackendApiPath() + "temprorary-status/inactive", params: Dictionary<String, AnyObject>(), token: getUserToken()!)
        } else {
            request = getURLRequest("DELETE", url: getBackendApiPath() + "temprorary-status/all", params: Dictionary<String, AnyObject>(), token: getUserToken()!)
        }
        
        let deletePinTask = URLSession.shared.dataTask(with: request, completionHandler: {
            (data, response, error) in
            
            if (error != nil) {
                // сюда надо добавить обработку ошибок
                DispatchQueue.main.async(execute: {
                    SwiftSpinner.hide()
                    
                    JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                })
            } else {
                let res = response as! HTTPURLResponse
                
                print(res.statusCode)
                
                if res.statusCode == 204 {
                    // метка успешно удалена
                    if mode == "active" {
                        self.pins.removeAll()
                        self.expiredPins.removeAll()
                    } else {
                        self.pins.removeAll()
                        self.expiredPins.removeAll()
                    }
                    
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        
                        self.tableView.reloadData()
                    })
                } else if res.statusCode == 401 {
                    error401Action(self)
                } else if res.statusCode == 500 {
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        
                        JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                    })
                }
            }
        }) 
        
        SwiftSpinner.show("Удаление...", animated: true, viewToDisplay: self.view)
        deletePinTask.resume()
    }
    
    @IBAction func deleteAllPins(_ sender: AnyObject) {
        let confirmAlertController : UIAlertController = UIAlertController(title: "Удаление меток", message: "Какие метки Вы хотите удалить?", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let deleteActiveAction : UIAlertAction = UIAlertAction(title: "Активные", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            
            self.deletePinsByType("active")
        })
        
        confirmAlertController.addAction(deleteActiveAction)
        
        let deleteInactiveAction : UIAlertAction = UIAlertAction(title: "Истекшие", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            
            self.deletePinsByType("inactive")
        })
        
        confirmAlertController.addAction(deleteInactiveAction)
        
        let deleteAllAction : UIAlertAction = UIAlertAction(title: "Все", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            
            self.deletePinsByType("all")
        })
        
        confirmAlertController.addAction(deleteAllAction)
        
        // отмена
        let cancelAction : UIAlertAction = UIAlertAction(title: "Отмена", style: UIAlertActionStyle.cancel, handler: nil)
        
        confirmAlertController.addAction(cancelAction)
        
        self.present(confirmAlertController, animated: true, completion: nil)

    }
    
    func loadPins(_ noAnimation : Bool = false, append: Bool = false) {
        self.isReloading = true
        
        if !noAnimation {
            SwiftSpinner.show("Загрузка меток...", animated: true, viewToDisplay: self.view)
        }
        
        let loadMarkersRequest = getURLRequest("GET", url: getBackendApiPath() + "temprorary-status/me?active=\(self.pinsTypeSwitcher.selectedSegmentIndex == 0 ? 1 : 0)&limit=\(self.pinsLimit)&offset=\(self.pinsTypeSwitcher.selectedSegmentIndex == 0 ? self.currentActiveOffset : currentExpiredOffset)", params: Dictionary<String,String>(), token: getUserToken()!)
        let loadMarkersDataTask = URLSession.shared.dataTask(with: loadMarkersRequest, completionHandler: { (data, response, error) in
            
            if (error != nil) {
                // сюда надо добавить обработку ошибок
                DispatchQueue.main.async(execute: {
                    SwiftSpinner.hide()
                    self.refreshControl?.endRefreshing()
                    
                    self.isReloading = false
                    self.loadMoreActive = false
                    self.loadMoreExpired = false
                    
                    JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                })
            } else {
                let markersResponse = response as! HTTPURLResponse
                
                print(markersResponse.statusCode)
                
                if markersResponse.statusCode == 200 {
                    
                    if !append {
                        if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
                            self.totalActiveCount = Int(markersResponse.allHeaderFields["X-Pagination-Total-Count"] as! String)!
                        } else {
                            self.totalExpiredCount = Int(markersResponse.allHeaderFields["X-Pagination-Total-Count"] as! String)!
                        }
                    }
                    
                    let markersResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [Dictionary<String, AnyObject>]
                    
                    if markersResult?.count == 0 && !append {
                        // меток нет вообще ((
                        if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
                            
                            // нет активных меток
                            self.allowLoadingMoreActive = false
                            self.pins = markersResult!
                        } else {
                            
                            // нет истекших меток
                            self.allowLoadingMoreExpired = false
                            self.expiredPins = markersResult!
                        }
                    } else if markersResult?.count == 0 && append {
                        // при подгрузке ничего не подгрузилось
                        if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
                            
                            self.currentActiveOffset = self.pins.count
                            self.allowLoadingMoreActive = false
                        } else {
                            self.currentExpiredOffset = self.expiredPins.count
                            self.allowLoadingMoreExpired = false
                        }
                    } else {
                        
                        if !append {
                            // это первая загрузка
                            if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
                                self.pins.removeAll()
                                self.pins = markersResult!
                                
                                if markersResult?.count < self.pinsLimit {
                                    self.allowLoadingMoreActive = false
                                }
                            } else {
                                self.expiredPins.removeAll()
                                self.expiredPins = markersResult!
                                
                                if markersResult?.count < self.pinsLimit {
                                    self.allowLoadingMoreExpired = false
                                }
                            }
                        } else {
                            // это подгрузка
                            if markersResult?.count >= self.pinsLimit {
                                if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
                                    self.pins.append(contentsOf: markersResult!)
                                    self.allowLoadingMoreActive = true
                                } else {
                                    self.expiredPins.append(contentsOf: markersResult!)
                                    self.allowLoadingMoreExpired = true
                                }
                            } else if markersResult?.count > 0 && markersResult?.count < self.pinsLimit {
                                if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
                                    self.pins.append(contentsOf: markersResult!)
                                    self.allowLoadingMoreActive = false
                                } else {
                                    self.expiredPins.append(contentsOf: markersResult!)
                                    self.allowLoadingMoreExpired = false
                                }
                            } else if markersResult?.count == 0 {
                                if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
                                    self.allowLoadingMoreActive = false
                                } else {
                                    self.allowLoadingMoreExpired = false
                                }
                            }
                        }
                    }
                    
                    if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
                        self.loadMoreActive = false
                        self.currentActiveOffset = self.pins.count
                        self.activeLoaded = true
                    } else {
                        self.loadMoreExpired = false
                        self.currentExpiredOffset = self.expiredPins.count
                        self.expiredLoaded = true
                    }
                    
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        
                        self.isReloading = false
                        self.loadMoreActive = false
                        self.loadMoreExpired = false
                        self.tableView.tableFooterView?.isHidden = true
                        
                        self.refreshControl?.endRefreshing()
                        self.tableView.reloadData()
                    })
                } else if markersResponse.statusCode == 401 {
                    error401Action(self)
                } else if markersResponse.statusCode == 500 {
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        
                        JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                    })
                }
            }
        }) 
        
        loadMarkersDataTask.resume()
    }
    
    
    // бесконечный скролл
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        let deltaOffset = maximumOffset - currentOffset
        
        if deltaOffset <= 0 {
            loadMore()
        }
    }
    
    func loadMore() {
        if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
            if (!self.loadMoreActive && self.allowLoadingMoreActive && !self.isReloading && self.currentActiveOffset >= self.pinsLimit) {
                self.loadMoreActive = true
                self.isReloading = true
                self.animationView.startAnimating()
                self.tableView.tableFooterView?.isHidden = false
                loadMoreBegin("Загрузка меток...",
                    loadMoreEnd: {(x:Int) -> () in
                })
            }
        } else {
            if (!self.loadMoreExpired && self.allowLoadingMoreExpired && !self.isReloading && self.currentExpiredOffset >= self.pinsLimit) {
                self.loadMoreExpired = true
                self.isReloading = true
                self.animationView.startAnimating()
                self.tableView.tableFooterView?.isHidden = false
                loadMoreBegin("Загрузка меток...",
                    loadMoreEnd: {(x:Int) -> () in
                })
            }
        }
    }
    
    func loadMoreBegin(_ newtext:String, loadMoreEnd:@escaping (Int) -> ()) {
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
            self.loadPins(true, append: true)//loadFriends((self.usersLimit), offset: (self.currentOffset), appendData: true)
            
            DispatchQueue.main.async {
                loadMoreEnd(0)
            }
        }
    }
    
    // -------------------------------------------------------------
    // ------------------ 3D Touch ---------------------------------
    // -------------------------------------------------------------
    
    // PEEK
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        
        self.previewVC = nil
        
        guard let indexPath = self.tableView.indexPathForRow(at: location) else {return nil}
        
        self.previewVC = self.storyboard?.instantiateViewController(withIdentifier: "viewPinDetail") as? PinDetailView
        self.previewVC!.pinId = self.pinsTypeSwitcher.selectedSegmentIndex == 0 ? self.pins[indexPath.row]["id"] as! Int : self.expiredPins[indexPath.row]["id"] as! Int
        
        
        return self.previewVC!
    }
    
    // POP
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        let vcToShow = self.previewVC!
        
        show(vcToShow, sender: self)
        self.previewVC = nil
    }
    
    // -------------------------------------------------------------
    // ------------------ /3D Touch --------------------------------
    // -------------------------------------------------------------
    
    // ----------- EMPTY DATASET --------------
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let string : String = "Нет меток"
        
        let attributes : [String : AnyObject] = [
            NSFontAttributeName : UIFont.boldSystemFont(ofSize: 32),
            NSForegroundColorAttributeName : UIColor.gray
        ]
        
        return NSAttributedString(string: string, attributes: attributes)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        var string : String = ""
        
        if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
            string = "У Вас нет активных меток"
        } else {
            string = "У Вас нет неактивных меток"
        }
        
        let paragraph : NSMutableParagraphStyle = NSMutableParagraphStyle()
        paragraph.lineBreakMode = .byWordWrapping
        paragraph.alignment = .center
        
        let attributes : [String : AnyObject] = [
            NSFontAttributeName : UIFont.systemFont(ofSize: 14),
            NSForegroundColorAttributeName : UIColor.lightGray,
            NSParagraphStyleAttributeName : paragraph
        ]
        
        return NSAttributedString(string: string, attributes: attributes)
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
            if self.pins.count == 0 {
                return true
            }
        } else {
            if self.expiredPins.count == 0 {
                return true
            }
        }
        
        return false
    }
    
    func emptyDataSetShouldAllowTouch(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    // ----------- EMPTY DATASET --------------
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToMyPinDetailView" {
            let destVC : PinDetailView = segue.destination as! PinDetailView
            destVC.pinId = self.pinsTypeSwitcher.selectedSegmentIndex == 0 ? self.pins[(self.tableView.indexPathForSelectedRow?.row)!]["id"] as! Int : self.expiredPins[(self.tableView.indexPathForSelectedRow?.row)!]["id"] as! Int
        } else if segue.identifier == "goToPinEdit" {
            let destVC : PostMessageNC = segue.destination as! PostMessageNC
            (destVC.viewControllers[0] as! PostMessage).pinId = self.pinToEdit!["id"] as? Int
            self.pinToEdit = nil
            (destVC.viewControllers[0] as! PostMessage).myPinsController = self
            (destVC.viewControllers[0] as! PostMessage).mode = "edit"
            (destVC.viewControllers[0] as! PostMessage).indexToUpdate = self.indexToUpdate
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}
