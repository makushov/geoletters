//
//  PinsComments.swift
//  Letters 1.1
//
//  Created by KingOP on 25.01.16.
//  Copyright © 2016 KingOP. All rights reserved.
//

import UIKit
import Haneke
import SafariServices
import CoreLocation
import TSMessages
import SZTextView

class PinsComments: UITableViewController, UITextViewDelegate, SFSafariViewControllerDelegate {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var comments : [Dictionary<String, AnyObject>] = []
    
    var pinViewController : PinDetailView = PinDetailView()
    
    var pin : StatusPin = StatusPin(coordinate: CLLocationCoordinate2D(), title: "", subtitle: "")
    
    var currentOffset : Int = 0
    var commentsLimit : Int = 20
    
    var normalToolboxHeight : CGFloat = 0
    var keyboardHeight : CGFloat = 0
    
    @IBOutlet weak var commentText: SZTextView!
    @IBOutlet weak var sendButton: UIButton!
    
    @IBOutlet weak var toolbarView: UIView!
    
    var toolbox : CommentsToolbox = CommentsToolbox()
    
    var tapRecognizer : UITapGestureRecognizer = UITapGestureRecognizer()
    
    var selectedIndex : Int? = nil
    
    var toolboxInstalled : Bool = false
    var doNotDestroyToolbox : Bool = false
    
    var loadingMoreView = UIView()
    var animationView = UIActivityIndicatorView()
    
    var loadMoreUsers = true
    var allowLoadingMore = true
    var usersLoaded : Bool = false
    
    var isLoaded : Bool = false
    
    var needsReload : Bool = false
    
    var isReloading : Bool = false
    
    var userIdForSegue : Int? = nil
    
    var tempText : String? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        self.commentText.delegate = self
        self.commentText.placeholder = "Ваш комментарий..."
        self.sendButton.isEnabled = false
        
        self.title = "Комментарии"
        
        self.tableView.tableHeaderView = UIView(frame: CGRect.zero)
        
        self.tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(PinsComments.dismissKeyboard(_:)))
        
        self.loadComments()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.tabBarController == nil {
            self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 51, 0)
        } else {
            self.tabBarController?.tabBar.isHidden = true
        }
        
        self.doNotDestroyToolbox = false
        
        if !toolboxInstalled {
            self.toolbox = CommentsToolbox.instanceFromNib() as! CommentsToolbox
            
            self.toolbox.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 51)
            
            let newFrame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - self.toolbox.bounds.size.height, width: UIScreen.main.bounds.size.width, height: self.toolbox.bounds.size.height)
            
            self.toolbox.frame = newFrame
            
            self.normalToolboxHeight = self.toolbox.bounds.size.height
            
            self.toolbox.commentText.delegate = self
            self.toolbox.commentText.placeholder = "Ваш комментарий..."
            
            self.toolbox.sendButton.addTarget(self, action: #selector(PinsComments.sendComment(_:)), for: UIControlEvents.touchUpInside)
            
            self.toolbox.sendButton.isEnabled = false
            
            if self.tempText != nil {
                self.toolbox.commentText.text = self.tempText!
                self.tempText = nil
                
                let fixedWidth = self.toolbox.commentText.frame.size.width
                self.toolbox.commentText.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                let newSize = self.toolbox.commentText.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                var newFrame = self.toolbox.commentText.frame
                newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                
                self.toolbox.commentText.frame = newFrame
                
                let newHeight : CGFloat = (newSize.height + 16)
                
                let newToolbarFrame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - newHeight, width: self.toolbox.frame.size.width, height: newHeight)
                
                self.toolbox.frame = newToolbarFrame
                
                self.toolbox.layoutIfNeeded()
                
                self.toolbox.sendButton.isEnabled = true
            }
            
            UIApplication.shared.keyWindow?.addSubview(self.toolbox)
            
            self.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height - self.toolbox.bounds.size.height)
            
            self.toolboxInstalled = true
        } else if self.toolbox.isHidden {
            self.toolbox.isHidden = false
        }
        
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        //putToolbarOnTopOfKeyboard(textView)
        self.view.addGestureRecognizer(self.tapRecognizer)
        
        NotificationCenter.default.addObserver(self, selector: #selector(PinsComments.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(PinsComments.keyboardDidShow(_:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        
        return true
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        NotificationCenter.default.addObserver(self, selector: #selector(PinsComments.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(PinsComments.keyboardDidHide(_:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)

        return true
    }
    
    func keyboardDidShow(_ notification: Notification) {
        
        if self.comments.count > 0 {
            self.tableView.scrollToRow(at: IndexPath(row: self.comments.count - 1, section: 0), at: .bottom, animated: true)
        }
    }
    
    func keyboardWillShow(_ notification: Notification) {
        
        let keyboardSize = ((notification as NSNotification).userInfo![UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        
        self.keyboardHeight = (keyboardSize?.height)!
        
        let frame = CGRect(x: 0,  y: ((UIScreen.main.bounds.size.height - self.toolbox.bounds.size.height) - (keyboardSize?.height)!), width: self.toolbox.frame.size.width, height: self.toolbox.frame.size.height)
        
        self.toolbox.frame = frame
    }
    
    func keyboardWillHide(_ notification: Notification) {
        
        if self.toolbox.commentText.text == "" {
            let frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - self.normalToolboxHeight, width: self.toolbox.frame.size.width, height: self.normalToolboxHeight)
            
            self.toolbox.frame = frame
        } else {
            let frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - self.toolbox.bounds.size.height, width: self.toolbox.frame.size.width, height: self.toolbox.bounds.size.height)
            
            self.toolbox.frame = frame
        }
    }
    
    func keyboardDidHide(_ notification: Notification) {
        if self.comments.count > 0 {
            self.tableView.scrollToRow(at: IndexPath(row: self.comments.count - 1, section: 0), at: .bottom, animated: true)
        }
        
        self.view.removeGestureRecognizer(self.tapRecognizer)
    }
    
    func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        self.hideKeyboard()
    }
    
    func hideKeyboard() {
        self.toolbox.endEditing(true)
    }
    
    func putToolbarOnTopOfKeyboard(_ textView: UITextView) {
        let toolbar : KeyboardToolbar = Bundle.main.loadNibNamed("KeyboardToolbar", owner: self, options: nil)![0] as! KeyboardToolbar
        
        toolbar.doneButton.action = #selector(PinsComments.hideKeyboard(_:))
        toolbar.doneButton.title = "Отмена"
        toolbar.doneButton.target = self
        textView.inputAccessoryView = toolbar
    }
    
    func hideKeyboard(_ sender: AnyObject) {
        self.toolbox.endEditing(true)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView == self.toolbox.commentText {
            
            let fixedWidth = textView.frame.size.width
            textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = textView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            
            textView.frame = newFrame
            
            let newHeight : CGFloat = (newSize.height + 16)
            
            let newToolbarFrame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - newHeight - self.keyboardHeight, width: self.toolbox.frame.size.width, height: newHeight)
            
            self.toolbox.frame = newToolbarFrame
            
            self.toolbox.layoutIfNeeded()
        }
        
        self.toolbox.sendButton.isEnabled = textView.text.length > 0
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
        
        if !doNotDestroyToolbox {
            self.toolbox.removeFromSuperview()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.comments.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let pixelsPerRow : CGFloat = 20
        let stringLenght : CGFloat = CGFloat((self.comments[indexPath.row]["comment"] as! String).length)
        let linesNumber : CGFloat = ceil(stringLenght / 34.5) + 1
        let textViewHeight : CGFloat = linesNumber * pixelsPerRow
        //let height = (textViewHeight + 53)//(self.textViewHeightForRowAtIndexPath(indexPath) + 53)
        
        /*
        NSString *text = [globalArrayWithStrings objectAtIndex:[indexPath row]];
        CGSize maxSize = CGSizeMake(textViewWidth, 999); // 999 can be any maxmimum height you want
        CGSize newSize = [text sizeWithFont:aFont constrainedToSize:maxSize lineBreakMode:textViewLineBreakMode];
        return newSize.height;
        */
        //let maxSize : CGSize = CGSizeMake((self.view.frame.size.width - 70), CGFloat.max)
        let newSize : CGSize = ("\(self.comments[indexPath.row]["comment"] as! String)\n").boundingRect( with: CGSize(width: (self.view.frame.size.width - 70), height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes:[NSFontAttributeName : UIFont.systemFont(ofSize: 15.0)], context:nil).size
        return max((newSize.height + 53), (textViewHeight + 53))
    }
    
    func textViewHeightForRowAtIndexPath(_ indexPath: IndexPath) -> CGFloat {
        let calculationView : UITextView = UITextView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.size.width - 70, height: CGFloat.greatestFiniteMagnitude))
        
        //calculationView.attributedText = NSAttributedString(string: "\((self.comments[indexPath.row] as! NSDictionary)["comment"] as! String)\n")
        calculationView.text = "\(self.comments[indexPath.row]["comment"] as! String)\n"
        
        var frame : CGRect = calculationView.frame
        frame.size.height = calculationView.contentSize.height
        calculationView.frame = frame
        
        //calculationView.textContainer.lineBreakMode = .ByWordWrapping
        
        //let size : CGSize = calculationView.sizeThatFits(CGSizeMake((self.tableView.frame.size.width - 70), CGFloat.max))
        
        return calculationView.frame.size.height//size.height
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : CommentCell? = tableView.dequeueReusableCell(withIdentifier: "commentsCell", for: indexPath) as? CommentCell
        
        if cell == nil {
            let nib : Array = Bundle.main.loadNibNamed("CommentCell", owner: self, options: nil)!
            cell = nib[0] as? CommentCell
        }
        
        print(cell?.userLogin)
        
        cell!.userLogin.text = self.comments[indexPath.row]["user"]?["username"] as? String
        cell!.commentDatetime.text = fullStringDateFromTimestamp(self.comments[indexPath.row]["created_at"] as! Double)
        cell!.commentText.text = "\(self.comments[indexPath.row]["comment"] as! String)\n"
        cell!.commentText.delegate = self
        
        /*let fixedWidth = cell!.commentText.frame.size.width
        cell!.commentText.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.max))
        let newSize = cell!.commentText.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.max))
        var newFrame = cell!.commentText.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        
        cell!.commentText.frame = newFrame*/
        
        //self.textViews[indexPath] = cell!.commentText
        
        cell!.userAvatar.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!, original: true)
        
        // подгрузим аву
        if !(self.comments[indexPath.row]["user"]?["photo"] is NSNull) && self.comments[indexPath.row]["user"]?["photo"] as? String != nil && self.comments[indexPath.row]["user"]?["photo"] as? String != "" {
            // есть аватарка
            
            let urlString = "\(getBackendDomainPath())\(self.comments[indexPath.row]["user"]?["photo"] as! String)"
            let imgURL = URL(string: urlString)
            
            if let img = avatarCache[urlString] {
                cell!.userAvatar!.image = img
                cell!.userAvatar!.tag = (indexPath.row + 1)
            }
            else {
                // The image isn't cached, download the img data
                // We should perform this in a background thread
                let request: URLRequest = URLRequest(url: imgURL!)
                let mainQueue = OperationQueue.main
                NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
                    if error == nil {
                        
                        let res = response as! HTTPURLResponse
                        if res.statusCode != 404 {
                            // Convert the downloaded data in to a UIImage object
                            
                            if data != nil {
                                let image : UIImage = maskedAvatarFromImage(UIImage(data: data!)!)
                                // Store the image in to our cache
                                avatarCache[urlString] = image
                                
                                // Update the cell
                                DispatchQueue.main.async(execute: {
                                    if let cellToUpdate = tableView.cellForRow(at: indexPath) as? CommentCell {
                                        cellToUpdate.userAvatar!.image = image
                                        cellToUpdate.userAvatar!.tag = ((indexPath as NSIndexPath).row + 1)
                                    }
                                })
                            } else {
                                DispatchQueue.main.async(execute: {
                                    if let cellToUpdate = tableView.cellForRow(at: indexPath) as? CommentCell {
                                        cellToUpdate.userAvatar!.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!, original: true)
                                        cellToUpdate.userAvatar!.tag = ((indexPath as NSIndexPath).row + 1)
                                    }
                                })
                            }
                        } else {
                            DispatchQueue.main.async(execute: {
                                if let cellToUpdate = tableView.cellForRow(at: indexPath) as? CommentCell {
                                    cellToUpdate.userAvatar!.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!, original: true)
                                    cellToUpdate.userAvatar!.tag = ((indexPath as NSIndexPath).row + 1)
                                }
                            })
                        }
                    }
                    else {
                        DispatchQueue.main.async(execute: {
                            if let cellToUpdate = tableView.cellForRow(at: indexPath) as? CommentCell {
                                cellToUpdate.userAvatar!.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!, original: true)
                                cellToUpdate.userAvatar!.tag = ((indexPath as NSIndexPath).row + 1)
                            }
                        })
                    }
                })
            }
        } else {
            cell!.userAvatar!.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!, original: true)
            cell!.userAvatar!.tag = ((indexPath as NSIndexPath).row + 1)
        }
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.toolbox.isHidden = true
        
        let actionsController : UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let user = self.comments[indexPath.row]["user"] as! Dictionary<String, AnyObject>
        
        if user["id"] as! Int != getUserId()! {
            
            actionsController.addAction(UIAlertAction(title: "Перейти к профилю \(user["username"] as! String)", style: .default, handler: { (action) -> Void in
                
                self.selectedIndex = indexPath.row
                self.performSegue(withIdentifier: "goToUserProfileFromCommentsList", sender: self)
                
                //self.toolbox.hidden = false
                self.tableView.deselectRow(at: indexPath, animated: true)
            }))
            
            actionsController.addAction(UIAlertAction(title: "Пожаловаться", style: .default, handler: { (action) -> Void in
                
                self.selectedIndex = indexPath.row
                self.reportComment()
                
                self.tableView.deselectRow(at: indexPath, animated: true)
            }))
        }
        
        actionsController.addAction(UIAlertAction(title: "Скопировать", style: .default, handler: { (action) -> Void in
            
            UIPasteboard.general.string = self.comments[indexPath.row]["comment"] as? String
            TSMessage.showNotification(
                in: self,//UIApplication.sharedApplication().delegate?.window??.rootViewController,
                title: "Успешно",
                subtitle: "Комментарий скопирован в буфер обмена",
                type: TSMessageNotificationType.success,
                duration: 2.0,
                canBeDismissedByUser: true
            )
            
            self.toolbox.isHidden = false
            self.tableView.deselectRow(at: indexPath, animated: true)
        }))

        // link detection
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector.matches(in: self.comments[indexPath.row]["comment"] as! String, options: [], range: NSMakeRange(0, (self.comments[indexPath.row]["comment"] as! String).characters.count))
        
        for match in matches {
            let urlString = (self.comments[indexPath.row]["comment"] as! NSString).substring(with: match.range)
            
            let url : URL = URL(string: urlString)!
            
            actionsController.addAction(UIAlertAction(title: urlString, style: .default, handler: { (action) -> Void in
                
                self.openURL(url)
                self.tableView.deselectRow(at: indexPath, animated: true)
            }))
        }
        
        // phone numbers detection
        let phoneDetector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
        let phoneMatches = phoneDetector.matches(in: self.comments[indexPath.row]["comment"] as! String, options: [], range: NSMakeRange(0, (self.comments[indexPath.row]["comment"] as! String).characters.count))
        
        for match in phoneMatches {
            let urlString = (self.comments[indexPath.row]["comment"] as! NSString).substring(with: match.range)
            
            let url : URL = URL(string: ("tel://" + urlString.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "-", with: "").replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "")))!
            
            actionsController.addAction(UIAlertAction(title: urlString, style: .default, handler: { (action) -> Void in
                
                self.openURL(url)
                self.tableView.deselectRow(at: indexPath, animated: true)
            }))
        }
        
        if self.comments[indexPath.row]["user"]?["id"] as! Int == getUserId()! || self.pin.userId == getUserId() {
            
            actionsController.addAction(UIAlertAction(title: "Удалить", style: .destructive, handler: { (action) -> Void in
                
                self.selectedIndex = (indexPath as NSIndexPath).row
                self.deleteComment()

                self.tableView.deselectRow(at: indexPath, animated: true)
            }))
        }
        
        actionsController.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: { (action) -> Void in
            
            self.toolbox.isHidden = false
            self.tableView.deselectRow(at: indexPath, animated: true)
        }))
        
        self.present(actionsController, animated: true, completion: nil)
    }
    
    func loadComments(_ noAnimation: Bool = false, append: Bool = false) {
        // загрузим комменты
        
        if !noAnimation {
            SwiftSpinner.show("Загрузка...", animated: true, viewToDisplay: self.view)
        }
        
        let getCommentsRequest = getURLRequest("GET", url: getBackendApiPath() + "temprorary-status/\(self.pin.id!)/comment?limit=\(self.commentsLimit)&offset=\(self.currentOffset)", params: Dictionary<String, AnyObject>(), token: getUserToken()!)
        
        self.tableView.register(UINib(nibName: "CommentCell", bundle: nil), forCellReuseIdentifier: "commentsCell")
        
        let loadCommentsTask = URLSession.shared.dataTask(with: getCommentsRequest, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                DispatchQueue.main.async(execute: {
                    SwiftSpinner.hide()
                    self.tableView.tableHeaderView = nil//UIView(frame: CGRect.zero)
                    
                    TSMessage.showNotification(
                        in: UIApplication.shared.delegate?.window??.rootViewController,
                        title: "Ошибка",
                        subtitle: error?.localizedDescription,
                        type: TSMessageNotificationType.error,
                        duration: 2.0,
                        canBeDismissedByUser: true
                    )
                })
            } else {
                let loadCommentsResponse = response as! HTTPURLResponse
                
                print(loadCommentsResponse.statusCode)
                
                if loadCommentsResponse.statusCode == 200 {
                    
                    //let list = NSMutableArray(array: (try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableArray).reverseObjectEnumerator().allObjects)
                    
                    var list : [Dictionary<String, AnyObject>] = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [Dictionary<String, AnyObject>]
                    
                    list = list.reversed()
                    
                    if list.count == 0 && !append {
                        self.allowLoadingMore = false
                        self.comments = list
                    } else if list.count == 0 && append {
                        self.currentOffset = self.comments.count
                        self.allowLoadingMore = false
                    } else {
                        
                        if !append {
                            self.comments = list
                            
                            if list.count < self.commentsLimit {
                                self.allowLoadingMore = false
                            }
                        } else {
                            if list.count >= self.commentsLimit {
                                list.append(contentsOf: self.comments)
                                self.comments = list
                                self.allowLoadingMore = true
                            } else if list.count > 0 && list.count < self.commentsLimit {
                                list.append(contentsOf: self.comments)
                                self.comments = list
                                self.allowLoadingMore = false
                            } else if list.count == 0 {
                                self.allowLoadingMore = false
                            }
                        }
                        
                        self.currentOffset = self.comments.count
                        self.loadMoreUsers = false
                        self.isReloading = false
                    }
                    
                    DispatchQueue.main.async(execute: {
                        self.animationView.stopAnimating()
                        self.tableView.tableHeaderView = nil//UIView(frame: CGRect.zero)
                        
                        self.tableView.reloadData()
                        
                        if append {
                            self.tableView.scrollToRow(at: IndexPath(row: self.commentsLimit - 1, section: 0), at: .bottom, animated: false)
                        }
                        
                        self.tableView.tableHeaderView = nil//UIView(frame: CGRect.zero)
                        
                        
                        if self.comments.count > 0 && !append {
                            self.tableView.scrollToRow(at: IndexPath(row: self.comments.count - 1, section: 0), at: .bottom, animated: false)
                        }
                        SwiftSpinner.hide()
                    })
                    
                } else if loadCommentsResponse.statusCode == 401 {
                    error401Action(self)
                } else if loadCommentsResponse.statusCode == 500 {
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        self.tableView.tableHeaderView = nil
                        
                        TSMessage.showNotification(
                            in: UIApplication.shared.delegate?.window??.rootViewController,
                            title: "Ошибка",
                            subtitle: error500,
                            type: TSMessageNotificationType.error,
                            duration: 2.0,
                            canBeDismissedByUser: true
                        )
                    })
                }
            }
        }) 
        
        loadCommentsTask.resume()
    }
    
    
    func sendComment(_ sender: AnyObject) {
        //self.toolbox.commentText.resignFirstResponder()
        self.blockUnblockSending(true)
        
        let sendCommentRequest = getURLRequest("POST", url: getBackendApiPath() + "temprorary-status/\(self.pin.id!)/comment", params: ["comment" : self.toolbox.commentText.text as AnyObject], token: getUserToken()!)
        
        let sendCommentTask = URLSession.shared.dataTask(with: sendCommentRequest, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                DispatchQueue.main.async(execute: {
                    self.blockUnblockSending(false)
                    
                    TSMessage.showNotification(
                        in: UIApplication.shared.delegate?.window??.rootViewController,
                        title: "Ошибка",
                        subtitle: error?.localizedDescription,
                        type: TSMessageNotificationType.error,
                        duration: 2.0,
                        canBeDismissedByUser: true
                    )
                })
            } else {
                let res = response as! HTTPURLResponse
                
                if res.statusCode == 201 {
                    // коммент успешно отправлен, отобразим его
                    var jsonResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary<String, AnyObject>
                    
                    /*let comment : NSMutableDictionary = NSMutableDictionary()
                    comment.setValue(jsonResult!["id"] as! Int, forKey: "id")
                    comment.setValue(jsonResult!["created_at"] as! Double, forKey: "created_at")
                    comment.setValue(jsonResult!["comment"] as! String, forKey: "comment")
                    //comment.setValue(jsonResult!["parent_id"] as! Int, forKey: "parent_id")
                    */
                    let apiV : String = isTesting == false ? backendApiVersion : devBackendApiVersion
                    
                    let user : Dictionary<String, AnyObject> = [
                        "id"        : getUserId()! as AnyObject,
                        "username"  : getUserLogin()! as AnyObject,
                        "photo"     : "/\(backendApiDirectory)/v\(apiV)/user/\(getUserId()!)/avatar" as AnyObject
                    ]
                    
                    jsonResult?["user"] = user as AnyObject?
                    self.comments.append(jsonResult!)
                    
                    DispatchQueue.main.async(execute: {
                        self.tableView.insertRows(at: [IndexPath(row: self.comments.count - 1, section: 0)], with: .bottom)
                        
                        self.blockUnblockSending(false)
                        self.toolbox.commentText.text = ""
                        self.toolbox.endEditing(true)
                        
                        self.pinViewController.commentsCount.text = (Int(self.pinViewController.commentsCount.text!)! + 1).description
                        
                        self.tableView.reloadData()
                        
                        //self.tableView.scrollToRowAtIndexPath(NSIndexPath(forRow: self.comments.count - 1, inSection: 0), atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
                    })
                } else if res.statusCode == 403 {
                    DispatchQueue.main.async(execute: {
                        self.blockUnblockSending(false)
                        
                        TSMessage.showNotification(
                            in: UIApplication.shared.delegate?.window??.rootViewController,
                            title: "Ошибка",
                            subtitle: "Вы находитесь в черном списке у данного пользователя и не моежете писать комментарии к его меткам",
                            type: TSMessageNotificationType.error,
                            duration: 3.0,
                            canBeDismissedByUser: true
                        )
                    })
                } else if res.statusCode == 422 {
                    let jsonResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSMutableArray
                    
                    if let parseJson = jsonResult {
                        var errorText = ""
                        
                        for error in parseJson {
                            errorText += (error as! NSDictionary)["message"] as! String + "\n"
                        }
                        
                        DispatchQueue.main.async(execute: {
                            self.blockUnblockSending(false)
                            
                            TSMessage.showNotification(
                                in: UIApplication.shared.delegate?.window??.rootViewController,
                                title: "Ошибка",
                                subtitle: errorText,
                                type: TSMessageNotificationType.error,
                                duration: 2.0,
                                canBeDismissedByUser: true
                            )
                        })
                    }
                } else if res.statusCode == 500 {
                    DispatchQueue.main.async(execute: {
                        self.blockUnblockSending(false)
                        
                        TSMessage.showNotification(
                            in: UIApplication.shared.delegate?.window??.rootViewController,
                            title: "Ошибка",
                            subtitle: error500,
                            type: TSMessageNotificationType.error,
                            duration: 2.0,
                            canBeDismissedByUser: true
                        )
                    })
                } else if res.statusCode == 401 {
                    error401Action(self)
                }
            }
        }) 
        
        sendCommentTask.resume()
    }
    
    @IBAction func postCommentAction(_ sender: AnyObject) {
        //
    }

    func blockUnblockSending(_ lock: Bool) {
        if lock {
            self.toolbox.sendButton.isEnabled = false
            self.toolbox.sendButton.setTitle("", for: UIControlState())
            self.toolbox.activityIndicator.startAnimating()
        } else {
            self.toolbox.sendButton.isEnabled = true
            self.toolbox.sendButton.setTitle("Отпр.", for: UIControlState())
            self.toolbox.activityIndicator.stopAnimating()
        }
    }
    
    func reportComment() {
        self.toolbox.isHidden = true
        
        let alertController = UIAlertController(title: "Пожаловаться", message: "Укажите, пожалуйста, причину жалобы (до 200 символов)", preferredStyle: UIAlertControllerStyle.alert)
        
        let reportAction = UIAlertAction(title: "Отправить жалобу", style: UIAlertActionStyle.destructive, handler: {
            alert -> Void in
            
            SwiftSpinner.show("Отправка жалобы...", animated: true, viewToDisplay: self.view)
            
            let firstTextField = alertController.textFields![0] as UITextField
            
            let reportText : String = firstTextField.text!.trimmingCharacters(in: CharacterSet.whitespaces)
            
            // вот тут отправка жалобы
            let sendReportRequest = getURLRequest("POST", url: "\(getBackendApiPath())report", params: ["description" : reportText as AnyObject, "target" : "comment", "target_id" : self.comments[self.selectedIndex!]["id"] as! Int], token: getUserToken()!)
            
            let sendReportTask = URLSession.shared.dataTask(with: sendReportRequest, completionHandler: {
                (data, response, error) in
                
                if error != nil {
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        self.toolbox.isHidden = false
                        
                        let errorView = JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                        
                        errorView.addAction({
                            self.navigationController?.popViewController(animated: true)
                        })
                    })
                } else {
                    let res = response as! HTTPURLResponse
                    
                    if res.statusCode == 201 {
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            
                            let result = JSSAlertView().info(self, title: "Спасибо!", text: "Ваша жалоба принята к рассмотрению.", buttonText: "Продолжить")
                            
                            result.addAction({
                                // удалим из списка
                                self.pinViewController.commentsCount.text = (Int(self.pinViewController.commentsCount.text!)! - 1).description
                                self.comments.remove(at: self.selectedIndex!)
                                self.tableView.deleteRows(at: [IndexPath(row: self.selectedIndex!, section: 0)], with: UITableViewRowAnimation.top)
                                self.toolbox.isHidden = false
                            })
                        })
                    } else if res.statusCode == 403 {
                        let json = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                        
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            self.toolbox.isHidden = false
                            TSMessage.showNotification(
                                in: UIApplication.shared.delegate?.window??.rootViewController,
                                title: "Ошибка",
                                subtitle: json!["message"] as! String,
                                type: TSMessageNotificationType.error,
                                duration: 2.0,
                                canBeDismissedByUser: true
                            )
                            playErrorSound()
                        })
                    } else if res.statusCode == 422 {
                        let jsonResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSMutableArray
                        
                        if let parseJson = jsonResult {
                            var errorText = ""
                            
                            for error in parseJson {
                                errorText += (error as! NSDictionary)["message"] as! String + "\n"
                            }
                            
                            DispatchQueue.main.async(execute: {
                                SwiftSpinner.hide()
                                self.toolbox.isHidden = false
                                TSMessage.showNotification(
                                    in: UIApplication.shared.delegate?.window??.rootViewController,
                                    title: "Ошибка",
                                    subtitle: errorText,
                                    type: TSMessageNotificationType.error,
                                    duration: 2.0,
                                    canBeDismissedByUser: true
                                )
                                playErrorSound()
                            })
                        }
                    } else if res.statusCode == 401 {
                        error401Action(self)
                    } else if res.statusCode == 500 {
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            
                            let errorAlert = JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                            
                            errorAlert.addAction({ () -> Void in
                                self.toolbox.isHidden = false
                            })
                        })
                    }
                }
            }) 
            
            sendReportTask.resume()
        })
        
        let cancelAction = UIAlertAction(title: "Отмена", style: UIAlertActionStyle.default, handler: {
            alert -> Void in
            
            self.toolbox.isHidden = false
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Причина жалобы..."
        }
        
        alertController.addAction(reportAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func deleteComment() {
        let confirmAlertController : UIAlertController = UIAlertController(title: "Подтверждение", message: "Удалить комментарий?", preferredStyle: .actionSheet)
        
        confirmAlertController.addAction(UIAlertAction(title: "Удалить", style: .destructive, handler: { (action) -> Void in
            
            SwiftSpinner.show("Удаление...", animated: true, viewToDisplay: self.view)
            
            let deleteCommentRequest = getURLRequest("DELETE", url: getBackendApiPath() + "temprorary-status/comment/\(self.comments[self.selectedIndex!]["id"] as! Int)", params: Dictionary<String, AnyObject>(), token: getUserToken()!)
            
            let deleteCommentTask = URLSession.shared.dataTask(with: deleteCommentRequest, completionHandler: { (data, response, error) -> Void in
                
                if error != nil {
                    DispatchQueue.main.async(execute: {
                        
                        SwiftSpinner.hide()
                        self.toolbox.isHidden = false
                        TSMessage.showNotification(
                            in: UIApplication.shared.delegate?.window??.rootViewController,
                            title: "Ошибка",
                            subtitle: error?.localizedDescription,
                            type: TSMessageNotificationType.error,
                            duration: 2.0,
                            canBeDismissedByUser: true
                        )
                        playErrorSound()
                    })
                } else {
                    let res = response as! HTTPURLResponse
                    
                    if res.statusCode == 204 {
                        self.comments.remove(at: self.selectedIndex!)
                        
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            self.toolbox.isHidden = false
                            self.tableView.deleteRows(at: [IndexPath(row: self.selectedIndex!, section: 0)], with: .top)
                        })
                    } else if res.statusCode == 401 {
                        error401Action(self)
                    } else if res.statusCode == 403 {
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            self.toolbox.isHidden = false
                            TSMessage.showNotification(
                                in: UIApplication.shared.delegate?.window??.rootViewController,
                                title: "Ошибка",
                                subtitle: "Вам не разрешено удалять этот комментарий",
                                type: TSMessageNotificationType.error,
                                duration: 2.0,
                                canBeDismissedByUser: true
                            )
                        })
                    } else if res.statusCode == 500 {
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            self.toolbox.isHidden = false
                            TSMessage.showNotification(
                                in: UIApplication.shared.delegate?.window??.rootViewController,
                                title: "Ошибка",
                                subtitle: error500,
                                type: TSMessageNotificationType.error,
                                duration: 2.0,
                                canBeDismissedByUser: true
                            )
                        })
                    }
                }
            })
            
            deleteCommentTask.resume()
        }))
        
        confirmAlertController.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: { (action) -> Void in
            
            self.toolbox.isHidden = false
        }))
        
        self.present(confirmAlertController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToUserProfileFromCommentsList" {
            let destVC : UserProfile = segue.destination as! UserProfile
            destVC.userId = self.userIdForSegue != nil ? self.userIdForSegue! : self.comments[self.selectedIndex!]["user"]?["id"] as! Int
            self.selectedIndex = nil
            self.userIdForSegue = nil
            self.doNotDestroyToolbox = true
            self.toolbox.isHidden = true
        }
    }
    
    // бесконечный скролл
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollCurrentOffset = scrollView.contentOffset.y
        //let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        //let deltaOffset = maximumOffset - currentOffset
        
        if /*deltaOffset*/scrollCurrentOffset <= 0 {
            loadMore()
        }
    }
    
    func loadMore() {
        if (!self.loadMoreUsers && self.allowLoadingMore && !self.isReloading && self.currentOffset >= self.commentsLimit) {
            self.showTableViewHeader()
            self.loadMoreUsers = true
            self.isReloading = true
            self.animationView.startAnimating()
            loadMoreBegin("загрузка...",
                loadMoreEnd: {(x:Int) -> () in
            })
        }
    }
    
    func loadMoreBegin(_ newtext:String, loadMoreEnd:@escaping (Int) -> ()) {
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
            self.loadComments(true, append: true)
            
            DispatchQueue.main.async {
                loadMoreEnd(0)
            }
        }
    }
    
    func showTableViewHeader() {
        // надо показать
        self.loadingMoreView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 60))
        self.animationView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        
        self.loadingMoreView.addSubview(animationView)
        
        self.animationView.center = self.loadingMoreView.center
        
        self.tableView.tableHeaderView = loadingMoreView
        self.tableView.tableHeaderView!.isHidden = false
    }
    
    func openURL(_ URL: Foundation.URL) {
        
        if URL.scheme == "http" || URL.scheme == "https" {
            
            if URL.host! == backendDomain && Int(URL.lastPathComponent)! != 0 {
                //self.toolbox.hidden = false
                //BridgeToUserProfile.openUserProfile(Int(URL.lastPathComponent!)!)
                self.userIdForSegue = Int(URL.lastPathComponent)
                self.performSegue(withIdentifier: "goToUserProfileFromCommentsList", sender: self)
            } else {
                if self.toolbox.commentText.text != "" {
                    self.tempText = self.toolbox.commentText.text
                }
                let vc = SFSafariViewController(url: URL, entersReaderIfAvailable: true)
                vc.delegate = self
                present(vc, animated: true, completion: nil)
            }
        } else if URL.scheme == "tel" {
            let confirmController : UIAlertController = UIAlertController(title: "Позвонить?", message: URL.host!, preferredStyle: .actionSheet)
            
            confirmController.addAction(UIAlertAction(title: "Позвонить", style: .default, handler: { (action) -> Void in
                
                self.toolbox.isHidden = false
                UIApplication.shared.openURL(URL)
            }))
            
            confirmController.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
            
            self.present(confirmController, animated: true, completion: nil)
        } else {
            self.toolbox.isHidden = false
            UIApplication.shared.openURL(URL)
        }
        
        self.selectedIndex = nil
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
        self.toolboxInstalled = false
    }
}
