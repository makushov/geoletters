//
//  SelectPinCategory.swift
//  Letters 1.1
//
//  Created by KingOP on 13.01.16.
//  Copyright © 2016 KingOP. All rights reserved.
//

import UIKit

class SelectPinCategory: UITableViewController {
    
    var postMessageController : PostMessage = PostMessage()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pinsCategories.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pinCategoryCell", for: indexPath)

        cell.imageView?.image = UIImage(named: pinsCategories[(indexPath as NSIndexPath).row].image)
        cell.textLabel?.text = pinsCategories[(indexPath as NSIndexPath).row].title

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.postMessageController.selectedCatId = (indexPath as NSIndexPath).row
        self.postMessageController.selectedCatTitle.text = pinsCategories[(indexPath as NSIndexPath).row].title
        self.postMessageController.selectedCatImage.image = self.tableView.cellForRow(at: indexPath)?.imageView?.image
        
        self.navigationController?.popViewController(animated: true)
    }
}
