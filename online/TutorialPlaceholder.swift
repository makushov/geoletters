//
//  TutorialPlaceholder.swift
//  Letters
//
//  Created by KingOP on 01.12.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit

class TutorialPlaceholder: UIViewController, UIPageViewControllerDataSource {
    
    var pageViewController : UIPageViewController!
    var pageTitles : [String]!
    var pageImages : [String]!
    var pageDescriptions : [String]!
    var pageButtonTitles : [String]!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.pageTitles = [
            "Добро пожаловать!",
            "Помощь друг другу",
            "Полезная информация",
            "Новые знакомства и общение",
            "Доступ к геолокации",
            "Спасибо!"
        ]
        
        self.pageImages = [
            "tut1.jpg",
            "tut2.jpg",
            "tut5.jpg",
            "tut3.jpg",
            "tut4.jpg",
            "logo.png"
        ]
        
        self.pageDescriptions = [
            "Мы рады приветствовать Вас в GeoLetters!",
            "Обращайтесь за помощью, оставляя метки на карте, и оказывайте помощь тому, кто в этом нуждается.",
            "Информируйте и получайте информацию об интересном вокруг Вас.",
            "Заводите новые знакомства и общайтесь с друзьями с помощью личных сообщений!",
            "Для полноценной работы приложению нужен доступ к Вашей геолокации. Разрешить доступ?",
            "Спасибо Вам за выбор GeoLetters! Приятной работы!"
        ]
        
        self.pageButtonTitles = [
            "",
            "",
            "",
            "",
            "Разрешить",
            "Продолжить"
        ]
        
        self.pageViewController = self.storyboard?.instantiateViewController(withIdentifier: "PageViewController") as! UIPageViewController
        
        self.pageViewController.dataSource = self
        
        let startVC = self.viewControllerAtIndex(0) 
        let viewControllers = [startVC]
        
        self.pageViewController.setViewControllers(viewControllers, direction: .forward, animated: true, completion: nil)
        
        self.pageViewController.view.frame = CGRect(x: 0, y: 30, width: self.view.frame.width, height: self.view.frame.size.height - 30)
        
        self.addChildViewController(self.pageViewController)
        self.view.addSubview(self.pageViewController.view)
        self.pageViewController.didMove(toParentViewController: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func viewControllerAtIndex(_ index: Int) -> ContentViewController {
        if self.pageTitles.count == 0 || index >= self.pageTitles.count {
             return ContentViewController()
        }
        
        let vc : ContentViewController = self.storyboard?.instantiateViewController(withIdentifier: "ContentViewController") as! ContentViewController
        
        vc.imageFile = self.pageImages[index]
        vc.titleText = self.pageTitles[index]
        vc.descriptionText = self.pageDescriptions[index]
        vc.buttonTitle = self.pageButtonTitles[index]
        
        vc.pageIndex = index
        
        return vc
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        let vc = viewController as! ContentViewController
        var index : Int = vc.pageIndex
        
        if index == 0 || index == NSNotFound {
            return nil
        }
        
        index -= 1
        
        return self.viewControllerAtIndex(index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        let vc = viewController as! ContentViewController
        var index : Int = vc.pageIndex
        
        if index == NSNotFound {
            return nil
        }
        
        index += 1
        
        if index == self.pageTitles.count {
            return nil
        }
        
        return self.viewControllerAtIndex(index)
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return self.pageTitles.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
}
