//
//  ClusterPinsList.swift
//  Letters
//
//  Created by KingOP on 10.01.16.
//  Copyright © 2016 KingOP. All rights reserved.
//

import UIKit

class ClusterPinsList: UITableViewController {
    
    var pins : [StatusPin]!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.tableView.indexPathForSelectedRow != nil {
            self.tableView.deselectRow(at: self.tableView.indexPathForSelectedRow!, animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pins.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return max(60, UITableViewAutomaticDimension)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pinInClusterRow", for: indexPath)

        cell.textLabel?.text = self.pins[(indexPath as NSIndexPath).row].subtitle == nil ? "" : self.pins[(indexPath as NSIndexPath).row].subtitle
        cell.detailTextLabel?.text = self.pins[(indexPath as NSIndexPath).row].title == nil ? "" : self.pins[(indexPath as NSIndexPath).row].title

        return cell
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "goToPinDetailViewFromClusterPinsList" {
            let destVC : PinDetailView = segue.destination as! PinDetailView
            destVC.pin = self.pins[((self.tableView.indexPathForSelectedRow as NSIndexPath?)?.row)!]
        }
    }

}
