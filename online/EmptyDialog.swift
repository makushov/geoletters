//
//  EmptyDialog.swift
//  Letters 1.1
//
//  Created by KingOP on 26.06.16.
//  Copyright © 2016 KingOP. All rights reserved.
//

import UIKit

class EmptyDialog: UIView {

    class func instanceFromNib() -> EmptyDialog {
        return UINib(nibName: "EmptyDialog", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! EmptyDialog
    }

}
