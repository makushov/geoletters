//
//  Recents.swift
//  Letters 1.1
//
//  Created by KingOP on 11.04.16.
//  Copyright © 2016 KingOP. All rights reserved.
//

import UIKit

class Recents: UITableViewController {
    
    @IBOutlet weak var tabSwitcher: UISegmentedControl!
    
    let titles : [String] = ["Празднование дня города", "makushov разместил(а) метку", "Пользователю dimms77 понравилась метка"]
    let subtitles : [String] = ["Сегодня, в 12:00 состоится мероприятие, посвященное празднованию дня города. Приглашаются все желающие. Встреча в центре за 30 минут до начала.", "Здесь сегодня происходит распродажа, не упустите своей возможности скупиться подешевле", "makushov: Здесь сегодня происходит распродажа, не упустите своей возможности скупиться подешевле"]
    
    let count : Int = 10

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "recentCell", for: indexPath)

        cell.textLabel?.text = self.titles[self.tabSwitcher.selectedSegmentIndex]
        cell.detailTextLabel?.text = self.subtitles[self.tabSwitcher.selectedSegmentIndex]

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func changeSegmentAction(_ sender: AnyObject) {
        
        self.tableView.reloadData()
    }
}
