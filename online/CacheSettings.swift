//
//  CacheSettings.swift
//  Letters 1.1
//
//  Created by KingOP on 24.03.16.
//  Copyright © 2016 KingOP. All rights reserved.
//

import UIKit

class CacheSettings: UITableViewController {

    @IBOutlet weak var cacheSize: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.cacheSize.text = getDocumentsFolderSize()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    @IBAction func clearCacheAction(_ sender: AnyObject) {
        let confirmController = UIAlertController(title: "Подтверждение", message: "Вы уверены, что хотите удалить все кэшированные изображения?", preferredStyle: .actionSheet)
        
        confirmController.addAction(UIAlertAction(title: "Очистить", style: .destructive, handler: { (action) -> Void in
            
            avatarsCacheDataStack.removeAllAvatarsFromCache()
            clearDeadCache()
            self.cacheSize.text = getDocumentsFolderSize()
        }))
        
        confirmController.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        
        self.present(confirmController, animated: true, completion: nil)
    }
}
