//
//  StatusPin.swift
//  Letters
//
//  Created by KingOP on 21.11.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit
import MapKit

class StatusPin: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    var userId: Int?
    var id: Int?
    var catId : Int!
    var attachment : String?
    var isUrgency : Bool
    var likesCount : Int = 0
    var commentsCount : Int = 0
    
    init(coordinate: CLLocationCoordinate2D, title: String, subtitle: String, id: Int? = nil, catId: Int = 0, attachment: String? = nil, isUrgency: Bool = false, likesCount: Int = 0, commentsCount: Int = 0) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
        self.id = id
        self.catId = catId
        self.attachment = attachment
        self.isUrgency = isUrgency
        self.likesCount = likesCount
        self.commentsCount = commentsCount
    }
}