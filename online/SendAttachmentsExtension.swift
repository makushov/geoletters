//
//  SendPhotosExtension.swift
//  Letters 1.1
//
//  Created by Stanislav Makushov on 08.10.16.
//  Copyright © 2016 KingOP. All rights reserved.
//

import UIKit
import CoreLocation
import TSMessages
import JSQMessagesViewController
import MapKit

extension Messages {
    
    override func didPressAccessoryButton(_ sender: UIButton!) {
        // отобразим возможные варианты аттача
        let attachAlertController : UIAlertController = UIAlertController(title: "Отправить вложение", message: "Выберите, что именно Вы хотите отправить", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        /*let test : UIAlertAction = UIAlertAction(title: "TEST", style: .default) { (action) in
            
            let userInfo : [AnyHashable: Any] = [
                "aps" : [
                    "badge" : 1,
                    "sound" : "default",
                    "alert" : "photo received",
                    "category"  : "REPLY_CATEGORY"
                ],
                "created_at" : 1475973910,
                "dialog_id" : 2,
                "dialog_title" : "title",
                "dialog_photo" : "https://geoletters.com/v\(isTesting == false ? backendApiVersion : devBackendApiVersion)/user/\(getUserId()!)/avatar",
                "master_user_id" : 2,
                "username" : "devour",
                "message_id" : 2,
                "message": "string",
                "event" : "new_message",
                "user_id" : 2,
                "photo" : "https://geoletters.com/v\(isTesting == false ? backendApiVersion : devBackendApiVersion)/user/\(getUserId()!)/avatar",
                "members" : [
                    [
                        "id" : 1,
                        "username" : "Andrey",
                        "photo" : "https://geoletters.com/v\(isTesting == false ? backendApiVersion : devBackendApiVersion)/user/\(getUserId()!)/avatar"
                    ],
                    [
                        "id" : 2,
                        "username" : "Andrey",
                        "photo" : "https://geoletters.com/v\(isTesting == false ? backendApiVersion : devBackendApiVersion)/user/\(getUserId()!)/avatar"
                    ],
                    [
                        "id" : 3,
                        "username" : "Andrey",
                        "photo" : "https://geoletters.com/v\(isTesting == false ? backendApiVersion : devBackendApiVersion)/user/\(getUserId()!)/avatar"
                    ]
                ],
                "grouped" : 1,
                "attachment" : [
                    "content": "https://geoletters.com/v\(isTesting == false ? backendApiVersion : devBackendApiVersion)/user/\(getUserId()!)/avatar",
                    "type": "image",
                    "id": 216
                ]
            ]
            
            (UIApplication.shared.delegate as! AppDelegate).application(UIApplication.shared, didReceiveRemoteNotification: userInfo)
        }
        
        attachAlertController.addAction(test)*/
        
        // отправить картинку
        let sendImage : UIAlertAction = UIAlertAction(title: "Изображение", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
            
            // тут короче будет выбор фото
            self.dismiss(animated: true, completion: nil)
            self.pickPhotos()
        }
        
        attachAlertController.addAction(sendImage)
        
        // только если разрешен доступ к геопозиции
        
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse {
            // отправить местоположение
            let sendGeoPosition : UIAlertAction = UIAlertAction(title: "Местоположение", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
                
                // тут короче будет отправка местоположения
                let currentLocation = self.locationManager.location
                
                if currentLocation == nil {
                    
                    TSMessage.showNotification(in: self,
                                               title: "Ошибка",
                                               subtitle: "Невозможно отправить текущее местоположение, так как его не удалось определить. Приносим извинения за временные неудобства.",
                                               type: TSMessageNotificationType.error,
                                               duration: 3.0,
                                               canBeDismissedByUser: true
                    )
                    playErrorSound()
                    
                } else {
                    let location : JSQLocationMediaItem = JSQLocationMediaItem(maskAsOutgoing: true)
                    let message = JSQMessage(senderId: getUserId()!.description, senderDisplayName: getUserLogin()!, date: Date(), media: location)
                    
                    self.messages.append(message!)
                    
                    let dictionary : Dictionary<String, AnyObject> = [
                        "user_id"       : getUserId()! as AnyObject,
                        "created_at"    : getCurrentTimestamp() as AnyObject,
                        "message"       : "Местоположение" as AnyObject,
                        "status"        : "sending" as AnyObject,
                        "id"            : -1 as AnyObject
                    ]
                    
                    self.message_dict.append(dictionary)
                    
                    let index : Int = self.messages.index(of: message!)!
                    self.view.endEditing(true)
                    self.finishSendingMessage(animated: true)
                    //playSendSound()
                    
                    // сначала проверим, мы создаем диалог, или отправляем в существующий
                    if self.dialogId == nil {
                        
                        // создаем новый диалог
                        let createDialogRequest : URLRequest = getURLRequest("POST", url: getBackendApiPath() + "dialog", params: ["users" : self.userId!.description], token: getUserToken())
                        
                        let createDialogTask = URLSession.shared.dataTask(with: createDialogRequest, completionHandler: { (data, response, error) in
                            
                            if (error != nil) {
                                // сюда надо добавить обработку ошибок
                                DispatchQueue.main.async(execute: {
                                    
                                    self.view.endEditing(true)
                                    
                                    TSMessage.showNotification(in: self,
                                                               title: "Ошибка",
                                                               subtitle: error?.localizedDescription,
                                                               type: TSMessageNotificationType.error,
                                                               duration: 2.0,
                                                               canBeDismissedByUser: true
                                    )
                                    playErrorSound()
                                })
                            } else {
                                let infoResponse = response as! HTTPURLResponse
                                
                                let infoResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                                
                                if infoResponse.statusCode == 401 {
                                    error401Action(self)
                                    playErrorSound()
                                } else if infoResponse.statusCode == 422 {
                                    DispatchQueue.main.async(execute: {
                                        
                                        self.view.endEditing(true)
                                        
                                        TSMessage.showNotification(in: self,
                                                                   title: "Ошибка",
                                                                   subtitle: infoResult!["message"] as? String,
                                                                   type: TSMessageNotificationType.error,
                                                                   duration: 2.0,
                                                                   canBeDismissedByUser: true
                                        )
                                        playErrorSound()
                                    })
                                } else if infoResponse.statusCode == 500 {
                                    DispatchQueue.main.async(execute: {
                                        SwiftSpinner.hide()
                                        
                                        self.view.endEditing(true)
                                        
                                        TSMessage.showNotification(in: self,
                                                                   title: "Ошибка",
                                                                   subtitle: error500,
                                                                   type: TSMessageNotificationType.error,
                                                                   duration: 2.0,
                                                                   canBeDismissedByUser: true
                                        )
                                        playErrorSound()
                                    })
                                } else if infoResponse.statusCode == 403 {
                                    DispatchQueue.main.async(execute: {
                                        
                                        self.view.endEditing(true)
                                        
                                        TSMessage.showNotification(in: self,
                                                                   title: "Ошибка",
                                                                   subtitle: "Вы находитесь в черном списке у этого пользователя и не можете отправлять ему сообщения.",
                                                                   type: TSMessageNotificationType.error,
                                                                   duration: 2.0,
                                                                   canBeDismissedByUser: true
                                        )
                                        playErrorSound()
                                    })
                                } else {
                                    // диалог успешно создан
                                    // отправим в него сообщение
                                    self.dialogId = infoResult!["id"] as? Int
                                    self.members = [infoResult!["members"] as! Dictionary<String, AnyObject>]
                                    
                                    let sendLocationRequest = getURLRequest("POST", url: getBackendApiPath() + "dialog/\(self.dialogId!)/attachments", params: ["id" : self.dialogId!, "type" : "location", "latitude" : (currentLocation?.coordinate.latitude)!, "longitude" : (currentLocation?.coordinate.longitude)!], token: getUserToken()!)
                                    
                                    let sendLocationTask = URLSession.shared.dataTask(with: sendLocationRequest, completionHandler: { (data, response, error) -> Void in
                                        
                                        if error != nil {
                                            DispatchQueue.main.async(execute: {
                                                TSMessage.showNotification(in: self,
                                                                           title: "Ошибка",
                                                                           subtitle: error?.localizedDescription,
                                                                           type: TSMessageNotificationType.error,
                                                                           duration: 2.0,
                                                                           canBeDismissedByUser: true
                                                )
                                                
                                                self.collectionView?.deleteItems(at: [IndexPath(row: index, section: 0)])
                                                playErrorSound()
                                            })
                                        } else {
                                            let res = response as! HTTPURLResponse
                                            
                                            let sendLocationResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary<String, AnyObject>
                                            
                                            if res.statusCode == 201 {
                                                DispatchQueue.main.async(execute: {
                                                    // тут надо обновить список диалогов, если он был открыт
                                                    if dialogsController != nil {
                                                        // диалога не было, его надо создать и впихнуть
                                                        
                                                        let new_message : Dictionary<String, AnyObject> = [
                                                            "user_id"       : getUserId()! as AnyObject,
                                                            "created_at"    : sendLocationResult!["created_at"]!,
                                                            "viewed"        : 1 as AnyObject,
                                                            "message"       : "Местоположение" as AnyObject,
                                                            "attachment"    : NSNull()
                                                        ]
                                                        
                                                        let dialog : Dictionary<String, AnyObject> = [
                                                            "message"           : new_message as AnyObject,
                                                            "id"                : self.dialogId as AnyObject,
                                                            "unread"            : "0" as AnyObject,
                                                            "master_user_id"    : getUserId()! as AnyObject,
                                                            "members"           : self.members as AnyObject
                                                        ]
                                                        
                                                        // сделаем перемещение
                                                        dialogsController!.dialogs.insert(dialog, at: 0)
                                                        dialogsController!.dialogIds.insert(self.dialogId!, at: 0)
                                                        dialogsController!.currentOffset += 1
                                                    }
                                                    
                                                    location.setLocation(currentLocation, region: MKCoordinateRegionMakeWithDistance((currentLocation?.coordinate)!, 500.0, 500.0), withCompletionHandler: { () -> Void in
                                                        
                                                        let index = self.message_dict.index(where: {$0["created_at"] as! Double == dictionary["created_at"] as! Double})
                                                        var message_in_dict = self.message_dict[index!]
                                                        
                                                        message_in_dict["status"] = "sent" as AnyObject?
                                                        message_in_dict["id"] = sendLocationResult!["id"]
                                                        
                                                        self.message_dict[index!] = message_in_dict
                                                        
                                                        self.collectionView?.reloadData()
                                                        playSendSound()
                                                    })
                                                })
                                            } else if res.statusCode == 400 {
                                                self.messages.remove(at: index)
                                                
                                                DispatchQueue.main.async(execute: {
                                                    TSMessage.showNotification(in: self,
                                                                               title: "Ошибка",
                                                                               subtitle: sendLocationResult!["message"] as? String,
                                                                               type: TSMessageNotificationType.error,
                                                                               duration: 2.0,
                                                                               canBeDismissedByUser: true
                                                    )
                                                    playErrorSound()
                                                    
                                                    self.collectionView?.deleteItems(at: [IndexPath(row: index, section: 0)])
                                                })
                                            } else if res.statusCode == 401 {
                                                self.messages.remove(at: self.messages.index(of: message!)!)
                                                
                                                error401Action(self)
                                            } else if res.statusCode == 403 {
                                                self.messages.remove(at: self.messages.index(of: message!)!)
                                                
                                                DispatchQueue.main.async(execute: {
                                                    TSMessage.showNotification(in: self,
                                                                               title: "Ошибка",
                                                                               subtitle: "Вы находитесь в черном списке у данного пользователя и не можете отправлять ему сообщения.",
                                                                               type: TSMessageNotificationType.error,
                                                                               duration: 2.0,
                                                                               canBeDismissedByUser: true
                                                    )
                                                    playErrorSound()
                                                    
                                                    self.collectionView?.deleteItems(at: [IndexPath(row: index, section: 0)])
                                                })
                                            } else if res.statusCode == 500 {
                                                self.messages.remove(at: self.messages.index(where: {$0.date == message!.date})!)
                                                
                                                DispatchQueue.main.async(execute: {
                                                    SwiftSpinner.hide()
                                                    
                                                    self.collectionView?.deleteItems(at: [IndexPath(row: index, section: 0)])
                                                    
                                                    TSMessage.showNotification(in: self,
                                                                               title: "Ошибка",
                                                                               subtitle: error500,
                                                                               type: TSMessageNotificationType.error,
                                                                               duration: 2.0,
                                                                               canBeDismissedByUser: true
                                                    )
                                                    playErrorSound()
                                                })
                                            }
                                        }
                                    })
                                    
                                    sendLocationTask.resume()
                                }
                            }
                        })
                        
                        createDialogTask.resume()
                    } else {
                        // отправляем в существующий
                        let sendLocationRequest = getURLRequest("POST", url: getBackendApiPath() + "dialog/\(self.dialogId!)/attachments", params: ["id" : self.dialogId!, "type" : "location", "latitude" : (currentLocation?.coordinate.latitude)!, "longitude" : (currentLocation?.coordinate.longitude)!], token: getUserToken()!)
                        
                        let sendLocationTask = URLSession.shared.dataTask(with: sendLocationRequest, completionHandler: { (data, response, error) -> Void in
                            
                            if error != nil {
                                DispatchQueue.main.async(execute: {
                                    TSMessage.showNotification(in: self,
                                                               title: "Ошибка",
                                                               subtitle: error?.localizedDescription,
                                                               type: TSMessageNotificationType.error,
                                                               duration: 2.0,
                                                               canBeDismissedByUser: true
                                    )
                                    playErrorSound()
                                    
                                    self.collectionView?.deleteItems(at: [IndexPath(row: index, section: 0)])
                                })
                            } else {
                                let res = response as! HTTPURLResponse
                                
                                let sendLocationResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary<String, AnyObject>
                                
                                if res.statusCode == 201 {
                                    DispatchQueue.main.async(execute: {
                                        
                                        if dialogsController != nil {
                                            // получим индекс
                                            let index = dialogsController?.dialogIds.index(of: self.dialogId!)
                                            
                                            if index != nil {
                                                // текущий диалог есть в списке диалогов, обновим его
                                                var dialog : Dictionary<String, AnyObject> = dialogsController!.dialogs[index!]
                                                
                                                if dialog["message"] == nil {
                                                    let message : Dictionary<String, AnyObject> = [
                                                        "user_id"       : getUserId()! as AnyObject,
                                                        "created_at"    : sendLocationResult!["created_at"]!,
                                                        "viewed"        : 0 as AnyObject,
                                                        "message"       : "Местоположение" as AnyObject
                                                    ]
                                                    
                                                    dialog["message"] = message as AnyObject?
                                                } else {
                                                    var new_message : Dictionary<String, AnyObject> = dialog["message"] as! Dictionary<String, AnyObject>
                                                    
                                                    new_message["user_id"] = getUserId()! as AnyObject?
                                                    new_message["created_at"] = sendLocationResult!["created_at"] as! Double as AnyObject?
                                                    new_message["viewed"] = 0 as AnyObject?
                                                    new_message["message"] = "Местоположение" as AnyObject?
                                                    
                                                    dialog["message"] = new_message as AnyObject?
                                                    dialog["unread"] = "0" as AnyObject?
                                                }
                                                
                                                // сделаем перемещение
                                                dialogsController!.dialogs.remove(at: index!)
                                                dialogsController!.dialogIds.remove(at: index!)
                                                
                                                dialogsController!.dialogs.insert(dialog, at: 0)
                                                dialogsController!.dialogIds.insert(self.dialogId!, at: 0)
                                            } else {
                                                // диалога не было, его надо создать и впихнуть
                                                
                                                let new_message : Dictionary<String, AnyObject> = [
                                                    "user_id"       : getUserId()! as AnyObject,
                                                    "created_at"    : sendLocationResult!["created_at"]!,
                                                    "viewed"        : 1 as AnyObject,
                                                    "message"       : "Местоположение" as AnyObject,
                                                    "attachment"    : NSNull()
                                                ]
                                                
                                                let dialog : Dictionary<String, AnyObject> = [
                                                    "message"           : new_message as AnyObject,
                                                    "id"                : self.dialogId as AnyObject,
                                                    "unread"            : "0" as AnyObject,
                                                    "master_user_id"    : getUserId()! as AnyObject,
                                                    "members"           : self.members as AnyObject
                                                ]
                                                
                                                // сделаем перемещение
                                                dialogsController!.dialogs.insert(dialog, at: 0)
                                                dialogsController!.dialogIds.insert(self.dialogId!, at: 0)
                                                dialogsController!.currentOffset += 1
                                            }
                                        }
                                        
                                        location.setLocation(currentLocation, region: MKCoordinateRegionMakeWithDistance((currentLocation?.coordinate)!, 500.0, 500.0), withCompletionHandler: { () -> Void in
                                            
                                            let index = self.message_dict.index(where: {$0["created_at"] as! Double == dictionary["created_at"] as! Double})
                                            var message_in_dict = self.message_dict[index!]
                                            
                                            message_in_dict["status"] = "sent" as AnyObject?
                                            message_in_dict["id"] = sendLocationResult!["id"]
                                            
                                            self.message_dict[index!] = message_in_dict
                                            
                                            self.collectionView?.reloadData()
                                            playSendSound()
                                        })
                                    })
                                } else if res.statusCode == 400 {
                                    self.messages.remove(at: index)
                                    
                                    DispatchQueue.main.async(execute: {
                                        TSMessage.showNotification(in: self,
                                                                   title: "Ошибка",
                                                                   subtitle: sendLocationResult!["message"] as? String,
                                                                   type: TSMessageNotificationType.error,
                                                                   duration: 2.0,
                                                                   canBeDismissedByUser: true
                                        )
                                        playErrorSound()
                                        
                                        self.collectionView?.deleteItems(at: [IndexPath(row: index, section: 0)])
                                    })
                                } else if res.statusCode == 401 {
                                    self.messages.remove(at: self.messages.index(where: {$0.date == message!.date})!)
                                    
                                    error401Action(self)
                                } else if res.statusCode == 403 {
                                    self.messages.remove(at: self.messages.index(where: {$0.date == message!.date})!)
                                    
                                    DispatchQueue.main.async(execute: {
                                        TSMessage.showNotification(in: self,
                                                                   title: "Ошибка",
                                                                   subtitle: "Вы находитесь в черном списке у данного пользователя и не можете отправлять ему сообщения.",
                                                                   type: TSMessageNotificationType.error,
                                                                   duration: 2.0,
                                                                   canBeDismissedByUser: true
                                        )
                                        playErrorSound()
                                        
                                        self.collectionView?.deleteItems(at: [IndexPath(row: index, section: 0)])
                                    })
                                } else if res.statusCode == 500 {
                                    self.messages.remove(at: self.messages.index(where: {$0.date == message!.date})!)
                                    
                                    DispatchQueue.main.async(execute: {
                                        SwiftSpinner.hide()
                                        
                                        self.collectionView?.deleteItems(at: [IndexPath(row: index, section: 0)])
                                        
                                        TSMessage.showNotification(in: self,
                                                                   title: "Ошибка",
                                                                   subtitle: error500,
                                                                   type: TSMessageNotificationType.error,
                                                                   duration: 2.0,
                                                                   canBeDismissedByUser: true
                                        )
                                        playErrorSound()
                                    })
                                }
                            }
                        })
                        
                        sendLocationTask.resume()
                    }
                }
            }
            attachAlertController.addAction(sendGeoPosition)
        }
        
        
        // отмена
        let cancelAction : UIAlertAction = UIAlertAction(title: "Отмена", style: UIAlertActionStyle.cancel, handler: nil)
        attachAlertController.addAction(cancelAction)
        
        
        self.present(attachAlertController, animated: true, completion: nil)
    }
    
    func pickImage() {
        let imagePickVariantController : UIAlertController = UIAlertController(title: "Сделайте выбор", message: "Что использовать для выбора изображения?", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        imagePickVariantController.addAction(UIAlertAction(title: "Камера", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            self.imagePicker.sourceType = .camera
            
            self.present(self.imagePicker, animated: true, completion: nil)
        }))
        
        imagePickVariantController.addAction(UIAlertAction(title: "Галерея", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            self.imagePicker.sourceType = .photoLibrary
            
            self.present(self.imagePicker, animated: true, completion: nil)
        }))
        
        imagePickVariantController.addAction(UIAlertAction(title: "Отмена", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(imagePickVariantController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.dismiss(animated: true, completion: nil)
            let photo : JSQPhotoMediaItem = JSQPhotoMediaItem(image: nil)
            photo.appliesMediaViewMaskAsOutgoing = true
            
            let message = JSQMessage(senderId: getUserId()!.description, senderDisplayName: getUserLogin()!, date: Date(), media: photo)
            
            self.messages.append(message!)
            
            let dictionary : Dictionary<String, AnyObject> = [
                "user_id"       : getUserId()! as AnyObject,
                "created_at"    : getCurrentTimestamp() as AnyObject,
                "message"       : "Изображение" as AnyObject,
                "status"        : "sending" as AnyObject,
                "id"            : -1 as AnyObject
            ]
            
            self.message_dict.append(dictionary)
            
            let index : Int = self.messages.index(where: {$0.date == message!.date})!
            self.view.endEditing(true)
            self.finishSendingMessage(animated: true)
            
            
            if self.dialogId == nil {
                // мы создаем новый диалог
                
                let createDialogRequest : URLRequest = getURLRequest("POST", url: getBackendApiPath() + "dialog", params: ["users" : self.userId!.description], token: getUserToken())
                
                let createDialogTask = URLSession.shared.dataTask(with: createDialogRequest, completionHandler: { (data, response, error) in
                    
                    if (error != nil) {
                        // сюда надо добавить обработку ошибок
                        DispatchQueue.main.async(execute: {
                            
                            self.view.endEditing(true)
                            
                            TSMessage.showNotification(in: self,
                                                       title: "Ошибка",
                                                       subtitle: error?.localizedDescription,
                                                       type: TSMessageNotificationType.error,
                                                       duration: 2.0,
                                                       canBeDismissedByUser: true
                            )
                            playErrorSound()
                        })
                    } else {
                        let infoResponse = response as! HTTPURLResponse
                        
                        let infoResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary<String, AnyObject>
                        
                        if infoResponse.statusCode == 401 {
                            error401Action(self)
                        } else if infoResponse.statusCode == 422 {
                            DispatchQueue.main.async(execute: {
                                
                                self.view.endEditing(true)
                                
                                TSMessage.showNotification(in: self,
                                                           title: "Ошибка",
                                                           subtitle: infoResult!["message"] as? String,
                                                           type: TSMessageNotificationType.error,
                                                           duration: 2.0,
                                                           canBeDismissedByUser: true
                                )
                                playErrorSound()
                            })
                        } else if infoResponse.statusCode == 500 {
                            DispatchQueue.main.async(execute: {
                                SwiftSpinner.hide()
                                
                                self.view.endEditing(true)
                                
                                TSMessage.showNotification(in: self,
                                                           title: "Ошибка",
                                                           subtitle: error500,
                                                           type: TSMessageNotificationType.error,
                                                           duration: 2.0,
                                                           canBeDismissedByUser: true
                                )
                                playErrorSound()
                            })
                        } else if infoResponse.statusCode == 403 {
                            DispatchQueue.main.async(execute: {
                                
                                self.view.endEditing(true)
                                
                                TSMessage.showNotification(in: self,
                                                           title: "Ошибка",
                                                           subtitle: "Вы находитесь в черном списке у этого пользователя и не моежете отправлять ему сообщения.",
                                                           type: TSMessageNotificationType.error,
                                                           duration: 2.0,
                                                           canBeDismissedByUser: true
                                )
                                playErrorSound()
                            })
                        } else {
                            // диалог успешно создан
                            // отправим в него сообщение
                            self.dialogId = infoResult!["id"] as? Int
                            self.members = infoResult!["members"] as! [Dictionary<String, AnyObject>]
                            
                            let imageData = UIImageJPEGRepresentation(pickedImage, 0.5)!
                            
                            let sendImageRequest = getURLRequest("POST", url: getBackendApiPath() + "dialog/\(self.dialogId!)/attachments", params: ["id" : self.dialogId!, "type" : "image", "image" : imageData.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters)], token: getUserToken()!)
                            
                            let sendImageTask = URLSession.shared.dataTask(with: sendImageRequest, completionHandler: { (data, response, error) -> Void in
                                
                                if error != nil {
                                    DispatchQueue.main.async(execute: {
                                        TSMessage.showNotification(in: self,
                                                                   title: "Ошибка",
                                                                   subtitle: error?.localizedDescription,
                                                                   type: TSMessageNotificationType.error,
                                                                   duration: 2.0,
                                                                   canBeDismissedByUser: true
                                        )
                                        playErrorSound()
                                        
                                        self.collectionView?.deleteItems(at: [IndexPath(row: index, section: 0)])
                                    })
                                } else {
                                    let res = response as! HTTPURLResponse
                                    
                                    let sendLocationResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary<String, AnyObject>
                                    
                                    if res.statusCode == 201 {
                                        DispatchQueue.main.async(execute: {
                                            
                                            // диалога не было, его надо создать и впихнуть
                                            if dialogsController != nil {
                                                // диалога не было, его надо создать и впихнуть
                                                
                                                let new_message : Dictionary<String, AnyObject> = [
                                                    "user_id"       : getUserId()! as AnyObject,
                                                    "created_at"    : sendLocationResult!["created_at"]!,
                                                    "viewed"        : 1 as AnyObject,
                                                    "message"       : "Изображение" as AnyObject,
                                                    "attachment"    : NSNull()
                                                ]
                                                
                                                let dialog : Dictionary<String, AnyObject> = [
                                                    "message"           : new_message as AnyObject,
                                                    "id"                : self.dialogId as AnyObject,
                                                    "unread"            : "0" as AnyObject,
                                                    "master_user_id"    : getUserId()! as AnyObject,
                                                    "members"           : self.members as AnyObject
                                                ]
                                                
                                                // сделаем перемещение
                                                dialogsController!.dialogs.insert(dialog, at: 0)
                                                dialogsController!.dialogIds.insert(self.dialogId!, at: 0)
                                                dialogsController!.currentOffset += 1
                                            }
                                            
                                            // сообщение отправилось, закэшируем изображение
                                            let attachment : Dictionary<String, AnyObject> = sendLocationResult!["attachment"] as! Dictionary<String, AnyObject>
                                            
                                            let imageUrl : URL = URL(string: "\(getBackendDomainPath())\(attachment["content"] as! String)")!
                                            
                                            photo.image = UIImage(data: imageData)
                                            
                                            let index = self.message_dict.index(where: {$0["created_at"] as! Double == dictionary["created_at"] as! Double})
                                            var message_in_dict = self.message_dict[index!]
                                            
                                            message_in_dict["status"] = "sent" as AnyObject?
                                            message_in_dict["id"] = sendLocationResult!["id"]
                                            
                                            self.message_dict[index!] = message_in_dict
                                            
                                            self.collectionView?.reloadData()
                                            playSendSound()
                                            
                                            avatarsCacheDataStack.saveAvatarToCache(attachment["id"] as! Int, fileName: imageUrl.lastPathComponent, imageData: imageData)
                                        })
                                    } else if res.statusCode == 400 {
                                        self.messages.remove(at: index)
                                        
                                        DispatchQueue.main.async(execute: {
                                            TSMessage.showNotification(in: self,
                                                                       title: "Ошибка",
                                                                       subtitle: sendLocationResult!["message"] as? String,
                                                                       type: TSMessageNotificationType.error,
                                                                       duration: 2.0,
                                                                       canBeDismissedByUser: true
                                            )
                                            playErrorSound()
                                            
                                            self.collectionView?.deleteItems(at: [IndexPath(row: index, section: 0)])
                                        })
                                    } else if res.statusCode == 401 {
                                        self.messages.remove(at: index)
                                        
                                        error401Action(self)
                                    } else if res.statusCode == 403 {
                                        self.messages.remove(at: index)
                                        
                                        DispatchQueue.main.async(execute: {
                                            
                                            self.collectionView?.deleteItems(at: [IndexPath(row: index, section: 0)])
                                            TSMessage.showNotification(in: self,
                                                                       title: "Ошибка",
                                                                       subtitle: "Вы находитесь в черном списке у данного пользователя и не можете отправлять ему сообщения.",
                                                                       type: TSMessageNotificationType.error,
                                                                       duration: 2.0,
                                                                       canBeDismissedByUser: true
                                            )
                                            playErrorSound()
                                        })
                                    } else if res.statusCode == 500 {
                                        self.messages.remove(at: index)
                                        
                                        DispatchQueue.main.async(execute: {
                                            SwiftSpinner.hide()
                                            
                                            TSMessage.showNotification(in: self,
                                                                       title: "Ошибка",
                                                                       subtitle: error500,
                                                                       type: TSMessageNotificationType.error,
                                                                       duration: 2.0,
                                                                       canBeDismissedByUser: true
                                            )
                                            playErrorSound()
                                            
                                            self.collectionView?.deleteItems(at: [IndexPath(row: index, section: 0)])
                                        })
                                    }
                                }
                            })
                            
                            sendImageTask.resume()
                        }
                    }
                })
                
                createDialogTask.resume()
                self.dismiss(animated: true, completion: nil)
            } else {
                // отправим в уже существующий
                let imageData: Data = UIImageJPEGRepresentation(pickedImage, 0.5)!
                
                let sendImageRequest = getURLRequest("POST", url: getBackendApiPath() + "dialog/\(self.dialogId!)/attachments", params: ["id" : self.dialogId!, "type" : "image", "image" : imageData.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters)], token: getUserToken()!)
                
                let sendImageTask = URLSession.shared.dataTask(with: sendImageRequest, completionHandler: { (data, response, error) -> Void in
                    
                    if error != nil {
                        DispatchQueue.main.async(execute: {
                            TSMessage.showNotification(in: self,
                                                       title: "Ошибка",
                                                       subtitle: error?.localizedDescription,
                                                       type: TSMessageNotificationType.error,
                                                       duration: 2.0,
                                                       canBeDismissedByUser: true
                            )
                            playErrorSound()
                            
                            self.collectionView?.deleteItems(at: [IndexPath(row: index, section: 0)])
                        })
                    } else {
                        let res = response as! HTTPURLResponse
                        
                        let sendLocationResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary<String, AnyObject>
                        
                        if res.statusCode == 201 {
                            DispatchQueue.main.async(execute: {
                                
                                if dialogsController != nil {
                                    // получим индекс
                                    let index = dialogsController?.dialogIds.index(of: self.dialogId!)
                                    
                                    if index != nil {
                                        // текущий диалог есть в списке диалогов, обновим его
                                        var dialog : Dictionary<String, AnyObject> = dialogsController!.dialogs[index!]
                                        
                                        if dialog["message"] == nil {
                                            let message : Dictionary<String, AnyObject> = [
                                                "user_id"       : getUserId()! as AnyObject,
                                                "created_at"    : sendLocationResult!["created_at"]!,
                                                "viewed"        : 0 as AnyObject,
                                                "message"       : "Изображение" as AnyObject
                                            ]
                                            dialog["message"] = message as AnyObject?
                                        } else {
                                            var new_message : Dictionary<String, AnyObject> = dialog["message"] as! Dictionary<String, AnyObject>
                                            
                                            new_message["user_id"] = getUserId()! as AnyObject?
                                            new_message["created_at"] = sendLocationResult!["created_at"] as! Double as AnyObject?
                                            new_message["viewed"] = 0 as AnyObject?
                                            new_message["message"] = "Изображение" as AnyObject?
                                            
                                            dialog["message"] = new_message as AnyObject?
                                            dialog["unread"] = "0" as AnyObject?
                                        }
                                        
                                        // сделаем перемещение
                                        dialogsController!.dialogs.remove(at: index!)
                                        dialogsController!.dialogIds.remove(at: index!)
                                        
                                        dialogsController!.dialogs.insert(dialog, at: 0)
                                        dialogsController!.dialogIds.insert(self.dialogId!, at: 0)
                                    } else {
                                        // диалога не было, его надо создать и впихнуть
                                        let new_message : Dictionary<String, AnyObject> = [
                                            "user_id"       : getUserId()! as AnyObject,
                                            "created_at"    : sendLocationResult!["created_at"]!,
                                            "viewed"        : 1 as AnyObject,
                                            "message"       : "Изображение" as AnyObject,
                                            "attachment"    : NSNull()
                                        ]
                                        
                                        let dialog : Dictionary<String, AnyObject> = [
                                            "message"           : new_message as AnyObject,
                                            "id"                : self.dialogId as AnyObject,
                                            "unread"            : "0" as AnyObject,
                                            "master_user_id"    : getUserId()! as AnyObject,
                                            "members"           : self.members as AnyObject
                                        ]
                                        
                                        // сделаем перемещение
                                        dialogsController!.dialogs.insert(dialog, at: 0)
                                        dialogsController!.dialogIds.insert(self.dialogId!, at: 0)
                                        dialogsController!.currentOffset += 1
                                    }
                                }
                                
                                // сообщение отправилось, закэшируем изображение
                                let imageUrl : URL = URL(string: "\(getBackendDomainPath())\(sendLocationResult!["attachment"]!["content"] as! String)")!
                                
                                photo.image = UIImage(data: imageData)
                                
                                let index = self.message_dict.index(where: {$0["created_at"] as! Double == dictionary["created_at"] as! Double})
                                var message_in_dict = self.message_dict[index!]
                                
                                message_in_dict["status"] = "sent" as AnyObject?
                                message_in_dict["id"] = sendLocationResult!["id"]
                                
                                self.message_dict[index!] = message_in_dict
                                
                                self.collectionView?.reloadData()
                                playSendSound()
                                
                                avatarsCacheDataStack.saveAvatarToCache(sendLocationResult!["attachment"]?["id"] as! Int, fileName: imageUrl.lastPathComponent, imageData: imageData)
                            })
                        } else if res.statusCode == 400 {
                            self.messages.remove(at: index)
                            
                            DispatchQueue.main.async(execute: {
                                TSMessage.showNotification(in: self,
                                                           title: "Ошибка",
                                                           subtitle: sendLocationResult!["message"] as? String,
                                                           type: TSMessageNotificationType.error,
                                                           duration: 2.0,
                                                           canBeDismissedByUser: true
                                )
                                playErrorSound()
                                
                                self.collectionView?.deleteItems(at: [IndexPath(row: index, section: 0)])
                            })
                        } else if res.statusCode == 401 {
                            self.messages.remove(at: index)
                            
                            error401Action(self)
                        } else if res.statusCode == 403 {
                            self.messages.remove(at: index)
                            
                            DispatchQueue.main.async(execute: {
                                
                                self.collectionView?.deleteItems(at: [IndexPath(row: index, section: 0)])
                                TSMessage.showNotification(in: self,
                                                           title: "Ошибка",
                                                           subtitle: "Вы находитесь в черном списке у данного пользователя и не можете отправлять ему сообщения.",
                                                           type: TSMessageNotificationType.error,
                                                           duration: 2.0,
                                                           canBeDismissedByUser: true
                                )
                                playErrorSound()
                            })
                        } else if res.statusCode == 500 {
                            self.messages.remove(at: index)
                            
                            DispatchQueue.main.async(execute: {
                                SwiftSpinner.hide()
                                
                                TSMessage.showNotification(in: self,
                                                           title: "Ошибка",
                                                           subtitle: error500,
                                                           type: TSMessageNotificationType.error,
                                                           duration: 2.0,
                                                           canBeDismissedByUser: true
                                )
                                playErrorSound()
                                
                                self.collectionView?.deleteItems(at: [IndexPath(row: index, section: 0)])
                            })
                        }
                    }
                })
                
                sendImageTask.resume()
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}
