//
//  PinComments.swift
//  Letters 1.1
//
//  Created by KingOP on 24.01.16.
//  Copyright © 2016 KingOP. All rights reserved.
//

import UIKit
import SZTextView
import TSMessages

class PinComments: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var commentText: SZTextView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var commentToolbar: UIView!
    
    var comments : NSMutableArray = NSMutableArray()
    
    var pinId : Int = 0
    
    var currentOffset : Int = 0
    var commentsLimit : Int = 20
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //self.tableView.delegate = self
        //self.tableView.dataSource = self
        
        self.commentText.delegate = self
        self.commentText.placeholder = "Ваш комментарий..."
        self.sendButton.isEnabled = false
        
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        // загрузим комменты
        SwiftSpinner.show("Загрузка...", animated: true, viewToDisplay: self.view)
        
        let getCommentsRequest = getURLRequest("GET", url: getBackendApiPath() + "temprorary-status/\(self.pinId)/comment", params: Dictionary<String, AnyObject>(), token: getUserToken()!)
        
        let loadCommentsTask = URLSession.shared.dataTask(with: getCommentsRequest, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                SwiftSpinner.hide()
                
                TSMessage.showNotification(
                    in: UIApplication.shared.delegate?.window??.rootViewController,
                    title: "Ошибка",
                    subtitle: error?.localizedDescription,
                    type: TSMessageNotificationType.error,
                    duration: 2.0,
                    canBeDismissedByUser: true
                )
            } else {
                let loadCommentsResponse = response as! HTTPURLResponse
                
                print(loadCommentsResponse.statusCode)
                
                if loadCommentsResponse.statusCode == 200 {
                    
                    self.comments = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableArray
                    
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        self.tableView.reloadData()
                    })
                    
                } else if loadCommentsResponse.statusCode == 401 {
                    error401Action(self)
                } else if loadCommentsResponse.statusCode == 500 {
                    SwiftSpinner.hide()
                    
                    TSMessage.showNotification(
                        in: UIApplication.shared.delegate?.window??.rootViewController,
                        title: "Ошибка",
                        subtitle: error500,
                        type: TSMessageNotificationType.error,
                        duration: 2.0,
                        canBeDismissedByUser: true
                    )
                }
            }
        }) 
        
        loadCommentsTask.resume()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.commentToolbar.superview?.bringSubview(toFront: self.commentToolbar)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : CommentCell? = tableView.dequeueReusableCell(withIdentifier: "commentCell", for: indexPath) as? CommentCell
        
        if cell == nil {
            let nib:Array = Bundle.main.loadNibNamed("cell", owner: self, options: nil)!
            cell = nib[0] as? CommentCell
        }
        
        cell!.userLogin.text = ((self.comments[(indexPath as NSIndexPath).row] as! NSDictionary)["user"] as! NSDictionary)["username"] as? String
        cell!.commentDatetime.text = fullStringDateFromTimestamp((self.comments[(indexPath as NSIndexPath).row] as! NSDictionary)["created_at"] as! Double)
        cell!.commentText.text = (self.comments[(indexPath as NSIndexPath).row] as! NSDictionary)["comment"] as! String
        
        return cell!
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView == self.commentText {
            
            let fixedWidth = textView.frame.size.width
            textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = textView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)

            textView.frame = newFrame
        }
        
        self.sendButton.isEnabled = textView.text.length > 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    @IBAction func postCommentAction(_ sender: AnyObject) {
        
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        NotificationCenter.default.addObserver(self, selector: #selector(PinComments.keyboardDidShow(_:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        
        return true
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        NotificationCenter.default.addObserver(self, selector: #selector(PinComments.keyboardDidHide(_:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
        
        self.view.endEditing(true)
        return true
    }
    
    func keyboardDidShow(_ notification: Notification) {
        let keyboardSize = ((notification as NSNotification).userInfo![UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        
        let frame = CGRect(x: 0, y: -(keyboardSize?.height)!, width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.view.frame = frame
    }
    
    func keyboardDidHide(_ notification: Notification) {
        let frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.view.frame = frame
    }
}
