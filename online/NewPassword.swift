//
//  NewPassword.swift
//  online
//
//  Created by KingOP on 28.10.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit

class NewPassword: UITableViewController, UITextFieldDelegate {

    @IBOutlet weak var newPassword: MKTextField!
    @IBOutlet weak var newPasswordAgain: MKTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.newPassword.delegate = self
        self.newPasswordAgain.delegate = self
        
        self.newPassword.layer.borderColor = UIColor.clear.cgColor
        self.newPassword.floatingPlaceholderEnabled = true
        self.newPassword.placeholder = "Новый пароль"
        self.newPassword.rippleLayerColor = UIColor.white
        self.newPassword.tintColor = UIColor.MKColor.Blue
        
        self.newPasswordAgain.layer.borderColor = UIColor.clear.cgColor
        self.newPasswordAgain.floatingPlaceholderEnabled = true
        self.newPasswordAgain.placeholder = "Подтверждение пароля"
        self.newPasswordAgain.rippleLayerColor = UIColor.white
        self.newPasswordAgain.tintColor = UIColor.MKColor.Blue
        
        self.newPassword.becomeFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    @IBAction func closeAction(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func savePassword(_ sender: AnyObject) {
        self.view.endEditing(true)
        
        let password = self.newPassword.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        let passwordConfirm = self.newPasswordAgain.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        
        if password == "" || passwordConfirm == "" {
            JSSAlertView().danger(self, title: "Ошибка", text: "Оба поля должны быть заполнены", buttonText: "Закрыть")
        } else if password != passwordConfirm {
            JSSAlertView().danger(self, title: "Ошибка", text: "Введенные пароли не совпадают", buttonText: "Закрыть")
        } else {
            SwiftSpinner.show("Отправка данных...", animated: true, viewToDisplay: self.view)
            
            let params = [
                "password"  : password,
                "token"     : resetPasswordToken
            ]
            
            let request : URLRequest = getURLRequest("POST", url: getBackendApiPath() + "user/reset-password", params: params)
            
            let resetPasswordTask = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                
                if (error != nil) {
                    // сюда надо добавить обработку ошибок
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        
                        JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                    })
                } else {
                    let resetPasswordResponse = response as! HTTPURLResponse
                    print(resetPasswordResponse.statusCode)
                    
                    if resetPasswordResponse.statusCode ==  204 {
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            
                            let success = JSSAlertView().info(self, title: "Успешно", text: "Новый пароль успешно установлен. Теперь Вы можете выполнить вход, используя новый пароль.", buttonText: "Продолжить")
                            
                            success.addAction({
                                self.navigationController?.dismiss(animated: true, completion: nil)
                            })
                        })
                    } else if resetPasswordResponse.statusCode == 422 {
                        let tokenResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                        
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            
                            JSSAlertView().danger(self, title: "Ошибка", text: tokenResult!["message"] as? String, buttonText: "Закрыть")
                        })
                    } else if resetPasswordResponse.statusCode == 500 {
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            
                            JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                        })
                    }
                }
            }) 
            
            resetPasswordTask.resume()
        }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
}
