//
//  AZNotification.swift
//  AZNotificationDemo
//
//  Created by Mohammad Azam on 6/4/14.
//  Copyright (c) 2014 AzamSharp Consulting LLC. All rights reserved.
//

import Foundation
import UIKit


class AZNotification
{
    class func showNotificationWithTitle(_ title :String, controller :UIViewController!, notificationType :AZNotificationType, duration : TimeInterval = 2.0, notificationTappedClosure: @escaping () -> Void, beforeOpen: () -> Void, afterClose: @escaping () -> Void)
    {
        var controller = controller
        if controller?.navigationController != nil {
            controller = controller?.navigationController
        }
        
        let azNotificationView = AZNotificationView(title: title, referenceView: controller!.view, notificationType: .success, notificationTappedClosure: notificationTappedClosure, afterClose: afterClose, duration: duration)

        beforeOpen()
        
        controller?.view.addSubview(azNotificationView)
        azNotificationView.applyDynamics()
    }
    
    class func showNotificationWithTitle(_ title :String, controller :UIViewController, notificationType :AZNotificationType, shouldShowNotificationUnderNavigationBar :Bool, duration: TimeInterval = 2.0, notificationTappedClosure: @escaping () -> Void, beforeOpen: () -> Void, afterClose: @escaping () -> Void)
    {
        let azNotificationView = AZNotificationView(title: title, referenceView: controller.view, notificationType: notificationType, showNotificationUnderNavigationBar: shouldShowNotificationUnderNavigationBar, notificationTappedClosure: notificationTappedClosure, afterClose: afterClose, duration: duration)
        
        beforeOpen()
        controller.view.addSubview(azNotificationView)
        azNotificationView.applyDynamics()
    }
 
        
    
}
