//
//  AZNotificationView.swift
//  AZNotificationDemo
//
//  Created by Mohammad Azam on 6/4/14.
//  Copyright (c) 2014 AzamSharp Consulting LLC. All rights reserved.
//

import Foundation
import UIKit

let notificationViewHeight :CGFloat = 64

enum AZNotificationType
{
    case success,error,warning,message
}

enum NotificationColors : Int
{
    case success = 0x0291E6,
    error = 0xBF1525,
    warning = 0xBF3E12,
    message = 0x7F7978
}

class AZNotificationView : UIView, UIGestureRecognizerDelegate
{
    var title = ""
    var referenceView = UIView()
    var showNotificationUnderNavigationBar = false
    var animator = UIDynamicAnimator()
    var gravity = UIGravityBehavior()
    var collision = UICollisionBehavior()
    var itemBehavior = UIDynamicItemBehavior()
    var notificationType = AZNotificationType.success
    var tapGestureRecognizer : UITapGestureRecognizer?
    var notificationTappedClosure : () -> () = {}
    var afterClose : () -> () = {}
    var duration : TimeInterval = 2
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    init(title :String, referenceView :UIView, notificationType :AZNotificationType, notificationTappedClosure: @escaping () -> () = {}, afterClose: @escaping () -> () = {}, duration : TimeInterval)
    {
        self.title = title
        self.referenceView = referenceView
        self.notificationType = notificationType
        self.notificationTappedClosure = notificationTappedClosure
        
        self.afterClose = afterClose
        self.duration = duration
        
        super.init(frame: CGRect(x: 0, y: 0, width: referenceView.bounds.size.width, height: notificationViewHeight))
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurView = UIVisualEffectView(effect: blurEffect)
        
        blurView.frame = self.frame
        
        self.insertSubview(blurView, at: 0)
        
        
        setup()
        
        self.tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AZNotificationView.notificationTapped(_:)))
        self.tapGestureRecognizer?.delegate = self
        self.addGestureRecognizer(self.tapGestureRecognizer!)
    }
    
    init(title :String, referenceView :UIView, notificationType :AZNotificationType, showNotificationUnderNavigationBar :Bool, notificationTappedClosure: @escaping () -> () = {}, afterClose: @escaping () -> () = {}, duration : TimeInterval)
    {
        self.title = title
        self.referenceView = referenceView
        self.notificationType = notificationType
        self.showNotificationUnderNavigationBar = showNotificationUnderNavigationBar
        self.notificationTappedClosure = notificationTappedClosure
        
        self.afterClose = afterClose
        self.duration = duration
        
        super.init(frame: CGRect(x: 0, y: 0, width: referenceView.bounds.size.width, height: notificationViewHeight))
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurView = UIVisualEffectView(effect: blurEffect)
        
        blurView.frame = self.frame
        
        self.insertSubview(blurView, at: 0)
        
        setup()
        
        self.tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AZNotificationView.notificationTapped(_:)))
        self.tapGestureRecognizer?.delegate = self
        self.addGestureRecognizer(self.tapGestureRecognizer!)
    }
    
    func notificationTapped(_ recognizer : UITapGestureRecognizer) {
        self.hideNotification()
        self.notificationTappedClosure()
    }
    
    func hideNotification()
    {
        animator.removeBehavior(gravity)
        gravity = UIGravityBehavior(items: [self])
        gravity.gravityDirection = CGVector(dx: 0, dy: -1)
        animator.addBehavior(gravity)
        self.afterClose()
    }
    
    func applyDynamics()
    {
        let boundaryYAxis :CGFloat = showNotificationUnderNavigationBar == true ? 1 : 1 //2 : 1
        animator = UIDynamicAnimator(referenceView: referenceView)
        gravity = UIGravityBehavior(items:[self])
        collision = UICollisionBehavior(items: [self])
        itemBehavior = UIDynamicItemBehavior(items: [self])
        
        itemBehavior.elasticity = 0.5

        collision.addBoundary(withIdentifier: "AZNotificationBoundary" as NSCopying, from: CGPoint(x: 0, y: self.bounds.size.height * boundaryYAxis), to: CGPoint(x: referenceView.bounds.size.width,y: self.bounds.size.height * boundaryYAxis))
        
        animator.addBehavior(gravity)
        animator.addBehavior(collision)
        animator.addBehavior(itemBehavior)
        
        Timer.scheduledTimer(timeInterval: self.duration, target: self, selector: #selector(AZNotificationView.hideNotification), userInfo: nil, repeats: false)
        
        
    }
    
    func setup()
    {
        let screenBounds = UIScreen.main.bounds
        self.frame = CGRect(x: 0, y: showNotificationUnderNavigationBar == true ? 1 : -1 * notificationViewHeight, width: screenBounds.size.width, height: notificationViewHeight)
        
        setupNotificationType()
     
        let labelRect = CGRect(x: 10,y: 10, width: screenBounds.size.width, height: notificationViewHeight)
        
        let titleLabel = UILabel(frame: labelRect)
        //titleLabel.textAlignment = NSTextAlignment.Center
        titleLabel.text = title
        titleLabel.font = UIFont(name: "HelveticaNeue-Light", size: 17)
        titleLabel.textColor = UIColor.white
        
        addSubview(titleLabel)
    }
    
    func setupNotificationType()
    {
        switch notificationType
        {
        case .success:
            backgroundColor = UIColor(netHex: NotificationColors.success.rawValue)
            break
        case .error:
            backgroundColor = UIColor(netHex: NotificationColors.error.rawValue)
            break
        case .warning:
            backgroundColor = UIColor(netHex: NotificationColors.warning.rawValue)
            break
        case .message:
            backgroundColor = UIColor(netHex: NotificationColors.message.rawValue)
            break
        }

    }
}
