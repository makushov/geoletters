//
//  Basic.swift
//  online
//
//  Created by KingOP on 26.10.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import Foundation
import UIKit

func getURLRequest(_ method: String, url: String, params: Dictionary<String, Any> = Dictionary(), token: String? = nil) -> URLRequest {
    let urlValue: URL = URL(string: url)!
    
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
    
    let request = NSMutableURLRequest(url: urlValue)
    request.httpMethod = method
    request.addValue("application/json", forHTTPHeaderField: "Accept")
    request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
    request.addValue("keep-alive", forHTTPHeaderField: "Connection")
    
    if token != nil {
        request.addValue("Bearer " + token!, forHTTPHeaderField: "Authorization")
    }
    
    if params.count > 0 {
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions())
        } catch let error as NSError {
            print(error.localizedDescription)
            request.httpBody = nil
        }
        
        request.addValue((request.httpBody?.count.description)!, forHTTPHeaderField: "Content-Length")
    }
    
    return request as URLRequest
}
