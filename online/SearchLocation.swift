//
//  SearchLocation.swift
//  Letters
//
//  Created by KingOP on 09.12.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit
import MapKit

class SearchLocation: UITableViewController, UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    var searchResults : NSMutableArray = NSMutableArray()
    var selectLocationController : SelectLocation = SelectLocation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchBar.delegate = self
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.showsCancelButton = true
        
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.showsCancelButton = false
        
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchResults.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return max(60, UITableViewAutomaticDimension)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        // вот тут поиск
        
        if searchBar.text != "" {
            let query = searchBar.text
            self.view.endEditing(true)
            
            let geocodeRequest = getURLRequest("GET", url: "https://maps.googleapis.com/maps/api/geocode/json?address=\(query!.URLEncodedString()!)&sensor=true")
            
            let geocodeTask = URLSession.shared.dataTask(with: geocodeRequest, completionHandler: {
                (data, response, error) in
                
                if error != nil {
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        
                        JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                    })
                } else {
                    let results = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSMutableDictionary
                    
                    self.searchResults = results!["results"] as! NSMutableArray
                    
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        
                        self.tableView.reloadData()
                    })
                }
            }) 
            
            SwiftSpinner.show("Поиск...", animated: true, viewToDisplay: self.view)
            geocodeTask.resume()
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "geoCodingAdressRow", for: indexPath)

        cell.textLabel?.text = (self.searchResults[(indexPath as NSIndexPath).row] as! NSDictionary)["formatted_address"] as? String

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let latitude : Double = (((self.searchResults[(indexPath as NSIndexPath).row] as! NSDictionary)["geometry"] as! NSDictionary)["location"] as! NSDictionary)["lat"] as! Double
        let longitude : Double = (((self.searchResults[(indexPath as NSIndexPath).row] as! NSDictionary)["geometry"] as! NSDictionary)["location"] as! NSDictionary)["lng"] as! Double
        
        self.selectLocationController.mapView.removeAnnotations(self.selectLocationController.mapView.annotations)
        
        let pinMarker = StatusPin(coordinate: CLLocationCoordinate2D(latitude: latitude, longitude: longitude), title: "", subtitle: "")
        self.selectLocationController.mapView.addAnnotation(pinMarker)
        
        let region = MKCoordinateRegion(center: pinMarker.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        self.selectLocationController.mapView.setRegion(region, animated: true)
        self.selectLocationController.latitude = latitude
        self.selectLocationController.longitude = longitude
        
        self.selectLocationController.locationLabel.text = (self.searchResults[(indexPath as NSIndexPath).row] as! NSDictionary)["formatted_address"] as? String
        
        self.navigationController?.popViewController(animated: true)
    }
}
