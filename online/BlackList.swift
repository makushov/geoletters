//
//  BlackList.swift
//  Letters
//
//  Created by KingOP on 16.11.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit
import Haneke
import DZNEmptyDataSet

class BlackList: UITableViewController, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, UIViewControllerPreviewingDelegate {
    
    var bannedUsers : [Dictionary<String, AnyObject>] = []
    var usersLimit : Int = 30
    var currentOffset : Int = 0
    
    var loadingMoreView = UIView()
    var animationView = UIActivityIndicatorView()
    
    var loadMoreUsers = true
    var allowLoadingMore = true
    var usersLoaded : Bool = false
    
    var isLoaded : Bool = false
    
    var needsReload : Bool = false
    
    var isReloading : Bool = false
    
    var previewVC : UserProfile? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(BlackList.reloadUsers), for: UIControlEvents.valueChanged)
        self.refreshControl = refreshControl
        
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        self.loadingMoreView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 60))
        self.animationView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        
        self.loadingMoreView.addSubview(animationView)
        
        self.animationView.center = self.loadingMoreView.center
        
        self.tableView.tableFooterView = loadingMoreView
        self.tableView.tableFooterView!.isHidden = true
        
        self.tableView.emptyDataSetDelegate = self
        self.tableView.emptyDataSetSource = self
        
        if traitCollection.forceTouchCapability == .available {
            registerForPreviewing(with: self, sourceView: self.tableView)
        }
        
        self.loadBlackList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.bannedUsers.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "bannedUser", for: indexPath)

        cell.imageView!.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!, original: true)
        
        let user : Dictionary<String, AnyObject> = self.bannedUsers[indexPath.row]["user"] as! Dictionary<String, AnyObject>
        
        if !(user["photo"] is NSNull) && user["photo"] != nil && user["photo"] as! String != "" {
            
            let urlString = "\(getBackendDomainPath())\(user["photo"] as! String)"
            let imgURL = URL(string: urlString)
            
            if let img = avatarCache[urlString] {
                cell.imageView!.image = img
                cell.imageView!.tag = ((indexPath as NSIndexPath).row + 1)
            }
            else {
                // The image isn't cached, download the img data
                // We should perform this in a background thread
                let request: URLRequest = URLRequest(url: imgURL!)
                let mainQueue = OperationQueue.main
                NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
                    if error == nil {
                        
                        let res = response as! HTTPURLResponse
                        if res.statusCode != 404 {
                            // Convert the downloaded data in to a UIImage object
                            
                            if data != nil {
                                let image : UIImage = maskedAvatarFromImage(UIImage(data: data!)!)
                                // Store the image in to our cache
                                avatarCache[urlString] = image
                                
                                // Update the cell
                                DispatchQueue.main.async(execute: {
                                    if let cellToUpdate = tableView.cellForRow(at: indexPath) {
                                        cellToUpdate.imageView!.image = image
                                        cellToUpdate.imageView!.tag = ((indexPath as NSIndexPath).row + 1)
                                    }
                                })
                            } else {
                                DispatchQueue.main.async(execute: {
                                    if let cellToUpdate = tableView.cellForRow(at: indexPath) {
                                        cell.imageView?.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!, original: true)
                                        cellToUpdate.imageView!.tag = ((indexPath as NSIndexPath).row + 1)
                                    }
                                })
                            }
                        } else {
                            DispatchQueue.main.async(execute: {
                                if let cellToUpdate = tableView.cellForRow(at: indexPath) {
                                    cell.imageView?.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!, original: true)
                                    cellToUpdate.imageView!.tag = ((indexPath as NSIndexPath).row + 1)
                                }
                            })
                        }
                    }
                    else {
                        DispatchQueue.main.async(execute: {
                            if let cellToUpdate = tableView.cellForRow(at: indexPath) {
                                cell.imageView?.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!, original: true)
                                cellToUpdate.imageView!.tag = ((indexPath as NSIndexPath).row + 1)
                            }
                        })
                    }
                })
            }
        } else {
            cell.imageView?.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!)
            cell.imageView!.tag = (indexPath as NSIndexPath).row + 1
        }
        
        cell.textLabel?.text = user["username"] as? String

        return cell
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // тут будет разбан
            let user : Dictionary<String, AnyObject> = self.bannedUsers[indexPath.row]["user"] as! Dictionary<String, AnyObject>
            
            let confirmAlertController = UIAlertController(title: "Подтверждение", message: "Разблокировать пользователя \"\(user["username"] as! String)\"", preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let unblockAction : UIAlertAction = UIAlertAction(title: "Разблокировать", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                
                // разблокируем
                let unblockRequest = getURLRequest("DELETE", url: getBackendApiPath() + "user/block/\(user["id"] as! Int)", params: Dictionary<String, AnyObject>(), token: getUserToken()!)
                
                let unblockUserTask = URLSession.shared.dataTask(with: unblockRequest, completionHandler: {
                    (data, response, error) in
                    
                    if (error != nil) {
                        // сюда надо добавить обработку ошибок
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            
                            JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                        })
                    } else {
                        let res = response as! HTTPURLResponse
                        
                        print(res.statusCode)
                        
                        if res.statusCode == 204 {
                            // пользователь успешно разблокирован
                            self.bannedUsers.remove(at: indexPath.row)
                            
                            DispatchQueue.main.async(execute: {
                                SwiftSpinner.hide()
                                
                                self.tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.left)
                                
                                if self.bannedUsers.count == 0 {
                                    self.tableView.reloadData()
                                }
                            })
                        } else if res.statusCode == 401 {
                            error401Action(self)
                        } else if res.statusCode == 404 {
                            DispatchQueue.main.async(execute: {
                                SwiftSpinner.hide()
                                
                                JSSAlertView().danger(self, title: "Ошибка", text: "Данного пользователя нет в вашем черном списке", buttonText: "Закрыть")
                            })
                        } else if res.statusCode == 500 {
                            DispatchQueue.main.async(execute: {
                                SwiftSpinner.hide()
                                
                                JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                            })
                        }
                    }
                }) 
                
                SwiftSpinner.show("Разблокировка...", animated: true, viewToDisplay: self.view)
                unblockUserTask.resume()
            })
            
            confirmAlertController.addAction(unblockAction)
            
            let cancelAction : UIAlertAction = UIAlertAction(title: "Отмена", style: UIAlertActionStyle.cancel, handler: {(UIAlertAction) ->
                Void in
                
                self.tableView.setEditing(false, animated: true)
            })
            
            confirmAlertController.addAction(cancelAction)
            
            self.present(confirmAlertController, animated: true, completion: nil)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Разблокировать"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.setAnimationsEnabled(true)
        if self.tableView.indexPathForSelectedRow != nil {
            self.tableView.deselectRow(at: self.tableView.indexPathForSelectedRow!, animated: true)
        }
    }
    
    func reloadUsers() {
        self.isReloading = true
        
        self.usersLoaded = false
        self.allowLoadingMore = true
        self.currentOffset = 0
        self.bannedUsers.removeAll()
        self.tableView.reloadData()
        
        self.loadBlackList(true)
    }
    
    func loadBlackList(_ noAnimation: Bool = false, append: Bool = false) {
        let getBlackListRequest = getURLRequest("GET", url: getBackendApiPath() + "user/block?limit=\(self.usersLimit)&offset=\(self.currentOffset)", params: Dictionary<String, AnyObject>(), token: getUserToken())
        
        let loadBlackListTask = URLSession.shared.dataTask(with: getBlackListRequest, completionHandler: { (data, response, error) in
            
            if (error != nil) {
                // сюда надо добавить обработку ошибок
                DispatchQueue.main.async(execute: {
                    SwiftSpinner.hide()
                    self.isReloading = false
                    self.loadMoreUsers = false
                    self.refreshControl?.endRefreshing()
                    
                    JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                })
            } else {
                let blacklistResponse = response as! HTTPURLResponse
                
                print(blacklistResponse.statusCode)
                
                if blacklistResponse.statusCode == 200 {
                    let blacklist = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [Dictionary<String, AnyObject>]
                    
                    
                    if blacklist.count == 0 && !append {
                        self.allowLoadingMore = false
                        self.bannedUsers = blacklist
                    } else if blacklist.count == 0 && append {
                        self.currentOffset = self.bannedUsers.count
                        self.allowLoadingMore = false
                    } else {
                        
                        if !append {
                            self.bannedUsers = blacklist
                            
                            if blacklist.count < self.usersLimit {
                                self.allowLoadingMore = false
                            }
                        } else {
                            if blacklist.count >= self.usersLimit {
                                self.bannedUsers.append(contentsOf: blacklist)
                                self.allowLoadingMore = true
                            } else if blacklist.count > 0 && blacklist.count < self.usersLimit {
                                self.bannedUsers.append(contentsOf: blacklist)
                                self.allowLoadingMore = false
                            } else if blacklist.count == 0 {
                                self.allowLoadingMore = false
                            }
                        }
                        
                        self.currentOffset = self.bannedUsers.count
                        self.loadMoreUsers = false
                        self.isReloading = false
                    }
                    
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        self.animationView.stopAnimating()
                        self.tableView.tableFooterView!.isHidden = true
                        self.tableView.reloadData()
                        self.tableView.reloadData()
                        self.refreshControl?.endRefreshing()
                    })
                } else if blacklistResponse.statusCode == 500 {
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        
                        JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                    })
                } else if blacklistResponse.statusCode == 401 {
                    error401Action(self)
                }
            }
        }) 
        
        if !noAnimation {
            SwiftSpinner.show("Загрузка...", animated: true, viewToDisplay: self.view)
        }
        
        loadBlackListTask.resume()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToViewUserFromBlackList" {
            let destVC : UserProfile = segue.destination as! UserProfile
            destVC.userId = (self.bannedUsers[(self.tableView.indexPathForSelectedRow?.row)!]["user"] as! Dictionary<String, AnyObject>)["id"] as! Int
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
    }
    
    // бесконечный скролл
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        let deltaOffset = maximumOffset - currentOffset
        
        if deltaOffset <= 0 {
            loadMore()
        }
    }
    
    func loadMore() {
        if (!self.loadMoreUsers && self.allowLoadingMore && !self.isReloading && self.currentOffset >= self.usersLimit) {
            self.loadMoreUsers = true
            self.isReloading = true
            self.animationView.startAnimating()
            self.tableView.tableFooterView!.isHidden = false
            loadMoreBegin("загрузка...",
                loadMoreEnd: {(x:Int) -> () in
            })
        }
    }
    
    func loadMoreBegin(_ newtext:String, loadMoreEnd:@escaping (Int) -> ()) {
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
            self.loadBlackList(true, append: true)
            
            DispatchQueue.main.async {
                loadMoreEnd(0)
            }
        }
    }
    
    // ----------- EMPTY DATASET --------------
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let string : String = "Список пуст"
        
        let attributes : [String : AnyObject] = [
            NSFontAttributeName : UIFont.boldSystemFont(ofSize: 32),
            NSForegroundColorAttributeName : UIColor.gray
        ]
        
        return NSAttributedString(string: string, attributes: attributes)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let string : String = "В Вашем черном списке нет ни одного пользователя"
        
        let paragraph : NSMutableParagraphStyle = NSMutableParagraphStyle()
        paragraph.lineBreakMode = .byWordWrapping
        paragraph.alignment = .center
        
        let attributes : [String : AnyObject] = [
            NSFontAttributeName : UIFont.systemFont(ofSize: 14),
            NSForegroundColorAttributeName : UIColor.lightGray,
            NSParagraphStyleAttributeName : paragraph
        ]
        
        return NSAttributedString(string: string, attributes: attributes)
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        if self.bannedUsers.count == 0 {
            return true
        }
        
        return false
    }
    
    func emptyDataSetShouldAllowTouch(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    // ----------- EMPTY DATASET --------------
    
    
    // -------------------------------------------------------------
    // ------------------ 3D Touch ---------------------------------
    // -------------------------------------------------------------
    
    // PEEK
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        
        self.previewVC = nil
        
        self.previewVC = self.storyboard?.instantiateViewController(withIdentifier: "userProfile") as? UserProfile
        
        guard let indexPath = self.tableView.indexPathForRow(at: location) else {return nil}
        
        self.previewVC!.userId = (self.bannedUsers[indexPath.row]["user"] as! Dictionary<String, AnyObject>)["id"] as! Int
        
        return self.previewVC!
    }
    
    // POP
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        
        let userProfileVC = self.previewVC!
        show(userProfileVC, sender: self)
        self.previewVC = nil
    }
    
    // -------------------------------------------------------------
    // ------------------ /3D Touch --------------------------------
    // -------------------------------------------------------------
}
