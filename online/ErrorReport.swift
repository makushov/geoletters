//
//  ErrorReport.swift
//  Letters
//
//  Created by KingOP on 07.12.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit
import SZTextView
import TSMessages

class ErrorReport: UITableViewController, UITextViewDelegate {

    @IBOutlet weak var event: SZTextView!
    @IBOutlet weak var reason: SZTextView!
    
    override func viewDidLoad() {
        self.event.delegate = self
        self.reason.delegate = self
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.tableView.estimatedRowHeight = 44.0
        
        super.viewDidLoad()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let fixedWidth = textView.frame.size.width
        textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        var newFrame = textView.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        textView.frame = newFrame
        
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath as NSIndexPath).section == 0 && (indexPath as NSIndexPath).row == 0 {
            let fixedWidth = self.event.frame.size.width
            self.event.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = self.event.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        
            return newSize.height + 10
        } else if (indexPath as NSIndexPath).section == 1 && (indexPath as NSIndexPath).row == 0 {
            let fixedWidth = self.reason.frame.size.width
            self.reason.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = self.reason.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            
            return max((newSize.height + 10), 100)
        } else {
            return UITableViewAutomaticDimension
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    @IBAction func sendReportAction(_ sender: AnyObject) {
        self.view.endEditing(true)
        
        let issueEvent : String = self.event.text.trimmingCharacters(in: CharacterSet.whitespaces)
        
        let issueReason : String = self.reason.text.trimmingCharacters(in: CharacterSet.whitespaces)
        
        if issueEvent == "" || issueReason == "" {
            TSMessage.showNotification(
                in: UIApplication.shared.delegate?.window??.rootViewController,
                title: "Ошибка",
                subtitle: "Оба поля обязательны к заполнению",
                type: TSMessageNotificationType.error,
                duration: 2.0,
                canBeDismissedByUser: true
            )
            
            playErrorSound()
        } else {
            let sendIssueReportRequest = getURLRequest("POST", url: "\(getBackendApiPath())issue", params: ["title" : issueEvent, "description" : issueReason, "os" : "ios"], token: nil)
            
            let sendIssueReportTask = URLSession.shared.dataTask(with: sendIssueReportRequest, completionHandler: {
                
                (data, response, error) in
                
                if error != nil {
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        TSMessage.showNotification(
                            in: UIApplication.shared.delegate?.window??.rootViewController,
                            title: "Ошибка",
                            subtitle: error?.localizedDescription,
                            type: TSMessageNotificationType.error,
                            duration: 2.0,
                            canBeDismissedByUser: true
                        )
                    })
                } else {
                    let res = response as! HTTPURLResponse
                    
                    if res.statusCode == 201 {
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            
                            let success = JSSAlertView().info(self, title: "Спасибо!", text: "Мы изучим проблему и исправим ее как можно быстрее!", buttonText: "Продолжить")
                            
                            success.addAction({
                                self.navigationController?.popViewController(animated: true)
                            })
                        })
                    } else if res.statusCode == 422 {
                        let jsonResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [Dictionary<String, AnyObject>]
                        
                        if let parseJson = jsonResult {
                            var errorText = ""
                            
                            for error in parseJson {
                                errorText += error["message"] as! String + "\n"
                            }
                            
                            DispatchQueue.main.async(execute: {
                                SwiftSpinner.hide()
                                TSMessage.showNotification(
                                    in: UIApplication.shared.delegate?.window??.rootViewController,
                                    title: "Ошибка",
                                    subtitle: errorText,
                                    type: TSMessageNotificationType.error,
                                    duration: 2.0,
                                    canBeDismissedByUser: true
                                )
                                playErrorSound()
                            })
                        }
                    } else if res.statusCode == 500 {
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            TSMessage.showNotification(
                                in: UIApplication.shared.delegate?.window??.rootViewController,
                                title: "Ошибка",
                                subtitle: error500,
                                type: TSMessageNotificationType.error,
                                duration: 2.0,
                                canBeDismissedByUser: true
                            )
                            playErrorSound()
                        })
                    }
                }
            }) 
            
            _ = SwiftSpinner.show("Отправка...", animated: true, viewToDisplay: self.view)
            sendIssueReportTask.resume()
        }
    }
}
