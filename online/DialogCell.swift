//
//  DialogCell.swift
//  online
//
//  Created by KingOP on 02.11.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit

class DialogCell: UITableViewCell {

    @IBOutlet weak var userAvatar: UIImageView!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var userLogin: UILabel!
    @IBOutlet weak var messageDate: UILabel!
    @IBOutlet weak var unreadCountBG: UIImageView!
    @IBOutlet weak var unreadCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
