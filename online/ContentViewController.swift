//
//  ContentViewController.swift
//  Letters
//
//  Created by KingOP on 01.12.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit
import CoreLocation

class ContentViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    
    var pageIndex : Int!
    var titleText : String!
    var imageFile : String!
    var descriptionText : String!
    var buttonTitle : String!
    
    var locationManager : CLLocationManager = CLLocationManager()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.image.image = UIImage(named: self.imageFile)
        
        if self.pageIndex == 5 {
            self.image.contentMode = .scaleAspectFit
        } else {
            self.image.contentMode = .scaleAspectFill
        }
        
        self.titleLabel.text = self.titleText
        self.descriptionLabel.text = self.descriptionText
        
        self.locationManager.delegate = self
        
        if self.pageIndex != 5 {
            if self.buttonTitle == "" {
                self.actionButton.isHidden = true
                self.actionButton.isEnabled = false
            } else {
                self.actionButton.isHidden = false
                self.actionButton.isEnabled = true
                self.actionButton.setTitle(self.buttonTitle, for: UIControlState())
            }
        } else {
            self.actionButton.isHidden = false
            self.actionButton.isEnabled = true
            self.actionButton.setTitle(self.buttonTitle, for: UIControlState())
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionAction(_ sender: AnyObject) {
        if self.actionButton.titleLabel?.text == "Продолжить" {
            setTutorialShown()
            
            let mainTBC : MainContainer = self.storyboard?.instantiateViewController(withIdentifier: "mainTBC") as! MainContainer
            
            UIApplication.shared.delegate?.window??.rootViewController = mainTBC
        } else if self.actionButton.titleLabel?.text == "Разрешить" {
            self.locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse && self.actionButton.titleLabel?.text == "Разрешить" {
            self.actionButton.isHidden = true
        }
    }
}
