//
//  AppleMap.swift
//  Letters
//
//  Created by KingOP on 21.11.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit
import MapKit
import TSMessages

class AppleMap: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource, UIViewControllerPreviewingDelegate {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var userpicButton: UIButton!
    
    var clusterTableView : ClusterTableView = ClusterTableView()
    
    var clusteringManager = FBClusteringManager()
    
    var tapRecognizer : UITapGestureRecognizer = UITapGestureRecognizer()
    var clusterTapRecognizer : UITapGestureRecognizer = UITapGestureRecognizer()
    
    lazy var locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        return manager
    }()
    
    var userIdForSegue : Int? = nil
    var pinForSegue : StatusPin? = nil
    var annotationsForSegue : [StatusPin]? = nil
    
    var needsFilterUpdate : Bool = false
    
    var frameForDisplayedClusterTableView = CGRect.zero
    var frameForHiddenClusterTableView = CGRect.zero
    var isClusteredTableViewShown : Bool = false
    
    var previewVC : PinDetailView? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mapView.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
        self.mapView.showsCompass = true
        self.mapView.showsTraffic = true
        self.mapView.showsScale = true
        
        self.tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(AppleMap.pinTapped(_:)))
        self.clusterTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(AppleMap.clusterTapped(_:)))
        
        if pushUserInfo != nil {
            // мы попали сюда из пуша
            if pushUserInfo!["event"] as! String == "new_message" {
                self.performSegue(withIdentifier: "goToMessagesFromStartPush", sender: self)
            } else {
                self.performSegue(withIdentifier: "goToPinViewFromPushNotification", sender: self)
            }
        }
        
        self.frameForDisplayedClusterTableView = CGRect(x: 0, y: self.view.frame.size.height / 2, width: self.view.frame.size.width, height: self.view.frame.size.height / 2)
        
        self.frameForHiddenClusterTableView = CGRect(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height / 2)
        
        self.clusterTableView = ClusterTableView.instanceFromNib() as! ClusterTableView
        self.clusterTableView.frame = self.frameForHiddenClusterTableView
        self.clusterTableView.tableView.delegate = self
        self.clusterTableView.tableView.dataSource = self
        self.clusterTableView.closeButton.addTarget(self, action: #selector(AppleMap.closeClusterView(_:)), for: .touchUpInside)
        self.clusterTableView.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        self.view.addSubview(self.clusterTableView)
        
        self.clusterTableView.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "clusterPinsListInMap")
        
        self.clusterTableView.tableView.backgroundColor = UIColor.clear
        
        self.clusterTableView.tableView.contentInset = UIEdgeInsetsMake(0, 0, (self.tabBarController?.tabBar.frame.size.height)!, 0)
        
        if isUserAuthenticated() {
            updateNotificationCount()
        }
        
        self.loadPins()
    }
    
    func closeClusterView(_ sender: AnyObject) {
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.clusterTableView.frame = self.frameForHiddenClusterTableView
        }) 
        self.isClusteredTableViewShown = false
        self.annotationsForSegue = nil
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return max(60, UITableViewAutomaticDimension)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "goToPinViewFromMapViewClusterViewList", sender: self)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "clusterPinsListInMap")
        
        if (cell != nil) {
            cell = UITableViewCell(style: UITableViewCellStyle.subtitle,
                reuseIdentifier: "clusterPinsListInMap")
        }
        
        cell!.textLabel?.text = self.annotationsForSegue![(indexPath as NSIndexPath).row].subtitle == nil ? "" : self.annotationsForSegue![(indexPath as NSIndexPath).row].subtitle
        cell!.textLabel?.numberOfLines = 2
        cell!.textLabel?.font = UIFont.systemFont(ofSize: 15)
        cell?.textLabel?.textColor = UIColor(netHex: 0x414141)
        
        cell!.detailTextLabel?.text = self.annotationsForSegue![(indexPath as NSIndexPath).row].title == nil ? "" : self.annotationsForSegue![(indexPath as NSIndexPath).row].title
        cell!.detailTextLabel?.font = UIFont.systemFont(ofSize: 12)
        cell?.detailTextLabel?.textColor = UIColor.darkGray
        
        var imageName : String = "pinIcon"
        
        if self.annotationsForSegue![(indexPath as NSIndexPath).row].catId < pinsCategories.count {
            imageName = pinsCategories[self.annotationsForSegue![(indexPath as NSIndexPath).row].catId].image
            
            if self.annotationsForSegue![(indexPath as NSIndexPath).row].userId != nil && isUserAuthenticated() && self.annotationsForSegue![(indexPath as NSIndexPath).row].userId! == getUserId()! {
                imageName = "\(imageName)My"
            }
            
            if self.annotationsForSegue![(indexPath as NSIndexPath).row].isUrgency {
                imageName = "\(imageName)Urgency"
            }
        }
        
        cell!.imageView?.image = UIImage(named: imageName)
        cell!.backgroundColor = UIColor.clear
        
        cell!.accessoryType = .disclosureIndicator
        
        if traitCollection.forceTouchCapability == .available {
            registerForPreviewing(with: self, sourceView: cell!)
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.annotationsForSegue == nil ? 0 : self.annotationsForSegue!.count
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if isUserAuthenticated() {
            subscribeForPushes()
            
            firstRegisterForPushes()
            
            if !isFirstSuggestionShown() {
                setFirstSuggestionShown()
            }
        } else {
            if !isFirstSuggestionShown() {
                let alertView : UIAlertController = UIAlertController(title: "Приветствуем!", message: "Для доступа ко всем возможностям приложения (личные сообщения, списки избранных пользователей, отметки \"Нравится\", комментарии к меткам и многие другие) мы рекомендуем Вам выполнить вход под своей учетной записью, либо зарегистрироваться, если учетной записи у Вас еще нет.", preferredStyle: UIAlertControllerStyle.alert)
                
                alertView.addAction(UIAlertAction(title: "Вход/регистрация", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                    
                    setFirstSuggestionShown()
                    
                    self.performSegue(withIdentifier: "goToAuth", sender: self)
                }))
                
                alertView.addAction(UIAlertAction(title: "Продолжить как гость", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                    
                    setFirstSuggestionShown()
                }))
                
                self.present(alertView, animated: true, completion: nil)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        UIView.setAnimationsEnabled(true)
        
        if self.clusterTableView.tableView.indexPathForSelectedRow != nil {
            self.clusterTableView.tableView.deselectRow(at: self.clusterTableView.tableView.indexPathForSelectedRow!, animated: true)
        }
        
        // установим картинку пользователя сверху слева
        if isUserAuthenticated() {
            self.userpicButton.setImage(maskedAvatarFromImage(UIImage(named: "user white")!), for: UIControlState())
        } else {
            self.userpicButton.setImage(maskedAvatarFromImage(UIImage(named: "user empty")!), for: UIControlState())
        }
        
        if self.needsFilterUpdate {
            self.reloadMapPins()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if view.annotation!.isKind(of: StatusPin.self) {
            view.addGestureRecognizer(self.tapRecognizer)
        } else if view.isKind(of: FBAnnotationClusterView.self) {
            self.annotationsForSegue = ((view as! FBAnnotationClusterView).annotation as! FBAnnotationCluster).annotations as? [StatusPin]
            
            if !self.isClusteredTableViewShown {
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    self.clusterTableView.frame = self.frameForDisplayedClusterTableView
                })
                self.isClusteredTableViewShown = true
            }
            
            self.clusterTableView.tableView.reloadData()
            view.addGestureRecognizer(self.clusterTapRecognizer)
        }
    }
    
    func clusterTapped(_ sender: UITapGestureRecognizer) {
        self.annotationsForSegue = ((sender.view as! FBAnnotationClusterView).annotation as! FBAnnotationCluster).annotations as? [StatusPin]
        
        if !self.isClusteredTableViewShown {
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.clusterTableView.frame = self.frameForDisplayedClusterTableView
            })
            self.isClusteredTableViewShown = true
        }
        
        self.clusterTableView.tableView.reloadData()
        (sender.view as! FBAnnotationClusterView).addGestureRecognizer(self.clusterTapRecognizer)
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        if view.annotation!.isKind(of: StatusPin.self) {
            view.removeGestureRecognizer(self.tapRecognizer)
        }
    }
    
    func pinTapped(_ sender: UITapGestureRecognizer) {
        self.pinForSegue = (sender.view as! MKAnnotationView).annotation as? StatusPin
        self.performSegue(withIdentifier: "goToDetailPinView", sender: self)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation.isKind(of: FBAnnotationCluster.self) {
            var clusterView = mapView.dequeueReusableAnnotationView(withIdentifier: "clusterpin")
            clusterView = FBAnnotationClusterView(annotation: annotation, reuseIdentifier: "clusterpin", options: nil)
            return clusterView
        } else if annotation.isKind(of: MKUserLocation.self) {
            return nil
        } else {
            let anno = annotation as! StatusPin
            
            let pin = mapView.dequeueReusableAnnotationView(withIdentifier: "pin") ?? MKAnnotationView(annotation: anno, reuseIdentifier: "pin")
            
            if let infoView = StatusView.instanceFromNib() as? StatusView {
                infoView.status.text = anno.subtitle!
                //infoView.likes.text = anno.likesCount.description
                //infoView.comments.text = anno.commentsCount.description
                pin.detailCalloutAccessoryView = infoView
            }
            
            let index : Int = anno.catId >= pinsCategories.count ? 0 : anno.catId
            
            var imageName : String = pinsCategories[index].image
            
            if anno.userId != nil && isUserAuthenticated() && anno.userId! == getUserId()! {
                imageName = "\(imageName)My"
            }
            
            if anno.isUrgency {
                imageName = "\(imageName)Urgency"
            }
            
            pin.image = UIImage(named: imageName)
            
            pin.canShowCallout = true
            
            if traitCollection.forceTouchCapability == .available {
                registerForPreviewing(with: self, sourceView: pin)
            }
            
            return pin
        }
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool){
        OperationQueue().addOperation({
            let mapBoundsWidth = Double(self.mapView.bounds.size.width)
            let mapRectWidth:Double = self.mapView.visibleMapRect.size.width
            let scale:Double = mapBoundsWidth / mapRectWidth
            let annotationArray = self.clusteringManager.clusteredAnnotationsWithinMapRect(self.mapView.visibleMapRect, withZoomScale:scale)
            self.clusteringManager.displayAnnotations(annotationArray, onMapView:self.mapView)
        })
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            self.locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            
            self.mapView.setRegion(region, animated: true)
            
            self.locationManager.stopUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {

    }
    
    func loadPins(_ noAnimation : Bool = false) {
        if !noAnimation {
            SwiftSpinner.show("Загрузка меток...", animated: true, viewToDisplay: self.view)
        }
        
        var loadMarkersRequest : URLRequest
        
        //if leftX != nil && rightX != nil && topY != nil && bottomY != nil {
            //loadMarkersRequest = getURLRequest("GET", url: getBackendApiPath() + "temprorary-status?leftX=\(leftX!)&rightX=\(rightX!)&topY=\(topY!)&bottomY=\(bottomY!)")
        //} else {
            //loadMarkersRequest = getURLRequest("GET", url: getBackendApiPath() + "temprorary-status") // добавить limit
        //}
        
        if !isUserAuthenticated() {
            var url : String = getBackendApiPath() + "temprorary-status"
            
            if filterString != nil {
                url = "\(url)?category_id=\(filterString!)"
            }
            
            loadMarkersRequest = getURLRequest("GET", url: url)
        } else {
            var url : String = getBackendApiPath() + "temprorary-status/authorized"
            
            if filterString != nil {
                url = "\(url)?category_id=\(filterString!)"
            }
            
            print(url)
            
            loadMarkersRequest = getURLRequest("GET", url: url, params: Dictionary<String, AnyObject>(), token: getUserToken()!)
        }
        
        let loadMarkersDataTask = URLSession.shared.dataTask(with: loadMarkersRequest, completionHandler: { (data, response, error) in
            
            self.needsFilterUpdate = false
            
            if (error != nil) {
                // сюда надо добавить обработку ошибок
                DispatchQueue.main.async(execute: {
                    SwiftSpinner.hide()
                    
                    TSMessage.showNotification(
                        in: UIApplication.shared.delegate?.window??.rootViewController,
                        title: "Ошибка",
                        subtitle: error?.localizedDescription,
                        type: TSMessageNotificationType.error,
                        duration: 2.0,
                        canBeDismissedByUser: true
                    )
                })
            } else {
                let markersResponse = response as! HTTPURLResponse
                
                print(markersResponse.statusCode)
                
                if markersResponse.statusCode == 200 {
                    let markersResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [Dictionary<String, AnyObject>]
                    
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        
                        for marker in markersResult! {
                            let longitude : CLLocationDegrees = (marker["longitude"] as! String).doubleValue
                            let latitude : CLLocationDegrees = (marker["latitude"] as! String).doubleValue
                            
                            let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
                            
                            if !(marker["user"] is NSNull) && marker["user"] != nil {
                                let user = marker["user"] as! Dictionary<String, AnyObject>
                                
                                let pinMarker = StatusPin(coordinate: coordinates, title: user["username"] as! String, subtitle: marker["status"] as! String, id: marker["id"] as? Int, catId: marker["category_id"] as! Int, isUrgency: (marker["is_special"] as! Int) == 0 ? false : true, likesCount: marker ["likes_count"] as! Int, commentsCount: marker["comments_count"] as! Int)
                                
                                pinMarker.userId = user["id"] as? Int
                                
                                self.clusteringManager.addAnnotations([pinMarker])
                                
                            } else {
                                let pinMarker = StatusPin(coordinate: coordinates, title: marker["name"] as! String, subtitle: marker["status"] as! String, id: marker["id"] as? Int, catId: marker["category_id"] as! Int, isUrgency: (marker["is_special"] as! Int) == 0 ? false : true, likesCount: marker["likes_count"] as! Int, commentsCount: marker["comments_count"] as! Int)
                                
                                self.clusteringManager.addAnnotations([pinMarker])
                            }
                        }
                        
                        let mapBoundsWidth = Double(self.mapView.bounds.size.width)
                        let mapRectWidth:Double = self.mapView.visibleMapRect.size.width
                        let scale:Double = mapBoundsWidth / mapRectWidth
                        let annotationArray = self.clusteringManager.clusteredAnnotationsWithinMapRect(self.mapView.visibleMapRect, withZoomScale:scale)
                        self.clusteringManager.displayAnnotations(annotationArray, onMapView:self.mapView)
                    })
                } else if markersResponse.statusCode == 500 {
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        
                        TSMessage.showNotification(
                            in: UIApplication.shared.delegate?.window??.rootViewController,
                            title: "Ошибка",
                            subtitle: error500,
                            type: TSMessageNotificationType.error,
                            duration: 2.0,
                            canBeDismissedByUser: true
                        )
                    })
                }
            }
        }) 
        
        loadMarkersDataTask.resume()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToUsersFromAppleMap" {
            let destVC : UserProfile = segue.destination as! UserProfile
            destVC.userId = self.userIdForSegue!
            self.userIdForSegue = nil
        } else if segue.identifier == "goToPostMessage" {
            let destNC : PostMessageNC = segue.destination as! PostMessageNC
            
            (destNC.viewControllers[0] as! PostMessage).mapController = self
        } else if segue.identifier == "goToPinViewFromPushNotification" {
            let destVC : PinDetailView = segue.destination as! PinDetailView
            destVC.pinId = pushUserInfo!["status_id"] as! Int
            
            if pushUserInfo!["event"] as! String == "comment" {
                destVC.goToComments = true
            }
        } else if segue.identifier == "goToMessagesFromStartPush" {
            let destVC : Messages = segue.destination as! Messages
            
            let dialogId = pushUserInfo!["dialog_id"] as! Int
            let username = pushUserInfo!["username"] as! String
            
            destVC.dialogId = dialogId
            destVC.userId = pushUserInfo!["user_id"] as? Int
            
            var usernameToDisplay = ""
            if (pushUserInfo!["members"] as! [Dictionary<String, AnyObject>]).count > 2 {
                if !(pushUserInfo!["dialog_title"] is NSNull) && pushUserInfo!["dialog_title"] != nil && pushUserInfo!["dialog_title"] as! String != "" {
                    
                    usernameToDisplay = pushUserInfo!["dialog_title"] as! String
                } else {
                    for user in (pushUserInfo!["members"] as! [Dictionary<String, AnyObject>]) {
                        usernameToDisplay += "\(user["username"] as! String), "
                    }
                    
                    usernameToDisplay = usernameToDisplay.truncate(usernameToDisplay.length - 2)
                }
            } else {
                usernameToDisplay = username
            }
            
            destVC.userName = usernameToDisplay
            
            destVC.members = pushUserInfo!["members"] as! [Dictionary<String, AnyObject>]
            
            if !(pushUserInfo!["dialog_photo"] is NSNull) && pushUserInfo!["dialog_photo"] != nil && (pushUserInfo!["dialog_photo"] as! String) != "" {
                
                destVC.avatarPath = pushUserInfo!["dialog_photo"] as? String
            } else if !(pushUserInfo!["photo"] is NSNull) && pushUserInfo!["photo"] != nil && (pushUserInfo!["photo"] as! String) != "" && (pushUserInfo!["members"] as! [Dictionary<String, AnyObject>]).count == 2 {
                destVC.avatarPath = pushUserInfo!["photo"] as? String
            }
        } else if segue.identifier == "goToDetailPinView" {
            let destVC : PinDetailView = segue.destination as! PinDetailView
            destVC.pin = self.pinForSegue!
            destVC.mapController = self
            self.pinForSegue = nil
        } else if segue.identifier == "goToPinViewFromMapViewClusterViewList" {
            let destVC : PinDetailView = segue.destination as! PinDetailView
            destVC.pin = self.annotationsForSegue![((self.clusterTableView.tableView.indexPathForSelectedRow as NSIndexPath?)?.row)!]
        } else if segue.identifier == "goToPinsFilter" {
            let destVC : PinsFilter = (segue.destination as! UINavigationController).viewControllers[0] as! PinsFilter
            
            destVC.mapController = self
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    
    @IBAction func accountAction(_ sender: AnyObject) {
        if !isUserAuthenticated() {
            self.performSegue(withIdentifier: "goToAuth", sender: self)
        } else {
            self.performSegue(withIdentifier: "goToAccountSettings", sender: self)
        }
    }
    
    func reloadMapPins() {
        if self.isClusteredTableViewShown {
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.clusterTableView.frame = self.frameForHiddenClusterTableView
            })
            self.isClusteredTableViewShown = false
            self.annotationsForSegue = nil
            self.clusterTableView.tableView.reloadData()
        }
        
        self.clusteringManager.setAnnotations([])
        self.mapView.removeAnnotations(self.mapView.annotations)
        self.loadPins()
    }
    
    @IBAction func menuAction(_ sender: AnyObject) {
        let actionsController : UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionsController.addAction(UIAlertAction(title: "Фильтр меток", style: .default, handler: { (action) -> Void in
            self.performSegue(withIdentifier: "goToPinsFilter", sender: self)
        }))
        
        actionsController.addAction(UIAlertAction(title: "Обновить карту", style: .default, handler: { (action) -> Void in
            self.reloadMapPins()
        }))
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            actionsController.addAction(UIAlertAction(title: "Моё местоположение", style: .default, handler: { (action) -> Void in
                
                let location = self.locationManager.location?.coordinate
                
                if location != nil {
                    let center = CLLocationCoordinate2D(latitude: location!.latitude, longitude: location!.longitude)
                    let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
                    
                    self.mapView.setRegion(region, animated: true)
                } else {
                    TSMessage.showNotification(
                        in: UIApplication.shared.delegate?.window??.rootViewController,
                        title: "Ошибка",
                        subtitle: "Не удалось определить текущее местоположение",
                        type: TSMessageNotificationType.error,
                        duration: 2.0,
                        canBeDismissedByUser: true
                    )
                }
                
            }))
        }
        
        actionsController.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        
        
        self.present(actionsController, animated: true, completion: nil)
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        
        if previewingContext.sourceView.isKind(of: MKAnnotationView.self) {
            guard let pin = (previewingContext.sourceView as? MKAnnotationView)?.annotation as? StatusPin else {return nil}
            
            self.previewVC = self.storyboard?.instantiateViewController(withIdentifier: "viewPinDetail") as? PinDetailView
            self.previewVC!.pin = pin
            self.previewVC!.mapController = self
            self.pinForSegue = nil
            
            return self.previewVC!
        } else if previewingContext.sourceView.isKind(of: UITableViewCell.self) {
            self.previewVC = self.storyboard?.instantiateViewController(withIdentifier: "viewPinDetail") as? PinDetailView
            self.previewVC!.pin = self.annotationsForSegue![((self.clusterTableView.tableView.indexPath(for: previewingContext.sourceView as! UITableViewCell) as NSIndexPath?)?.row)!]
            self.previewVC!.mapController = self
            self.pinForSegue = nil
            
            return self.previewVC!
        }
        
        return nil
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        let vcToShow = self.previewVC!
        
        show(vcToShow, sender: self)
        self.previewVC = nil
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        self.frameForDisplayedClusterTableView = CGRect(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height / 2)
        
        self.frameForHiddenClusterTableView = CGRect(x: 0, y: self.view.frame.size.height * 8, width: self.view.frame.size.width, height: self.view.frame.size.height / 2)
        
        if self.isClusteredTableViewShown {
            self.clusterTableView.frame = frameForDisplayedClusterTableView
        } else {
            self.clusterTableView.frame = frameForHiddenClusterTableView
        }
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        self.frameForDisplayedClusterTableView = CGRect(x: 0, y: self.view.frame.size.height / 2, width: self.view.frame.size.width, height: self.view.frame.size.height / 2)
        
        self.frameForHiddenClusterTableView = CGRect(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height / 2)
        
        if self.isClusteredTableViewShown {
            if fromInterfaceOrientation == .landscapeLeft || fromInterfaceOrientation == .landscapeRight {
                self.clusterTableView.frame = frameForDisplayedClusterTableView
            } else {
                UIView.animate(withDuration: 0.1, animations: { () -> Void in
                    self.clusterTableView.frame = self.frameForDisplayedClusterTableView
                })
            }
        } else {
            self.clusterTableView.frame = frameForHiddenClusterTableView
        }
    }
}
