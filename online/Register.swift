//
//  Register.swift
//  online
//
//  Created by KingOP on 26.10.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit
import RSKImageCropper
import WatchConnectivity

class Register: UITableViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, RSKImageCropViewControllerDelegate, WCSessionDelegate {

    @IBOutlet weak var login: MKTextField!
    @IBOutlet weak var email: MKTextField!
    @IBOutlet weak var password: MKTextField!
    @IBOutlet weak var passwordConfirm: MKTextField!
    @IBOutlet weak var phone: MKTextField!
    @IBOutlet weak var avatarPreview: UIImageView!
    
    @IBOutlet weak var sexSwitcher: UISegmentedControl!
    
    let imagePicker = CustomPickerController()
    
    var avatarImage : UIImage? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.login.layer.borderColor = UIColor.clear.cgColor
        self.login.floatingPlaceholderEnabled = true
        self.login.placeholder = "Логин"
        self.login.rippleLayerColor = UIColor.white
        self.login.tintColor = UIColor.MKColor.Blue
        
        self.login.delegate = self
        
        self.password.layer.borderColor = UIColor.clear.cgColor
        self.password.floatingPlaceholderEnabled = true
        self.password.placeholder = "Пароль"
        self.password.rippleLayerColor = UIColor.white
        self.password.tintColor = UIColor.MKColor.Blue
        
        self.password.delegate = self
        
        self.passwordConfirm.layer.borderColor = UIColor.clear.cgColor
        self.passwordConfirm.floatingPlaceholderEnabled = true
        self.passwordConfirm.placeholder = "Подтверждение пароля"
        self.passwordConfirm.rippleLayerColor = UIColor.white
        self.passwordConfirm.tintColor = UIColor.MKColor.Blue
        
        self.passwordConfirm.delegate = self
        
        self.email.layer.borderColor = UIColor.clear.cgColor
        self.email.floatingPlaceholderEnabled = true
        self.email.placeholder = "Email"
        self.email.rippleLayerColor = UIColor.white
        self.email.tintColor = UIColor.MKColor.Blue
        
        self.email.delegate = self
        
        self.phone.layer.borderColor = UIColor.clear.cgColor
        self.phone.floatingPlaceholderEnabled = true
        self.phone.placeholder = "Телефон"
        self.phone.rippleLayerColor = UIColor.white
        self.phone.tintColor = UIColor.MKColor.Blue
        
        self.phone.delegate = self
        
        self.imagePicker.delegate = self
        
        self.avatarPreview.image = maskedAvatarFromImage(self.avatarPreview.image!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.setAnimationsEnabled(true)
        self.tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count : Int = 0
        
        if section == 0 {
            count = 4
        } else if section == 1 && self.avatarImage != nil {
            count = 4
        } else {
            count = 3
        }
        
        return count
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    @IBAction func registerAction(_ sender: AnyObject) {
        self.view.endEditing(true)
        
        let login = self.login.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        let password = self.password.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        let passwordConfirm = self.passwordConfirm.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        let email = self.email.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        
        let phone = self.phone.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        
        var error : String = ""
        
        if login == "" {
            error += "Вы не указали логин.\n"
        } else if login.length > 20 {
            error += "Длина логина не должна превышать 20 символов.\n"
        }
        if password == "" {
            error += "Вы не указали пароль.\n"
        } else if password != "" && passwordConfirm == "" {
            error += "Вы не подтвердили пароль.\n"
        } else if password != "" && passwordConfirm != "" && password != passwordConfirm {
            error += "Введенные пароли не совпадают.\n"
        }
        if email == "" {
            error += "Вы не указали email.\n"
        } else if email != "" && !isEmailCorrect(email) {
            error += "Вы указали некорректный email.\n"
        }
        if phone != "" && !isPhoneNumberCorrect(phone) {
            error += "Вы указали некорректный номер телефона.\n"
        }
        
        if error != "" {
            JSSAlertView().danger(self, title: "Ошибка", text: error, buttonText: "Закрыть")
        } else {
            SwiftSpinner.show("Отправка данных...", animated: true, viewToDisplay: self.view)
            
            var sex = ""
            
            if self.sexSwitcher.selectedSegmentIndex == 1 {
                sex = "m"
            } else if self.sexSwitcher.selectedSegmentIndex == 2 {
                sex = "f"
            }
            
            var params : Dictionary<String, AnyObject>
            
            if self.avatarImage != nil {
                
                let data = UIImageJPEGRepresentation(self.avatarImage!, 0.5)
                let encodedImage = data!.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters)
                
                params = [
                    "username"          : login as AnyObject,
                    "password"          : password as AnyObject,
                    "confirm_password"  : passwordConfirm as AnyObject,
                    "email"             : email as AnyObject,
                    "phone"             : phone as AnyObject,
                    "gender"            : sex as AnyObject,
                    "photo"             : encodedImage as AnyObject
                ]
            } else {
                params = [
                    "username"          : login as AnyObject,
                    "password"          : password as AnyObject,
                    "confirm_password"  : passwordConfirm as AnyObject,
                    "email"             : email as AnyObject,
                    "phone"             : phone as AnyObject,
                    "gender"            : sex as AnyObject
                ]
            }
            
            let request : URLRequest = getURLRequest("POST", url: getBackendApiPath() + "user/register", params: params)
            let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                
                if (error != nil) {
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                    })
                } else {
                    let res = response as! HTTPURLResponse
                    
                    print(res.statusCode)
                    
                    if res.statusCode == 201 {
                        let jsonResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                        
                        if let regJson = jsonResult {
                            DispatchQueue.main.async(execute: {
                                SwiftSpinner.hide()
                                
                                let successAlertView = JSSAlertView().info(self, title: "Поздравляем!", text: "Регистрация прошла успешно, сейчас Вы будете авторизованы.", buttonText: "Продолжить")
                                
                                successAlertView.addAction({
                                    SwiftSpinner.show("Авторизация...", animated: true, viewToDisplay: self.view)
                                    
                                    // ну и вот тут короче пойдет запрос токена
                                    let tokenParams = [
                                        "username"      : regJson["username"] as! String,
                                        "password"      : password,
                                        "client_id"     : "help_application",
                                        "client_secret" : "help_application_zx",
                                        "grant_type"    : "password"
                                    ]
                                    
                                    let request : URLRequest = getURLRequest("POST", url: getBackendApiPath() + "oauth2/token", params: tokenParams)
                                    
                                    let getTokenTask = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                                        
                                        if (error != nil) {
                                            // сюда надо добавить обработку ошибок
                                            DispatchQueue.main.async(execute: {
                                                SwiftSpinner.hide()
                                                
                                                JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                                            })
                                        } else {
                                            let tokenResponse = response as! HTTPURLResponse
                                            
                                            let tokenResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                                            
                                            
                                            if let json = tokenResult {
                                                if tokenResponse.statusCode ==  200 {
                                                    
                                                    authenticateUser(regJson["id"] as! Int, token: json["access_token"] as! String, login: regJson["username"] as! String)

                                                    if WCSession.isSupported() { //makes sure it's not an iPad or iPod
                                                        let watchSession = WCSession.default()
                                                        watchSession.delegate = self
                                                        watchSession.activate()
                                                        if watchSession.isPaired && watchSession.isWatchAppInstalled {
                                                            do {
                                                                try watchSession.updateApplicationContext(getUserDataForWatchKit())
                                                            } catch let error as NSError {
                                                                print(error.description)
                                                            }
                                                        }
                                                    }
                                                    
                                                    DispatchQueue.main.async(execute: {
                                                        SwiftSpinner.hide()
                                                        
                                                        let success = JSSAlertView().info(self, title: "Поздравляем!", text: "Вы успешно авторизованы в системе", buttonText: "Продолжить")
                                                        
                                                        success.addAction({
                                                            self.dismiss(animated: true, completion: nil)
                                                        })
                                                    })
                                                } else if tokenResponse.statusCode == 400 {
                                                    DispatchQueue.main.async(execute: {
                                                        SwiftSpinner.hide()
                                                        
                                                        let error = JSSAlertView().danger(self, title: "Ошибка", text: json["message"] as? String, buttonText: "Закрыть")
                                                        
                                                        error.addAction({
                                                            self.dismiss(animated: true, completion: nil)
                                                        })
                                                    })
                                                }
                                            }
                                        }
                                    }) 

                                    getTokenTask.resume()
                                })
                            })
                        }
                    } else if res.statusCode == 422 {
                        let jsonResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSMutableArray
                        
                        if let parseJson = jsonResult {
                            var errorText = ""
                            
                            for error in parseJson {
                                errorText += (error as! NSDictionary)["message"] as! String + "\n"
                            }
                            
                            DispatchQueue.main.async(execute: {
                                SwiftSpinner.hide()
                                
                                JSSAlertView().danger(self, title: "Ошибка", text: errorText, buttonText: "Закрыть")
                            })
                        }
                    }
                }
            }) 
            task.resume()
        }
    }
    
    @IBAction func uploadPhoto(_ sender: AnyObject) {
        self.imagePicker.allowsEditing = false
        
        let imagePickVariantController : UIAlertController = UIAlertController(title: "Сделайте выбор", message: "Что использовать для выбора изображения?", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        imagePickVariantController.addAction(UIAlertAction(title: "Камера", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            self.imagePicker.sourceType = .camera
            
            self.present(self.imagePicker, animated: true, completion: nil)
        }))
        
        imagePickVariantController.addAction(UIAlertAction(title: "Галерея", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            self.imagePicker.sourceType = .photoLibrary
            
            self.present(self.imagePicker, animated: true, completion: nil)
        }))
        
        imagePickVariantController.addAction(UIAlertAction(title: "Отмена", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(imagePickVariantController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.avatarImage = pickedImage
        }
        
        self.dismiss(animated: true, completion: nil)
        
        let imageCropVC : RSKImageCropViewController = RSKImageCropViewController(image: self.avatarImage)
        imageCropVC.delegate = self
        self.navigationController?.pushViewController(imageCropVC, animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController!) {
        self.avatarImage = nil
        self.navigationController?.popViewController(animated: true)
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController!, didCropImage croppedImage: UIImage!, usingCropRect cropRect: CGRect) {
        SwiftSpinner.hide()
        
        self.avatarPreview.image = maskedAvatarFromImage(croppedImage)
        self.avatarImage = croppedImage
        self.navigationController?.popViewController(animated: true)
        self.tableView.reloadData()
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController!, willCropImage originalImage: UIImage!) {
        SwiftSpinner.show("Сохранение...", animated: true, viewToDisplay: controller.view)
    }
    
    @IBAction func deletePhotoAction(_ sender: AnyObject) {
        self.avatarImage = nil
        self.avatarPreview.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!)
        self.tableView.reloadData()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
}
