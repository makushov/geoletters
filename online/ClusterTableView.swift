//
//  ClusterTableView.swift
//  Letters 1.1
//
//  Created by KingOP on 27.01.16.
//  Copyright © 2016 KingOP. All rights reserved.
//

import UIKit

class ClusterTableView: UIView {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var closeButton: UIButton!
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "ClusterTableView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
}
