//
//  LikedList.swift
//  Letters 1.1
//
//  Created by KingOP on 25.01.16.
//  Copyright © 2016 KingOP. All rights reserved.
//

import UIKit
import Haneke
import DZNEmptyDataSet
import CoreLocation
import TSMessages

class LikedList: UITableViewController, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource {
    
    var likedUsers : NSMutableArray = NSMutableArray()
    var pin : StatusPin = StatusPin(coordinate: CLLocationCoordinate2D(), title: "", subtitle: "")
    var pinViewController : PinDetailView = PinDetailView()
    
    var currentOffset : Int = 0
    var likedUsersLimit : Int = 30
    
    var loadingMoreView = UIView()
    var animationView = UIActivityIndicatorView()
    
    var loadMoreUsers = true
    var allowLoadingMore = true
    var usersLoaded : Bool = false
    
    var isLoaded : Bool = false
    
    var needsReload : Bool = false
    
    var isReloading : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(LikedList.reloadUsers), for: UIControlEvents.valueChanged)
        self.refreshControl = refreshControl
        
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        self.loadingMoreView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 60))
        self.animationView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        
        self.loadingMoreView.addSubview(animationView)
        
        self.animationView.center = self.loadingMoreView.center
        
        self.tableView.tableFooterView = loadingMoreView
        self.tableView.tableFooterView!.isHidden = true
        
        self.tableView.emptyDataSetDelegate = self
        self.tableView.emptyDataSetSource = self
        
        self.loadLikedUsers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.tableView.indexPathForSelectedRow != nil {
            self.tableView.deselectRow(at: self.tableView.indexPathForSelectedRow!, animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.likedUsers.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "likedUserCell", for: indexPath)
        
        cell.textLabel?.text = ((self.likedUsers[(indexPath as NSIndexPath).row] as! NSDictionary)["user"] as! NSDictionary)["username"] as? String
        
        if cell.imageView!.viewWithTag((indexPath as NSIndexPath).row + 1) == nil {
            cell.imageView?.image = maskedAvatarFromImage((cell.imageView?.image)!, original: true)
            
            if !(((self.likedUsers[(indexPath as NSIndexPath).row] as! NSDictionary)["user"] as! NSDictionary)["photo"] is NSNull) && ((self.likedUsers[(indexPath as NSIndexPath).row] as! NSDictionary)["user"] as! NSDictionary)["photo"] != nil && ((self.likedUsers[(indexPath as NSIndexPath).row] as! NSDictionary)["user"] as! NSDictionary)["photo"] as! String != "" {
                
                let urlString = "\(getBackendDomainPath())\(((self.likedUsers[(indexPath as NSIndexPath).row] as! NSDictionary)["user"] as! NSDictionary)["photo"] as! String)"
                let imgURL = URL(string: urlString)
                
                if let img = avatarCache[urlString] {
                    cell.imageView!.image = img
                    cell.imageView!.tag = ((indexPath as NSIndexPath).row + 1)
                }
                else {
                    // The image isn't cached, download the img data
                    // We should perform this in a background thread
                    let request: URLRequest = URLRequest(url: imgURL!)
                    let mainQueue = OperationQueue.main
                    NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
                        if error == nil {
                            
                            let res = response as! HTTPURLResponse
                            if res.statusCode != 404 {
                                // Convert the downloaded data in to a UIImage object
                                
                                if data != nil {
                                    let image : UIImage = maskedAvatarFromImage(UIImage(data: data!)!)
                                    // Store the image in to our cache
                                    avatarCache[urlString] = image
                                    
                                    // Update the cell
                                    DispatchQueue.main.async(execute: {
                                        if let cellToUpdate = tableView.cellForRow(at: indexPath) {
                                            cellToUpdate.imageView!.image = image
                                            cellToUpdate.imageView!.tag = ((indexPath as NSIndexPath).row + 1)
                                        }
                                    })
                                } else {
                                    DispatchQueue.main.async(execute: {
                                        if let cellToUpdate = tableView.cellForRow(at: indexPath) {
                                            cell.imageView?.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!, original: true)
                                            cellToUpdate.imageView!.tag = ((indexPath as NSIndexPath).row + 1)
                                        }
                                    })
                                }
                            } else {
                                DispatchQueue.main.async(execute: {
                                    if let cellToUpdate = tableView.cellForRow(at: indexPath) {
                                        cell.imageView?.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!, original: true)
                                        cellToUpdate.imageView!.tag = ((indexPath as NSIndexPath).row + 1)
                                    }
                                })
                            }
                        }
                        else {
                            DispatchQueue.main.async(execute: {
                                if let cellToUpdate = tableView.cellForRow(at: indexPath) {
                                    cell.imageView?.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!, original: true)
                                    cellToUpdate.imageView!.tag = ((indexPath as NSIndexPath).row + 1)
                                }
                            })
                        }
                    })
                }
            } else {
                cell.imageView?.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!, original: true)
                cell.imageView!.tag = ((indexPath as NSIndexPath).row + 1)
            }
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToUserProfileFromLikedUsersList" {
            let destVC : UserProfile = segue.destination as! UserProfile
            destVC.userId = ((self.likedUsers[((self.tableView.indexPathForSelectedRow as NSIndexPath?)?.row)!] as! NSDictionary)["user"] as! NSDictionary)["id"] as! Int
        }
    }
    
    func reloadUsers() {
        self.isReloading = true
        
        self.usersLoaded = false
        self.allowLoadingMore = true
        self.currentOffset = 0
        self.likedUsers.removeAllObjects()
        self.tableView.reloadData()
        
        self.loadLikedUsers(true)
    }
    
    func loadLikedUsers(_ noAnimation: Bool = false, append: Bool = false) {
        let loadLikedUsersRequest = getURLRequest("GET", url: getBackendApiPath() + "temprorary-status/\(self.pin.id!)/like?limit=\(self.likedUsersLimit)&offset=\(self.currentOffset)", params: Dictionary<String, AnyObject>(), token: getUserToken()!)
        
        let loadLikedUsersTask = URLSession.shared.dataTask(with: loadLikedUsersRequest, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                DispatchQueue.main.async(execute: {
                    SwiftSpinner.hide()
                    
                    TSMessage.showNotification(
                        in: UIApplication.shared.delegate?.window??.rootViewController,
                        title: "Ошибка",
                        subtitle: error?.localizedDescription,
                        type: TSMessageNotificationType.error,
                        duration: 2.0,
                        canBeDismissedByUser: true
                    )
                    playErrorSound()
                })
            } else {
                let res = response as! HTTPURLResponse
                
                if res.statusCode == 200 {
                    let list = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableArray
                    
                    if list.count == 0 && !append {
                        self.allowLoadingMore = false
                        self.likedUsers = list
                    } else if list.count == 0 && append {
                        self.currentOffset = self.likedUsers.count
                        self.allowLoadingMore = false
                    } else {
                        
                        if !append {
                            self.likedUsers = list
                            
                            if list.count < self.likedUsersLimit {
                                self.allowLoadingMore = false
                            }
                        } else {
                            if list.count >= self.likedUsersLimit {
                                self.likedUsers.addObjects(from: list as [AnyObject])
                                self.allowLoadingMore = true
                            } else if list.count > 0 && list.count < self.likedUsersLimit {
                                self.likedUsers.addObjects(from: list as [AnyObject])
                                self.allowLoadingMore = false
                            } else if list.count == 0 {
                                self.allowLoadingMore = false
                            }
                        }
                        
                        self.currentOffset = self.likedUsers.count
                        self.loadMoreUsers = false
                        self.isReloading = false
                    }
                    
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        self.animationView.stopAnimating()
                        self.tableView.tableFooterView!.isHidden = true
                        self.tableView.reloadData()
                        self.tableView.reloadData()
                        self.refreshControl?.endRefreshing()
                        self.pinViewController.likesCount.text = self.likedUsers.count.description
                    })
                } else if res.statusCode == 401 {
                    error401Action(self)
                } else if res.statusCode == 500 {
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        
                        TSMessage.showNotification(
                            in: UIApplication.shared.delegate?.window??.rootViewController,
                            title: "Ошибка",
                            subtitle: error500,
                            type: TSMessageNotificationType.error,
                            duration: 2.0,
                            canBeDismissedByUser: true
                        )
                        playErrorSound()
                    })
                }
            }
        }) 
        
        if !noAnimation {
            SwiftSpinner.show("Загрузка...", animated: true, viewToDisplay: self.view)
        }
        
        loadLikedUsersTask.resume()
    }
    
    // бесконечный скролл
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        let deltaOffset = maximumOffset - currentOffset
        
        if deltaOffset <= 0 {
            loadMore()
        }
    }
    
    func loadMore() {
        if (!self.loadMoreUsers && self.allowLoadingMore && !self.isReloading && self.currentOffset >= self.likedUsersLimit) {
            self.loadMoreUsers = true
            self.isReloading = true
            self.animationView.startAnimating()
            self.tableView.tableFooterView!.isHidden = false
            loadMoreBegin("загрузка...",
                loadMoreEnd: {(x:Int) -> () in
            })
        }
    }
    
    func loadMoreBegin(_ newtext:String, loadMoreEnd:@escaping (Int) -> ()) {
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
            self.loadLikedUsers(true, append: true)
            
            DispatchQueue.main.async {
                loadMoreEnd(0)
            }
        }
    }
    
    // ----------- EMPTY DATASET --------------
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let string : String = "Нет отметок"
        
        let attributes : [String : AnyObject] = [
            NSFontAttributeName : UIFont.boldSystemFont(ofSize: 32),
            NSForegroundColorAttributeName : UIColor.gray
        ]
        
        return NSAttributedString(string: string, attributes: attributes)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let string : String = "Никто не отметил эту метку понравившейся"
        
        let paragraph : NSMutableParagraphStyle = NSMutableParagraphStyle()
        paragraph.lineBreakMode = .byWordWrapping
        paragraph.alignment = .center
        
        let attributes : [String : AnyObject] = [
            NSFontAttributeName : UIFont.systemFont(ofSize: 14),
            NSForegroundColorAttributeName : UIColor.lightGray,
            NSParagraphStyleAttributeName : paragraph
        ]
        
        return NSAttributedString(string: string, attributes: attributes)
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        if self.likedUsers.count == 0 {
            return true
        }
        
        return false
    }
    
    func emptyDataSetShouldAllowTouch(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    // ----------- EMPTY DATASET --------------
}
