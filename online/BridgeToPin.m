//
//  BridgeToPin.m
//  Letters 1.1
//
//  Created by KingOP on 05.02.16.
//  Copyright © 2016 KingOP. All rights reserved.
//

#import "BridgeToPin.h"
#import "online-Swift.h"

@implementation BridgeToPin

+(void)openPin:(NSInteger)pinId {
    
    PinDetailView *pin = (PinDetailView *)[[[[UIApplication sharedApplication] delegate] window].rootViewController.storyboard instantiateViewControllerWithIdentifier:@"viewPinDetail"];
    
    pin.pinId = pinId;
    
    [(UINavigationController*)[(MainContainer*)[[[UIApplication sharedApplication] delegate] window].rootViewController selectedViewController] pushViewController:pin animated:YES];
}

@end
