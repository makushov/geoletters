//
//  PushSettings.swift
//  Letters 1.1
//
//  Created by KingOP on 27.01.16.
//  Copyright © 2016 KingOP. All rights reserved.
//

import UIKit
import TSMessages

class PushSettings: UITableViewController {

    
    @IBOutlet weak var messagesSwitch: UISwitch!
    @IBOutlet weak var likeSwitcher: UISwitch!
    @IBOutlet weak var commentSwitcher: UISwitch!
    @IBOutlet weak var favouritesPinsSwitcher: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.messagesSwitch.isOn = false
        self.favouritesPinsSwitcher.isOn = false
        self.likeSwitcher.isOn = false
        self.commentSwitcher.isOn = false
        
        // получим текущие настройки
        let getPushSettingsRequest = getURLRequest("GET", url: getBackendApiPath() + "push?mobile=ios", params: Dictionary<String, AnyObject>(), token: getUserToken()!)
        
        let getPushSettingsTask = URLSession.shared.dataTask(with: getPushSettingsRequest, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                DispatchQueue.main.async(execute: {
                    SwiftSpinner.hide()
                    
                    let error = JSSAlertView().danger(self, title: "Ошибка", text: error?.localizedDescription, buttonText: "Закрыть")
                    
                    error.addAction({
                        self.navigationController?.popViewController(animated: true)
                    })
                })
            } else {
                let res = response as! HTTPURLResponse
                
                if res.statusCode == 401 {
                    error401Action(self)
                } else if res.statusCode == 500 {
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        
                        let error = JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                        
                        error.addAction({
                            self.navigationController?.popViewController(animated: true)
                        })
                    })
                } else if res.statusCode == 200 {
                    var pushSettings : NSDictionary? = nil
                    
                    let results = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSMutableArray
                    
                    for result in results! {
                        if (result as! NSDictionary)["device_token"] as! String == getPushToken() && (result as! NSDictionary)["device_id"] as! String == deviceUDID {
                            
                            // это наш токен
                            pushSettings = result as? NSDictionary
                            break
                        }
                    }
                    
                    DispatchQueue.main.async(execute: {
                        
                        SwiftSpinner.hide()
                        
                        if pushSettings != nil {
                            self.messagesSwitch.isOn = pushSettings!["on_message"] as! Int == 1
                            self.favouritesPinsSwitcher.isOn = pushSettings!["on_friend_status"] as! Int == 1
                            self.likeSwitcher.isOn = pushSettings!["on_like"] as! Int == 1
                            self.commentSwitcher.isOn = pushSettings!["on_comment"] as! Int == 1
                        } else {
                            self.messagesSwitch.isOn = false
                            self.favouritesPinsSwitcher.isOn = false
                            self.likeSwitcher.isOn = false
                            self.commentSwitcher.isOn = false
                        }
                    })
                }
            }
        }) 
        
        if pushToken != "" {
            SwiftSpinner.show("Загрузка...", animated: true, viewToDisplay: self.view)
            
            getPushSettingsTask.resume()
        } else {
            self.messagesSwitch.isOn = false
            self.favouritesPinsSwitcher.isOn = false
            self.likeSwitcher.isOn = false
            self.commentSwitcher.isOn = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func savePushSettings(_ sender: AnyObject) {
        
        if pushToken != "" {
            let onComment : Int = self.commentSwitcher.isOn == true ? 1 : 0
            let onLike : Int = self.likeSwitcher.isOn == true ? 1 : 0
            let onMessage : Int = self.messagesSwitch.isOn == true ? 1 : 0
            let onFriendStatus : Int = self.favouritesPinsSwitcher.isOn == true ? 1 : 0
            
            let savePushSettingsRequest = getURLRequest("POST", url: getBackendApiPath() + "push/subscribe", params: ["device_token" : pushToken, "device_id" : deviceUDID, "mobile" : "ios", "on_message" : onMessage, "on_like" : onLike, "on_friend_status" : onFriendStatus, "on_comment" : onComment], token: getUserToken()!)
            
            let savePushSettingsTask = URLSession.shared.dataTask(with: savePushSettingsRequest, completionHandler: { (data, response, error) -> Void in
                
                if error != nil {
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        
                        JSSAlertView().danger(self, title: "Ошибка", text: error?.localizedDescription, buttonText: "Закрыть")
                    })
                } else {
                    let res = response as! HTTPURLResponse
                    
                    if res.statusCode == 401 {
                        error401Action(self)
                    } else if res.statusCode == 422 {
                        let jsonResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSMutableArray
                        
                        if let parseJson = jsonResult {
                            var errorText = ""
                            
                            for error in parseJson {
                                errorText += (error as! NSDictionary)["message"] as! String + "\n"
                            }
                            
                            DispatchQueue.main.async(execute: {
                                SwiftSpinner.hide()
                                
                                JSSAlertView().danger(self, title: "Ошибка", text: errorText, buttonText: "Закрыть")
                            })
                        }
                    } else if res.statusCode == 500 {
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            
                            JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                        })
                    } else if res.statusCode == 201 {
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            
                            TSMessage.showNotification(
                                in: UIApplication.shared.delegate?.window??.rootViewController,
                                title: "Успешно",
                                subtitle: "Настройки уведомлений успешно сохранены",
                                type: TSMessageNotificationType.success,
                                duration: 2.0,
                                canBeDismissedByUser: true
                            )
                        })
                    }
                }
            }) 
            
            SwiftSpinner.show("Сохранение...", animated: true, viewToDisplay: self.view)
            
            savePushSettingsTask.resume()
        } else {
            JSSAlertView().danger(self, title: "Ошибка", text: "Настройки уведомлений не могут быть сохранены, так как у приложения отключены уведомления в настройках системы", buttonText: "Закрыть")
        }
    }
}
