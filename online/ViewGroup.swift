//
//  ViewGroup.swift
//  Letters
//
//  Created by KingOP on 26.11.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit
import Haneke

class ViewGroup: UITableViewController {
    
    @IBOutlet var groupAvatar: UIImageView!
    
    var avatarPath : String? = nil
    
    let heightConstant : CGFloat = 200
    
    var members : [Dictionary<String, AnyObject>] = []
    var creatorId : Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.title = "Участники диалога"
        
        if self.avatarPath != nil {
            self.groupAvatar.hnk_setImageFromURL(URL(string: "\(getBackendDomainPath())\(self.avatarPath!)")!, placeholder: UIImage(named: "nophoto.jpg"), format: nil, failure: nil, success: nil)
            
            self.groupAvatar.frame = CGRect(x: 0, y: -(self.tableView.frame.size.width / 2), width: self.tableView.frame.size.width, height: (self.tableView.frame.size.width / 2))
            
            self.tableView.addSubview(self.groupAvatar)
            self.tableView.contentInset = UIEdgeInsetsMake(self.heightConstant, 0, 0, 0)
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.avatarPath != nil {
            let yOffset = scrollView.contentOffset.y
            
            if yOffset < -self.heightConstant {
                var frame : CGRect = self.groupAvatar.frame
                frame.origin.y = yOffset
                frame.size.height = -yOffset
                self.groupAvatar.frame = frame
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.tableView.indexPathForSelectedRow != nil {
            self.tableView.deselectRow(at: self.tableView.indexPathForSelectedRow!, animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.members.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dialogMemberCell", for: indexPath)

        cell.textLabel?.text = self.members[indexPath.row]["username"] as? String
        
        if self.creatorId == self.members[indexPath.row]["id"] as! Int {
            cell.detailTextLabel?.text = "Создатель диалога"
        } else {
            cell.detailTextLabel?.text = ""
        }
        
        let cache = Shared.imageCache
        
        let iconFormat = Format<UIImage>(name: "icons", diskCapacity: 10 * 1024 * 1024) { image in
            return maskedAvatarFromImage(image)
        }
        cache.addFormat(iconFormat)
        
        if !(self.members[indexPath.row]["photo"] is NSNull) && self.members[indexPath.row]["photo"] != nil && self.members[indexPath.row]["photo"] as! String != "" {
            
            cell.imageView?.hnk_setImageFromURL(URL(string: "\(getBackendDomainPath())\((self.members[indexPath.row]["photo"] as? String)!)")!, placeholder: UIImage(named: "nophoto_60.jpg"), format: iconFormat, failure: nil, success: nil)
        } else {
            cell.imageView?.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!)
        }

        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToViewUserFromGroupChat" {
            let destVC : UserProfile = segue.destination as! UserProfile
            destVC.userId = self.members[(self.tableView.indexPathForSelectedRow?.row)!]["id"] as! Int
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
