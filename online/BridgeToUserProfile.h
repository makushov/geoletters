//
//  BridgeToUserProfile.h
//  Letters
//
//  Created by KingOP on 29.11.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

#ifndef BridgeToUserProfile_h
#define BridgeToUserProfile_h


#endif /* BridgeToUserProfile_h */

#import <UIKit/UIKit.h>

@interface BridgeToUserProfile : NSObject 

+(void)openUserProfile:(NSInteger)userId;
+(UIColor *)colorFromHexString:(NSString *)hexString;

@end
