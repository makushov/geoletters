//
//  PinDetailView.swift
//  Letters
//
//  Created by KingOP on 09.12.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit
import SafariServices
import Haneke
import NYTPhotoViewer
import CoreLocation
import TSMessages
import MapKit

@objc class PinDetailView: UITableViewController, UITextViewDelegate, SFSafariViewControllerDelegate, UIViewControllerPreviewingDelegate {

    @IBOutlet weak var userAvatar: UIImageView!
    @IBOutlet weak var pinStatus: UILabel!
    @IBOutlet weak var fullText: UITextView!
    @IBOutlet var mapSnapshot: UIImageView!
    @IBOutlet weak var attachedImageView: UIImageView!
    @IBOutlet weak var isUrgently: UILabel!
    @IBOutlet weak var likeImage: UIImageView!
    
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryTitle: UILabel!
    @IBOutlet weak var attachButton: UIButton!
    
    @IBOutlet weak var userLogin: UILabel!
    @IBOutlet weak var pinsDatetime: UILabel!
    
    @IBOutlet weak var viewForCategory: UIVisualEffectView!
    
    @IBOutlet weak var likeActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var likesCount: UILabel!
    @IBOutlet weak var commentsCount: UILabel!
    
    @IBOutlet weak var userProfileButton: UIButton!
    
    var viewForSpinner : UIView = UIView()
    
    var pin : StatusPin = StatusPin(coordinate: CLLocationCoordinate2D(), title: "", subtitle: "")
    
    var snapshottedImage : UIImage = UIImage()
    
    //let heightConstant : CGFloat = 220
    
    var mapController : AppleMap? = nil
    
    var attachedImage : UIImage? = nil
    
    var hasAttach : Bool = false
    
    var hasLike : Bool = false
    
    var pinId : Int = -1
    
    var goToComments : Bool = false
    
    var previewVC : UserProfile? = nil
    var previewPhotoVC : NYTPhotosViewController? = nil
    var previewMapVC : ViewLocation? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.userAvatar.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!)
        
        self.isUrgently.isHidden = true
        
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        self.fullText.delegate = self
        
        var request : URLRequest
        
        let id : Int = self.pinId == -1 ? self.pin.id! : self.pinId
        
        if isUserAuthenticated() {
            request = getURLRequest("GET", url: "\(getBackendApiPath())temprorary-status/authorized/\(id)", params: Dictionary<String, AnyObject>(), token: getUserToken()!)
        } else {
            request = getURLRequest("GET", url: "\(getBackendApiPath())temprorary-status/\(id)")
        }
        
        let loadPinTask = URLSession.shared.dataTask(with: request, completionHandler: {
            (data, response, error) in
            
            if error != nil {
                DispatchQueue.main.async(execute: {
                    self.hideSpinner()
                    playErrorSound()
                    
                    let errorView = JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                    
                    errorView.addAction({
                        self.navigationController?.popViewController(animated: true)
                    })
                })
            } else {
                let res = response as! HTTPURLResponse
                
                if res.statusCode == 200 {
                    
                    let pinInfoResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSMutableDictionary
                    
                    if self.mapController == nil {
                        let longitude : CLLocationDegrees = (pinInfoResult!["longitude"] as! String).doubleValue
                        let latitude : CLLocationDegrees = (pinInfoResult!["latitude"] as! String).doubleValue
                        
                        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
                        
                        let user = pinInfoResult!["user"] as! NSDictionary
                        
                        self.pin = StatusPin(coordinate: coordinates, title: user["username"] as! String, subtitle: pinInfoResult!["status"] as! String, id: pinInfoResult!["id"] as? Int, catId: pinInfoResult!["category_id"] as! Int, isUrgency: (pinInfoResult!["is_special"] as! Int) == 0 ? false : true, likesCount: pinInfoResult!["likes_count"] as! Int, commentsCount: pinInfoResult!["comments_count"] as! Int)
                        
                        self.pin.userId = user["id"] as? Int
                    }
                    
                    DispatchQueue.main.async(execute: {
                        viewPinController = self
                        
                        if isUserAuthenticated() {
                            if pinInfoResult!["hasYourLike"] as! Int == 0 {
                                self.hasLike = false
                                self.likeImage.image = UIImage(named: "nolike.png")
                            } else {
                                self.hasLike = true
                                self.likeImage.image = UIImage(named: "mylike.png")
                            }
                        }
                        
                        if pinInfoResult!["user"] is NSNull || pinInfoResult!["user"] == nil {
                            self.userLogin.text = pinInfoResult!["name"] as? String
                            self.userAvatar.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!)
                        } else {
                            self.userLogin.text = (pinInfoResult!["user"] as! NSDictionary)["username"] as? String
                            
                            if !((pinInfoResult!["user"] as! NSDictionary)["photo"] is NSNull) && (pinInfoResult!["user"] as! NSDictionary)["photo"] != nil && (pinInfoResult!["user"] as! NSDictionary)["photo"] as! String != "" {
                                
                                
                                let cache = Shared.imageCache
                                let iconFormat = Format<UIImage>(name: "icons", diskCapacity: 10 * 1024 * 1024) { image in
                                    return maskedAvatarFromImage(image)
                                }
                                cache.addFormat(iconFormat)
                                
                                self.userAvatar.hnk_setImageFromURL(URL(string: "\(getBackendDomainPath())\((pinInfoResult!["user"] as! NSDictionary)["photo"] as! String)")!, placeholder: UIImage(named: "nophoto_60.jpg"), format: iconFormat, failure: nil, success: nil)
                            } else {
                                self.userAvatar.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!)
                            }
                        }
                        
                        self.pinsDatetime.text = fullStringDateFromTimestamp(pinInfoResult!["created_at"] as! Double)
                        
                        if !(pinInfoResult!["text"] is NSNull) && pinInfoResult!["text"] != nil && pinInfoResult!["text"] as! String != "" {
                            
                            self.fullText.text = "\(pinInfoResult!["text"] as! String)\n"
                        } else {
                            self.fullText.text = ""
                        }
                        
                        self.fullText.sizeToFit()
                        self.pinStatus.text = pinInfoResult!["status"] as? String
                        
                        self.likesCount.text = (pinInfoResult!["likes_count"] as! Int).description
                        self.commentsCount.text = (pinInfoResult!["comments_count"] as! Int).description
                        
                        if self.pin.isUrgency {
                            self.isUrgently.isHidden = false
                        }
                        
                        if self.pin.catId != 0 && self.pin.catId < pinsCategories.count {
                            self.viewForCategory.isHidden = false
                            self.categoryTitle.text = pinsCategories[self.pin.catId].title
                            self.categoryImage.image = UIImage(named: pinsCategories[self.pin.catId].image)
                        }
                        
                        self.tableView.reloadData()
                        
                        // сделаем снэпшот
                        let options : MKMapSnapshotOptions = MKMapSnapshotOptions()
                        options.region = MKCoordinateRegion(center: self.pin.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
                        options.size = CGSize(width: self.mapSnapshot.frame.width, height: self.mapSnapshot.frame.height)
                        options.scale = UIScreen.main.scale
                        
                        let snapshotter : MKMapSnapshotter = MKMapSnapshotter(options: options)
                        snapshotter.start(with: DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default)) { (snapshot, error) -> Void in
                            
                            if error == nil {
                                let new_pin : MKAnnotationView = MKAnnotationView(annotation: nil, reuseIdentifier: nil)
                                var coordinatePoint : CGPoint = (snapshot?.point(for: self.pin.coordinate))!
                                
                                let image : UIImage = (snapshot?.image)!
                                
                                coordinatePoint.x += new_pin.centerOffset.x - (new_pin.bounds.width / 2)
                                coordinatePoint.y += new_pin.centerOffset.y - (new_pin.bounds.height / 2)
                                
                                UIGraphicsBeginImageContextWithOptions(image.size, true, image.scale)
                                
                                image.draw(at: CGPoint.zero)
                                
                                //let index : Int = pinInfoResult!["category_id"] as! Int >= pinsCategories.count ? 0 : pinInfoResult!["category_id"] as! Int
                                
                                new_pin.image = UIImage(named: /*pinsCategories[index].image*/"pinIcon")
                                new_pin.image?.draw(at: coordinatePoint)
                                
                                self.snapshottedImage = UIGraphicsGetImageFromCurrentImageContext()!
                                
                                UIGraphicsEndImageContext()
                                
                                DispatchQueue.main.async(execute: {
                                    self.mapSnapshot.image = self.snapshottedImage
                                    self.hideSpinner()
                                })
                            } else {
                                DispatchQueue.main.async(execute: {
                                    self.hideSpinner()
                                })
                            }
                        }
                        
                        if !(pinInfoResult!["photo"] is NSNull) && pinInfoResult!["photo"] != nil && pinInfoResult!["photo"] as! String != "" {
                            
                            self.hasAttach = true
                            self.tableView.reloadData()
                            
                            // загрузим изображение
                            DispatchQueue(label: "loadAttach", attributes: []).async(execute: {
                                let imageData : Data? = try? Data(contentsOf: URL(string: "\(getBackendDomainPath())\(pinInfoResult!["photo"] as! String)")!)
                                
                                if imageData != nil {
                                    self.attachedImage = UIImage(data: imageData!)
                                    self.attachButton.isEnabled = true
                                    
                                    if self.traitCollection.forceTouchCapability == .available {
                                        self.registerForPreviewing(with: self, sourceView: self.attachButton)
                                    }
                                    
                                    DispatchQueue.main.async(execute: {
                                        self.attachedImageView.image = self.attachedImage
                                    })
                                }
                            })
                        }
                        
                        if self.goToComments {
                            self.goToComments = false
                            self.performSegue(withIdentifier: "goToPinsComments", sender: self)
                        }
                    })
                    
                } else if res.statusCode == 404 {
                    DispatchQueue.main.async(execute: {
                        self.hideSpinner()
                        playErrorSound()
                        
                        let errorView = JSSAlertView().danger(self, title: "Ошибка", text: "Выбранной метки не существует.", buttonText: "Закрыть")
                        
                        errorView.addAction({
                            self.navigationController?.popViewController(animated: true)
                        })
                    })
                } else if res.statusCode == 500 {
                    DispatchQueue.main.async(execute: {
                        self.hideSpinner()
                        playErrorSound()
                        
                        let errorView = JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                        
                        errorView.addAction({
                            self.navigationController?.popViewController(animated: true)
                        })
                    })
                }
            }
        }) 
        
        self.showSpinner("Загрузка...")
        
        loadPinTask.resume()
        
        pushUserInfo = nil
        
        if traitCollection.forceTouchCapability == .available && self.pin.userId != nil {
            registerForPreviewing(with: self, sourceView: self.userProfileButton)
            registerForPreviewing(with: self, sourceView: self.tableView.cellForRow(at: IndexPath(row: 0, section: 0))!)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.setAnimationsEnabled(true)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        
        if URL.scheme == "http" || URL.scheme == "https" {
            
            if URL.host! == backendDomain && Int(URL.lastPathComponent)! != 0 {
                
                BridgeToUserProfile.open(Int(URL.lastPathComponent)!)
                return false
            }
            
            let vc = SFSafariViewController(url: URL, entersReaderIfAvailable: true)
            vc.delegate = self
            present(vc, animated: true, completion: nil)
            
            return false
        }
        
        return true
    }
    
    /*override func scrollViewDidScroll(scrollView: UIScrollView) {
        let yOffset = scrollView.contentOffset.y
        
        if yOffset < -self.heightConstant {
            var frame : CGRect = self.mapSnapshot.frame
            frame.origin.y = yOffset
            frame.size.height = -yOffset
            self.mapSnapshot.frame = frame
        }
    }*/
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath as NSIndexPath).section == 0 && (indexPath as NSIndexPath).row == 3 {
            
            if self.fullText.text == "" || self.fullText.text == "\n" {
                return CGFloat.leastNormalMagnitude
            }
            
            var frame : CGRect = self.fullText.frame
            frame.size.height = self.fullText.contentSize.height
            self.fullText.frame = frame
            
            return max(44, self.fullText.frame.size.height)
        } else if (indexPath as NSIndexPath).section == 0 && (indexPath as NSIndexPath).row == 1 {
            return 57
        } else if (indexPath as NSIndexPath).section == 0 && (indexPath as NSIndexPath).row == 4 {
            return self.hasAttach == false ? CGFloat.leastNormalMagnitude : 55
        } else if (indexPath as NSIndexPath).section == 0 && (indexPath as NSIndexPath).row == 2 {
            return 60
        } else if (indexPath as NSIndexPath).section == 0 && (indexPath as NSIndexPath).row == 0 {
            return 173
        } else {
            return 44
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    @IBAction func userAvatarClick(_ sender: AnyObject) {
        if self.pin.userId != nil {
            self.performSegue(withIdentifier: "goToUserViewFromDetailPinView", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToUserViewFromDetailPinView" {
            let destVC : UserProfile = segue.destination as! UserProfile
            destVC.userId = self.pin.userId!
        } else if segue.identifier == "goToPinsComments" {
            let destVC : PinsComments = segue.destination as! PinsComments
            destVC.pin = self.pin
            destVC.pinViewController = self
        } else if segue.identifier == "goToLikedUsersList" {
            let destVC : LikedList = segue.destination as! LikedList
            destVC.pin = self.pin
            destVC.pinViewController = self
        } else if segue.identifier == "goToPinSendToFav" {
            let destVC : UINavigationController = segue.destination as! UINavigationController
            (destVC.viewControllers[0] as! SendPinToFav).pinId = self.pinId == -1 ? self.pin.id! : self.pinId
        } else if segue.identifier == "goToDetailLocationFromPinView" {
            let destVC : ViewLocation = segue.destination as! ViewLocation
            destVC.location = CLLocation(latitude: self.pin.coordinate.latitude, longitude: self.pin.coordinate.longitude)
            destVC.userName = self.pin.title!
            destVC.subtitle = self.pin.subtitle!
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    
    func showSpinner(_ title: String) {
        UIView.setAnimationsEnabled(true)
        self.viewForSpinner = UIView(frame: CGRect(x: 0, y: self.view.frame.minY, width: self.view.frame.size.width, height: self.view.frame.size.height))
        self.view.addSubview(viewForSpinner)
        SwiftSpinner.show(title, animated: true, viewToDisplay: self.viewForSpinner)
    }
    
    func hideSpinner() {
        SwiftSpinner.hide()
        self.viewForSpinner.removeFromSuperview()
        self.viewForSpinner = UIView()
    }
    
    @IBAction func actionsAction(_ sender: AnyObject) {
        let actionsController : UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionsController.addAction(UIAlertAction(title: "Открыть в приложении \"Карты\"", style: .default, handler: { (action) -> Void in
            
            self.openInMaps()
        }))
        
        actionsController.addAction(UIAlertAction(title: "Скопировать ссылку", style: .default, handler: { (action) -> Void in
            
            UIPasteboard.general.string = "\(getBackendDomainPath())/pin/\(self.pinId == -1 ? self.pin.id! : self.pinId)"
            TSMessage.showNotification(
                in: self,//UIApplication.sharedApplication().delegate?.window??.rootViewController,
                title: "Успешно",
                subtitle: "Ссылка на метку скопирована в буфер обмена",
                type: TSMessageNotificationType.success,
                duration: 2.0,
                canBeDismissedByUser: true
            )
        }))
        
        if isUserAuthenticated() {
            
            actionsController.addAction(UIAlertAction(title: "Отправить в сообщении", style: .default, handler: { (action) -> Void in
                
                self.sharePin()
            }))
        }
        
        actionsController.addAction(UIAlertAction(title: "Поделиться в...", style: .default, handler: { (action) in
            
            self.sharePinToApps()
        }))
        
        
        if self.pin.userId != getUserId() {
            actionsController.addAction(UIAlertAction(title: "Пожаловаться", style: .destructive, handler: { (action) -> Void in
                
                self.reportPin()
            }))
        }
        
        actionsController.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        
        self.present(actionsController, animated: true, completion: nil)
    }
    
    func openInMaps() {
        let confirmAlertView : UIAlertController = UIAlertController(title: "Подтверждение", message: "Открыть это местоположение в приложении \"Карты\"?", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let openAction : UIAlertAction = UIAlertAction(title: "Открыть", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
            
            let startPlacemark = MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: self.pin.coordinate.latitude, longitude: self.pin.coordinate.longitude), addressDictionary: nil)
            
            let start = MKMapItem(placemark: startPlacemark)
            start.name = self.pin.title! + ": " + self.pin.subtitle!
            
            MKMapItem.openMaps(with: [start], launchOptions: nil)
        }
        
        confirmAlertView.addAction(openAction)
        
        let cancelAction : UIAlertAction = UIAlertAction(title: "Отмена", style: UIAlertActionStyle.cancel, handler: nil)
        
        confirmAlertView.addAction(cancelAction)
        
        self.present(confirmAlertView, animated: true, completion: nil)
    }
    
    func sharePin() {
        self.performSegue(withIdentifier: "goToPinSendToFav", sender: self)
    }
    
    func sharePinToApps() {
        let shareText = "\(self.pinStatus.text!) - http://geoletters.com/pin/\(self.pinId == -1 ? self.pin.id! : self.pinId)"
        
        if let image = UIImage(named: "logo.png") {
            let vc = UIActivityViewController(activityItems: [shareText, image], applicationActivities: [])
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func reportPin(_ controller: UIViewController? = nil) {
        if isUserAuthenticated() {
            let alertController = UIAlertController(title: "Пожаловаться", message: "Укажите, пожалуйста, причину жалобы (до 200 символов)", preferredStyle: UIAlertControllerStyle.alert)
            
            let reportAction = UIAlertAction(title: "Отправить жалобу", style: UIAlertActionStyle.destructive, handler: {
                alert -> Void in
                
                self.showSpinner("Отправка жалобы...")
                
                let firstTextField = alertController.textFields![0] as UITextField
                
                let reportText : String = firstTextField.text!.trimmingCharacters(in: CharacterSet.whitespaces)
                
                // вот тут отправка жалобы
                let sendReportRequest = getURLRequest("POST", url: "\(getBackendApiPath())report", params: ["description" : reportText, "target" : "status", "target_id" : self.pin.id!], token: getUserToken()!)
                
                let sendReportTask = URLSession.shared.dataTask(with: sendReportRequest, completionHandler: {
                    (data, response, error) in
                    
                    if error != nil {
                        DispatchQueue.main.async(execute: {
                            self.hideSpinner()
                            
                            let errorView = JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                            
                            errorView.addAction({
                                self.navigationController?.popViewController(animated: true)
                            })
                        })
                    } else {
                        let res = response as! HTTPURLResponse
                        
                        if res.statusCode == 201 {
                            DispatchQueue.main.async(execute: {
                                
                                if self.mapController != nil {
                                    
                                    self.mapController!.clusteringManager.setAnnotations([])
                                    self.mapController!.mapView.removeAnnotations(self.mapController!.mapView.annotations)
                                    self.mapController!.loadPins(true)
                                    
                                    self.hideSpinner()
                                    
                                    let result = JSSAlertView().info(self, title: "Спасибо!", text: "Ваша жалоба принята к рассмотрению. Данная метка больше Вам показываться не будет.", buttonText: "Продолжить")
                                    
                                    result.addAction({
                                        self.navigationController?.popViewController(animated: true)
                                    })
                                    
                                } else {
                                    DispatchQueue.main.async(execute: {
                                        self.hideSpinner()
                                        
                                        let result = JSSAlertView().info(self, title: "Спасибо!", text: "Ваша жалоба принята к рассмотрению.", buttonText: "Продолжить")
                                        
                                        result.addAction({
                                            self.navigationController?.popViewController(animated: true)
                                        })
                                    })
                                }
                            })
                        } else if res.statusCode == 403 {
                            let json = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                            
                            DispatchQueue.main.async(execute: {
                                self.hideSpinner()
                                TSMessage.showNotification(
                                    in: UIApplication.shared.delegate?.window??.rootViewController,
                                    title: "Ошибка",
                                    subtitle: json!["message"] as! String,
                                    type: TSMessageNotificationType.error,
                                    duration: 2.0,
                                    canBeDismissedByUser: true
                                )
                                playErrorSound()
                            })
                        } else if res.statusCode == 422 {
                            let jsonResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSMutableArray
                            
                            if let parseJson = jsonResult {
                                var errorText = ""
                                
                                for error in parseJson {
                                    errorText += (error as! NSDictionary)["message"] as! String + "\n"
                                }
                                
                                DispatchQueue.main.async(execute: {
                                    self.hideSpinner()
                                    TSMessage.showNotification(
                                        in: UIApplication.shared.delegate?.window??.rootViewController,
                                        title: "Ошибка",
                                        subtitle: errorText,
                                        type: TSMessageNotificationType.error,
                                        duration: 2.0,
                                        canBeDismissedByUser: true
                                    )
                                    playErrorSound()
                                })
                            }
                        } else if res.statusCode == 401 {
                            error401Action(self)
                        } else if res.statusCode == 500 {
                            DispatchQueue.main.async(execute: {
                                self.hideSpinner()
                                
                                JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                            })
                        }
                    }
                }) 
                
                sendReportTask.resume()
            })
            
            let cancelAction = UIAlertAction(title: "Отмена", style: UIAlertActionStyle.cancel, handler: nil)
            
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "Причина жалобы..."
            }
            
            alertController.addAction(reportAction)
            alertController.addAction(cancelAction)
            
            if controller == nil {
                self.present(alertController, animated: true, completion: nil)
            } else {
                controller!.present(alertController, animated: true, completion: nil)
            }
        } else {
            let info = JSSAlertView().info(self, title: "Отправить жалобу", text: "Для того, чтобы отправить жалобу, пожалуйста, выполните вход под вашей учетной записью.", buttonText: "Войти", cancelButtonText: "Отмена")
            
            info.addAction({ () -> Void in
                self.performSegue(withIdentifier: "goToLoginFromPinDetailViewView", sender: self)
            })
        }
    }
    
    @IBAction func likeAction(_ sender: AnyObject) {
        
        if isUserAuthenticated() {
            self.likeActivityIndicator.startAnimating()
            
            if self.hasLike {
                // дислайк
                let postLikeRequest = getURLRequest("DELETE", url: getBackendApiPath() + "temprorary-status/\(self.pin.id!)/like", params: Dictionary<String, AnyObject>(), token: getUserToken()!)
                
                let postLikeTask = URLSession.shared.dataTask(with: postLikeRequest, completionHandler: { (data, response, error) -> Void in
                    
                    if error != nil {
                        DispatchQueue.main.async(execute: {
                            self.likeActivityIndicator.stopAnimating()
                            
                            TSMessage.showNotification(
                                in: UIApplication.shared.delegate?.window??.rootViewController,
                                title: "Ошибка",
                                subtitle: error?.localizedDescription,
                                type: TSMessageNotificationType.error,
                                duration: 2.0,
                                canBeDismissedByUser: true
                            )
                            playErrorSound()
                        })
                    } else {
                        let res = response as! HTTPURLResponse
                        
                        if res.statusCode == 204 {
                            // все гуд
                            DispatchQueue.main.async(execute: {
                                self.likeActivityIndicator.stopAnimating()
                                self.likesCount.text = (Int(self.likesCount.text!)! - 1).description
                                // заменим значок и отобразим ненажатый лайк
                                self.likeImage.image = UIImage(named: "nolike.png")
                                self.hasLike = false
                            })
                        } else if res.statusCode == 403 {
                            DispatchQueue.main.async(execute: {
                                self.likeActivityIndicator.stopAnimating()
                                
                                TSMessage.showNotification(
                                    in: UIApplication.shared.delegate?.window??.rootViewController,
                                    title: "Ошибка",
                                    subtitle: "Вы уже отмечали эту метку как понравившуюся.",
                                    type: TSMessageNotificationType.error,
                                    duration: 2.0,
                                    canBeDismissedByUser: true
                                )
                                playErrorSound()
                            })
                        } else if res.statusCode == 401 {
                            error401Action(self)
                        } else if res.statusCode == 500 {
                            DispatchQueue.main.async(execute: {
                                self.likeActivityIndicator.stopAnimating()
                                
                                TSMessage.showNotification(
                                    in: UIApplication.shared.delegate?.window??.rootViewController,
                                    title: "Ошибка",
                                    subtitle: error500,
                                    type: TSMessageNotificationType.error,
                                    duration: 2.0,
                                    canBeDismissedByUser: true
                                )
                                playErrorSound()
                            })
                        }
                    }
                }) 
                
                postLikeTask.resume()
            } else {
                let postLikeRequest = getURLRequest("POST", url: getBackendApiPath() + "temprorary-status/\(self.pin.id!)/like", params: Dictionary<String, AnyObject>(), token: getUserToken()!)
                
                let postLikeTask = URLSession.shared.dataTask(with: postLikeRequest, completionHandler: { (data, response, error) -> Void in
                    
                    if error != nil {
                        DispatchQueue.main.async(execute: {
                            self.likeActivityIndicator.stopAnimating()
                            
                            TSMessage.showNotification(
                                in: UIApplication.shared.delegate?.window??.rootViewController,
                                title: "Ошибка",
                                subtitle: error?.localizedDescription,
                                type: TSMessageNotificationType.error,
                                duration: 2.0,
                                canBeDismissedByUser: true
                            )
                            playErrorSound()
                        })
                    } else {
                        let res = response as! HTTPURLResponse
                        
                        if res.statusCode == 201 {
                            // все гуд
                            DispatchQueue.main.async(execute: {
                                self.likeActivityIndicator.stopAnimating()
                                self.likesCount.text = (Int(self.likesCount.text!)! + 1).description
                                // заменим значок и отобразим нажатый лайк
                                self.likeImage.image = UIImage(named: "mylike.png")
                                self.hasLike = true
                            })
                        } else if res.statusCode == 403 {
                            DispatchQueue.main.async(execute: {
                                self.likeActivityIndicator.stopAnimating()
                                
                                TSMessage.showNotification(
                                    in: UIApplication.shared.delegate?.window??.rootViewController,
                                    title: "Ошибка",
                                    subtitle: "Вы уже отмечали эту метку как понравившуюся.",
                                    type: TSMessageNotificationType.error,
                                    duration: 2.0,
                                    canBeDismissedByUser: true
                                )
                                playErrorSound()
                            })
                        } else if res.statusCode == 401 {
                            error401Action(self)
                        } else if res.statusCode == 500 {
                            DispatchQueue.main.async(execute: {
                                self.likeActivityIndicator.stopAnimating()
                                
                                TSMessage.showNotification(
                                    in: UIApplication.shared.delegate?.window??.rootViewController,
                                    title: "Ошибка",
                                    subtitle: error500,
                                    type: TSMessageNotificationType.error,
                                    duration: 2.0,
                                    canBeDismissedByUser: true
                                )
                                playErrorSound()
                            })
                        }
                    }
                }) 
                
                postLikeTask.resume()
            }
        } else {
            let info = JSSAlertView().info(self, title: "Понравилось", text: "Возможность ставить лайк метке доступна только зарегистрированным пользователям", buttonText: "Войти", cancelButtonText: "Отмена")
            
            info.addAction({ () -> Void in
                self.performSegue(withIdentifier: "goToLoginFromPinDetailViewView", sender: self)
            })
        }
    }
    
    @IBAction func viewAttachment(_ sender: AnyObject) {
        let photo : attachedPhoto = attachedPhoto(image: self.attachedImage!, attributedCaptionTitle: NSAttributedString(string: ""))
        
        let photosViewController : NYTPhotosViewController = NYTPhotosViewController(photos: [photo])
        
        self.present(photosViewController, animated: true, completion: nil)
    }
    
    @IBAction func goToLikesList(_ sender: AnyObject) {
        if isUserAuthenticated() {
            self.performSegue(withIdentifier: "goToLikedUsersList", sender: self)
        } else {
            let info = JSSAlertView().info(self, title: "Понравилось", text: "Просмотр отметок \"Понравилось\" доступен только зарегистрированным пользователям", buttonText: "Войти", cancelButtonText: "Отмена")
            
            info.addAction({ () -> Void in
                self.performSegue(withIdentifier: "goToLoginFromPinDetailViewView", sender: self)
            })
        }
    }

    @IBAction func goToCommentsList(_ sender: AnyObject) {
        
        if isUserAuthenticated() {
            self.performSegue(withIdentifier: "goToPinsComments", sender: self)
        } else {
            let info = JSSAlertView().info(self, title: "Комментарии", text: "Комментарии доступны только зарегистрированным пользователям", buttonText: "Войти", cancelButtonText: "Отмена")
            
            info.addAction({ () -> Void in
                self.performSegue(withIdentifier: "goToLoginFromPinDetailViewView", sender: self)
            })
        }
    }
    
    override var previewActionItems : [UIPreviewActionItem] {
        
        let likeAction = UIPreviewAction(title: self.hasLike == true ? "Не нравится" : "Нравится", style: .default) { (action: UIPreviewAction, vc: UIViewController) -> Void in
            
            if !isUserAuthenticated() {
                JSSAlertView().info(self, title: "Понравилось", text: "Возможность ставить лайк метке доступна только зарегистрированным пользователям", buttonText: "Закрыть")
            } else {
                // поставим лайк
                if self.hasLike {
                    // дислайк
                    let postLikeRequest = getURLRequest("DELETE", url: getBackendApiPath() + "temprorary-status/\(self.pin.id!)/like", params: Dictionary<String, AnyObject>(), token: getUserToken()!)
                    
                    let postLikeTask = URLSession.shared.dataTask(with: postLikeRequest, completionHandler: { (data, response, error) -> Void in
                        
                        if error != nil {
                            DispatchQueue.main.async(execute: {
                                self.likeActivityIndicator.stopAnimating()
                                
                                TSMessage.showNotification(
                                    in: UIApplication.shared.delegate?.window??.rootViewController,
                                    title: "Ошибка",
                                    subtitle: error?.localizedDescription,
                                    type: TSMessageNotificationType.error,
                                    duration: 2.0,
                                    canBeDismissedByUser: true
                                )
                                playErrorSound()
                            })
                        } else {
                            let res = response as! HTTPURLResponse
                            
                            if res.statusCode == 204 {
                                // все гуд
                                DispatchQueue.main.async(execute: {
                                    // ничего
                                })
                            } else if res.statusCode == 403 {
                                DispatchQueue.main.async(execute: {
                                    self.likeActivityIndicator.stopAnimating()
                                    
                                    TSMessage.showNotification(
                                        in: UIApplication.shared.delegate?.window??.rootViewController,
                                        title: "Ошибка",
                                        subtitle: "Вы уже отмечали эту метку как понравившуюся.",
                                        type: TSMessageNotificationType.error,
                                        duration: 2.0,
                                        canBeDismissedByUser: true
                                    )
                                    playErrorSound()
                                })
                            } else if res.statusCode == 401 {
                                error401Action(self)
                            } else if res.statusCode == 500 {
                                DispatchQueue.main.async(execute: {
                                    self.likeActivityIndicator.stopAnimating()
                                    
                                    TSMessage.showNotification(
                                        in: UIApplication.shared.delegate?.window??.rootViewController,
                                        title: "Ошибка",
                                        subtitle: error500,
                                        type: TSMessageNotificationType.error,
                                        duration: 2.0,
                                        canBeDismissedByUser: true
                                    )
                                    playErrorSound()
                                })
                            }
                        }
                    }) 
                    
                    postLikeTask.resume()
                } else {
                    let postLikeRequest = getURLRequest("POST", url: getBackendApiPath() + "temprorary-status/\(self.pin.id!)/like", params: Dictionary<String, AnyObject>(), token: getUserToken()!)
                    
                    let postLikeTask = URLSession.shared.dataTask(with: postLikeRequest, completionHandler: { (data, response, error) -> Void in
                        
                        if error != nil {
                            DispatchQueue.main.async(execute: {
                                self.likeActivityIndicator.stopAnimating()
                                
                                TSMessage.showNotification(
                                    in: UIApplication.shared.delegate?.window??.rootViewController,
                                    title: "Ошибка",
                                    subtitle: error?.localizedDescription,
                                    type: TSMessageNotificationType.error,
                                    duration: 2.0,
                                    canBeDismissedByUser: true
                                )
                                playErrorSound()
                            })
                        } else {
                            let res = response as! HTTPURLResponse
                            
                            if res.statusCode == 201 {
                                // все гуд
                                DispatchQueue.main.async(execute: {
                                    // ничего
                                })
                            } else if res.statusCode == 403 {
                                DispatchQueue.main.async(execute: {
                                    self.likeActivityIndicator.stopAnimating()
                                    
                                    TSMessage.showNotification(
                                        in: UIApplication.shared.delegate?.window??.rootViewController,
                                        title: "Ошибка",
                                        subtitle: "Вы уже отмечали эту метку как понравившуюся.",
                                        type: TSMessageNotificationType.error,
                                        duration: 2.0,
                                        canBeDismissedByUser: true
                                    )
                                    playErrorSound()
                                })
                            } else if res.statusCode == 401 {
                                error401Action(self)
                            } else if res.statusCode == 500 {
                                DispatchQueue.main.async(execute: {
                                    self.likeActivityIndicator.stopAnimating()
                                    
                                    TSMessage.showNotification(
                                        in: UIApplication.shared.delegate?.window??.rootViewController,
                                        title: "Ошибка",
                                        subtitle: error500,
                                        type: TSMessageNotificationType.error,
                                        duration: 2.0,
                                        canBeDismissedByUser: true
                                    )
                                    playErrorSound()
                                })
                            }
                        }
                    }) 
                    
                    postLikeTask.resume()
                }
            }
        }
        
        let shareToAppsAction = UIPreviewAction(title: "Поделиться в...", style: .default) { (action, vc) in
            
            let shareText = "\(self.pinStatus.text!) - http://geoletters.com/pin/\(self.pinId == -1 ? self.pin.id! : self.pinId)"
            
            if let image = UIImage(named: "logo.png") {
                let activityVC = UIActivityViewController(activityItems: [shareText, image], applicationActivities: [])
                UIApplication.shared.delegate?.window??.rootViewController?.present(activityVC, animated: true, completion: nil)
            }
        }
        
        if self.pin.userId != getUserId() {
            
            let reportAction = UIPreviewAction(title: "Пожаловаться", style: .destructive) { (action: UIPreviewAction, vc: UIViewController) -> Void in
                
                if !isUserAuthenticated() {
                    JSSAlertView().info(self, title: "Отправить жалобу", text: "Для того, чтобы отправить жалобу, пожалуйста, выполните вход под вашей учетной записью.", buttonText: "Закрыть")
                } else {
                    // пожалуемся на метку
                    self.reportPin(UIApplication.shared.delegate?.window??.rootViewController)
                }
            }
            
            return [likeAction, shareToAppsAction, reportAction]
        } else {
            return [likeAction, shareToAppsAction]
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath as NSIndexPath).row == 0 && (indexPath as NSIndexPath).section == 0 {
            self.performSegue(withIdentifier: "goToDetailLocationFromPinView", sender: self)
        }
    }
    
    
    // -------------------------------------------------------------
    // ------------------ 3D Touch ---------------------------------
    // -------------------------------------------------------------
    
    // PEEK
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        
        if previewingContext.sourceView.isEqual(self.attachButton) {
            self.previewPhotoVC = nil
            
            let photo : attachedPhoto = attachedPhoto(image: self.attachedImage!, attributedCaptionTitle: NSAttributedString(string: ""))
            
            self.previewPhotoVC = NYTPhotosViewController(photos: [photo])
            return self.previewPhotoVC
        } else if previewingContext.sourceView.isKind(of: UITableViewCell.self) {
            self.previewMapVC = self.storyboard?.instantiateViewController(withIdentifier: "viewLocationVC") as? ViewLocation
            
            self.previewMapVC!.location = CLLocation(latitude: self.pin.coordinate.latitude, longitude: self.pin.coordinate.longitude)
            self.previewMapVC!.userName = self.pin.title!
            self.previewMapVC!.subtitle = self.pin.subtitle!
            
            return self.previewMapVC!
        } else {
            self.previewVC = self.storyboard?.instantiateViewController(withIdentifier: "userProfile") as? UserProfile
            
            previewVC?.userId = self.pin.userId!
            
            return previewVC
        }
    }
    
    // POP
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        
        if previewingContext.sourceView.isEqual(self.attachButton) {
            let previewImageVC = self.previewPhotoVC
            self.present(previewImageVC!, animated: false, completion: nil)
            self.previewPhotoVC = nil
        } else if previewingContext.sourceView.isKind(of: UITableViewCell.self) {
            let previewMap = self.previewMapVC!
            show(previewMap, sender: self)
            self.previewMapVC = nil
        } else {
            let userProfileVC = self.previewVC!
            show(userProfileVC, sender: self)
            self.previewVC = nil
        }
    }
    
    // -------------------------------------------------------------
    // ------------------ /3D Touch --------------------------------
    // -------------------------------------------------------------
}
