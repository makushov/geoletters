//
//  ViewLocation.swift
//  online
//
//  Created by KingOP on 09.11.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit
import MapKit

class ViewLocation: UIViewController, MKMapViewDelegate {

    var location : CLLocation = CLLocation()
    var userName : String = ""
    var subtitle : String = ""
    
    @IBOutlet weak var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mapView.delegate = self
        self.mapView.showsCompass = true
        self.mapView.showsTraffic = true
        self.mapView.showsScale = true
        
        let pinMarker = StatusPin(coordinate: self.location.coordinate, title: self.userName, subtitle: subtitle)
        self.mapView.addAnnotation(pinMarker)
        
        self.mapView.selectAnnotation(pinMarker, animated: true)
        
        let region = MKCoordinateRegion(center: self.location.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        self.mapView.setRegion(region, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let pin = mapView.dequeueReusableAnnotationView(withIdentifier: "pin") ?? MKAnnotationView(annotation: annotation, reuseIdentifier: "pin")
        
        pin.image = UIImage(named: "pinIcon")
        
        pin.canShowCallout = true
        
        return pin
    }
    
    @IBAction func openInMaps(_ sender: AnyObject) {
        let confirmAlertView : UIAlertController = UIAlertController(title: "Подтверждение", message: "Открыть выбранное местоположение в приложении \"Карты\"?", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let openAction : UIAlertAction = UIAlertAction(title: "Открыть", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
            
            let startPlacemark = MKPlacemark(coordinate: self.location.coordinate, addressDictionary: nil)
            let start = MKMapItem(placemark: startPlacemark)
            
            start.name = self.userName
            
            if self.subtitle != "" {
                start.name! += ": \(self.subtitle)"
            }
            
            MKMapItem.openMaps(with: [start], launchOptions: nil)
        }
        
        confirmAlertView.addAction(openAction)
        
        let cancelAction : UIAlertAction = UIAlertAction(title: "Отмена", style: UIAlertActionStyle.cancel, handler: nil)
        
        confirmAlertView.addAction(cancelAction)
        
        self.present(confirmAlertView, animated: true, completion: nil)
    }
    
    override var previewActionItems : [UIPreviewActionItem] {
        let openInMapsAction = UIPreviewAction(title: "Открыть в приложении \"Карты\"", style: .default) { (action: UIPreviewAction, vc: UIViewController) -> Void in
            
            let startPlacemark = MKPlacemark(coordinate: self.location.coordinate, addressDictionary: nil)
            let start = MKMapItem(placemark: startPlacemark)
            
            start.name = self.userName
            
            if self.subtitle != "" {
                start.name! += ": \(self.subtitle)"
            }
            
            MKMapItem.openMaps(with: [start], launchOptions: nil)
        }
        
        return [openInMapsAction]
    }
}
