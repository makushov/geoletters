//
//  PinsCategories.swift
//  Letters 1.1
//
//  Created by KingOP on 13.01.16.
//  Copyright © 2016 KingOP. All rights reserved.
//

import Foundation

let pinsCategories = [
    
    (title: "Без категории",        image: "pinIcon"),              // 0
    (title: "Важно!",               image: "importantPin"),         // 1
    (title: "Добрые дела",          image: "goodThings"),           // 2
    (title: "Животные, природа",    image: "nature"),               // 3
    (title: "Знакомства",           image: "znakomstva"),           // 4
    (title: "Информация",           image: "information"),          // 5
    (title: "Встречи",              image: "meetings"),             // 6
    (title: "Путешествия",          image: "travelling"),           // 7
    (title: "Помощь",               image: "helping"),              // 8
    (title: "Стол находок",         image: "finding"),              // 9
    (title: "Ищу людей",            image: "peopleSearch")          // 10
    
]