//
//  UpdateStatus.swift
//  online
//
//  Created by KingOP on 28.10.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit
import SZTextView

class UpdateStatus: UITableViewController, UITextViewDelegate {

    @IBOutlet weak var statusText: SZTextView!
    @IBOutlet weak var limitLabel: UILabel!
    
    var viewAccountController : AccountView = AccountView()
    var currentStatus : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.statusText.delegate = self
        self.statusText.becomeFirstResponder()
        self.statusText.text = self.currentStatus
        self.limitLabel.text = "Осталось знаков: " + (500 - self.statusText.text.length).description
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        self.limitLabel.text = "Осталось знаков: " + (500 - ((textView.text.length - range.length) + text.length)).description
        
        return textView.text.length + (text.length - range.length) < 500
    }
    
    @IBAction func saveStatus(_ sender: AnyObject) {
        self.view.endEditing(true)
        
        SwiftSpinner.show("Сохранение", animated: true, viewToDisplay: self.view)
        
        // вот тут будет сохранение статуса
        let params = [
            "status" : self.statusText.text
        ]
        
        let saveStatusRequest = getURLRequest("POST", url: getBackendApiPath() + "profile-status", params: params, token: getUserToken())
        
        let saveStatusTask = URLSession.shared.dataTask(with: saveStatusRequest, completionHandler: { (data, response, error) in
            
            if (error != nil) {
                // сюда надо добавить обработку ошибок
                DispatchQueue.main.async(execute: {
                    SwiftSpinner.hide()
                    JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                })
            } else {
                let res = response as! HTTPURLResponse
                
                print(res.statusCode)
                
                if res.statusCode == 201 {
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        
                        self.viewAccountController.status.text = self.statusText.text
                        self.navigationController?.popViewController(animated: true)
                    })
                } else if res.statusCode == 422 {
                    let jsonResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [Dictionary<String, AnyObject>]
                    
                    if let parseJson = jsonResult {
                        var errorText = ""
                        
                        for error in parseJson {
                            errorText += error["message"] as! String + "\n"
                        }
                        
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            
                            JSSAlertView().danger(self, title: "Ошибка", text: errorText, buttonText: "Закрыть")
                        })
                    }
                } else if res.statusCode == 500 {
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        
                        JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                    })
                } else if res.statusCode == 401 {
                    error401Action(self)
                }
            }
        }) 
        saveStatusTask.resume()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func putToolbarOnTopOfKeyboard(_ textView: UITextView) {
        let toolbar : KeyboardToolbar = Bundle.main.loadNibNamed("KeyboardToolbar", owner: self, options: nil)![0] as! KeyboardToolbar
        
        toolbar.doneButton.action = #selector(UpdateStatus.hideKeyboard(_:))
        textView.inputAccessoryView = toolbar
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        putToolbarOnTopOfKeyboard(textView)
        return true
    }
    
    func hideKeyboard(_ sender: AnyObject) {
        self.view.endEditing(true)
    }
}
