//
//  AppDelegate.swift
//  online
//
//  Created by KingOP on 22.10.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit
import Haneke
import AudioToolbox
import TSMessages
import CoreLocation
import JSQMessagesViewController
import MapKit
import Crashlytics
import Fabric
import SocketIO

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, TSMessageViewProtocol {

    var window: UIWindow?
    var shortcutItem: UIApplicationShortcutItem?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        var performShortcutDelegate = true

        Fabric.with([Crashlytics.self])
        
        if let shortcutItem = launchOptions?[UIApplicationLaunchOptionsKey.shortcutItem] as? UIApplicationShortcutItem {
            
            print("Application launched via shortcut")
            self.shortcutItem = shortcutItem
            
            performShortcutDelegate = false
        }
        
        if isUserAuthenticated() {
            // создадим быстрый ярлык для создания метки
            self.createDynamicShortcutItems()
            self.logUser()
        } else {
            // если юзер разлогинен, надо удалить ярлыки
            UIApplication.shared.shortcutItems = []
        }
        
        UINavigationBar.appearance().barTintColor = UIColor(netHex: 0x185996)
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        
        UINavigationBar.appearance().barStyle = UIBarStyle.black
        
        // проверим, а был ли пуш...
        if let userInfo = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? Dictionary<String, AnyObject> {
            // мы пришли сюда из пуша
            
            pushUserInfo = userInfo
        }
        
        print(pushToken)
        
        TSMessage.setDelegate(self)
        TSMessageView.appearance().messageIsFullyDisplayed = false
        
        // туториал при первом запуске
        let pageController = UIPageControl.appearance()
        pageController.pageIndicatorTintColor = UIColor.lightGray
        pageController.currentPageIndicatorTintColor = UIColor(netHex: 0x3e89c3)
        pageController.backgroundColor = UIColor.white
        
        if !isTOSShown() {
            let tosNC : UINavigationController = self.window?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: "tosNC") as! UINavigationController
            
            self.window?.rootViewController = tosNC
        }

        return performShortcutDelegate
    }
    
    func logUser() {
        Crashlytics.sharedInstance().setUserIdentifier(getUserId()!.description)
        Crashlytics.sharedInstance().setUserName(getUserLogin()!)
    }
    
    func customize(_ messageView: TSMessageView!) {
        TSMessageView.appearance().backgroundColor = UIColor(netHex: 0x3e89c3)
        TSMessageView.appearance().messageIsFullyDisplayed = false
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        let scheme : String = url.scheme!
        
        if scheme == "helprp" {
            resetPasswordToken = url.host!
            
            let newPasswordNC = self.window?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: "NewPasswordNC") as! NewPasswordNC
            
            self.window?.rootViewController?.present(newPasswordNC, animated: true, completion: nil)
        } else if scheme == "helpvu" {
            let viewUser = self.window?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: "userProfile") as! UserProfile
            
            viewUser.userId = Int(url.host!)!
            
            ((self.window?.rootViewController as! MainContainer).selectedViewController as! UINavigationController).pushViewController(viewUser, animated: true)
        } else if scheme == "helpvpin" {
            let viewPin = self.window?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: "viewPinDetail") as! PinDetailView
            
            viewPin.pinId = Int(url.host!)!
            
            ((self.window?.rootViewController as! MainContainer).selectedViewController as! UINavigationController).pushViewController(viewPin, animated: true)
        }
        
        return true
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        
        pushToken = token
        
        savePushToken(pushToken)
        
        print("Token: \(token)")
        
        firstRegisterForPushes()
    }
    
    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, for notification: UILocalNotification, withResponseInfo responseInfo: [AnyHashable: Any], completionHandler: @escaping () -> Void) {
        
        var badgeCount : Int = (notification.userInfo!["aps"] as! NSDictionary)["badge"] as! Int
        var dialogUnread : Int = 0
        
        if identifier == "TEXT_ACTION" {
            let text : String = responseInfo[UIUserNotificationActionResponseTypedTextKey] as! String
            let dialogId : Int = notification.userInfo!["dialog_id"] as! Int
            
            let params = [
                "message" : text
            ]
            
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.high).async(execute: {
                let sendMessageRequest = getURLRequest("POST", url: getBackendApiPath() + "dialog/\(dialogId)/messages", params: params, token: getUserToken()!)
                
                var response : URLResponse?
                var data : Data?
                do {
                    data = try NSURLConnection.sendSynchronousRequest(sendMessageRequest, returning: &response)
                    
                    if data != nil {
                        let res = response as! HTTPURLResponse
                        if res.statusCode == 201 {
                            
                            playSendSound()
                            
                            DispatchQueue.main.async(execute: {
                                
                                if dialogsController != nil {
                                    let dialogId : Int = notification.userInfo!["dialog_id"] as! Int
                                    let date : Date = Date()
                                    
                                    let index = dialogsController?.dialogIds.index(of: dialogId)
                                    
                                    if index != nil {
                                        // текущий диалог есть в списке диалогов, обновим его
                                        var dialog : Dictionary<String, AnyObject> = dialogsController!.dialogs[index!]
                                        
                                        if dialog["message"] == nil {
                                            let message = NSMutableDictionary()
                                            message.setValue(getUserId()!, forKey: "user_id")
                                            message.setValue(Double(date.timeIntervalSince1970), forKey: "created_at")
                                            message.setValue(0, forKey: "viewed")
                                            message.setValue(text, forKey: "message")
                                            
                                            dialog["message"] = message
                                        } else {
                                            (dialog["message"] as! NSMutableDictionary)["user_id"] = getUserId()!
                                            (dialog["message"] as! NSMutableDictionary)["created_at"] = Double(date.timeIntervalSince1970)
                                            (dialog["message"] as! NSMutableDictionary)["viewed"] = 0
                                            (dialog["message"] as! NSMutableDictionary)["message"] = text
                                        }
                                        dialogUnread = Int(dialog["unread"] as! String)!
                                        dialog["unread"] = "0" as AnyObject?
                                        
                                        // сделаем перемещение
                                        dialogsController!.dialogs.remove(at: index!)
                                        dialogsController!.dialogIds.remove(at: index!)
                                        
                                        dialogsController!.dialogs.insert(dialog, at: 0)
                                        dialogsController!.dialogIds.insert(dialogId, at: 0)
                                    }
                                    
                                    // сообщение успешно отправлено
                                    // проверим, вдруг мы в текущем диалоге
                                    if messagesController != nil && messagesController?.dialogId == dialogId {
                                        let newMessage = JSQMessage(senderId: getUserId()!.description, senderDisplayName: getUserLogin()!, date: date, text: text)
                                        messagesController!.messages.append(newMessage!)
                                        
                                        messagesController!.collectionView?.reloadData()
                                    }
                                    
                                    dialogsController?.tableView.reloadData()
                                    badgeCount -= dialogUnread
                                    UIApplication.shared.applicationIconBadgeNumber = max(badgeCount, 0)
                                    mainContainer?.tabBar.items![1].badgeValue = badgeCount <= 0 ? nil : badgeCount.description
                                }
                                
                                completionHandler()
                            })
                        }
                    }
                } catch {
                    
                }
            })
        } else if identifier == "MARK_READ_ACTION" {
            // отметка диалога прочитанным
            
            let dialogId : Int = notification.userInfo!["dialog_id"] as! Int
            
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.high).async(execute: {
                let sendMessageRequest = getURLRequest("POST", url: getBackendApiPath() + "dialog/\(dialogId)/read", params: Dictionary<String, String>(), token: getUserToken()!)
                
                var response : URLResponse?
                var data : Data?
                do {
                    data = try NSURLConnection.sendSynchronousRequest(sendMessageRequest, returning: &response)
                    
                    if data != nil {
                        let res = response as! HTTPURLResponse
                        if res.statusCode == 204 {
                            
                            DispatchQueue.main.async(execute: {
                                
                                if dialogsController != nil {
                                    let dialogId : Int = notification.userInfo!["dialog_id"] as! Int
                                    let index = dialogsController?.dialogIds.index(of: dialogId)
                                    
                                    if index != nil {
                                        // текущий диалог есть в списке диалогов, обновим его
                                        var dialog : Dictionary<String, AnyObject> = dialogsController!.dialogs[index!]
                                        dialogUnread = Int(dialog["unread"] as! String)!
                                        dialog["unread"] = "0" as AnyObject?
                                    }
                                }
                                
                                badgeCount -= dialogUnread
                                UIApplication.shared.applicationIconBadgeNumber = max(badgeCount, 0)
                                mainContainer?.tabBar.items![1].badgeValue = badgeCount <= 0 ? nil : badgeCount.description
                                
                                dialogsController?.tableView.reloadData()
                                
                                completionHandler()
                            })
                        }
                    }
                } catch {
                    
                }
            })
        }
    }
    
    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, forRemoteNotification userInfo: [AnyHashable: Any], withResponseInfo responseInfo: [AnyHashable: Any], completionHandler: @escaping () -> Void) {
        
        var dialogUnread = 0
        var badgeCount : Int = (userInfo["aps"] as! NSDictionary)["badge"] as! Int
        
        if identifier == "TEXT_ACTION" {
            let text : String = responseInfo[UIUserNotificationActionResponseTypedTextKey] as! String
            let dialogId : Int = userInfo["dialog_id"] as! Int
            
            let params = [
                "message" : text
            ]
            
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.high).async(execute: {
                let markDialogRequest = getURLRequest("POST", url: getBackendApiPath() + "dialog/\(dialogId)/messages", params: params, token: getUserToken()!)
                
                var response : URLResponse?
                var data : Data?
                do {
                    data = try NSURLConnection.sendSynchronousRequest(markDialogRequest, returning: &response)
                    
                    if data != nil {
                        let res = response as! HTTPURLResponse
                        if res.statusCode == 201 {
                            
                            playSendSound()
                            
                            DispatchQueue.main.async(execute: {
                                
                                if dialogsController != nil {
                                    let dialogId : Int = userInfo["dialog_id"] as! Int
                                    let date : Date = Date()
                                    
                                    let index = dialogsController?.dialogIds.index(of: dialogId)
                                    
                                    if index != nil {
                                        // текущий диалог есть в списке диалогов, обновим его
                                        var dialog : Dictionary<String, AnyObject> = dialogsController!.dialogs[index!]
                                        
                                        if dialog["message"] == nil {
                                            let message : Dictionary<String, AnyObject> = [
                                                "user_id"       : getUserId()! as AnyObject,
                                                "created_at"    : Double(date.timeIntervalSince1970) as AnyObject,
                                                "viewed"        : 0 as AnyObject,
                                                "message"       : text as AnyObject
                                            ]
                                            
                                            dialog["message"] = message as AnyObject?
                                        } else {
                                            var new_message : Dictionary<String, AnyObject> = dialog["message"] as! Dictionary<String, AnyObject>
                                            new_message["user_id"] = getUserId()! as AnyObject?
                                            new_message["created_at"] = Double(date.timeIntervalSince1970) as AnyObject?
                                            new_message["viewed"] = 0 as AnyObject?
                                            new_message["message"] = text as AnyObject?
                                            
                                            dialog["message"] = new_message as AnyObject?
                                        }
                                        
                                        dialogUnread = Int(dialog["unread"] as! String)!
                                        dialog["unread"] = "0" as AnyObject?
                                        
                                        // сделаем перемещение
                                        dialogsController!.dialogs.remove(at: index!)
                                        dialogsController!.dialogIds.remove(at: index!)
                                        
                                        dialogsController!.dialogs.insert(dialog, at: 0)
                                        dialogsController!.dialogIds.insert(dialogId, at: 0)
                                    }
                                    
                                    // сообщение успешно отправлено
                                    // проверим, вдруг мы в текущем диалоге
                                    if messagesController != nil && messagesController?.dialogId == dialogId {
                                        let newMessage = JSQMessage(senderId: getUserId()!.description, senderDisplayName: getUserLogin()!, date: date, text: text)
                                        messagesController!.messages.append(newMessage!)
                                        
                                        messagesController!.collectionView?.reloadData()
                                    }
                                    
                                    dialogsController?.tableView.reloadData()
                                    
                                    
                                }
                                
                                badgeCount -= dialogUnread
                                UIApplication.shared.applicationIconBadgeNumber = max(badgeCount, 0)
                                mainContainer?.tabBar.items![1].badgeValue = badgeCount <= 0 ? nil : badgeCount.description
                                
                                dialogsController?.tableView.reloadData()
                                
                                completionHandler()
                            })
                        }
                    }
                } catch {
                    
                }
            })
        } else if identifier == "MARK_READ_ACTION" {
            // отметка диалога прочитанным
            
            let dialogId : Int = userInfo["dialog_id"] as! Int
            
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.high).async(execute: {
                let markDialogRequest = getURLRequest("POST", url: getBackendApiPath() + "dialog/\(dialogId)/read", params: Dictionary<String, String>(), token: getUserToken()!)
                
                var response : URLResponse?
                var data : Data?
                do {
                    data = try NSURLConnection.sendSynchronousRequest(markDialogRequest, returning: &response)
                    
                    if data != nil {
                        let res = response as! HTTPURLResponse
                        if res.statusCode == 204 {
                            
                            DispatchQueue.main.async(execute: {
                                
                                if dialogsController != nil {
                                    let dialogId : Int = userInfo["dialog_id"] as! Int
                                    let index = dialogsController?.dialogIds.index(of: dialogId)
                                    
                                    if index != nil {
                                        // текущий диалог есть в списке диалогов, обновим его
                                        var dialog = dialogsController!.dialogs[index!]
                                        dialogUnread = Int(dialog["unread"] as! String)!
                                        dialog["unread"] = "0" as AnyObject?
                                    }
                                    
                                    dialogsController?.tableView.reloadData()
                                }
                                
                                badgeCount -= dialogUnread
                                UIApplication.shared.applicationIconBadgeNumber = max(badgeCount, 0)
                                mainContainer?.tabBar.items![1].badgeValue = badgeCount <= 0 ? nil : badgeCount.description
                                
                                dialogsController?.tableView.reloadData()
                                
                                completionHandler()
                            })
                        }
                    }
                } catch {
                    
                }
            })
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        if UIApplication.shared.applicationState == UIApplicationState.background {
            let aps : Dictionary<String, AnyObject> = userInfo["aps"] as! Dictionary<String, AnyObject>;
            
            let badgeCount : Int = aps["badge"] as! Int
            let userId : Int = userInfo["user_id"] as! Int
            let dialogId = userInfo["dialog_id"] as! Int
            let username = userInfo["username"] as! String
            
            var usernameToDisplay = ""
            
            let members : [Dictionary<String, AnyObject>] = userInfo["members"] as! [Dictionary<String, AnyObject>]
            
            if members.count > 2 {
                if !(userInfo["dialog_title"] is NSNull) && userInfo["dialog_title"] != nil && userInfo["dialog_title"] as! String != "" {
                    
                    usernameToDisplay = userInfo["dialog_title"] as! String
                } else {
                    for user in members {
                        usernameToDisplay += "\(user["username"] as! String), "
                    }
                    
                    usernameToDisplay = usernameToDisplay.truncate(usernameToDisplay.length - 2)
                }
            } else {
                usernameToDisplay = username
            }
            
            // обновим список диалогов
            if dialogsController != nil {
                print("received!!!")
                // обновим список диалогов
                var delta = 1
                
                if mainContainer?.tabBar.items![1].badgeValue != nil {
                    delta = badgeCount - Int((mainContainer?.tabBar.items![1].badgeValue)!)!
                }
                
                
                // получим индекс
                let index = dialogsController?.dialogIds.index(of: dialogId)
                
                var text = userInfo["message"] as! String
                
                if !(userInfo["attachment"] is NSNull) && userInfo["attachment"] != nil {
                    // есть аттач, надо проверить тип
                    let attach = userInfo["attachment"] as! Dictionary<String, AnyObject>
                    
                    if attach["type"] as! String == "location" {
                        text = "Местоположение"
                    } else {
                        text = "Изображение"
                    }
                }
                
                if index != nil {
                    // текущий диалог есть в списке диалогов, обновим его
                    var dialog : Dictionary<String, AnyObject> = dialogsController!.dialogs[index!]
                    
                    if dialog["message"] == nil {
                        let new_message : Dictionary<String, AnyObject> = [
                            "user_id"       : userId as AnyObject,
                            "created_at"    : userInfo["created_at"] as AnyObject,
                            "viewed"        : 0 as AnyObject,
                            "message"       : text as AnyObject
                        ]
                        
                        dialog["message"] = new_message as AnyObject?
                    } else {
                        var new_message : Dictionary<String, AnyObject> = dialog["message"]! as! Dictionary<String, AnyObject>
                        
                        new_message["user_id"] = getUserId()! as AnyObject?
                        new_message["created_at"] = userInfo["created_at"] as! Double as AnyObject?
                        new_message["viewed"] = 0 as AnyObject?
                        new_message["message"] = text as AnyObject?
                        
                        if userInfo["attachment"] is NSNull || userInfo["attachment"] == nil {
                            new_message["attachment"] = NSNull()
                        }
                        
                        dialog["message"] = new_message as AnyObject?
                    }
                    
                    if messagesController == nil || messagesController?.dialogId != dialogId {
                        let currentUnread = Int(dialog["unread"] as! String)
                        
                        dialog["unread"] = (currentUnread! + delta).description as AnyObject?
                    } else {
                        dialog["unread"] = "0" as AnyObject?
                    }
                    
                    // сделаем перемещение
                    dialogsController!.dialogs.remove(at: index!)
                    dialogsController!.dialogIds.remove(at: index!)
                    
                    dialogsController!.dialogs.insert(dialog, at: 0)
                    dialogsController!.dialogIds.insert(dialogId, at: 0)
                } else {
                    // диалога не было, его надо создать и впихнуть
                    
                    let new_message : Dictionary<String, AnyObject> = [
                        "user_id"       : userId as AnyObject,
                        "created_at"    : userInfo["created_at"] as AnyObject,
                        "viewed"        : 0 as AnyObject,
                        "message"       : userInfo["message"] as AnyObject,
                        "attachment"    : NSNull()
                    ]
                    
                    let dialog : Dictionary<String, AnyObject> = [
                        "message"   : new_message as AnyObject,
                        "id"        : dialogId as AnyObject,
                        "unread"    : delta.description as AnyObject,
                        "members"   : userInfo["members"] as AnyObject
                    ]
                    
                    // сделаем перемещение
                    dialogsController!.dialogs.insert(dialog, at: 0)
                    dialogsController!.dialogIds.insert(dialogId, at: 0)
                    dialogsController!.currentOffset += 1
                }
            }
            
            // проверим, а вдруг мы в текущем диалоге
            if messagesController != nil && messagesController?.dialogId == dialogId {
                // мы и так в нужном диалоге, просто отобразим новое сообщение
                
                if !(userInfo["attachment"] is NSNull) && userInfo["attachment"] != nil {
                    // есть аттач, надо проверить тип
                    let attach = userInfo["attachment"] as! Dictionary<String, AnyObject>
                    
                    if attach["type"] as! String == "location" {
                        
                        // отобразим местоположение
                        let location : JSQLocationMediaItem = JSQLocationMediaItem(maskAsOutgoing: (userInfo["user_id"] as? Int) == getUserId()!)
                        let message = JSQMessage(senderId: (userInfo["user_id"] as? Int)!.description, senderDisplayName: username, date: Date(timeIntervalSince1970: TimeInterval(userInfo["created_at"] as! Int)), media: location)
                        
                        messagesController!.messages.append(message!)
                        
                        let dictionary : Dictionary<String, AnyObject> = [
                            "user_id"       : (userInfo["user_id"] as? Int)!.description as AnyObject,
                            "created_at"    : userInfo["created_at"] as AnyObject,
                            "message"       : "Местоположение" as AnyObject,
                            "status"        : "sent" as AnyObject,
                            "id"            : userInfo["id"] as AnyObject
                        ]
                        
                        
                        messagesController!.message_dict.append(dictionary)
                        
                        let coordinates = (attach["content"] as! String).characters.split{$0 == ";"}.map(String.init)
                        
                        let locationToShow = CLLocation(latitude: Double(coordinates[0])!, longitude: Double(coordinates[1])!)
                        
                        location.setLocation(locationToShow, region: MKCoordinateRegionMakeWithDistance(locationToShow.coordinate, 500.0, 500.0), withCompletionHandler: { () -> Void in
                            messagesController!.collectionView?.reloadData()
                        })
                    } else {
                        let photo : JSQPhotoMediaItem = JSQPhotoMediaItem(image: nil)
                        photo.appliesMediaViewMaskAsOutgoing = (userInfo["user_id"] as? Int) == getUserId()!
                        
                        let message = JSQMessage(senderId: (userInfo["user_id"] as? Int)!.description, senderDisplayName: username, date: Date(timeIntervalSince1970: TimeInterval(userInfo["created_at"] as! Int)), media: photo)
                        
                        let dictionary : Dictionary<String, AnyObject> = [
                            "user_id"       : (userInfo["user_id"] as? Int)!.description as AnyObject,
                            "created_at"    : userInfo["created_at"] as AnyObject,
                            "message"       : "Изображение" as AnyObject,
                            "status"        : "sent" as AnyObject,
                            "id"            : userInfo["id"] as AnyObject
                        ]
                        
                        
                        messagesController!.message_dict.append(dictionary)
                        
                        messagesController!.messages.append(message!)
                        
                        DispatchQueue(label: "loadImg", attributes: []).async(execute: {
                            let imageUrl : URL = URL(string: getBackendDomainPath() + (attach["content"] as! String))!
                            
                            let imageData : Data? = try? Data(contentsOf: imageUrl)
                            
                            if imageData != nil {
                                let image = UIImage(data: imageData!)
                                
                                DispatchQueue.main.async(execute: {
                                    photo.image = image
                                    messagesController!.collectionView?.reloadData()
                                    
                                    avatarsCacheDataStack.saveAvatarToCache(attach["id"] as! Int, fileName: imageUrl.lastPathComponent, imageData: imageData!)
                                })
                            }
                        })
                    }
                } else {
                    let newMessage = JSQMessage(senderId: (userInfo["user_id"] as? Int)!.description, senderDisplayName: username, date: Date(timeIntervalSince1970: TimeInterval(userInfo["created_at"] as! Int)), text: userInfo["message"] as! String)
                    messagesController!.messages.append(newMessage!)
                }
                
                messagesController!.currentOffset += 1
            }
            
            DispatchQueue.main.async(execute: {
                if messagesController != nil && messagesController?.dialogId == dialogId {
                    if userInfo["user_id"] as! Int == getUserId()! {
                        messagesController!.finishSendingMessage(animated: true)
                        //playSendSound()
                    } else {
                        messagesController!.finishReceivingMessage(animated: true)
                        //playReceivedSound()
                        // пошлем на сервер сигнал о том, что мы это сообщение прочитали
                        if messagesController!.socket.status == SocketIOClientStatus.connected {
                            messagesController!.socket.emit("read")
                        } else {
                            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.high).async(execute: {
                                let markDialogRequest = getURLRequest("POST", url: getBackendApiPath() + "dialog/\(dialogId)/read", params: Dictionary<String, String>(), token: getUserToken()!)
                                
                                var response : URLResponse?
                                
                                do {
                                    try NSURLConnection.sendSynchronousRequest(markDialogRequest, returning: &response)
                                } catch {
                                    
                                }
                            })
                        }
                    }
                }
                
                if dialogsController != nil {
                    dialogsController?.tableView.reloadData()
                }
                
                UIApplication.shared.applicationIconBadgeNumber = badgeCount
                if mainContainer != nil {
                    mainContainer?.tabBar.items![1].badgeValue = badgeCount <= 0 ? nil :badgeCount.description
                }
            })
        } else {
            self.application(application, didReceiveRemoteNotification: userInfo)
        }
        
        completionHandler(.newData)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        let event : String = userInfo["event"] as! String
        
        if event == "new_message" {
            var badgeCount : Int = (userInfo["aps"] as! NSDictionary)["badge"] as! Int
            let oldCount : Int = mainContainer?.tabBar.items![1].badgeValue == nil ? 0 : Int((mainContainer?.tabBar.items![1].badgeValue)!)!
            
            let dialogId = userInfo["dialog_id"] as! Int
            let username = userInfo["username"] as! String
            let userId : Int = userInfo["user_id"] as! Int
            
            var usernameToDisplay = ""
            
            let members : [Dictionary<String, AnyObject>] = userInfo["members"] as! [Dictionary<String, AnyObject>]
            
            if members.count > 2 {
                if !(userInfo["dialog_title"] is NSNull) && userInfo["dialog_title"] != nil && userInfo["dialog_title"] as! String != "" {
                    
                    usernameToDisplay = userInfo["dialog_title"] as! String
                } else {
                    for user in members {
                        usernameToDisplay += "\(user["username"] as! String), "
                    }
                    
                    usernameToDisplay = usernameToDisplay.truncate(usernameToDisplay.length - 2)
                }
            } else {
                usernameToDisplay = username
            }
            
            let messagesVC : Messages = self.window?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: "messagesVC") as! Messages
            
            messagesVC.dialogId = dialogId
            messagesVC.userId = userInfo["user_id"] as? Int
            messagesVC.userName = usernameToDisplay
            messagesVC.members = userInfo["members"] as! [Dictionary<String, AnyObject>]
            
            if !(userInfo["master_user_id"] is NSNull) && userInfo["master_user_id"] != nil && userInfo["master_user_id"] as! Int != 0 {
                messagesVC.creatorId = userInfo["master_user_id"] as? Int
            }
            
            if !(userInfo["dialog_photo"] is NSNull) && userInfo["dialog_photo"] != nil && (userInfo["dialog_photo"] as! String) != "" {
                
                messagesVC.avatarPath = userInfo["dialog_photo"] as? String
            } else if !(userInfo["photo"] is NSNull) && userInfo["photo"] != nil && (userInfo["photo"] as! String) != "" && (userInfo["members"] as! [Dictionary<String, AnyObject>]).count == 2 {
                messagesVC.avatarPath = userInfo["photo"] as? String
            }
            
            // обновим список диалогов
            if dialogsController != nil {
                print("received!!!")
                // обновим список диалогов
                var delta = 1
                
                if mainContainer?.tabBar.items![1].badgeValue != nil {
                    delta = badgeCount - oldCount
                }
                
                
                // получим индекс
                let index = dialogsController?.dialogIds.index(of: dialogId)
                
                var text = userInfo["message"] as! String
                
                if !(userInfo["attachment"] is NSNull) && userInfo["attachment"] != nil {
                    // есть аттач, надо проверить тип
                    let attach = userInfo["attachment"] as! Dictionary<String, AnyObject>
                    
                    if attach["type"] as! String == "location" {
                        text = "Местоположение"
                    } else {
                        text = "Изображение"
                    }
                }
                
                if index != nil {
                    // текущий диалог есть в списке диалогов, обновим его
                    var dialog : Dictionary<String, AnyObject> = dialogsController!.dialogs[index!]
                    
                    if dialog["message"] == nil {
                        let message : Dictionary<String, AnyObject> = [
                            "user_id"       : userId as AnyObject,
                            "created_at"    : userInfo["created_at"] as AnyObject,
                            "viewed"        : 0 as AnyObject,
                            "message"       : text as AnyObject
                        ]
                        
                        dialog["message"] = message as AnyObject?
                    } else {
                        var new_message : Dictionary<String, AnyObject> = dialog["message"] as! Dictionary<String, AnyObject>
                        new_message["user_id"] = userId as AnyObject?
                        new_message["created_at"] = userInfo["created_at"] as AnyObject?
                        new_message["viewed"] = 0 as AnyObject?
                        new_message["message"] = text as AnyObject?
                        
                        if userInfo["attachment"] is NSNull || userInfo["attachment"] == nil {
                            new_message["attachment"] = NSNull()
                        }
                        
                        dialog["message"] = new_message as AnyObject?
                    }
                    
                    if messagesController == nil || messagesController?.dialogId != dialogId {
                        let currentUnread = Int(dialog["unread"] as! String)
                        
                        dialog["unread"] = (currentUnread! + delta).description as AnyObject?
                    } else {
                        dialog["unread"] = "0" as AnyObject?
                    }
                    
                    // сделаем перемещение
                    dialogsController!.dialogs.remove(at: index!)
                    dialogsController!.dialogIds.remove(at: index!)
                    
                    dialogsController!.dialogs.insert(dialog, at: 0)
                    dialogsController!.dialogIds.insert(dialogId, at: 0)
                } else {
                    // диалога не было, его надо создать и впихнуть
                    
                    let new_message : Dictionary<String, AnyObject> = [
                        "user_id"       : userId as AnyObject,
                        "created_at"    : userInfo["created_at"] as AnyObject,
                        "viewed"        : 1 as AnyObject,
                        "message"       : userInfo["message"] as AnyObject,
                        "attachment"    : NSNull()
                    ]
                    
                    let dialog : Dictionary<String, AnyObject> = [
                        "message"   : new_message as AnyObject,
                        "id"        : dialogId as AnyObject,
                        "unread"    : delta.description as AnyObject,
                        "members"   : userInfo["members"] as AnyObject
                    ]
                    
                    // сделаем перемещение
                    dialogsController!.dialogs.insert(dialog, at: 0)
                    dialogsController!.dialogIds.insert(dialogId, at: 0)
                    dialogsController!.currentOffset += 1
                }
                DispatchQueue.main.async(execute: {
                    // если вдруг тут это не сработает, перенесем это в applicationDidBecomeActive
                    
                    UIApplication.shared.applicationIconBadgeNumber = badgeCount
                    mainContainer?.tabBar.items![1].badgeValue = badgeCount <= 0 ? nil : badgeCount.description
                    dialogsController?.tableView.reloadData()
                })
            }
            
            if UIApplication.shared.applicationState == UIApplicationState.active {
                
                // проверим, а вдруг мы в текущем диалоге
                if messagesController != nil && messagesController?.dialogId == dialogId {
                    // мы и так в нужном диалоге, просто отобразим новое сообщение
                    
                    if !(userInfo["attachment"] is NSNull) && userInfo["attachment"] != nil {
                        // есть аттач, надо проверить тип
                        let attach = userInfo["attachment"] as! NSDictionary
                        
                        if attach["type"] as! String == "location" {
                            
                            // отобразим местоположение
                            let location : JSQLocationMediaItem = JSQLocationMediaItem(maskAsOutgoing: (userInfo["user_id"] as? Int) == getUserId()!)
                            let message = JSQMessage(senderId: (userInfo["user_id"] as? Int)!.description, senderDisplayName: username, date: Date(timeIntervalSince1970: TimeInterval(userInfo["created_at"] as! Int)), media: location)
                            
                            messagesController!.messages.append(message!)
                            
                            let dictionary : Dictionary<String, AnyObject> = [
                                "user_id"       : (userInfo["user_id"] as? Int)!.description as AnyObject,
                                "created_at"    : userInfo["created_at"] as AnyObject,
                                "message"       : "Местоположение" as AnyObject,
                                "status"        : "sent" as AnyObject,
                                "id"            : userInfo["id"] as AnyObject
                            ]
                            
                            
                            messagesController!.message_dict.append(dictionary)
                            
                            let coordinates = (attach["content"] as! String).characters.split{$0 == ";"}.map(String.init)
                            
                            let locationToShow = CLLocation(latitude: Double(coordinates[0])!, longitude: Double(coordinates[1])!)
                            
                            location.setLocation(locationToShow, region: MKCoordinateRegionMakeWithDistance(locationToShow.coordinate, 500.0, 500.0), withCompletionHandler: { () -> Void in
                                messagesController!.collectionView?.reloadData()
                            })
                        } else {
                            let photo : JSQPhotoMediaItem = JSQPhotoMediaItem(image: nil)
                            photo.appliesMediaViewMaskAsOutgoing = (userInfo["user_id"] as? Int) == getUserId()!
                            
                            let message = JSQMessage(senderId: (userInfo["user_id"] as? Int)!.description, senderDisplayName: username, date: Date(timeIntervalSince1970: TimeInterval(userInfo["created_at"] as! Int)), media: photo)
                            
                            messagesController!.messages.append(message!)
                            
                            let dictionary : Dictionary<String, AnyObject> = [
                                "user_id"       : (userInfo["user_id"] as? Int)!.description as AnyObject,
                                "created_at"    : userInfo["created_at"] as AnyObject,
                                "message"       : "Изображение" as AnyObject,
                                "status"        : "sent" as AnyObject,
                                "id"            : userInfo["id"] as AnyObject
                            ]
                            
                            
                            messagesController!.message_dict.append(dictionary)
                            
                            DispatchQueue(label: "loadImg\(attach["id"] as! Int)", attributes: []).async(execute: {
                                // закэшируем изображение
                                let imageUrl : URL = URL(string: getBackendDomainPath() + (attach["content"] as! String))!
                                
                                let imageData : Data? = try? Data(contentsOf: imageUrl)
                                
                                if imageData != nil {
                                    let image = UIImage(data: imageData!)
                                    
                                    DispatchQueue.main.async(execute: {
                                        photo.image = image
                                        messagesController!.collectionView?.reloadData()
                                    })
                                }
                            })
                        }
                    } else {
                        let newMessage = JSQMessage(senderId: (userInfo["user_id"] as? Int)!.description, senderDisplayName: username, date: Date(timeIntervalSince1970: TimeInterval(userInfo["created_at"] as! Int)), text: userInfo["message"] as! String)
                        
                        let dictionary : Dictionary<String, AnyObject> = [
                            "user_id"       : (userInfo["user_id"] as? Int)!.description as AnyObject,
                            "created_at"    : userInfo["created_at"] as AnyObject,
                            "message"       : userInfo["message"] as AnyObject,
                            "status"        : "sent" as AnyObject,
                            "id"            : userInfo["id"] as AnyObject
                        ]
                        
                        
                        messagesController!.message_dict.append(dictionary)
                        
                        messagesController!.messages.append(newMessage!)
                    }
                    
                    messagesController!.currentOffset += 1
                    
                    badgeCount -= 1
                    
                    UIApplication.shared.applicationIconBadgeNumber = badgeCount
                    mainContainer?.tabBar.items![1].badgeValue = badgeCount <= 0 ? nil : badgeCount.description
                    
                    if userInfo["user_id"] as! Int == getUserId()! {
                        messagesController!.finishSendingMessage(animated: true)
                        playSendSound()
                    } else {
                        messagesController!.finishReceivingMessage(animated: true)
                        playReceivedSound()
                        // пошлем на сервер сигнал о том, что мы это сообщение прочитали
                        if messagesController!.socket.status == SocketIOClientStatus.connected {
                            messagesController!.socket.emit("read")
                        } else {
                            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.high).async(execute: {
                                let markDialogRequest = getURLRequest("POST", url: getBackendApiPath() + "dialog/\(dialogId)/read", params: Dictionary<String, String>(), token: getUserToken()!)
                                
                                var response : URLResponse?
                                
                                do {
                                    try NSURLConnection.sendSynchronousRequest(markDialogRequest, returning: &response)
                                } catch {
                                    
                                }
                            })
                        }
                    }
                } else {
                    var longMessage : String? = nil
                    var newMessage : String = ""
                    
                    if userInfo["attachment"] is NSNull || userInfo["attachment"] == nil {
                        longMessage = "Сообщение от \(username): \(userInfo["message"] as! String)"
                        newMessage = userInfo["message"] as! String
                    } else {
                        if (userInfo["attachment"] as! NSDictionary)["type"] as! String == "location" {
                            longMessage = "Пользователь \(username) прислал(а) местоположение"
                            newMessage = "Местоположение"
                        } else {
                            longMessage = "Пользователь \(username) прислал(а) изображение"
                            newMessage = "Изображение"
                        }
                    }
                    
                    // мы не в текущем диалоге
                    // воспроизведем звук уведомления
                    let fileUrl : URL = URL(fileURLWithPath: "/System/Library/Audio/UISounds/ReceivedMessage.caf")
                    var soundId : SystemSoundID = SystemSoundID()
                    AudioServicesCreateSystemSoundID(fileUrl as CFURL, &soundId)
                    AudioServicesPlaySystemSound(soundId)
                    
                    // тут надо показать уведомления
                    
                    TSMessage.dismissActiveNotification()
                    
                    if isDialogsListShown == true && dialogsController != nil {
                        dialogsController?.tableView.reloadData()
                    } else {
                        TSMessage.showNotification(in: self.window?.rootViewController, title: username, subtitle: newMessage.truncate(100, trailing: "..."), image: UIImage(named: "chat white"), type: TSMessageNotificationType.success, duration: 4.0, callback: {() -> Void in
                            
                            ((self.window?.rootViewController as! MainContainer).selectedViewController as! UINavigationController).pushViewController(messagesVC, animated: true)
                            
                            TSMessage.dismissActiveNotification()
                            
                            }, buttonTitle: nil, buttonCallback: nil, at: TSMessageNotificationPosition.top, canBeDismissedByUser: true)
                    }
                    
                    
                    // сохраним уведомление в центре уведомлений
                    let localNotification : UILocalNotification = UILocalNotification()
                    
                    // зададим userInfo
                    if !(userInfo["photo"] is NSNull) && userInfo["photo"] != nil && (userInfo["photo"] as! String) != "" {
                        localNotification.userInfo = [
                            "user_id"   : userInfo["user_id"]!,
                            "dialog_id" : userInfo["dialog_id"]!,
                            "username"  : userInfo["username"]!,
                            "photo"     : userInfo["photo"]!
                        ]
                    } else {
                        localNotification.userInfo = [
                            "user_id" : userInfo["user_id"]!,
                            "dialog_id" : userInfo["dialog_id"]!,
                            "username" : userInfo["username"]!
                        ]
                    }
                    
                    localNotification.alertBody = longMessage!
                    localNotification.fireDate = Date()
                    
                    localNotification.category = "REPLY_CATEGORY"
                    
                    UIApplication.shared.scheduleLocalNotification(localNotification)
                }
            } else {
                // тут все действия
                
                if messagesController == nil || messagesController?.dialogId != dialogId {
                    
                    ((self.window?.rootViewController as! MainContainer).selectedViewController as! UINavigationController).pushViewController(messagesVC, animated: true)
                }
            }
            
            DispatchQueue.main.async(execute: {
                UIApplication.shared.applicationIconBadgeNumber = badgeCount
                dialogsController?.tableView.reloadData()
                
                if messagesController == nil || messagesController?.dialogId != dialogId {
                    if mainContainer != nil {
                        mainContainer?.tabBar.items![1].badgeValue = badgeCount <= 0 ? nil : badgeCount.description
                    }
                }
            })
        } else {
            let pinViewController : PinDetailView = self.window?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: "viewPinDetail") as! PinDetailView
            let pinId : Int = userInfo["status_id"] as! Int
            pinViewController.pinId = pinId
            
            if UIApplication.shared.applicationState == UIApplicationState.active {
                TSMessage.dismissActiveNotification()
                
                let title = "Уведомление"
                let newMessage = (userInfo["aps"] as! NSDictionary)["alert"] as! String
                var imageName = ""
                
                if event == "like" || event == "fav_pin" {
                    
                    imageName = event == "like" ? "like notification" : "new pin notification"
                    
                    TSMessage.showNotification(in: self.window?.rootViewController, title: title, subtitle: newMessage, image: UIImage(named: imageName), type: TSMessageNotificationType.success, duration: 3.0, callback: {() -> Void in
                        
                        ((self.window?.rootViewController as! MainContainer).selectedViewController as! UINavigationController).pushViewController(pinViewController, animated: true)
                        
                        TSMessage.dismissActiveNotification()
                        
                        }, buttonTitle: nil, buttonCallback: nil, at: TSMessageNotificationPosition.top, canBeDismissedByUser: true)
                } else {
                    pinViewController.goToComments = true
                    
                    TSMessage.showNotification(in: self.window?.rootViewController, title: title, subtitle: newMessage, image: UIImage(named: "comment notification"), type: TSMessageNotificationType.success, duration: 3.0, callback: {() -> Void in
                        
                        ((self.window?.rootViewController as! MainContainer).selectedViewController as! UINavigationController).pushViewController(pinViewController, animated: true)
                        
                        TSMessage.dismissActiveNotification()
                        
                        }, buttonTitle: nil, buttonCallback: nil, at: TSMessageNotificationPosition.top, canBeDismissedByUser: true)
                }
            } else {
                if event == "like" || event == "fav_pin" {
                    ((self.window?.rootViewController as! MainContainer).selectedViewController as! UINavigationController).pushViewController(pinViewController, animated: true)
                } else if event == "comment" {
                    pinViewController.goToComments = true
                    ((self.window?.rootViewController as! MainContainer).selectedViewController as! UINavigationController).pushViewController(pinViewController, animated: true)
                }
            }
        }
    }
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        
        if UIApplication.shared.applicationState != .active {
            let dialogId = notification.userInfo!["dialog_id"] as! Int
            //let message = notification.userInfo!["message"] as! String
            let username = notification.userInfo!["username"] as! String
            var usernameToDisplay = ""
            if (notification.userInfo!["members"] as! [Dictionary<String, AnyObject>]).count > 2 {
                if !(notification.userInfo!["dialog_title"] is NSNull) && notification.userInfo!["dialog_title"] != nil && notification.userInfo!["dialog_title"] as! String != "" {
                    
                    usernameToDisplay = notification.userInfo!["dialog_title"] as! String
                } else {
                    for user in (notification.userInfo!["members"] as! [Dictionary<String, AnyObject>]) {
                        usernameToDisplay += "\(user["username"] as! String), "
                    }
                    
                    usernameToDisplay = usernameToDisplay.truncate(usernameToDisplay.length - 2)
                }
            } else {
                usernameToDisplay = username
            }
            
            let messagesVC : Messages = self.window?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: "messagesVC") as! Messages
            
            messagesVC.dialogId = dialogId
            messagesVC.userId = notification.userInfo!["user_id"] as? Int
            messagesVC.userName = usernameToDisplay
            messagesVC.members = notification.userInfo!["members"] as! [Dictionary<String, AnyObject>]
            
            if !(notification.userInfo!["master_user_id"] is NSNull) && notification.userInfo!["master_user_id"] != nil && notification.userInfo!["master_user_id"] as! Int != 0 {
                messagesVC.creatorId = notification.userInfo!["master_user_id"] as? Int
            }
            
            if !(notification.userInfo!["dialog_photo"] is NSNull) && notification.userInfo!["dialog_photo"] != nil && (notification.userInfo!["dialog_photo"] as! String) != "" {
                
                messagesVC.avatarPath = notification.userInfo!["dialog_photo"] as? String
            } else if !(notification.userInfo!["photo"] is NSNull) && notification.userInfo!["photo"] != nil && (notification.userInfo!["photo"] as! String) != "" && (notification.userInfo!["members"] as! [Dictionary<String, AnyObject>]).count == 2 {
                messagesVC.avatarPath = notification.userInfo!["photo"] as? String
            }
            
            // тут все действия
            ((self.window?.rootViewController as! MainContainer).selectedViewController as! UINavigationController).pushViewController(messagesVC, animated: true)
        }
    }
    
    // ----------------------------------------------------------------------
    // ------------------------------ 3DTouch -------------------------------
    // ----------------------------------------------------------------------
    
    // создадим быстрые ярлыки
    func createDynamicShortcutItems() {
        let addPinShortcut : UIApplicationShortcutItem = UIApplicationShortcutItem(type: "com.makushov.help.addPin", localizedTitle: "Оставить метку", localizedSubtitle: nil, icon: UIApplicationShortcutIcon(type: UIApplicationShortcutIconType.compose), userInfo: nil)
        
        
        UIApplication.shared.shortcutItems = [addPinShortcut]
    }
    
    // обработаем нажатие
    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        
        completionHandler(self.goToPinCreation(shortcutItem))
    }
    
    // перейдем к созданию метки
    func goToPinCreation(_ item: UIApplicationShortcutItem) -> Bool {
        var succeeded = false
        
        if(item.type == "com.makushov.help.addPin") {
            let mapController : AppleMap = (mainContainer?.viewControllers![0] as! UINavigationController).viewControllers[0] as! AppleMap
            
            mapController.performSegue(withIdentifier: "goToPostMessage", sender: mapController)
            
            succeeded = true
        }
        
        return succeeded
    }
    
    // ----------------------------------------------------------------------
    // ----------------------------- /3DTouch -------------------------------
    // ----------------------------------------------------------------------

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        guard let shortcut = self.shortcutItem else { return }
        
        self.goToPinCreation(shortcut)
        
        self.shortcutItem = nil
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

