//
//  AccountView.swift
//  online
//
//  Created by KingOP on 27.10.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit
import Haneke
import SafariServices

class AccountView: UITableViewController, UITextViewDelegate, SFSafariViewControllerDelegate {

    @IBOutlet weak var login: UILabel!
    @IBOutlet weak var status: UITextView!
    @IBOutlet weak var avatar: UIImageView!
    
    var userInfo : NSDictionary = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.status.delegate = self
        self.login.text = getUserLogin()
        self.avatar.image = maskedAvatarFromImage(self.avatar.image!)
        SwiftSpinner.show("Загрузка...", animated: true, viewToDisplay: self.view)
        
        let getUserRequest = getURLRequest("GET", url: getBackendApiPath() + "user/\(getUserId()!)")
        let getUserTask = URLSession.shared.dataTask(with: getUserRequest, completionHandler: { (data, response, error) in
            
            if (error != nil) {
                // сюда надо добавить обработку ошибок
                DispatchQueue.main.async(execute: {
                    SwiftSpinner.hide()
                    
                    JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                })
            } else {
                let userInfoResponse = response as! HTTPURLResponse
                
                let userInfoResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                
                if userInfoResponse.statusCode == 401 {
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        
                        JSSAlertView().danger(self, title: "Ошибка", text: userInfoResult!["message"] as? String, buttonText: "Закрыть")
                    })
                } else {
                    self.userInfo = userInfoResult!
                    
                    DispatchQueue.main.async(execute: {
                        // обновим данные пользователя в окне
                        self.login.text = userInfoResult!["username"] as? String
                        
                        if !(userInfoResult!["profileStatus"] is NSNull) && userInfoResult!["profileStatus"] != nil {
                            self.status.text = (userInfoResult!["profileStatus"] as! NSDictionary)["status"] as! String
                        } else {
                            self.status.text = ""
                        }
                        
                        if self.login.text != getUserLogin() {
                            setUserLogin(self.login.text!)
                        }
                        
                        // вот начинается мутня с синхронизацией пользовательской аватарки
                        // для начала проверим, есть ли ава у полученного пользователя
                        if !(userInfoResult!["photo"] is NSNull) && userInfoResult!["photo"] != nil && (userInfoResult!["photo"] as? String) != "" {
                            
                            let cache = Shared.imageCache
                            
                            let iconFormat = Format<UIImage>(name: "icons", diskCapacity: 10 * 1024 * 1024) { image in
                                return maskedAvatarFromImage(image, original: true)
                            }
                            cache.addFormat(iconFormat)
                            
                            self.avatar.hnk_setImageFromURL(URL(string: getBackendDomainPath() + (userInfoResult!["photo"] as? String)!)!, placeholder: UIImage(named: "nophoto.jpg"), format: iconFormat)
                        }
                        SwiftSpinner.hide()
                    })
                }
            }
        }) 
        
        getUserTask.resume()
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        if URL.scheme == "http" || URL.scheme == "https" {
            
            if URL.host! == backendDomain && Int(URL.lastPathComponent)! != 0 {
                
                return true
            }
            
            let vc = SFSafariViewController(url: URL, entersReaderIfAvailable: true)
            
            vc.delegate = self
            
            present(vc, animated: true, completion: nil)
            
            return false
        }
        
        return true
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath as NSIndexPath).section == 3 && (indexPath as NSIndexPath).row == 0 {
            let confirmAlert : UIAlertController = UIAlertController(title: "Подтверждение", message: "Вы действительно хотите выйти?", preferredStyle: UIAlertControllerStyle.actionSheet)
            
            confirmAlert.addAction(UIAlertAction(title: "Выйти", style: UIAlertActionStyle.destructive, handler: { (UIAlertAction) -> Void in
                
                SwiftSpinner.show("Выход...", animated: true, viewToDisplay: self.view)
                
                let unsubscribeFromPushesRequest = getURLRequest("POST", url: getBackendApiPath() + "push/unsubscribe", params: ["device_id" : deviceUDID], token: getUserToken()!)
                
                let unsubscribeFromPushesTask = URLSession.shared.dataTask(with: unsubscribeFromPushesRequest, completionHandler: {
                    (data, response, error) in
                    
                    if error == nil {
                        let pushRegisterResponse = response as! HTTPURLResponse
                        
                        if pushRegisterResponse.statusCode == 204 || pushRegisterResponse.statusCode == 403 || pushRegisterResponse.statusCode == 401 {
                            
                            DispatchQueue.main.async(execute: {
                                let logoutRequest = getURLRequest("POST", url: getBackendApiPath() + "user/logout", params: ["token" : getUserToken()!], token: getUserToken()!)
                                
                                let logoutTask = URLSession.shared.dataTask(with: logoutRequest, completionHandler: { (data, response, error) in
                                    
                                    if (error != nil) {
                                        // сюда надо добавить обработку ошибок
                                        DispatchQueue.main.async(execute: {
                                            SwiftSpinner.hide()
                                            
                                            JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                                        })
                                    } else {
                                        let logoutResponse = response as! HTTPURLResponse
                                        print(logoutResponse.statusCode)
                                        
                                        if logoutResponse.statusCode ==  204 || logoutResponse.statusCode == 200 {
                                            DispatchQueue.main.async(execute: {
                                                SwiftSpinner.hide()
                                                
                                                UserDefaults.standard.set(false, forKey: "isRegisteredForPushes")
                                                
                                                logout()
                                                self.dismiss(animated: true, completion: nil)
                                            })
                                        } else if logoutResponse.statusCode == 401 {
                                            let tokenResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                                            
                                            DispatchQueue.main.async(execute: {
                                                SwiftSpinner.hide()
                                                
                                                JSSAlertView().danger(self, title: "Ошибка", text: tokenResult!["message"] as? String, buttonText: "Закрыть")
                                            })
                                        } else if logoutResponse.statusCode == 500 {
                                            DispatchQueue.main.async(execute: {
                                                SwiftSpinner.hide()
                                                
                                                JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                                            })
                                        }
                                    }
                                }) 
                                
                                logoutTask.resume()
                            })
                        } else {
                            DispatchQueue.main.async(execute: {
                                SwiftSpinner.hide()
                                
                                JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                            })
                        }
                    } else {
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            
                            JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                        })
                    }
                }) 
                
                unsubscribeFromPushesTask.resume()
            }))
            
            confirmAlert.addAction(UIAlertAction(title: "Отмена", style: UIAlertActionStyle.cancel, handler: {
                (UIAlertAction) -> Void in
                
                self.tableView.deselectRow(at: indexPath, animated: true)
            }))
            
            self.present(confirmAlert, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.setAnimationsEnabled(true)
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    @IBAction func closeAction(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToStatusUpdate" {
            let destVC : UpdateStatus = segue.destination as! UpdateStatus
            destVC.viewAccountController = self
            destVC.currentStatus = self.status.text!
        } else if segue.identifier == "goToProfileEdit" {
            let destVC : EditProfile = segue.destination as! EditProfile
            destVC.currentUserInfo = self.userInfo as! NSMutableDictionary
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
}
