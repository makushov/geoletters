//
//  AvatarsCacheDataStack.swift
//  online
//
//  Created by KingOP on 30.10.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import Foundation
import CoreData

class AvatarsCacheDataStack {
    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "kingop.MegainfoCMSApp" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1] as URL
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "AvatarsCache", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("online.sqlite")//NSFileManager.defaultManager().containerURLForSecurityApplicationGroupIdentifier("group.com.megainfo.migo")?.URLByAppendingPathComponent("MegainfoCMSApp.sqlite")
        var error: NSError? = nil
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator!.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch var error1 as NSError {
            error = error1
            coordinator = nil
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            dict[NSUnderlyingErrorKey] = error
            error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(error), \(error!.userInfo)")
            abort()
        } catch {
            fatalError()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext? = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        if coordinator == nil {
            return nil
        }
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    func saveAvatarToCache(_ id: Int, fileName: String, imageData: Data) -> Bool {
        
        let newImg = NSEntityDescription.insertNewObject(forEntityName: "OfflineAvatars", into: managedObjectContext!) as! OfflineAvatars
        
        newImg.id = id.description
        let fileName = saveImageFile(fileName, image: imageData)
        
        if fileName != nil {
            newImg.fileName = fileName
                
            saveContext()
            
            print("avatar cached")
            
            return true
        }
        
        return false
    }
    
    func updateAvatarInCache(_ id: Int, fileName: String, imageData: Data) -> Bool {
        self.removeAvatarFromCache(id)
        print("avatar updated")
        return self.saveAvatarToCache(id, fileName: fileName, imageData: imageData)
    }
    
    func saveContext () {
        if let moc = self.managedObjectContext {
            if moc.hasChanges {
                do {
                    try moc.save()
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func isAvatarInCache(_ id : Int) -> Bool {
        let predicate : NSPredicate = NSPredicate(format: "id == %@", id.description)
        
        let fetchRequest : NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "OfflineAvatars")
        fetchRequest.predicate = predicate
        
        do {
            let fetchResults = try managedObjectContext!.fetch(fetchRequest) as? [NSManagedObject]
            
            if fetchResults!.count > 0 {
                return true
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        return false
    }
    
    func getAvatarImageFromCache(_ id: Int) -> UIImage? {
        let predicate : NSPredicate = NSPredicate(format: "id == %@", id.description)
        
        let fetchRequest : NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "OfflineAvatars")
        fetchRequest.predicate = predicate
        
        do {
            let fetchResults = try managedObjectContext!.fetch(fetchRequest) as? [NSManagedObject]
            
            if fetchResults!.count > 0 {
                
                let paths : NSArray = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
                let documentsDirectory : NSString = paths.object(at: 0)  as! NSString
                
                let filename = (fetchResults?.first?.value(forKey: "fileName"))
                
                if filename == nil {
                    self.safeRemoveAvatarFromCache(id)
                    return nil
                } else {
                    let fullPath : String = documentsDirectory.appendingPathComponent((filename)! as! String)
                    
                    if FileManager.default.fileExists(atPath: fullPath) {
                        return UIImage(named: fullPath as String)!
                    } else {
                        return nil
                    }
                }
            }
        } catch let error as NSError {
            print(error.localizedDescription)
            return nil
        }
        
        return nil
    }
    
    func getAvatarFileNameFromCache(_ id: Int) -> String {
        let predicate : NSPredicate = NSPredicate(format: "id == %@", id.description)
        
        let fetchRequest : NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "OfflineAvatars")
        fetchRequest.predicate = predicate
        
        do {
            let fetchResults = try managedObjectContext!.fetch(fetchRequest) as? [NSManagedObject]
            
            if fetchResults!.count > 0 {
                
                let paths : NSArray = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
                let documentsDirectory : NSString = paths.object(at: 0)  as! NSString
                
                let fullPath = documentsDirectory.appendingPathComponent((fetchResults?.first?.value(forKey: "fileName"))! as! String)
                
                if FileManager.default.fileExists(atPath: fullPath) {
                    return fullPath as String
                } else {
                    return "nophoto.jpg"
                }
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        return "nophoto.jpg"
    }
    
    func removeAvatarFromCache(_ id : Int) {
        let predicate : NSPredicate = NSPredicate(format: "id == %@", id.description)
        
        let fetchRequest : NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "OfflineAvatars")
        fetchRequest.predicate = predicate
        
        do {
            let fetchResults = try managedObjectContext!.fetch(fetchRequest) as? [NSManagedObject]
            
            if fetchResults!.count > 0 {
                if deleteImageFile((fetchResults?.first?.value(forKey: "fileName"))! as! String) == true {
                    managedObjectContext?.delete((fetchResults?.first)!)
                }
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        saveContext()
    }
    
    func safeRemoveAvatarFromCache(_ id : Int) {
        let predicate : NSPredicate = NSPredicate(format: "id == %@", id.description)
        
        let fetchRequest : NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "OfflineAvatars")
        fetchRequest.predicate = predicate
        
        do {
            let fetchResults = try managedObjectContext!.fetch(fetchRequest) as? [NSManagedObject]
            
            if fetchResults!.count > 0 {
                managedObjectContext?.delete((fetchResults?.first)!)
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        saveContext()
    }
    
    func removeAllAvatarsFromCache() {
        let fetchRequest : NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "OfflineAvatars")
        
        do {
            let fetchResults = try managedObjectContext!.fetch(fetchRequest) as? [NSManagedObject]
            
            if fetchResults!.count > 0 {
                
                for item in fetchResults! {
                    if item.value(forKey: "fileName") == nil {
                        managedObjectContext?.delete(item)
                    } else {
                        deleteImageFile(item.value(forKey: "fileName") as! String)
                        managedObjectContext?.delete(item)
                    }
                }
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        saveContext()
    }

}
