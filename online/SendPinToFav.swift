//
//  SendPinToFav.swift
//  Letters 1.1
//
//  Created by KingOP on 05.02.16.
//  Copyright © 2016 KingOP. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import TSMessages

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class SendPinToFav: UITableViewController, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource {
    
    var pinId : Int = 0
    
    var favs : [Dictionary<String, AnyObject>] = []
    
    var favsLoaded : Bool = false
    
    var usersLimit : Int = 30
    var currentFavsOffset : Int = 0
    
    var totalFavsCount : Int = 0
    
    var allowLoadingMoreFavs : Bool = true
    
    var isReloading : Bool = false
    
    var loadingMoreView : UIView = UIView()
    var animationView : UIActivityIndicatorView = UIActivityIndicatorView()
    
    var loadMoreFavs : Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()

        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(SendPinToFav.reloadFavs), for: UIControlEvents.valueChanged)
        self.refreshControl = refreshControl
        
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        self.loadingMoreView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 60))
        self.animationView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        
        self.loadingMoreView.addSubview(animationView)
        
        self.animationView.center = self.loadingMoreView.center
        
        self.tableView.tableFooterView = loadingMoreView
        self.tableView.tableFooterView!.isHidden = true
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        self.tableView.emptyDataSetDelegate = self
        self.tableView.emptyDataSetSource = self
        
        self.loadFavs()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func closeAction(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }

    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.favs.count
    }

    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "favForPinSend", for: indexPath)
        
        cell.imageView!.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!, original: true)
        
        cell.textLabel?.text = self.favs[indexPath.row]["user"]?["username"] as? String
        
        if !(self.favs[indexPath.row]["user"]?["photo"] is NSNull) && self.favs[indexPath.row]["user"]?["photo"] as! String != "" {
            
            let urlString = "\(getBackendDomainPath())\(self.favs[indexPath.row]["user"]?["photo"] as! String)"
            let imgURL = URL(string: urlString)
            
            if let img = avatarCache[urlString] {
                cell.imageView!.image = img
                cell.imageView!.tag = (indexPath.row + 1)
            }
            else {
                // The image isn't cached, download the img data
                // We should perform this in a background thread
                let request: URLRequest = URLRequest(url: imgURL!)
                let mainQueue = OperationQueue.main
                NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
                    if error == nil {
                        
                        let res = response as! HTTPURLResponse
                        if res.statusCode != 404 {
                            // Convert the downloaded data in to a UIImage object
                            
                            if data != nil {
                                let image : UIImage = maskedAvatarFromImage(UIImage(data: data!)!)
                                // Store the image in to our cache
                                avatarCache[urlString] = image
                                
                                // Update the cell
                                DispatchQueue.main.async(execute: {
                                    if let cellToUpdate = tableView.cellForRow(at: indexPath) {
                                        cellToUpdate.imageView!.image = image
                                        cellToUpdate.imageView!.tag = ((indexPath as NSIndexPath).row + 1)
                                    }
                                })
                            } else {
                                DispatchQueue.main.async(execute: {
                                    if let cellToUpdate = tableView.cellForRow(at: indexPath) {
                                        cell.imageView?.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!, original: true)
                                        cellToUpdate.imageView!.tag = ((indexPath as NSIndexPath).row + 1)
                                    }
                                })
                            }
                        } else {
                            DispatchQueue.main.async(execute: {
                                if let cellToUpdate = tableView.cellForRow(at: indexPath) {
                                    cell.imageView?.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!, original: true)
                                    cellToUpdate.imageView!.tag = ((indexPath as NSIndexPath).row + 1)
                                }
                            })
                        }
                    }
                    else {
                        DispatchQueue.main.async(execute: {
                            if let cellToUpdate = tableView.cellForRow(at: indexPath) {
                                cell.imageView?.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!, original: true)
                                cellToUpdate.imageView!.tag = ((indexPath as NSIndexPath).row + 1)
                            }
                        })
                    }
                })
            }
        } else {
            cell.imageView?.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!)
            cell.imageView!.tag = (indexPath as NSIndexPath).row + 1
        }
        
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alertController = UIAlertController(title: "Комментарий", message: nil, preferredStyle: UIAlertControllerStyle.alert)
        
        let reportAction = UIAlertAction(title: "Отправить метку", style: UIAlertActionStyle.default, handler: {
            alert -> Void in
            
            SwiftSpinner.show("Отправка...", animated: true, viewToDisplay: self.view)
            
            let firstTextField = alertController.textFields![0] as UITextField
            
            let commentText : String = firstTextField.text!.trimmingCharacters(in: CharacterSet.whitespaces)
            
            let pinURL : String = "https://geoletters.com/pin/\(self.pinId)"
            
            let messageText : String = commentText.length > 0 ? "\(commentText)\n\(pinURL)" : pinURL
            
            let userId : Int = self.favs[indexPath.row]["user"]?["id"] as! Int
            
            // а вот тут пиздец...
            let isDialog : Bool = true
            var dialogId : Int = 0
            var members : [Dictionary<String, AnyObject>] = []
            
            let checkDialogRequest = getURLRequest("GET", url: getBackendApiPath() + "dialog/check?user_id=\(userId)&expand=members", params: Dictionary<String, AnyObject>()/*["expand" : "members"]*/, token: getUserToken()!)
            
            let checkDialogTask = URLSession.shared.dataTask(with: checkDialogRequest, completionHandler: { (data, response, error) -> Void in
                
                if error != nil {
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        
                        TSMessage.showNotification(in: self,
                            title: "Ошибка",
                            subtitle: error?.localizedDescription,
                            type: TSMessageNotificationType.error,
                            duration: 2.0,
                            canBeDismissedByUser: true
                        )
                        playErrorSound()
                    })
                } else {
                    let res = response as! HTTPURLResponse
                    
                    if res.statusCode == 200 {
                        
                        let checkInfoResult = (try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [Dictionary<String, AnyObject>])![0]
                        
                        DispatchQueue.main.async(execute: {
                            dialogId = checkInfoResult["id"] as! Int
                            members = checkInfoResult["members"] as! [Dictionary<String, AnyObject>]
                            
                            let params = [
                                "message" : messageText
                            ]
                            
                            let sendMessageRequest = getURLRequest("POST", url: getBackendApiPath() + "dialog/\(dialogId)/messages", params: params, token: getUserToken())
                            
                            let sendMessageTask = URLSession.shared.dataTask(with: sendMessageRequest, completionHandler: { (data, response, error) in
                                
                                if (error != nil) {
                                    // сюда надо добавить обработку ошибок
                                    DispatchQueue.main.async(execute: {
                                        SwiftSpinner.hide()
                                        
                                        TSMessage.showNotification(in: self,
                                            title: "Ошибка",
                                            subtitle: error?.localizedDescription,
                                            type: TSMessageNotificationType.error,
                                            duration: 2.0,
                                            canBeDismissedByUser: true
                                        )
                                        playErrorSound()
                                    })
                                } else {
                                    let sendMessageResponse = response as! HTTPURLResponse
                                    
                                    print(sendMessageResponse.statusCode)
                                    
                                    if sendMessageResponse.statusCode == 201 {
                                        // сообщение успешно отправлено
                                        let message = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, AnyObject>
                                        
                                        DispatchQueue.main.async(execute: {
                                            SwiftSpinner.hide()
                                            playSendSound()
                                            
                                            // тут надо обновить список диалогов, если он был открыт
                                            if dialogsController != nil {
                                                // получим индекс
                                                let index = dialogsController?.dialogIds.index(of: dialogId)
                                                
                                                if index != nil {
                                                    // текущий диалог есть в списке диалогов, обновим его
                                                    var dialog : Dictionary<String, AnyObject> = dialogsController!.dialogs[index!]
                                                    
                                                    if dialog["message"] == nil {
                                                        let new_message : Dictionary<String, AnyObject> = [
                                                            "user_id"       : getUserId()! as AnyObject,
                                                            "created_at"    : message["created_at"]!,
                                                            "viewed"        : 0 as AnyObject,
                                                            "message"       : message["message"]!
                                                        ]
                                                        
                                                        dialog["message"] = new_message as AnyObject?
                                                    } else {
                                                        var new_message : Dictionary<String, AnyObject> = dialog["message"] as! Dictionary<String, AnyObject>
                                                        
                                                        new_message["user_id"] = getUserId()! as AnyObject?
                                                        new_message["created_at"] = message["created_at"] as! Double as AnyObject?
                                                        new_message["viewed"] = 0 as AnyObject?
                                                        new_message["message"] = message["message"] as! String as AnyObject?
                                                        
                                                        dialog["message"] = new_message as AnyObject?
                                                    }
                                                    
                                                    dialog["unread"] = "0" as AnyObject?
                                                    
                                                    // сделаем перемещение
                                                    dialogsController!.dialogs.remove(at: index!)
                                                    dialogsController!.dialogIds.remove(at: index!)
                                                    
                                                    dialogsController!.dialogs.insert(dialog, at: 0)
                                                    dialogsController!.dialogIds.insert(dialogId, at: 0)
                                                } else {
                                                    // диалога не было, его надо создать и впихнуть
                                                    
                                                    let new_message : Dictionary<String, AnyObject> = [
                                                        "user_id"       : getUserId()! as AnyObject,
                                                        "created_at"    : message["created_at"]!,
                                                        "viewed"        : 1 as AnyObject,
                                                        "message"       : message["message"]!,
                                                        "attachment"    : NSNull()
                                                    ]
                                                    
                                                    let dialog : Dictionary<String, AnyObject> = [
                                                        "message"           : new_message as AnyObject,
                                                        "id"                : dialogId as AnyObject,
                                                        "unread"            : 0 as AnyObject,
                                                        "master_user_id"    : getUserId()! as AnyObject,
                                                        "members"           : members as AnyObject
                                                    ]
                                                    
                                                    // сделаем перемещение
                                                    dialogsController!.dialogs.insert(dialog, at: 0)
                                                    dialogsController!.dialogIds.insert(dialogId, at: 0)
                                                    dialogsController!.currentOffset += 1
                                                }
                                            }
                                            
                                            let info = JSSAlertView().info(self, title: "Успешно", text: "Метка успешно отправлена", buttonText: "Продолжить")
                                            
                                            info.addAction({ () -> Void in
                                                self.dismiss(animated: true, completion: nil)
                                            })
                                        })
                                    } else if sendMessageResponse.statusCode == 401 {
                                        error401Action(self)
                                    } else if sendMessageResponse.statusCode == 403 {
                                        DispatchQueue.main.async(execute: {
                                            SwiftSpinner.hide()
                                            
                                            self.view.endEditing(true)
                                            
                                            TSMessage.showNotification(in: self,
                                                title: "Ошибка",
                                                subtitle: "Вы находитесь в черном списке у этого пользователя и не можете отправлять ему сообщения.",
                                                type: TSMessageNotificationType.error,
                                                duration: 2.0,
                                                canBeDismissedByUser: true
                                            )
                                            playErrorSound()
                                        })
                                    } else if sendMessageResponse.statusCode == 500 {
                                        DispatchQueue.main.async(execute: {
                                            SwiftSpinner.hide()
                                            
                                            self.view.endEditing(true)
                                            SwiftSpinner.hide()
                                            
                                            TSMessage.showNotification(in: self,
                                                title: "Ошибка",
                                                subtitle: error500,
                                                type: TSMessageNotificationType.error,
                                                duration: 2.0,
                                                canBeDismissedByUser: true
                                            )
                                            playErrorSound()
                                        })
                                    }
                                }
                            }) 
                            
                            sendMessageTask.resume()
                        })
                        
                    } else if res.statusCode == 204 {
                        DispatchQueue.main.async(execute: {
                            let params = [
                                "users"     : userId.description,
                            ]
                            
                            let createDialogRequest : URLRequest = getURLRequest("POST", url: getBackendApiPath() + "dialog", params: params, token: getUserToken())
                            
                            let createDialogTask = URLSession.shared.dataTask(with: createDialogRequest, completionHandler: { (data, response, error) in
                                
                                if (error != nil) {
                                    // сюда надо добавить обработку ошибок
                                    DispatchQueue.main.async(execute: {
                                        SwiftSpinner.hide()
                                        
                                        TSMessage.showNotification(in: self,
                                            title: "Ошибка",
                                            subtitle: error?.localizedDescription,
                                            type: TSMessageNotificationType.error,
                                            duration: 2.0,
                                            canBeDismissedByUser: true
                                        )
                                        playErrorSound()
                                    })
                                } else {
                                    let infoResponse = response as! HTTPURLResponse
                                    
                                    let infoResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                                    
                                    if infoResponse.statusCode == 401 {
                                        error401Action(self)
                                    } else if infoResponse.statusCode == 422 {
                                        DispatchQueue.main.async(execute: {
                                            SwiftSpinner.hide()
                                            
                                            self.view.endEditing(true)
                                            
                                            TSMessage.showNotification(in: self,
                                                title: "Ошибка",
                                                subtitle: infoResult!["message"] as? String,
                                                type: TSMessageNotificationType.error,
                                                duration: 2.0,
                                                canBeDismissedByUser: true
                                            )
                                            playErrorSound()
                                        })
                                    } else if infoResponse.statusCode == 500 {
                                        DispatchQueue.main.async(execute: {
                                            SwiftSpinner.hide()
                                            
                                            self.view.endEditing(true)
                                            
                                            TSMessage.showNotification(in: self,
                                                title: "Ошибка",
                                                subtitle: error500,
                                                type: TSMessageNotificationType.error,
                                                duration: 2.0,
                                                canBeDismissedByUser: true
                                            )
                                            playErrorSound()
                                        })
                                    } else if infoResponse.statusCode == 403 {
                                        DispatchQueue.main.async(execute: {
                                            SwiftSpinner.hide()
                                            
                                            self.view.endEditing(true)
                                            
                                            TSMessage.showNotification(in: self,
                                                title: "Ошибка",
                                                subtitle: "Вы находитесь в черном списке у этого пользователя и не можете отправлять ему сообщения.",
                                                type: TSMessageNotificationType.error,
                                                duration: 2.0,
                                                canBeDismissedByUser: true
                                            )
                                            playErrorSound()
                                        })
                                    } else {
                                        // диалог успешно создан
                                        // отправим в него сообщение
                                        dialogId = infoResult!["id"] as! Int
                                        members = infoResult!["members"] as! [Dictionary<String, AnyObject>]
                                        
                                        let params = [
                                            "message" : messageText
                                        ]
                                        
                                        let sendMessageRequest = getURLRequest("POST", url: getBackendApiPath() + "dialog/\(dialogId)/messages", params: params, token: getUserToken())
                                        
                                        let sendMessageTask = URLSession.shared.dataTask(with: sendMessageRequest, completionHandler: { (data, response, error) in
                                            
                                            if (error != nil) {
                                                // сюда надо добавить обработку ошибок
                                                DispatchQueue.main.async(execute: {
                                                    SwiftSpinner.hide()
                                                    
                                                    TSMessage.showNotification(in: self,
                                                        title: "Ошибка",
                                                        subtitle: error?.localizedDescription,
                                                        type: TSMessageNotificationType.error,
                                                        duration: 2.0,
                                                        canBeDismissedByUser: true
                                                    )
                                                    playErrorSound()
                                                })
                                            } else {
                                                let sendMessageResponse = response as! HTTPURLResponse
                                                
                                                print(sendMessageResponse.statusCode)
                                                
                                                if sendMessageResponse.statusCode == 201 {
                                                    // сообщение успешно отправлено
                                                    let message = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, AnyObject>
                                                    
                                                    DispatchQueue.main.async(execute: {
                                                        SwiftSpinner.hide()
                                                        playSendSound()
                                                        
                                                        if dialogsController != nil {
                                                            // диалога не было, его надо создать и впихнуть
                                                            
                                                            let new_message : Dictionary<String, AnyObject> = [
                                                                "user_id"       : getUserId()! as AnyObject,
                                                                "created_at"    : message["created_at"]!,
                                                                "viewed"        : 1 as AnyObject,
                                                                "message"       : message["message"]!,
                                                                "attachment"    : NSNull()
                                                            ]
                                                            
                                                            let dialog : Dictionary<String, AnyObject> = [
                                                                "message"           : new_message as AnyObject,
                                                                "id"                : dialogId as AnyObject,
                                                                "unread"            : 0 as AnyObject,
                                                                "master_user_id"    : getUserId()! as AnyObject,
                                                                "members"           : members as AnyObject
                                                            ]
                                                            
                                                            // сделаем перемещение
                                                            dialogsController!.dialogs.insert(dialog, at: 0)
                                                            dialogsController!.dialogIds.insert(dialogId, at: 0)
                                                            dialogsController!.currentOffset += 1
                                                        }
                                                        
                                                        let info = JSSAlertView().info(self, title: "Успешно", text: "Метка успешно отправлена", buttonText: "Продолжить")
                                                        
                                                        info.addAction({ () -> Void in
                                                            self.dismiss(animated: true, completion: nil)
                                                        })
                                                    })
                                                } else if sendMessageResponse.statusCode == 401 {
                                                    error401Action(self)
                                                } else if sendMessageResponse.statusCode == 403 {
                                                    DispatchQueue.main.async(execute: {
                                                        SwiftSpinner.hide()
                                                        
                                                        self.view.endEditing(true)
                                                        
                                                        TSMessage.showNotification(in: self,
                                                            title: "Ошибка",
                                                            subtitle: "Вы находитесь в черном списке у этого пользователя и не можете отправлять ему сообщения.",
                                                            type: TSMessageNotificationType.error,
                                                            duration: 2.0,
                                                            canBeDismissedByUser: true
                                                        )
                                                        playErrorSound()
                                                    })
                                                } else if sendMessageResponse.statusCode == 500 {
                                                    DispatchQueue.main.async(execute: {
                                                        SwiftSpinner.hide()
                                                        
                                                        self.view.endEditing(true)
                                                        
                                                        TSMessage.showNotification(in: self,
                                                            title: "Ошибка",
                                                            subtitle: error500,
                                                            type: TSMessageNotificationType.error,
                                                            duration: 2.0,
                                                            canBeDismissedByUser: true
                                                        )
                                                        playErrorSound()
                                                    })
                                                }
                                            }
                                        }) 
                                        
                                        sendMessageTask.resume()
                                    }
                                }
                            }) 
                            
                            createDialogTask.resume()
                        })
                    } else if res.statusCode == 401 {
                        error401Action(self)
                    } else if res.statusCode == 500 {
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            
                            self.view.endEditing(true)
                            
                            TSMessage.showNotification(in: self,
                                title: "Ошибка",
                                subtitle: error500,
                                type: TSMessageNotificationType.error,
                                duration: 2.0,
                                canBeDismissedByUser: true
                            )
                            playErrorSound()
                        })
                    }
                }
            })
            
            checkDialogTask.resume()
            
            if !isDialog {
                
                // мы создаем новый диалог
                
            } else {
                // диалог существует
                // отправляем сообщение в существующий
                
            }
        })
        
        let cancelAction = UIAlertAction(title: "Отмена", style: UIAlertActionStyle.cancel, handler: nil)
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Ваш комментарий..."
        }
        
        alertController.addAction(reportAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func reloadFavs() {
        self.isReloading = true
        
        self.favsLoaded = false
        self.allowLoadingMoreFavs = true
        self.currentFavsOffset = 0
        self.favs.removeAll()
    }
    
    func loadFavs(_ noAnimation : Bool = false, append : Bool = false) {
        self.isReloading = true
        
        if !noAnimation {
            SwiftSpinner.show("Загрузка...", animated: true, viewToDisplay: self.view)
        }
        
        let loadFavsRequest = getURLRequest("GET", url: getBackendApiPath() + "user/favorite?my=1&limit=\(self.usersLimit)&offset=\(self.currentFavsOffset)", params: Dictionary<String,String>(), token: getUserToken()!)
        let loadFavsDataTask = URLSession.shared.dataTask(with: loadFavsRequest, completionHandler: { (data, response, error) in
            
            if (error != nil) {
                // сюда надо добавить обработку ошибок
                DispatchQueue.main.async(execute: {
                    SwiftSpinner.hide()
                    self.refreshControl?.endRefreshing()
                    
                    self.isReloading = false
                    self.loadMoreFavs = false
                    
                    JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                })
            } else {
                let favsResponse = response as! HTTPURLResponse
                
                print(favsResponse.statusCode)
                
                if favsResponse.statusCode == 200 {
                    
                    if !append {
                        self.totalFavsCount = Int(favsResponse.allHeaderFields["X-Pagination-Total-Count"] as! String)!
                    }
                    
                    let favsResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [Dictionary<String, AnyObject>]
                    
                    if favsResult?.count == 0 && !append {
                        // избранных нет вообще ((
                        self.allowLoadingMoreFavs = false
                        self.favs = favsResult!
                    } else if favsResult?.count == 0 && append {
                        // при подгрузке ничего не подгрузилось
                        self.currentFavsOffset = self.favs.count
                        self.allowLoadingMoreFavs = false
                    } else {
                        
                        if !append {
                            // это первая загрузка
                            self.favs = favsResult!
                            
                            if favsResult?.count < self.usersLimit {
                                self.allowLoadingMoreFavs = false
                            }
                        } else {
                            // это подгрузка
                            if favsResult?.count >= self.usersLimit {
                                self.favs.append(contentsOf: favsResult!)
                                self.allowLoadingMoreFavs = true
                            } else if favsResult?.count > 0 && favsResult?.count < self.usersLimit {
                                self.favs.append(contentsOf: favsResult!)
                                self.allowLoadingMoreFavs = false
                            } else if favsResult?.count == 0 {
                                self.allowLoadingMoreFavs = false
                            }
                        }
                    }
                    
                    self.loadMoreFavs = false
                    self.currentFavsOffset = self.favs.count
                    self.favsLoaded = true
                    
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        
                        self.isReloading = false
                        self.loadMoreFavs = false
                        self.tableView.tableFooterView?.isHidden = true
                        
                        self.refreshControl?.endRefreshing()
                        self.tableView.reloadData()
                    })
                } else if favsResponse.statusCode == 401 {
                    error401Action(self)
                } else if favsResponse.statusCode == 500 {
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        self.refreshControl?.endRefreshing()
                        
                        JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                    })
                }
            }
        }) 
        
        loadFavsDataTask.resume()
    }
    
    // бесконечный скролл
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        let deltaOffset = maximumOffset - currentOffset
        
        if deltaOffset <= 0 {
            loadMore()
        }
    }
    
    func loadMore() {
        if (!self.loadMoreFavs && self.allowLoadingMoreFavs && !self.isReloading && self.currentFavsOffset >= self.usersLimit) {
            self.loadMoreFavs = true
            self.isReloading = true
            self.animationView.startAnimating()
            self.tableView.tableFooterView?.isHidden = false
            loadMoreBegin("Загрузка пользователей...",
                loadMoreEnd: {(x:Int) -> () in
            })
        }
    }
    
    func loadMoreBegin(_ newtext:String, loadMoreEnd:@escaping (Int) -> ()) {
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
            self.loadFavs(true, append: true)
            
            DispatchQueue.main.async {
                loadMoreEnd(0)
            }
        }
    }
    
    // ----------- EMPTY DATASET --------------
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let string : String = "Список пуст"
        
        let attributes : [String : AnyObject] = [
            NSFontAttributeName : UIFont.boldSystemFont(ofSize: 32),
            NSForegroundColorAttributeName : UIColor.gray
        ]
        
        return NSAttributedString(string: string, attributes: attributes)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        var string : String = ""
        
        string = "Вы пока никого не добавили в избранные"
        
        let paragraph : NSMutableParagraphStyle = NSMutableParagraphStyle()
        paragraph.lineBreakMode = .byWordWrapping
        paragraph.alignment = .center
        
        let attributes : [String : AnyObject] = [
            NSFontAttributeName : UIFont.systemFont(ofSize: 14),
            NSForegroundColorAttributeName : UIColor.lightGray,
            NSParagraphStyleAttributeName : paragraph
        ]
        
        return NSAttributedString(string: string, attributes: attributes)
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        if self.favs.count == 0 {
            return true
        }
        
        return false
    }
    
    func emptyDataSetShouldAllowTouch(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    // ----------- EMPTY DATASET --------------
}
