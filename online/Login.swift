//
//  Login.swift
//  online
//
//  Created by KingOP on 26.10.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit
import WatchConnectivity

class Login: UITableViewController, UITextFieldDelegate, WCSessionDelegate {

    @IBOutlet weak var login: MKTextField!
    @IBOutlet weak var password: MKTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.login.layer.borderColor = UIColor.clear.cgColor
        self.login.floatingPlaceholderEnabled = true
        self.login.placeholder = "Логин"
        self.login.rippleLayerColor = UIColor.white
        self.login.tintColor = UIColor.MKColor.Blue
        
        self.login.delegate = self
        
        self.password.layer.borderColor = UIColor.clear.cgColor
        self.password.floatingPlaceholderEnabled = true
        self.password.placeholder = "Пароль"
        self.password.rippleLayerColor = UIColor.white
        self.password.tintColor = UIColor.MKColor.Blue
        
        self.password.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.setAnimationsEnabled(true)
        self.tableView.reloadData()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    @IBAction func loginAction(_ sender: AnyObject) {
        self.view.endEditing(true)
        
        let login = self.login.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        let password = self.password.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        
        var error : String = ""
        
        if login == "" {
            error += "Вы не указали логин.\n"
        }
        if password == "" {
            error += "Вы не указали пароль.\n"
        }
        
        if error != "" {
            JSSAlertView().danger(self, title: "Ошибка", text: error, buttonText: "Закрыть")
        } else {
            SwiftSpinner.show("Авторизация...", animated: true, viewToDisplay: self.view)
            
            // ну и вот тут короче пойдет запрос токена
            let tokenParams = [
                "username"      : login,
                "password"      : password,
                "client_id"     : "help_application",
                "client_secret" : "help_application_zx",
                "grant_type"    : "password"
            ]
            
            let request : URLRequest = getURLRequest("POST", url: getBackendApiPath() + "oauth2/token", params: tokenParams)
            
            let getTokenTask = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                
                if (error != nil) {
                    // сюда надо добавить обработку ошибок
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        
                        JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                    })
                } else {
                    let tokenResponse = response as! HTTPURLResponse
                    print(tokenResponse.statusCode)
                    
                    let tokenResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                    
                    
                    if let json = tokenResult {
                        if tokenResponse.statusCode ==  200 {
                            // тут нужно:
                            // 1. Получить инфо о пользователе, чтобы сохранить id
                            let getInfoRequest : URLRequest = getURLRequest("GET", url: getBackendApiPath() + "user/me", params: Dictionary<String, String>(), token: json["access_token"] as? String)
                            
                            let getInfoTask = URLSession.shared.dataTask(with: getInfoRequest, completionHandler: { (data, response, error) in
                                
                                if (error != nil) {
                                    // сюда надо добавить обработку ошибок
                                    DispatchQueue.main.async(execute: {
                                        SwiftSpinner.hide()
                                        
                                        JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                                    })
                                } else {
                                    let infoResponse = response as! HTTPURLResponse
                                    
                                    let infoResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                                    
                                    if infoResponse.statusCode == 401 {
                                        DispatchQueue.main.async(execute: {
                                            SwiftSpinner.hide()
                                            
                                            JSSAlertView().danger(self, title: "Ошибка", text: infoResult!["message"] as? String, buttonText: "Закрыть")
                                        })
                                    } else if infoResponse.statusCode == 500 {
                                        DispatchQueue.main.async(execute: {
                                            SwiftSpinner.hide()
                                            
                                            JSSAlertView().danger(self, title: error500, buttonText: "Закрыть")
                                        })
                                    } else {
                                        // данные получены
                                        authenticateUser(infoResult!["id"] as! Int, token: json["access_token"] as! String, login: infoResult!["username"] as! String)
                                        
                                        if WCSession.isSupported() { //makes sure it's not an iPad or iPod
                                            let watchSession = WCSession.default()
                                            watchSession.delegate = self
                                            watchSession.activate()
                                        }
                                        
                                        DispatchQueue.main.async(execute: {
                                            SwiftSpinner.hide()
                                            
                                            let success = JSSAlertView().info(self, title: "Поздравляем!", text: "Вы успешно авторизованы в системе", buttonText: "Продолжить")
                                            
                                                success.addAction({
                                                    updateNotificationCount()
                                                    
                                                    (UIApplication.shared.delegate as! AppDelegate).createDynamicShortcutItems()
                                                    
                                                self.dismiss(animated: true, completion: nil)
                                                
                                            })
                                        })
                                    }
                                }
                            }) 
                            
                            getInfoTask.resume()
                            
                        } else if tokenResponse.statusCode == 400 || tokenResponse.statusCode == 401 {
                            DispatchQueue.main.async(execute: {
                                SwiftSpinner.hide()
                                
                                JSSAlertView().danger(self, title: "Ошибка", text: json["message"] as? String, buttonText: "Закрыть")
                            })
                        } else if tokenResponse.statusCode == 500 {
                            DispatchQueue.main.async(execute: {
                                SwiftSpinner.hide()
                                
                                JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                            })
                        }
                    }
                }
            }) 
            
            getTokenTask.resume()
        }
    }
    
    @IBAction func cancelAction(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
        guard error == nil else {
            print(error!.localizedDescription)
            return
        }
        
        //print("\(watchSession.isPaired), \(watchSession.isWatchAppInstalled)")
        if session.isPaired && session.isWatchAppInstalled {
            do {
                let userData = getUserDataForWatchKit()
                try session.updateApplicationContext(userData)
            } catch let error as NSError {
                print(error.description)
            }
        }
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
}
