//
//  CommentsToolbox.swift
//  Letters 1.1
//
//  Created by KingOP on 25.01.16.
//  Copyright © 2016 KingOP. All rights reserved.
//

import UIKit
import SZTextView

class CommentsToolbox: UIView {

    @IBOutlet weak var commentText: SZTextView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "CommentsToolbox", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }

}
