//
//  PinsFilter.swift
//  Letters 1.1
//
//  Created by KingOP on 13.01.16.
//  Copyright © 2016 KingOP. All rights reserved.
//

import UIKit

class PinsFilter: UITableViewController {
    
    var filterArray : [String] = []
    var selectedIndexes : [Bool] = []
    var mapController : AppleMap = AppleMap()
    
    @IBOutlet weak var resetFilterButton: UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        if filterString != nil {
            self.filterArray = filterString!.components(separatedBy: ",")
        }
        
        for _ in pinsCategories {
            self.selectedIndexes.append(true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.resetFilterButton.isEnabled = filterString != nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pinsCategories.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "filterCategoryCell", for: indexPath)
        
        cell.imageView?.image = UIImage(named: pinsCategories[(indexPath as NSIndexPath).row].image)
        cell.textLabel?.text = pinsCategories[(indexPath as NSIndexPath).row].title
        
        if cell.viewWithTag((indexPath as NSIndexPath).row + 1) == nil {
            let switcher : UISwitch = UISwitch(frame: CGRect.zero)
            cell.accessoryView = switcher
            //switcher.onTintColor = UIColor(netHex: 0x015283)
            switcher.addTarget(self, action: #selector(PinsFilter.ChangeState(_:)), for: UIControlEvents.valueChanged)
            
            switcher.tag = (indexPath as NSIndexPath).row + 1
            
            if filterString != nil && self.filterArray.contains((indexPath as NSIndexPath).row.description) {
                self.selectedIndexes[(indexPath as NSIndexPath).row] = true
                switcher.isOn = true
            } else if filterString == nil {
                self.selectedIndexes[(indexPath as NSIndexPath).row] = true
                switcher.isOn = true
            } else {
                self.selectedIndexes[(indexPath as NSIndexPath).row] = false
                switcher.isOn = false
            }
        }
        
        return cell
    }
    
    func ChangeState(_ switcher: UISwitch) {
        let indexPath = (self.tableView.indexPath(for: switcher.superview as! UITableViewCell))!
        
        self.selectedIndexes[(indexPath as NSIndexPath).row] = switcher.isOn
    }
    
    @IBAction func closeAction(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func applyFilter(_ sender: AnyObject) {
        
        filterString = ""
        
        for i in 0...(pinsCategories.count - 1) {
            if self.selectedIndexes[i] == true {
                filterString! += "\(i),"
            }
        }
        
        if filterString!.length > 0 {
            filterString!.remove(at: filterString!.characters.index(before: filterString!.endIndex))
        }
        
        self.mapController.needsFilterUpdate = true
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func resetFilter(_ sender: AnyObject) {
        let confirmController : UIAlertController = UIAlertController(title: "Подтверждение", message: "Сбросить фильтр?", preferredStyle: .actionSheet)
        
        confirmController.addAction(UIAlertAction(title: "Сбросить", style: .destructive, handler: { (action) -> Void in
            
            filterString = nil
            
            self.mapController.needsFilterUpdate = true
            
            self.dismiss(animated: true, completion: nil)
            
        }))
        
        confirmController.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        
        self.present(confirmController, animated: true, completion: nil)
    }
}
