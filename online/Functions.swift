//
//  Functions.swift
//  online
//
//  Created by KingOP on 23.10.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import Foundation
import AudioToolbox
import UIKit

func maskedAvatarFromImage(_ image: UIImage, original: Bool = false) -> UIImage {
    var width : CGFloat = 0
    var height: CGFloat = 0
    
    if !original {
        width = 60
        height = 60
    } else {
        width = image.size.width
        height = image.size.height
    }
    let imageRect : CGRect = CGRect(x: 0, y: 0, width: width, height: height)
    UIGraphicsBeginImageContextWithOptions(imageRect.size, false, 0)
    
    let circlePath : UIBezierPath = UIBezierPath(ovalIn: imageRect)
    circlePath.addClip()
    
    image.draw(in: imageRect)
    let maskedImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return maskedImage!
}

// проверка валидности номера телефона
func isPhoneNumberCorrect(_ value: String) -> Bool {
    if value.length < 7 {
        return false
    }
    
    let charcter  = CharacterSet(charactersIn: "+0123456789()-").inverted
    var filtered : String!
    let inputString : [String] = value.components(separatedBy: charcter)
    filtered = inputString.joined(separator: "")
    return  value == filtered
}

// проверка валидности email
func isEmailCorrect(_ value: String) -> Bool {
    let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: value)
}


// получение текущего времени
func getCurrentTimestamp() -> Double {
    return Date().timeIntervalSince1970.description.doubleValue
}

// получение верхнего ViewController
func getTopController() -> UIViewController {
    var topViewController : UIViewController = UIApplication.shared.keyWindow!.rootViewController!
    
    while ((topViewController.presentedViewController) != nil) {
        topViewController = topViewController.presentedViewController!
    }
    
    return topViewController
}


// ресайз картинки
func imageResize (_ imageObj:UIImage, sizeChange:CGSize)-> UIImage {
    
    let hasAlpha = false
    let scale: CGFloat = 1.0 // Automatically use scale factor of main screen
    
    UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
    imageObj.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
    
    let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return scaledImage!
}

// получение строки с датой из таймштампа
func stringDateFromTimestamp(_ timestamp: TimeInterval) -> String {
    let date : Date = Date(timeIntervalSince1970: timestamp)
    
    let dateFormatter = DateFormatter()
    
    if Calendar.current.isDateInToday(date) {
        dateFormatter.dateStyle = DateFormatter.Style.none//ShortStyle
        dateFormatter.timeStyle = DateFormatter.Style.short//.ShortStyle
    } else {
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.none//.ShortStyle
    }
    
    return dateFormatter.string(from: date)
}


// получение полной строки с датой из таймштампа
func fullStringDateFromTimestamp(_ timestamp: TimeInterval) -> String {
    let date : Date = Date(timeIntervalSince1970: timestamp)
    
    let dateFormatter = DateFormatter()
    
    dateFormatter.dateStyle = DateFormatter.Style.short
    dateFormatter.timeStyle = DateFormatter.Style.short
    
    return dateFormatter.string(from: date)
}


// подписка на пуши
func subscribeForPushes() {
    // push notifications
    let notificationTypes : UIUserNotificationType = [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound]
    
    
    // push notification actions
    let textAction = UIMutableUserNotificationAction()
    textAction.identifier = "TEXT_ACTION"
    textAction.title = "Ответить"
    textAction.activationMode = .background
    textAction.isAuthenticationRequired = true
    textAction.isDestructive = false
    textAction.behavior = .textInput
    
    let markReadAction = UIMutableUserNotificationAction()
    markReadAction.identifier = "MARK_READ_ACTION"
    markReadAction.title = "Прочитано"
    markReadAction.activationMode = .background
    markReadAction.isAuthenticationRequired = true
    markReadAction.isDestructive = false
    
    let replyActions : NSArray = [markReadAction, textAction]
    
    let replyCategories : UIMutableUserNotificationCategory = UIMutableUserNotificationCategory()
    replyCategories.identifier = "REPLY_CATEGORY"
    
    replyCategories.setActions(replyActions as? [UIUserNotificationAction], for: UIUserNotificationActionContext.default)
    replyCategories.setActions(replyActions as? [UIUserNotificationAction], for: UIUserNotificationActionContext.minimal)
    
    let categories = NSSet(object: replyCategories)
    
    let notificationSettings : UIUserNotificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: categories as? Set<UIUserNotificationCategory>)
    
    UIApplication.shared.registerUserNotificationSettings(notificationSettings)
}

func playSendSound() {
    // воспроизведем звук отправленного сообщения
    let fileUrl : URL = URL(fileURLWithPath: "/System/Library/Audio/UISounds/SentMessage.caf")
    var soundId : SystemSoundID = SystemSoundID()
    AudioServicesCreateSystemSoundID(fileUrl as CFURL, &soundId)
    AudioServicesPlaySystemSound(soundId)
}

func playReceivedSound() {
    // воспроизведем звук отправленного сообщения
    let fileUrl : URL = URL(fileURLWithPath: "/System/Library/Audio/UISounds/ReceivedMessage.caf")
    var soundId : SystemSoundID = SystemSoundID()
    AudioServicesCreateSystemSoundID(fileUrl as CFURL, &soundId)
    AudioServicesPlaySystemSound(soundId)
    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
}

func playErrorSound() {
    // воспроизведем звук ошибки
    let fileUrl : URL = URL(fileURLWithPath: "/System/Library/Audio/UISounds/end_record.caf")
    var soundId : SystemSoundID = SystemSoundID()
    AudioServicesCreateSystemSoundID(fileUrl as CFURL, &soundId)
    AudioServicesPlaySystemSound(soundId)
}

func error401Action(_ controller: UIViewController) {
    DispatchQueue.main.async(execute: {
        if controller.isMember(of: Dialogs.self) {
            (controller as! Dialogs).stopActivityAnimating()
            (controller as! Dialogs).refreshControl?.endRefreshing()
        } else if controller.isMember(of: PinDetailView.self) {
            (controller as! PinDetailView).hideSpinner()
        } else if controller.isMember(of: UserProfile.self) {
            (controller as! UserProfile).hideSpinner()
        }
        
        SwiftSpinner.hide()
        logout()
        let errorView = JSSAlertView().danger(controller, title: "Ошибка", text: error401, buttonText: "Войти", cancelButtonText: "Отмена")
        
        errorView.addAction({
            let loginNC : UINavigationController = controller.storyboard?.instantiateViewController(withIdentifier: "loginNC") as! UINavigationController
            
            controller.present(loginNC, animated: true, completion: nil)
        })
    })
}

func updateNotificationCount() {
    let updateCountRequest = getURLRequest("GET", url: "\(getBackendApiPath())dialog/unread-count", params: Dictionary<String, AnyObject>(), token: getUserToken()!)
    
    let updateCountTask = URLSession.shared.dataTask(with: updateCountRequest, completionHandler: {
        (data, response, error) in
        
        if error == nil {
            let res = response as! HTTPURLResponse
            if res.statusCode == 204 {
                DispatchQueue.main.async(execute: {
                    let count : Int = Int(res.allHeaderFields["X-Pagination-Total-Count"] as! String)!
                    UIApplication.shared.applicationIconBadgeNumber = count
                    
                    if mainContainer != nil {
                        mainContainer?.tabBar.items![1].badgeValue = count <= 0 ? nil : count.description
                    }
                })
            }
        } else {
            print(error?.localizedDescription)
        }
    }) 
    
    updateCountTask.resume()
}
