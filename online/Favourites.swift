//
//  Favourites.swift
//  Letters
//
//  Created by KingOP on 17.11.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit
import Haneke
import DZNEmptyDataSet

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class Favourites: UITableViewController, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource, UIViewControllerPreviewingDelegate {
    
    var myFavs : NSMutableArray = NSMutableArray()
    var inFavs : NSMutableArray = NSMutableArray()
    
    var myFavsLoaded : Bool = false
    var inFavsLoaded : Bool = false
    
    var usersLimit : Int = 30
    var currentMyFavsOffset : Int = 0
    var currentInFavsOffset : Int = 0
    
    var totalMyFavsCount : Int = 0
    var totalInFavsCount : Int = 0
    
    var allowLoadingMoreMyFavs : Bool = true
    var allowLoadingMoreInFavs : Bool = true
    
    var isReloading : Bool = false
    
    var loadingMoreView : UIView = UIView()
    var animationView : UIActivityIndicatorView = UIActivityIndicatorView()
    
    var loadMoreMyFavs : Bool = true
    var loadMoreInFavs : Bool = true
    
    var previewVC : UserProfile? = nil
    var selectedIndexPath : IndexPath? = nil

    @IBOutlet weak var favTypeSwitcher: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(Favourites.reloadFavs), for: UIControlEvents.valueChanged)
        self.refreshControl = refreshControl
        
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        self.loadingMoreView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 60))
        self.animationView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        
        self.loadingMoreView.addSubview(animationView)
        
        self.animationView.center = self.loadingMoreView.center
        
        self.tableView.tableFooterView = loadingMoreView
        self.tableView.tableFooterView!.isHidden = true
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        self.tableView.emptyDataSetDelegate = self
        self.tableView.emptyDataSetSource = self
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !isUserAuthenticated() {
            let info = JSSAlertView().info(self, title: "Избранные", text: "Возможность добавлять избранных пользователей доступна только авторизованным пользователям", buttonText: "Войти", cancelButtonText: "Отмена")
            
            info.addAction({ () -> Void in
                self.performSegue(withIdentifier: "goToLoginFromFavs", sender: self)
            })
        } else {
            if self.favTypeSwitcher.selectedSegmentIndex == 0 {
                if !self.myFavsLoaded {
                    self.loadFavs()
                }
            } else {
                if !self.inFavsLoaded {
                    self.loadFavs()
                }
            }
            
            if traitCollection.forceTouchCapability == .available {
                registerForPreviewing(with: self, sourceView: self.tableView)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UIView.setAnimationsEnabled(true)
        
        if (self.inFavsLoaded || self.myFavsLoaded) && !isUserAuthenticated() {
            self.myFavs.removeAllObjects()
            self.inFavs.removeAllObjects()
            self.inFavsLoaded = false
            self.myFavsLoaded = false
            
            self.tableView.reloadData()
        } else {
            if self.tableView.indexPathForSelectedRow != nil {
                self.tableView.deselectRow(at: self.tableView.indexPathForSelectedRow!, animated: true)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "favUser", for: indexPath)
        
        let userKey : String = self.favTypeSwitcher.selectedSegmentIndex == 0 ? "user" : "owner"
        
        cell.imageView!.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!, original: true)
        
        if self.favTypeSwitcher.selectedSegmentIndex == 0 {
            cell.textLabel?.text = self.favTypeSwitcher.selectedSegmentIndex == 0 ? ((self.myFavs[(indexPath as NSIndexPath).row] as! NSDictionary)[userKey] as! NSDictionary)["username"] as? String : ((self.myFavs[(indexPath as NSIndexPath).row] as! NSDictionary)[userKey] as! NSDictionary)["username"] as? String
            
            if !(((self.myFavs[(indexPath as NSIndexPath).row] as! NSDictionary)[userKey] as! NSDictionary)["photo"] is NSNull) && ((self.myFavs[(indexPath as NSIndexPath).row] as! NSDictionary)[userKey] as! NSDictionary)["photo"] as! String != "" {
                
                let urlString = "\(getBackendDomainPath())\(((self.myFavs[(indexPath as NSIndexPath).row] as! NSDictionary)[userKey] as! NSDictionary)["photo"] as! String)"
                let imgURL = URL(string: urlString)
                
                if let img = avatarCache[urlString] {
                    cell.imageView!.image = img
                    cell.imageView!.tag = ((indexPath as NSIndexPath).row + 1)
                }
                else {
                    // The image isn't cached, download the img data
                    // We should perform this in a background thread
                    let request: URLRequest = URLRequest(url: imgURL!)
                    let mainQueue = OperationQueue.main
                    NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
                        if error == nil {
                            
                            let res = response as! HTTPURLResponse
                            if res.statusCode != 404 {
                                // Convert the downloaded data in to a UIImage object
                                
                                if data != nil {
                                    let image : UIImage = maskedAvatarFromImage(UIImage(data: data!)!)
                                    // Store the image in to our cache
                                    avatarCache[urlString] = image
                                    
                                    // Update the cell
                                    DispatchQueue.main.async(execute: {
                                        if let cellToUpdate = tableView.cellForRow(at: indexPath) {
                                            cellToUpdate.imageView!.image = image
                                            cellToUpdate.imageView!.tag = ((indexPath as NSIndexPath).row + 1)
                                        }
                                    })
                                } else {
                                    DispatchQueue.main.async(execute: {
                                        if let cellToUpdate = tableView.cellForRow(at: indexPath) {
                                            cell.imageView?.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!, original: true)
                                            cellToUpdate.imageView!.tag = ((indexPath as NSIndexPath).row + 1)
                                        }
                                    })
                                }
                            } else {
                                DispatchQueue.main.async(execute: {
                                    if let cellToUpdate = tableView.cellForRow(at: indexPath) {
                                        cell.imageView?.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!, original: true)
                                        cellToUpdate.imageView!.tag = ((indexPath as NSIndexPath).row + 1)
                                    }
                                })
                            }
                        }
                        else {
                            DispatchQueue.main.async(execute: {
                                if let cellToUpdate = tableView.cellForRow(at: indexPath) {
                                    cell.imageView?.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!, original: true)
                                    cellToUpdate.imageView!.tag = ((indexPath as NSIndexPath).row + 1)
                                }
                            })
                        }
                    })
                }
            } else {
                cell.imageView?.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!)
                cell.imageView!.tag = (indexPath as NSIndexPath).row + 1
            }
        } else {
            cell.textLabel?.text = self.favTypeSwitcher.selectedSegmentIndex == 0 ? ((self.inFavs[(indexPath as NSIndexPath).row] as! NSDictionary)[userKey] as! NSDictionary)["username"] as? String : ((self.inFavs[(indexPath as NSIndexPath).row] as! NSDictionary)[userKey] as! NSDictionary)["username"] as? String

            if !(((self.inFavs[(indexPath as NSIndexPath).row] as! NSDictionary)[userKey] as! NSDictionary)["photo"] is NSNull) && ((self.inFavs[(indexPath as NSIndexPath).row] as! NSDictionary)[userKey] as! NSDictionary)["photo"] as! String != "" {
                
                let urlString = "\(getBackendDomainPath())\(((self.inFavs[(indexPath as NSIndexPath).row] as! NSDictionary)[userKey] as! NSDictionary)["photo"] as! String)"
                let imgURL = URL(string: urlString)
                
                if let img = avatarCache[urlString] {
                    cell.imageView!.image = img
                    cell.imageView!.tag = ((indexPath as NSIndexPath).row + 1)
                }
                else {
                    // The image isn't cached, download the img data
                    // We should perform this in a background thread
                    let request: URLRequest = URLRequest(url: imgURL!)
                    let mainQueue = OperationQueue.main
                    NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
                        if error == nil {
                            
                            let res = response as! HTTPURLResponse
                            if res.statusCode != 404 {
                                // Convert the downloaded data in to a UIImage object
                                
                                if data != nil {
                                    let image : UIImage = maskedAvatarFromImage(UIImage(data: data!)!)
                                    // Store the image in to our cache
                                    avatarCache[urlString] = image
                                    
                                    // Update the cell
                                    DispatchQueue.main.async(execute: {
                                        if let cellToUpdate = tableView.cellForRow(at: indexPath) {
                                            cellToUpdate.imageView!.image = image
                                            cellToUpdate.imageView!.tag = ((indexPath as NSIndexPath).row + 1)
                                        }
                                    })
                                } else {
                                    DispatchQueue.main.async(execute: {
                                        if let cellToUpdate = tableView.cellForRow(at: indexPath) {
                                            cell.imageView?.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!, original: true)
                                            cellToUpdate.imageView!.tag = ((indexPath as NSIndexPath).row + 1)
                                        }
                                    })
                                }
                            } else {
                                DispatchQueue.main.async(execute: {
                                    if let cellToUpdate = tableView.cellForRow(at: indexPath) {
                                        cell.imageView?.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!, original: true)
                                        cellToUpdate.imageView!.tag = ((indexPath as NSIndexPath).row + 1)
                                    }
                                })
                            }
                        }
                        else {
                            DispatchQueue.main.async(execute: {
                                if let cellToUpdate = tableView.cellForRow(at: indexPath) {
                                    cell.imageView?.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!, original: true)
                                    cellToUpdate.imageView!.tag = ((indexPath as NSIndexPath).row + 1)
                                }
                            })
                        }
                    })
                }
            } else {
                cell.imageView?.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!)
                cell.imageView!.tag = (indexPath as NSIndexPath).row + 1
            }
        }
        
        
        
        return cell
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.favTypeSwitcher.selectedSegmentIndex == 0 ? self.myFavs.count : self.inFavs.count
    }

    @IBAction func favsTypeChange(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            if !self.myFavsLoaded {
                self.loadFavs()
            } else {
                self.tableView.reloadData()
            }
        } else {
            if !self.inFavsLoaded {
                self.loadFavs()
            } else {
                self.tableView.reloadData()
            }
        }
    }
    
    func reloadFavs() {
        self.isReloading = true
        
        if self.favTypeSwitcher.selectedSegmentIndex == 0 {
            self.myFavsLoaded = false
            self.allowLoadingMoreMyFavs = true
            self.currentMyFavsOffset = 0
            self.myFavs.removeAllObjects()
            //self.tableView.reloadData()
            
            self.loadFavs(true)
        } else {
            self.inFavsLoaded = false
            self.allowLoadingMoreInFavs = true
            self.currentInFavsOffset = 0
            self.inFavs.removeAllObjects()
            //self.tableView.reloadData()
            
            self.loadFavs(true)
        }
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // удаление из избранного
            
            let confirmAlertController = UIAlertController(title: "Подтверждение", message: "Удалить из избранных пользователя \"\(((self.myFavs[(indexPath as NSIndexPath).row] as! NSDictionary)["user"] as! NSDictionary)["username"] as! String)\"?", preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let unFavAction : UIAlertAction = UIAlertAction(title: "Удалить", style: UIAlertActionStyle.destructive, handler: { (UIAlertAction) -> Void in
                
                // удалим
                let deleteFromFavRequest = getURLRequest("DELETE", url: getBackendApiPath() + "user/favorite/\(((self.myFavs[(indexPath as NSIndexPath).row] as! NSDictionary)["user"] as! NSDictionary)["id"] as! Int)", params: Dictionary<String, AnyObject>(), token: getUserToken()!)
                
                let deleteFromFavTask = URLSession.shared.dataTask(with: deleteFromFavRequest, completionHandler: {
                    (data, response, error) in
                    
                    if (error != nil) {
                        // сюда надо добавить обработку ошибок
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            
                            JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                        })
                    } else {
                        let res = response as! HTTPURLResponse
                        
                        print(res.statusCode)
                        
                        if res.statusCode == 204 {
                            // пользователь успешно удален из избранных
                            self.myFavs.removeObject(at: (indexPath as NSIndexPath).row)
                            
                            DispatchQueue.main.async(execute: {
                                SwiftSpinner.hide()
                                
                                self.tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.left)
                                
                                if self.favTypeSwitcher.selectedSegmentIndex == 0 && self.myFavs.count == 0 {
                                    self.tableView.reloadData()
                                }
                            })
                        } else if res.statusCode == 401 {
                            error401Action(self)
                        } else if res.statusCode == 404 {
                            DispatchQueue.main.async(execute: {
                                SwiftSpinner.hide()
                                
                                JSSAlertView().danger(self, title: "Ошибка", text: "Данного пользователя нет у вас в избранных", buttonText: "Закрыть")
                            })
                        } else if res.statusCode == 500 {
                            DispatchQueue.main.async(execute: {
                                SwiftSpinner.hide()
                                
                                JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                            })
                        }
                    }
                }) 
                
                SwiftSpinner.show("Удаление...", animated: true, viewToDisplay: self.view)
                deleteFromFavTask.resume()
            })
            
            confirmAlertController.addAction(unFavAction)
            
            let cancelAction : UIAlertAction = UIAlertAction(title: "Отмена", style: UIAlertActionStyle.cancel, handler: {(UIAlertAction) ->
                Void in
                
                self.tableView.setEditing(false, animated: true)
            })
            
            confirmAlertController.addAction(cancelAction)
            
            self.present(confirmAlertController, animated: true, completion: nil)
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if self.favTypeSwitcher.selectedSegmentIndex == 0 {
            return true
        } else {
            return false
        }
    }
    
    func loadFavs(_ noAnimation : Bool = false, append : Bool = false) {
        self.isReloading = true
        
        if !noAnimation {
            SwiftSpinner.show("Загрузка...", animated: true, viewToDisplay: self.view)
        }
        
        print(getBackendApiPath() + "user/favorite?my=\(self.favTypeSwitcher.selectedSegmentIndex == 0 ? 1 : 0)&limit=\(self.usersLimit)&offset=\(self.favTypeSwitcher.selectedSegmentIndex == 0 ? self.currentMyFavsOffset : currentInFavsOffset)")
        
        let loadFavsRequest = getURLRequest("GET", url: getBackendApiPath() + "user/favorite?my=\(self.favTypeSwitcher.selectedSegmentIndex == 0 ? 1 : 0)&limit=\(self.usersLimit)&offset=\(self.favTypeSwitcher.selectedSegmentIndex == 0 ? self.currentMyFavsOffset : currentInFavsOffset)", params: Dictionary<String,AnyObject>(), token: getUserToken()!)
        let loadFavsDataTask = URLSession.shared.dataTask(with: loadFavsRequest, completionHandler: { (data, response, error) in
            
            if (error != nil) {
                // сюда надо добавить обработку ошибок
                DispatchQueue.main.async(execute: {
                    SwiftSpinner.hide()
                    self.refreshControl?.endRefreshing()
                    
                    self.isReloading = false
                    self.loadMoreMyFavs = false
                    self.loadMoreInFavs = false
                    
                    JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                })
            } else {
                let favsResponse = response as! HTTPURLResponse
                
                print(favsResponse.statusCode)
                
                if favsResponse.statusCode == 200 {
                    
                    if !append {
                        if self.favTypeSwitcher.selectedSegmentIndex == 0 {
                            self.totalMyFavsCount = Int(favsResponse.allHeaderFields["X-Pagination-Total-Count"] as! String)!
                        } else {
                            self.totalInFavsCount = Int(favsResponse.allHeaderFields["X-Pagination-Total-Count"] as! String)!
                        }
                    }
                    
                    let favsResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSMutableArray
                    
                    if favsResult?.count == 0 && !append {
                        // избранных нет вообще ((
                        if self.favTypeSwitcher.selectedSegmentIndex == 0 {
                            
                            // нет моих избранных
                            self.allowLoadingMoreMyFavs = false
                            self.myFavs = favsResult!
                        } else {
                            
                            // нет меня ни у кого в избранном((
                            self.allowLoadingMoreInFavs = false
                            self.inFavs = favsResult!
                        }
                    } else if favsResult?.count == 0 && append {
                        // при подгрузке ничего не подгрузилось
                        if self.favTypeSwitcher.selectedSegmentIndex == 0 {
                            
                            self.currentMyFavsOffset = self.myFavs.count
                            self.allowLoadingMoreMyFavs = false
                        } else {
                            self.currentInFavsOffset = self.inFavs.count
                            self.allowLoadingMoreInFavs = false
                        }
                    } else {
                        
                        if !append {
                            // это первая загрузка
                            if self.favTypeSwitcher.selectedSegmentIndex == 0 {
                                self.myFavs = favsResult!
                                
                                if favsResult?.count < self.usersLimit {
                                    self.allowLoadingMoreMyFavs = false
                                }
                            } else {
                                self.inFavs = favsResult!
                                
                                if favsResult?.count < self.usersLimit {
                                    self.allowLoadingMoreInFavs = false
                                }
                            }
                        } else {
                            // это подгрузка
                            if favsResult?.count >= self.usersLimit {
                                if self.favTypeSwitcher.selectedSegmentIndex == 0 {
                                    self.myFavs.addObjects(from: favsResult! as [AnyObject])
                                    self.allowLoadingMoreMyFavs = true
                                } else {
                                    self.inFavs.addObjects(from: favsResult! as [AnyObject])
                                    self.allowLoadingMoreInFavs = true
                                }
                            } else if favsResult?.count > 0 && favsResult?.count < self.usersLimit {
                                if self.favTypeSwitcher.selectedSegmentIndex == 0 {
                                    self.myFavs.addObjects(from: favsResult! as [AnyObject])
                                    self.allowLoadingMoreMyFavs = false
                                } else {
                                    self.inFavs.addObjects(from: favsResult! as [AnyObject])
                                    self.allowLoadingMoreInFavs = false
                                }
                            } else if favsResult?.count == 0 {
                                if self.favTypeSwitcher.selectedSegmentIndex == 0 {
                                    self.allowLoadingMoreMyFavs = false
                                } else {
                                    self.allowLoadingMoreInFavs = false
                                }
                            }
                        }
                    }
                    
                    if self.favTypeSwitcher.selectedSegmentIndex == 0 {
                        self.loadMoreMyFavs = false
                        self.currentMyFavsOffset = self.myFavs.count
                        self.myFavsLoaded = true
                    } else {
                        self.loadMoreInFavs = false
                        self.currentInFavsOffset = self.inFavs.count
                        self.inFavsLoaded = true
                    }
                    
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        
                        self.isReloading = false
                        self.loadMoreMyFavs = false
                        self.loadMoreInFavs = false
                        self.tableView.tableFooterView?.isHidden = true
                        
                        self.refreshControl?.endRefreshing()
                        self.tableView.reloadData()
                    })
                } else if favsResponse.statusCode == 401 {
                    error401Action(self)
                } else if favsResponse.statusCode == 500 {
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        self.refreshControl?.endRefreshing()
                        
                        JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                    })
                }
            }
        }) 
        
        loadFavsDataTask.resume()
    }
    
    // бесконечный скролл
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        let deltaOffset = maximumOffset - currentOffset
        
        if deltaOffset <= 0 {
            loadMore()
        }
    }
    
    func loadMore() {
        if self.favTypeSwitcher.selectedSegmentIndex == 0 {
            if (!self.loadMoreMyFavs && self.allowLoadingMoreMyFavs && !self.isReloading && self.currentMyFavsOffset >= self.usersLimit) {
                self.loadMoreMyFavs = true
                self.isReloading = true
                self.animationView.startAnimating()
                self.tableView.tableFooterView?.isHidden = false
                loadMoreBegin("Загрузка пользователей...",
                    loadMoreEnd: {(x:Int) -> () in
                })
            }
        } else {
            if (!self.loadMoreInFavs && self.allowLoadingMoreInFavs && !self.isReloading && self.currentInFavsOffset >= self.usersLimit) {
                self.loadMoreInFavs = true
                self.isReloading = true
                self.animationView.startAnimating()
                self.tableView.tableFooterView?.isHidden = false
                loadMoreBegin("Загрузка меток...",
                    loadMoreEnd: {(x:Int) -> () in
                })
            }
        }
    }
    
    func loadMoreBegin(_ newtext:String, loadMoreEnd:@escaping (Int) -> ()) {
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
            self.loadFavs(true, append: true)
            
            DispatchQueue.main.async {
                loadMoreEnd(0)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToProfileViewFromFavs" {
            let destVC : UserProfile = segue.destination as! UserProfile
            destVC.userId = self.favTypeSwitcher.selectedSegmentIndex == 0 ? ((self.myFavs[((self.tableView.indexPathForSelectedRow as NSIndexPath?)?.row)!] as! NSDictionary)["user"] as! NSDictionary)["id"] as! Int : ((self.inFavs[((self.tableView.indexPathForSelectedRow as NSIndexPath?)?.row)!] as! NSDictionary)["owner"] as! NSDictionary)["id"] as! Int
        }
    }
    
    // -------------------------------------------------------------
    // ------------------ 3D Touch ---------------------------------
    // -------------------------------------------------------------
    
    // PEEK
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        
        self.previewVC = nil
        
        self.previewVC = self.storyboard?.instantiateViewController(withIdentifier: "userProfile") as? UserProfile
        
        guard let indexPath = self.tableView.indexPathForRow(at: location) else {return nil}
        
        self.selectedIndexPath = indexPath
        
        self.previewVC!.userId = self.favTypeSwitcher.selectedSegmentIndex == 0 ? ((self.myFavs[(indexPath as NSIndexPath).row] as! NSDictionary)["user"] as! NSDictionary)["id"] as! Int : ((self.inFavs[(indexPath as NSIndexPath).row] as! NSDictionary)["owner"] as! NSDictionary)["id"] as! Int
        
        return self.previewVC!
    }
    
    // POP
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        
        let userProfileVC = self.previewVC!
        show(userProfileVC, sender: self)
        self.previewVC = nil
        self.selectedIndexPath = nil
    }
    
    // -------------------------------------------------------------
    // ------------------ /3D Touch --------------------------------
    // -------------------------------------------------------------
    
    // ----------- EMPTY DATASET --------------
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let string : String = "Список пуст"
        
        let attributes : [String : AnyObject] = [
            NSFontAttributeName : UIFont.boldSystemFont(ofSize: 32),
            NSForegroundColorAttributeName : UIColor.gray
        ]
        
        return NSAttributedString(string: string, attributes: attributes)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        var string : String = ""
        
        if self.favTypeSwitcher.selectedSegmentIndex == 0 {
            string = "Вы пока никого не добавили в избранные"
        } else {
            string = "Вас пока никто не добавил в избранные"
        }
        
        let paragraph : NSMutableParagraphStyle = NSMutableParagraphStyle()
        paragraph.lineBreakMode = .byWordWrapping
        paragraph.alignment = .center
        
        let attributes : [String : AnyObject] = [
            NSFontAttributeName : UIFont.systemFont(ofSize: 14),
            NSForegroundColorAttributeName : UIColor.lightGray,
            NSParagraphStyleAttributeName : paragraph
        ]
        
        return NSAttributedString(string: string, attributes: attributes)
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        if self.favTypeSwitcher.selectedSegmentIndex == 0 {
            if self.myFavs.count == 0 {
                return true
            }
        } else {
            if self.inFavs.count == 0 {
                return true
            }
        }
        
        return false
    }
    
    func emptyDataSetShouldAllowTouch(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    // ----------- EMPTY DATASET --------------
}
