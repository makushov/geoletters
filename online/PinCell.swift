//
//  PinCell.swift
//  Letters 1.1
//
//  Created by KingOP on 27.01.16.
//  Copyright © 2016 KingOP. All rights reserved.
//

import UIKit

class PinCell: UITableViewCell {

    @IBOutlet weak var pinTitle: UILabel!
    @IBOutlet weak var pinCatImage: UIImageView!
    @IBOutlet weak var pinDatetime: UILabel!
    @IBOutlet weak var likesCount: UILabel!
    @IBOutlet weak var commentsCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
