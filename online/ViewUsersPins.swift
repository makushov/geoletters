//
//  ViewUsersPins.swift
//  Letters
//
//  Created by KingOP on 09.12.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class ViewUsersPins: UITableViewController, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource, UIViewControllerPreviewingDelegate {
    
    var activePins : NSMutableArray = NSMutableArray()
    var expiredPins : NSMutableArray = NSMutableArray()
    
    var userId : Int = 0

    @IBOutlet weak var pinsTypeSwitcher: UISegmentedControl!
    
    var activeLoaded : Bool = false
    var expiredLoaded : Bool = false
    
    var pinsLimit : Int = 30
    var currentActiveOffset : Int = 0
    var currentExpiredOffset : Int = 0
    
    var totalActiveCount : Int = 0
    var totalExpiredCount : Int = 0
    
    var allowLoadingMoreActive : Bool = true
    var allowLoadingMoreExpired : Bool = true
    
    var isReloading : Bool = false
    
    var loadingMoreView : UIView = UIView()
    var animationView : UIActivityIndicatorView = UIActivityIndicatorView()
    
    var loadMoreActive : Bool = true
    var loadMoreExpired : Bool = true
    
    var previewVC : PinDetailView? = nil
    var selectedIndexPath : IndexPath? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(ViewUsersPins.reloadPins), for: UIControlEvents.valueChanged)
        self.refreshControl = refreshControl
        
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        self.loadingMoreView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 60))
        self.animationView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        
        self.loadingMoreView.addSubview(animationView)
        
        self.animationView.center = self.loadingMoreView.center
        
        self.tableView.tableFooterView = loadingMoreView
        self.tableView.tableFooterView!.isHidden = true
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        //self.tableView.registerNib(UINib(nibName: "PinCell", bundle: nil), forCellReuseIdentifier: "userPin")
        
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        
        if traitCollection.forceTouchCapability == .available {
            registerForPreviewing(with: self, sourceView: self.tableView)
        }
        
        self.loadPins()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.tableView.indexPathForSelectedRow != nil {
            self.tableView.deselectRow(at: self.tableView.indexPathForSelectedRow!, animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        self.selectedIndexPath = nil
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 83//max(60, UITableViewAutomaticDimension)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
            return self.activePins.count
        } else {
            return self.expiredPins.count
        }
    }
    
    @IBAction func changePinsType(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            if !activeLoaded {
                self.loadPins()
            } else {
                self.tableView.reloadData()
            }
        } else {
            if !expiredLoaded {
                self.loadPins()
            } else {
                self.tableView.reloadData()
            }
        }
    }
    
    func reloadPins() {
        self.isReloading = true
        
        if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
            self.activeLoaded = false
            self.allowLoadingMoreActive = true
            self.currentActiveOffset = 0
            //self.activePins.removeAllObjects()
            //self.tableView.reloadData()
            
            self.loadPins(true)
        } else {
            self.expiredLoaded = false
            self.allowLoadingMoreExpired = true
            self.currentExpiredOffset = 0
            //self.expiredPins.removeAllObjects()
            //self.tableView.reloadData()
            
            self.loadPins(true)
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        /*var cell : PinCell? = tableView.dequeueReusableCellWithIdentifier("userPin", forIndexPath: indexPath) as? PinCell
        
        if cell == nil {
            let nib : Array = NSBundle.mainBundle().loadNibNamed("PinCell", owner: self, options: nil)
            cell = nib[0] as? PinCell
        }
        
        cell!.pinTitle.text = self.pinsTypeSwitcher.selectedSegmentIndex == 0 ? (self.activePins[indexPath.row] as! NSDictionary)["status"] as? String : (self.expiredPins[indexPath.row] as! NSDictionary)["status"] as? String
        
        cell!.pinTitle.sizeToFit()
        
        cell!.pinDatetime.text  = stringDateFromTimestamp(self.pinsTypeSwitcher.selectedSegmentIndex == 0 ? (self.activePins[indexPath.row] as! NSDictionary)["created_at"] as! Double : (self.expiredPins[indexPath.row] as! NSDictionary)["created_at"] as! Double)
        
        var imageName : String = "pinIcon"
        
        if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
            if ((self.activePins[indexPath.row] as! NSDictionary)["category_id"] as! Int) < pinsCategories.count {
                imageName = pinsCategories[(self.activePins[indexPath.row] as! NSDictionary)["category_id"] as! Int].image
                
                if ((self.activePins[indexPath.row] as! NSDictionary)["is_special"] as! Int) == 1 {
                    imageName = "\(imageName)Urgency"
                }
            }
        } else {
            if ((self.expiredPins[indexPath.row] as! NSDictionary)["category_id"] as! Int) < pinsCategories.count {
                imageName = pinsCategories[(self.expiredPins[indexPath.row] as! NSDictionary)["category_id"] as! Int].image
                
                if ((self.expiredPins[indexPath.row] as! NSDictionary)["is_special"] as! Int) == 1 {
                    imageName = "\(imageName)Urgency"
                }
            }
        }
        
        cell!.pinCatImage.image = UIImage(named: imageName)
        cell!.likesCount.text = "Понравилось: \(self.pinsTypeSwitcher.selectedSegmentIndex == 0 ? ((self.activePins[indexPath.row] as! NSDictionary)["likes_count"] as! Int).description : ((self.expiredPins[indexPath.row] as! NSDictionary)["likes_count"] as! Int).description)"
        
        cell!.commentsCount.text = "Комментарии: \(self.pinsTypeSwitcher.selectedSegmentIndex == 0 ? ((self.activePins[indexPath.row] as! NSDictionary)["comments_count"] as! Int).description : ((self.expiredPins[indexPath.row] as! NSDictionary)["comments_count"] as! Int).description)"
        
        return cell!*/
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "userPin", for: indexPath)
        
        cell.textLabel?.text = self.pinsTypeSwitcher.selectedSegmentIndex == 0 ? (self.activePins[(indexPath as NSIndexPath).row] as! NSDictionary)["status"] as? String : (self.expiredPins[(indexPath as NSIndexPath).row] as! NSDictionary)["status"] as? String
        
        cell.detailTextLabel?.text = "\(stringDateFromTimestamp(self.pinsTypeSwitcher.selectedSegmentIndex == 0 ? (self.activePins[(indexPath as NSIndexPath).row] as! NSDictionary)["created_at"] as! Double : (self.expiredPins[(indexPath as NSIndexPath).row] as! NSDictionary)["created_at"] as! Double))\nПонравилось: \(self.pinsTypeSwitcher.selectedSegmentIndex == 0 ? ((self.activePins[(indexPath as NSIndexPath).row] as! NSDictionary)["likes_count"] as! Int).description : ((self.expiredPins[(indexPath as NSIndexPath).row] as! NSDictionary)["likes_count"] as! Int).description)\nКомментарии: \(self.pinsTypeSwitcher.selectedSegmentIndex == 0 ? ((self.activePins[(indexPath as NSIndexPath).row] as! NSDictionary)["comments_count"] as! Int).description : ((self.expiredPins[(indexPath as NSIndexPath).row] as! NSDictionary)["comments_count"] as! Int).description)"
        
        
        var imageName : String = "pinIcon"
        
        if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
            if ((self.activePins[(indexPath as NSIndexPath).row] as! NSDictionary)["category_id"] as! Int) < pinsCategories.count {
                imageName = pinsCategories[(self.activePins[(indexPath as NSIndexPath).row] as! NSDictionary)["category_id"] as! Int].image
                
                if ((self.activePins[(indexPath as NSIndexPath).row] as! NSDictionary)["is_special"] as! Int) == 1 {
                    imageName = "\(imageName)Urgency"
                }
            }
        } else {
            if ((self.expiredPins[(indexPath as NSIndexPath).row] as! NSDictionary)["category_id"] as! Int) < pinsCategories.count {
                imageName = pinsCategories[(self.expiredPins[(indexPath as NSIndexPath).row] as! NSDictionary)["category_id"] as! Int].image
                
                if ((self.expiredPins[(indexPath as NSIndexPath).row] as! NSDictionary)["is_special"] as! Int) == 1 {
                    imageName = "\(imageName)Urgency"
                }
            }
        }
        
        cell.imageView?.image = UIImage(named: imageName)
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToPinViewFromUsersPinList" {
            let destVC : PinDetailView = segue.destination as! PinDetailView
            
            if self.tableView.indexPathForSelectedRow != nil {
                self.selectedIndexPath = self.tableView.indexPathForSelectedRow
            }
            
            print((self.selectedIndexPath as NSIndexPath?)?.row)
            
            destVC.pinId = self.pinsTypeSwitcher.selectedSegmentIndex == 0 ? (self.activePins[((self.selectedIndexPath as NSIndexPath?)?.row)!] as! NSDictionary)["id"] as! Int : (self.expiredPins[((self.selectedIndexPath as NSIndexPath?)?.row)!] as! NSDictionary)["id"] as! Int
            
            self.selectedIndexPath = nil
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func loadPins(_ noAnimation : Bool = false, append: Bool = false) {
        self.isReloading = true
        
        if !noAnimation {
            SwiftSpinner.show("Загрузка меток...", animated: true, viewToDisplay: self.view)
        }
        
        let loadMarkersRequest = getURLRequest("GET", url: getBackendApiPath() + "temprorary-status/user/\(self.userId)?active=\(self.pinsTypeSwitcher.selectedSegmentIndex == 0 ? 1 : 0)&limit=\(self.pinsLimit)&offset=\(self.pinsTypeSwitcher.selectedSegmentIndex == 0 ? self.currentActiveOffset : currentExpiredOffset)")
        let loadMarkersDataTask = URLSession.shared.dataTask(with: loadMarkersRequest, completionHandler: { (data, response, error) in
            
            if (error != nil) {
                // сюда надо добавить обработку ошибок
                DispatchQueue.main.async(execute: {
                    SwiftSpinner.hide()
                    self.refreshControl?.endRefreshing()
                    
                    self.isReloading = false
                    self.loadMoreActive = false
                    self.loadMoreExpired = false
                    
                    JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                })
            } else {
                let markersResponse = response as! HTTPURLResponse
                
                print(markersResponse.statusCode)
                
                if markersResponse.statusCode == 200 {
                    
                    if !append {
                        if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
                            self.totalActiveCount = Int(markersResponse.allHeaderFields["X-Pagination-Total-Count"] as! String)!
                        } else {
                            self.totalExpiredCount = Int(markersResponse.allHeaderFields["X-Pagination-Total-Count"] as! String)!
                        }
                    }
                    
                    let markersResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSMutableArray
                    
                    if markersResult?.count == 0 && !append {
                        // меток нет вообще ((
                        if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
                            
                            // нет активных меток
                            self.allowLoadingMoreActive = false
                            self.activePins = markersResult!
                        } else {
                            
                            // нет истекших меток
                            self.allowLoadingMoreExpired = false
                            self.expiredPins = markersResult!
                        }
                    } else if markersResult?.count == 0 && append {
                        // при подгрузке ничего не подгрузилось
                        if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
                            
                            self.currentActiveOffset = self.activePins.count
                            self.allowLoadingMoreActive = false
                        } else {
                            self.currentExpiredOffset = self.expiredPins.count
                            self.allowLoadingMoreExpired = false
                        }
                    } else {
                        
                        if !append {
                            // это первая загрузка
                            if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
                                self.activePins.removeAllObjects()
                                self.activePins = markersResult!
                                
                                if markersResult?.count < self.pinsLimit {
                                    self.allowLoadingMoreActive = false
                                }
                            } else {
                                self.expiredPins.removeAllObjects()
                                self.expiredPins = markersResult!
                                
                                if markersResult?.count < self.pinsLimit {
                                    self.allowLoadingMoreExpired = false
                                }
                            }
                        } else {
                            // это подгрузка
                            if markersResult?.count >= self.pinsLimit {
                                if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
                                    self.activePins.addObjects(from: markersResult! as [AnyObject])
                                    self.allowLoadingMoreActive = true
                                } else {
                                    self.expiredPins.addObjects(from: markersResult! as [AnyObject])
                                    self.allowLoadingMoreExpired = true
                                }
                            } else if markersResult?.count > 0 && markersResult?.count < self.pinsLimit {
                                if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
                                    self.activePins.addObjects(from: markersResult! as [AnyObject])
                                    self.allowLoadingMoreActive = false
                                } else {
                                    self.expiredPins.addObjects(from: markersResult! as [AnyObject])
                                    self.allowLoadingMoreExpired = false
                                }
                            } else if markersResult?.count == 0 {
                                if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
                                    self.allowLoadingMoreActive = false
                                } else {
                                    self.allowLoadingMoreExpired = false
                                }
                            }
                        }
                    }
                    
                    if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
                        self.loadMoreActive = false
                        self.currentActiveOffset = self.activePins.count
                        self.activeLoaded = true
                    } else {
                        self.loadMoreExpired = false
                        self.currentExpiredOffset = self.expiredPins.count
                        self.expiredLoaded = true
                    }
                    
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        
                        self.isReloading = false
                        self.loadMoreActive = false
                        self.loadMoreExpired = false
                        self.tableView.tableFooterView?.isHidden = true
                        
                        self.refreshControl?.endRefreshing()
                        self.tableView.reloadData()
                    })
                } else if markersResponse.statusCode == 401 {
                    error401Action(self)
                } else if markersResponse.statusCode == 500 {
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        
                        JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                    })
                }
            }
        }) 
        
        loadMarkersDataTask.resume()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndexPath = indexPath
    }
    
    
    // бесконечный скролл
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        let deltaOffset = maximumOffset - currentOffset
        
        if deltaOffset <= 0 {
            loadMore()
        }
    }
    
    func loadMore() {
        if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
            if (!self.loadMoreActive && self.allowLoadingMoreActive && !self.isReloading && self.currentActiveOffset >= self.pinsLimit) {
                self.loadMoreActive = true
                self.isReloading = true
                self.animationView.startAnimating()
                self.tableView.tableFooterView?.isHidden = false
                loadMoreBegin("Загрузка меток...",
                    loadMoreEnd: {(x:Int) -> () in
                })
            }
        } else {
            if (!self.loadMoreExpired && self.allowLoadingMoreExpired && !self.isReloading && self.currentExpiredOffset >= self.pinsLimit) {
                self.loadMoreExpired = true
                self.isReloading = true
                self.animationView.startAnimating()
                self.tableView.tableFooterView?.isHidden = false
                loadMoreBegin("Загрузка меток...",
                    loadMoreEnd: {(x:Int) -> () in
                })
            }
        }
    }
    
    func loadMoreBegin(_ newtext:String, loadMoreEnd:@escaping (Int) -> ()) {
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
            self.loadPins(true, append: true)
            
            DispatchQueue.main.async {
                loadMoreEnd(0)
            }
        }
    }
    
    // -------------------------------------------------------------
    // ------------------ 3D Touch ---------------------------------
    // -------------------------------------------------------------
    
    // PEEK
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        
        self.previewVC = nil
        
        guard let indexPath = self.tableView.indexPathForRow(at: location) else {return nil}
        
        self.selectedIndexPath = indexPath
        
        self.previewVC = self.storyboard?.instantiateViewController(withIdentifier: "viewPinDetail") as? PinDetailView
        self.previewVC!.pinId = self.pinsTypeSwitcher.selectedSegmentIndex == 0 ? (self.activePins[(indexPath as NSIndexPath).row] as! NSDictionary)["id"] as! Int : (self.expiredPins[(indexPath as NSIndexPath).row] as! NSDictionary)["id"] as! Int
        
        if self.tableView.indexPathForSelectedRow != nil {
            self.tableView.deselectRow(at: self.tableView.indexPathForSelectedRow!, animated: true)
        }
        
        
        return self.previewVC!
    }
    
    // POP
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        let vcToShow = self.previewVC!
        
        show(vcToShow, sender: self)
        self.previewVC = nil
        self.selectedIndexPath = nil
    }
    
    // -------------------------------------------------------------
    // ------------------ /3D Touch --------------------------------
    // -------------------------------------------------------------
    
    // ----------- EMPTY DATASET --------------
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let string : String = "Нет меток"
        
        let attributes : [String : AnyObject] = [
            NSFontAttributeName : UIFont.boldSystemFont(ofSize: 32),
            NSForegroundColorAttributeName : UIColor.gray
        ]
        
        return NSAttributedString(string: string, attributes: attributes)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        var string : String = ""
        
        if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
            string = "Нет активных меток"
        } else {
            string = "Нет неактивных меток"
        }
        
        let paragraph : NSMutableParagraphStyle = NSMutableParagraphStyle()
        paragraph.lineBreakMode = .byWordWrapping
        paragraph.alignment = .center
        
        let attributes : [String : AnyObject] = [
            NSFontAttributeName : UIFont.systemFont(ofSize: 14),
            NSForegroundColorAttributeName : UIColor.lightGray,
            NSParagraphStyleAttributeName : paragraph
        ]
        
        return NSAttributedString(string: string, attributes: attributes)
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        if self.pinsTypeSwitcher.selectedSegmentIndex == 0 {
            if self.activePins.count == 0 {
                return true
            }
        } else {
            if self.expiredPins.count == 0 {
                return true
            }
        }
        
        return false
    }
    
    func emptyDataSetShouldAllowTouch(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}
