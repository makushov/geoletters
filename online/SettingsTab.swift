//
//  SettingsTab.swift
//  online
//
//  Created by KingOP on 31.10.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit
import SafariServices
import MessageUI

class SettingsTab: UITableViewController, SFSafariViewControllerDelegate, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var tosButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func tosAction(_ sender: AnyObject) {
        let vc : SFSafariViewController = SFSafariViewController(url: URL(string: "\(getBackendApiPath())page?locale=ru&alias=tos&as_html=1")!, entersReaderIfAvailable: true)
        
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (indexPath as NSIndexPath).section == 1 {
            if (indexPath as NSIndexPath).row == 0 {
                // пригласить друга
                let mailComposeViewController = configuredMailComposeViewController()
                if MFMailComposeViewController.canSendMail() {
                    self.present(mailComposeViewController, animated: true, completion: nil)
                } else {
                    self.showSendMailErrorAlert()
                }
            } else if (indexPath as NSIndexPath).row == 1 {
                // рассказать о GeoLetters
                let activityController : UIActivityViewController = UIActivityViewController(activityItems: ["Оставьте свой след вместе с уникальным картографическим мессенджером GeoLetters! Загрузить приложение - https://geoletters.com/download"], applicationActivities: nil)
                
                self.present(activityController, animated: true, completion: {
                    self.tableView.deselectRow(at: IndexPath(row: 0, section: 0), animated: true)
                })
            }
        }
        
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setSubject("GeoLetters - новый уровень общения!")
        mailComposerVC.setMessageBody("<div>Приглашаю установить уникальный картографический мессенджер GeoLetters и всегда оставаться на связи!</div><br /><div><a href=\"https://geoletters.com/download\">Загрузить приложение</div>", isHTML: true)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        JSSAlertView().danger(self, title: "Ошибка", text: "Ваше устройство не поддерживает отправку Email", buttonText: "Закрыть")
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        
        
    }
    
    @IBAction func rateAppAction(_ sender: AnyObject) {
        // оценить в AppStore
        UIApplication.shared.openURL(URL(string: "https://itunes.apple.com/app/id1065782314")!)
    }
}
