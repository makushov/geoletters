//
//  SelectLocation.swift
//  online
//
//  Created by KingOP on 31.10.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit
import MapKit
import TSMessages

class SelectLocation: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var locateMeButton: UIButton!
    
    var longitude : Double? = nil
    var latitude : Double? = nil
    var locationText : String? = nil
    
    var postMessageController : PostMessage = PostMessage()
    let locationManager = CLLocationManager()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
        self.mapView.showsCompass = true
        self.mapView.showsTraffic = true
        self.mapView.showsScale = true
        self.mapView.delegate = self
        self.loadingIndicator.isHidden = true
        
        self.locateMeButton.isHidden = true
        
        let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SelectLocation.mapTapped(_:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        self.mapView.addGestureRecognizer(tap)
        
        if self.longitude != nil && self.latitude != nil {
            let pinMarker = StatusPin(coordinate: CLLocationCoordinate2D(latitude: self.latitude!, longitude: self.longitude!), title: "", subtitle: "")
            self.mapView.addAnnotation(pinMarker)
            
            let region = MKCoordinateRegion(center: pinMarker.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            
            self.mapView.setRegion(region, animated: true)
            
            self.locationLabel.text = self.locationText
        } else if CLLocationManager.authorizationStatus() ==  CLAuthorizationStatus.authorizedWhenInUse {
            
            // отметим на карте текущее местоположение
            self.locationLabel.text = ""
            self.loadingIndicator.isHidden = false
            self.loadingIndicator.startAnimating()
            
            let location = self.locationManager.location
            
            if location != nil {
                let geoCoder = CLGeocoder()
                
                geoCoder.reverseGeocodeLocation(location!) {
                    (placemarks, error) -> Void in
                    
                    var address : String = ""
                    
                    let placeArray = placemarks as [CLPlacemark]!
                    
                    // Place details
                    var placeMark: CLPlacemark!
                    placeMark = placeArray?[0]
                    
                    // Location name
                    if let locationName = placeMark.addressDictionary?["Name"] as? String {
                        address += locationName + ", "
                    }
                    
                    // Street address
                    if let street = placeMark.addressDictionary?["Thoroughfare"] as? String {
                        if !address.contains(street) {
                            address += street + ", "
                        }
                    }
                    
                    // City
                    if let city = placeMark.addressDictionary?["City"] as? String {
                        if !address.contains(city) {
                            address += city + ", "
                        }
                    }
                    
                    // Zip code
                    if let zip = placeMark.addressDictionary?["ZIP"] as? String {
                        address += zip + ", "
                    }
                    
                    // Country
                    if let country = placeMark.addressDictionary?["Country"] as? String {
                        address += country + ", "
                    }
                    
                    address = address.truncate(address.length - 2)
                    
                    self.loadingIndicator.stopAnimating()
                    self.loadingIndicator.isHidden = true
                    self.locationLabel.text = address
                    
                    let pinMarker = StatusPin(coordinate: (self.locationManager.location?.coordinate)!, title: address, subtitle: "")
                    
                    self.mapView.addAnnotation(pinMarker)
                    
                    let region = MKCoordinateRegion(center: pinMarker.coordinate, span: self.mapView.region.span)
                    
                    self.mapView.setRegion(region, animated: true)
                    
                    self.longitude = self.locationManager.location?.coordinate.longitude
                    self.latitude = self.locationManager.location?.coordinate.latitude
                }
            } else {
                self.loadingIndicator.stopAnimating()
                self.loadingIndicator.isHidden = true
                self.locationLabel.text = "Не удалось определить текущее местоположение"
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation.isKind(of: MKUserLocation.self) {
            return nil
        } else {
            let pin = mapView.dequeueReusableAnnotationView(withIdentifier: "pin") ?? MKAnnotationView(annotation: annotation, reuseIdentifier: "pin")
            
            pin.image = UIImage(named: "pinIcon")
            
            pin.canShowCallout = true
            
            return pin
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse && (self.latitude == nil && self.longitude == nil) {
            
            if status == .authorizedWhenInUse {
                self.locateMeButton.isHidden = false
                self.locationManager.startUpdatingLocation()
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if self.latitude == nil && self.longitude == nil {
            if let location = locations.first {
                let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
                
                self.mapView.setRegion(region, animated: true)
                
                self.locationManager.stopUpdatingLocation()
            }
        } else {
            self.locationManager.stopUpdatingLocation()
        }
    }
    
    func mapTapped(_ recognizer: UITapGestureRecognizer) {
        let point : CGPoint = recognizer.location(in: self.mapView)
        let coordinate : CLLocationCoordinate2D = self.mapView.convert(point, toCoordinateFrom: self.view)
        
        self.mapView.removeAnnotations(self.mapView.annotations)
        self.locationLabel.text = ""
        self.loadingIndicator.isHidden = false
        self.loadingIndicator.startAnimating()
        
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)) {
            (placemarks, error) -> Void in
            
            var address : String = ""
            
            let placeArray = placemarks as [CLPlacemark]!
            
            // Place details
            var placeMark: CLPlacemark!
            placeMark = placeArray?[0]
            
            // Location name
            if let locationName = placeMark.addressDictionary?["Name"] as? String {
                address += locationName + ", "
            }
            
            // Street address
            if let street = placeMark.addressDictionary?["Thoroughfare"] as? String {
                if !address.contains(street) {
                    address += street + ", "
                }
            }
            
            // City
            if let city = placeMark.addressDictionary?["City"] as? String {
                if !address.contains(city) {
                    address += city + ", "
                }
            }
            
            // Zip code
            if let zip = placeMark.addressDictionary?["ZIP"] as? String {
                address += zip + ", "
            }
            
            // Country
            if let country = placeMark.addressDictionary?["Country"] as? String {
                address += country + ", "
            }
            
            address = address.truncate(address.length - 2)
            
            self.loadingIndicator.stopAnimating()
            self.loadingIndicator.isHidden = true
            self.locationLabel.text = address
            
            let pinMarker = StatusPin(coordinate: coordinate, title: address, subtitle: "")
            self.mapView.addAnnotation(pinMarker)
            
            let region = MKCoordinateRegion(center: pinMarker.coordinate, span: self.mapView.region.span)
            self.mapView.setRegion(region, animated: true)
            
            self.longitude = coordinate.longitude
            self.latitude = coordinate.latitude
        }
    }
    
    @IBAction func submitLocation(_ sender: AnyObject) {
        if self.latitude != nil && self.longitude != nil {
            self.postMessageController.selectedLatitude = self.latitude
            self.postMessageController.selectedLongitude = self.longitude
            
            self.postMessageController.currentLocationLabel.text = self.locationLabel.text
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToLocationSearch" {
            let destVC : SearchLocation = segue.destination as! SearchLocation
            destVC.selectLocationController = self
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    
    @IBAction func locateMeAction(_ sender: AnyObject) {
        
        let location = self.locationManager.location?.coordinate
        
        if location != nil {
            let center = CLLocationCoordinate2D(latitude: location!.latitude, longitude: location!.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            
            self.mapView.setRegion(region, animated: true)
        } else {
            TSMessage.showNotification(
                in: self,
                title: "Ошибка",
                subtitle: "Не удалось определить текущее местоположение",
                type: TSMessageNotificationType.error,
                duration: 2.0,
                canBeDismissedByUser: true
            )
        }
    }
}
