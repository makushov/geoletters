//
//  Messages.swift
//  online
//
//  Created by KingOP on 23.10.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit
import AudioToolbox
import Haneke
import Photos
import JSQMessagesViewController
import QBImagePickerController
import DZNEmptyDataSet
import NYTPhotoViewer
import TSMessages
import SocketIO

class Messages: JSQMessagesViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate, QBImagePickerControllerDelegate, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource, UIViewControllerPreviewingDelegate {
    
    @IBOutlet weak var userPhoto: UIImageView!
    
    var timer : Timer = Timer()
    
    var dialogId : Int? = nil
    var userId : Int? = nil
    
    var members : [Dictionary<String, AnyObject>] = []
    var creatorId : Int? = nil
    
    var avatar : UIImage? = nil
    var avatarPath : String? = nil
    var fromDialogsList : Bool = false
    var pms : [Dictionary<String, AnyObject>] = []
    var userName : String = ""
    var messages = [JSQMessage]()

    var message_dict : [Dictionary<String, AnyObject>] = []
    
    let incomingBubble = JSQMessagesBubbleImageFactory().incomingMessagesBubbleImage(with: UIColor(netHex: 0xE6E7EC))
    let outgoingBubble = JSQMessagesBubbleImageFactory().outgoingMessagesBubbleImage(with: UIColor(netHex: 0x376fa2))
    let sendingBubble = JSQMessagesBubbleImageFactory().outgoingMessagesBubbleImage(with: UIColor(netHex: 0x88c0fd))
    let errorSendingBubble = JSQMessagesBubbleImageFactory().outgoingMessagesBubbleImage(with: UIColor(netHex: 0xFF4848))
    
    let socket = SocketIOClient(socketURL: URL(string: messageSocketUrl)!)
    
    let messagesLimit = 20
    var currentOffset = 0
    var totalCount = 0
    
    var isTyping : Bool = false
    
    var isLoaded : Bool = false
    
    let imagePicker = UIImagePickerController()
    
    var locationToView : CLLocation? = nil
    var locationManager : CLLocationManager = CLLocationManager()
    
    var notWillDismiss : Bool = false
    
    var userNameToViewInLocation : String? = nil
    
    var previewMapVC : ViewLocation? = nil
    var previewImageVC : NYTPhotosViewController? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if traitCollection.forceTouchCapability == .available {
            registerForPreviewing(with: self, sourceView: self.collectionView!)
        }
        
        // зададим "себе" имя и id в чате
        self.senderDisplayName = self.userName
        self.senderId = getUserId()!.description
        
        self.title = self.userName
        
        self.locationManager.delegate = self
        self.imagePicker.delegate = self
        
        if self.avatar != nil {
            self.userPhoto.image = maskedAvatarFromImage(self.avatar!)
        } else {
            if self.members.count > 2 {
                self.userPhoto.image = maskedAvatarFromImage(UIImage(named: "group.png")!)
            } else {
                self.userPhoto.image = maskedAvatarFromImage(self.userPhoto.image!)
            }
        }

        self.collectionView!.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        self.collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        
        self.collectionView!.emptyDataSetDelegate = self
        self.collectionView!.emptyDataSetSource = self
        
        self.collectionView?.backgroundColor = UIColor.white//(netHex: 0xecf3fb)
        
        pushUserInfo = nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        messagesController = self
        
        if !self.isLoaded {
            if self.dialogId != nil  {
                self.loadMessages()
            } else {
                // мы пришли сюда не из диалогов, а из профиля пользователя
                // поэтому сначала выясним, а есть ли у нас с ним диалог
                
                let checkDialogRequest = getURLRequest("GET", url: getBackendApiPath() + "dialog/check?user_id=\(self.userId!.description)&expand=members,message", params: Dictionary<String, AnyObject>(), token: getUserToken())
                let checkDialogTask = URLSession.shared.dataTask(with: checkDialogRequest, completionHandler: { (data, response, error) in
                    
                    if (error != nil) {
                        // сюда надо добавить обработку ошибок
                        DispatchQueue.main.async(execute: {
                            
                            TSMessage.showNotification(in: self,
                                title: "Ошибка",
                                subtitle: error?.localizedDescription,
                                type: TSMessageNotificationType.error,
                                duration: 2.0,
                                canBeDismissedByUser: true
                            )
                            playErrorSound()
                        })
                    } else {
                        let checkResponse = response as! HTTPURLResponse
                        print(checkResponse.statusCode)
                        if checkResponse.statusCode == 200 {
                            // диалог есть, надо бы его загрузить
                            let checkResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [Dictionary<String, AnyObject>]
                            
                            self.dialogId = checkResult![0]["id"] as? Int
                            
                            self.loadMessages()
                        } else if checkResponse.statusCode == 204 {
                            // диалога нет
                            self.isLoaded = true
                            DispatchQueue.main.async(execute: {
                                self.collectionView?.reloadData()
                            })
                        } else if checkResponse.statusCode == 401 {
                            error401Action(self)
                        } else if checkResponse.statusCode == 500 {
                            self.isLoaded = true
                            DispatchQueue.main.async(execute: {
                                SwiftSpinner.hide()
                                
                                TSMessage.showNotification(in: self,
                                    title: "Ошибка",
                                    subtitle: error500,
                                    type: TSMessageNotificationType.error,
                                    duration: 2.0,
                                    canBeDismissedByUser: true
                                )
                                playErrorSound()
                            })
                        }
                    }
                }) 

                checkDialogTask.resume()
            }
        }
        
        if self.avatarPath != nil {
            // мы пришли из пуша, надо подгрузить авку
            // есть аватарка
            let cache = Shared.imageCache
            
            let iconFormat = Format<UIImage>(name: "icons", diskCapacity: 10 * 1024 * 1024) { image in
                return maskedAvatarFromImage(image)
            }
            cache.addFormat(iconFormat)
            
            self.userPhoto.hnk_setImageFromURL(URL(string: "\(getBackendDomainPath())\(self.avatarPath!)")!, placeholder: UIImage(named: "nophoto.jpg"), format: iconFormat, failure: nil, success: nil)
        }
    }
    
    func setupSocket() {
        // установим коннект с сервером через сокет
        // это нужно для мгновенного обновления данных в чате
        self.addHandlers()
        
        self.socket.connect()
    }
    
    func addHandlers() {
        self.socket.on("connect") { (data, ack) -> Void in
            
            // установлен коннект
            self.socket.emit("joinserver", getUserToken()!, deviceUDID, self.dialogId!)
        }
        
        self.socket.on("message") { (data, ack) -> Void in
            
            // пришло сообщение в чат, надо его отобразить
            let message = data[0] as? Dictionary<String, AnyObject>
            
            let new_message : JSQMessage = JSQMessage(senderId: (message!["user_id"] as! Int).description, senderDisplayName: message!["username"] as! String, date: Date(timeIntervalSince1970: Double(message!["created_at"] as! Int)), text: message!["message"] as! String)
            
            let dictionary : Dictionary<String, AnyObject> = [
                "user_id"       : message!["user_id"]!,
                "created_at"    : message!["created_at"]!,
                "status"        : "sent" as AnyObject,
                "message"       : message!["message"]!
            ]
            
            self.message_dict.append(dictionary)
            
            self.messages += [new_message]
            self.finishSendingMessage()
            
            self.currentOffset += 1
            
            if dialogsController != nil {
                // обновим список диалогов
                // получим индекс
                let index = dialogsController?.dialogIds.index(of: self.dialogId!)
                
                // текущий диалог есть в списке диалогов, обновим его
                var dialog : Dictionary<String, AnyObject> = dialogsController!.dialogs[index!]
                
                let new_message : Dictionary<String, AnyObject> = [
                    "user_id"       : message!["user_id"]!,
                    "created_at"    : message!["created_at"]!,
                    "viewed"        : 1 as AnyObject,
                    "message"       : message!["message"]!
                ]
                
                dialog["message"] = new_message as AnyObject?
                dialog["unread"] = "0" as AnyObject?
                
                // сделаем перемещение
                dialogsController!.dialogs.remove(at: index!)
                dialogsController!.dialogIds.remove(at: index!)
                
                dialogsController!.dialogs.insert(dialog, at: 0)
                dialogsController!.dialogIds.insert(self.dialogId!, at: 0)
            }
            
            if message!["user_id"] as! Int == getUserId()! {
                self.finishSendingMessage(animated: true)
                playSendSound()
            } else {
                self.finishReceivingMessage(animated: true)
                playReceivedSound()
                // пошлем на сервер сигнал о том, что мы это сообщение прочитали
                self.socket.emit("read")
            }
        }
        
        self.socket.on("istyping") { (data, ack) -> Void in
            self.showTypingIndicator = true
        }
        
        self.socket.on("reconnect") { (data, ack) -> Void in
            self.socket.emit("joinserver", getUserToken()!, deviceUDID, self.dialogId!)
        }
        
        self.socket.on("stoptyping") { (data, ack) -> Void in
            self.showTypingIndicator = false
        }
        
        self.socket.on("blacklisted") { (data, ack) -> Void in
            self.view.endEditing(true)
            
            TSMessage.showNotification(in: self,
                title: "Ошибка",
                subtitle: "Вы находитесь в черном списке у этого пользователя и не можете отправлять ему сообщения.",
                type: TSMessageNotificationType.error,
                duration: 2.0,
                canBeDismissedByUser: true
            )
            playErrorSound()
        }
        
        self.socket.onAny {print("Got event: \($0.event), with items: \($0.items)")}
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellBottomLabelAt indexPath: IndexPath!) -> CGFloat {
        
        /*if self.messages_dict[indexPath.row]["status"] as! String == "errorSending" {
            return 10
        }*/
        
        return kJSQMessagesCollectionViewCellLabelHeightDefault//10//CGFloat.min
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellBottomLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        
        var bottomString : NSMutableAttributedString = NSMutableAttributedString(string: JSQMessagesTimestampFormatter.shared().time(for: self.messages[indexPath.row].date))
        
        if self.message_dict[indexPath.row]["status"] as! String == "errorSending" {
            bottomString = NSMutableAttributedString(string: "Не отправлено")
        } else if self.message_dict[indexPath.row]["status"] as! String == "sending" {
            bottomString = NSMutableAttributedString(string: "Отправка...")
        }
        
        return bottomString
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        let data = self.messages[indexPath.row]
        
        return data
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        //return JSQMessagesTimestampFormatter.sharedFormatter().attributedTimestampForDate(messages[indexPath.row].date)
        
        return NSAttributedString(string: JSQMessagesTimestampFormatter.shared().relativeDate(for: self.messages[indexPath.row].date))
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellTopLabelAt indexPath: IndexPath!) -> CGFloat {
        var height : CGFloat = 0
        
        if indexPath.row == 0 {
            height = kJSQMessagesCollectionViewCellLabelHeightDefault
        } else if indexPath.row > 0 {
            if self.collectionView(collectionView, attributedTextForCellTopLabelAt: indexPath) != self.collectionView(collectionView, attributedTextForCellTopLabelAt: IndexPath(row: indexPath.row - 1, section: indexPath.section)) {
                
                height = kJSQMessagesCollectionViewCellLabelHeightDefault
            }
        }
        
        return height
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let data = self.messages[indexPath.row]
        if (data.senderId == self.senderId) {
            if message_dict[indexPath.row]["status"] as! String == "sending" {
                return self.sendingBubble
            } else if message_dict[indexPath.row]["status"] as! String == "errorSending" {
                return self.errorSendingBubble
            }
            
            return self.outgoingBubble
        } else {
            return self.incomingBubble
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        
        /*if self.members.count > 2 {
            return nil
        } else {
            if self.messages[indexPath.row].senderId != getUserId()!.description {
                return JSQMessagesAvatarImage(avatarImage: self.userPhoto.image, highlightedImage: self.userPhoto.image, placeholderImage: maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!))
            }
        }*/
        
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        
        if self.members.count > 2 {
            if self.messages[indexPath.row].senderId == getUserId()?.description {
                return NSAttributedString(string: "")
            }
            
            return NSAttributedString(string: self.messages[indexPath.row].senderDisplayName)
        }
        
        return NSAttributedString(string: "")
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        
        if self.members.count > 2 {
            if self.messages[indexPath.row].senderId == getUserId()?.description {
                return 0
            }
            
            return 15
        }
        
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.messages.count;
    }
    
    override func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        // вот тут мы начали набирать текст, пошлем сигнал о наборе на сервер
        if !self.isTyping {
            self.socket.emit("istyping")
            self.isTyping = true
        }
        
        self.timer.invalidate()
        self.timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(Messages.stopTyping), userInfo: nil, repeats: true)
        
        return true
    }
    
    override func finishSendingMessage(animated: Bool) {
        super.finishSendingMessage(animated: animated)
    }
    
    func stopTyping() {
        // сообщим серверу, что мы уже не набираем текст
        self.timer.invalidate()
        self.isTyping = false
        self.socket.emit("stoptyping")
    }
    
    func pickPhotos() {
        
        let imagePickVariantController : UIAlertController = UIAlertController(title: "Сделайте выбор", message: "Что использовать для выбора изображения?", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        imagePickVariantController.addAction(UIAlertAction(title: "Камера", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            self.imagePicker.sourceType = .camera
            
            self.present(self.imagePicker, animated: true, completion: nil)
        }))
        
        imagePickVariantController.addAction(UIAlertAction(title: "Галерея", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            
            let imagePicker : QBImagePickerController = QBImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsMultipleSelection = true
            imagePicker.minimumNumberOfSelection = 1
            imagePicker.maximumNumberOfSelection = 5
            
            imagePicker.mediaType = .image
            
            self.present(imagePicker, animated: true, completion: nil)
        }))
        
        imagePickVariantController.addAction(UIAlertAction(title: "Отмена", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(imagePickVariantController, animated: true, completion: nil)
    }
    
    func qb_imagePickerController(_ imagePickerController: QBImagePickerController!, didFinishPickingAssets assets: [AnyObject]!) {
        
        imagePickerController.dismiss(animated: true, completion: nil)
        
        if self.dialogId == nil {
            let createDialogRequest : URLRequest = getURLRequest("POST", url: getBackendApiPath() + "dialog", params: ["users" : self.userId!.description as AnyObject], token: getUserToken())
            
            let createDialogTask = URLSession.shared.dataTask(with: createDialogRequest, completionHandler: { (data, response, error) in
                
                if (error != nil) {
                    // сюда надо добавить обработку ошибок
                    DispatchQueue.main.async(execute: {
                        
                        self.view.endEditing(true)
                        
                        TSMessage.showNotification(in: self,
                            title: "Ошибка",
                            subtitle: error?.localizedDescription,
                            type: TSMessageNotificationType.error,
                            duration: 2.0,
                            canBeDismissedByUser: true
                        )
                        playErrorSound()
                    })
                } else {
                    let infoResponse = response as! HTTPURLResponse
                    
                    let infoResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                    
                    if infoResponse.statusCode == 401 {
                        error401Action(self)
                    } else if infoResponse.statusCode == 422 {
                        DispatchQueue.main.async(execute: {
                            
                            self.view.endEditing(true)
                            
                            TSMessage.showNotification(in: self,
                                title: "Ошибка",
                                subtitle: infoResult!["message"] as? String,
                                type: TSMessageNotificationType.error,
                                duration: 2.0,
                                canBeDismissedByUser: true
                            )
                            playErrorSound()
                        })
                    } else if infoResponse.statusCode == 500 {
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            
                            self.view.endEditing(true)
                            
                            TSMessage.showNotification(in: self,
                                title: "Ошибка",
                                subtitle: error500,
                                type: TSMessageNotificationType.error,
                                duration: 2.0,
                                canBeDismissedByUser: true
                            )
                            playErrorSound()
                        })
                    } else if infoResponse.statusCode == 403 {
                        DispatchQueue.main.async(execute: {
                            
                            self.view.endEditing(true)
                            
                            TSMessage.showNotification(in: self,
                                title: "Ошибка",
                                subtitle: "Вы находитесь в черном списке у этого пользователя и не моежете отправлять ему сообщения.",
                                type: TSMessageNotificationType.error,
                                duration: 2.0,
                                canBeDismissedByUser: true
                            )
                            playErrorSound()
                        })
                    } else {
                        // диалог успешно создан
                        // отправим в него сообщение
                        self.dialogId = infoResult!["id"] as? Int
                        self.members = infoResult!["members"] as! [Dictionary<String, AnyObject>]
                        
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            
                            self.sendMultiplePhotos(assets as! [PHAsset])
                        })
                    }
                }
            }) 
            
            SwiftSpinner.show("Подготовка...", animated: true, viewToDisplay: self.view)
            
            createDialogTask.resume()
        } else {
            self.sendMultiplePhotos(assets as! [PHAsset])
        }
    }
    
    func sendMultiplePhotos(_ assets: [PHAsset]) {
        for asset in assets {
            
            let sendPhotoQueue : DispatchQueue = DispatchQueue(label: "sendPhoto\(asset.creationDate?.timeIntervalSince1970.description)", attributes: [])
            
            sendPhotoQueue.async(execute: {
                
                let pickedImage = self.getAssetImage(asset)
                
                DispatchQueue.main.async(execute: {
                    let photo : JSQPhotoMediaItem = JSQPhotoMediaItem(image: nil)
                    photo.appliesMediaViewMaskAsOutgoing = true
                    
                    let message = JSQMessage(senderId: getUserId()!.description, senderDisplayName: getUserLogin()!, date: Date(), media: photo)
                    
                    self.messages.append(message!)
                    
                    let dictionary : Dictionary<String, AnyObject> = [
                        "user_id"       : getUserId()! as AnyObject,
                        "created_at"    : getCurrentTimestamp() as AnyObject,
                        "message"       : "Изображение" as AnyObject,
                        "status"        : "sending" as AnyObject,
                        "id"            : -1 as AnyObject
                    ]
                    
                    self.message_dict.append(dictionary)
                    
                    let index : Int = self.messages.index(of: message!)!
                    self.view.endEditing(true)
                    self.finishSendingMessage(animated: true)
                    
                    // отправим в уже существующий
                    let imageData : Data = UIImageJPEGRepresentation(pickedImage, 0.5)!
                    
                    let sendImageRequest = getURLRequest("POST", url: getBackendApiPath() + "dialog/\(self.dialogId!)/attachments", params: ["id" : self.dialogId!, "type" : "image", "image" : imageData.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters)], token: getUserToken()!)
                    
                    let sendImageTask = URLSession.shared.dataTask(with: sendImageRequest, completionHandler: { (data, response, error) -> Void in
                        
                        if error != nil {
                            DispatchQueue.main.async(execute: {
                                TSMessage.showNotification(in: self,
                                    title: "Ошибка",
                                    subtitle: error?.localizedDescription,
                                    type: TSMessageNotificationType.error,
                                    duration: 2.0,
                                    canBeDismissedByUser: true
                                )
                                playErrorSound()
                                
                                self.collectionView?.deleteItems(at: [IndexPath(row: index, section: 0)])
                            })
                        } else {
                            let res = response as! HTTPURLResponse
                            
                            let sendLocationResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary<String, AnyObject>
                            
                            if res.statusCode == 201 {
                                DispatchQueue.main.async(execute: {
                                    
                                    if dialogsController != nil {
                                        // получим индекс
                                        let index = dialogsController?.dialogIds.index(of: self.dialogId!)
                                        
                                        if index != nil {
                                            // текущий диалог есть в списке диалогов, обновим его                                            
                                            var dialog : Dictionary<String, AnyObject> = dialogsController!.dialogs[index!]
                                            
                                            if dialog["message"] == nil {
                                                let message : Dictionary<String, AnyObject> = [
                                                    "user_id"       : getUserId()! as AnyObject,
                                                    "created_at"    : sendLocationResult!["created_at"] as AnyObject,
                                                    "viewed"        : 0 as AnyObject,
                                                    "message"       : "Изображение" as AnyObject
                                                ]
                                                
                                                dialog["message"] = message as AnyObject?
                                            } else {
                                                var new_message : Dictionary<String, AnyObject> = dialog["message"] as! Dictionary<String, AnyObject>
                                                new_message["user_id"] = getUserId()! as AnyObject?
                                                new_message["created_at"] = sendLocationResult!["created_at"] as AnyObject?
                                                new_message["viewed"] = 0 as AnyObject?
                                                new_message["message"] = "Изображение" as AnyObject?
                                                
                                                dialog["message"] = new_message as AnyObject?
                                            }
                                            
                                            // сделаем перемещение
                                            dialogsController!.dialogs.remove(at: index!)
                                            dialogsController!.dialogIds.remove(at: index!)
                                            
                                            dialogsController!.dialogs.insert(dialog, at: 0)
                                            dialogsController!.dialogIds.insert(self.dialogId!, at: 0)
                                        } else {
                                            // диалога не было, его надо создать и впихнуть
                                            let new_message : Dictionary<String, AnyObject> = [
                                                "user_id"       : getUserId()! as AnyObject,
                                                "created_at"    : sendLocationResult!["created_at"] as AnyObject,
                                                "viewed"        : 1 as AnyObject,
                                                "message"       : "Изображение" as AnyObject,
                                                "attachment"    : NSNull()
                                            ]
                                            
                                            let dialog : Dictionary<String, AnyObject> = [
                                                "message"   : new_message as AnyObject,
                                                "id"        : self.dialogId as AnyObject,
                                                "unread"    : "0" as AnyObject,
                                                "members"   : self.members as AnyObject
                                            ]
                                            
                                            // сделаем перемещение
                                            dialogsController!.dialogs.insert(dialog, at: 0)
                                            dialogsController!.dialogIds.insert(self.dialogId!, at: 0)
                                            dialogsController!.currentOffset += 1
                                        }
                                    }
                                    
                                    // сообщение отправилось, закэшируем изображение
                                    let imageUrl : URL = URL(string: "\(getBackendDomainPath())\((sendLocationResult!["attachment"] as! NSDictionary)["content"] as! String)")!
                                    
                                    photo.image = UIImage(data: imageData)
                                    
                                    let index = self.message_dict.index(where: {$0["created_at"] as! Double == dictionary["created_at"] as! Double})
                                    var message_in_dict = self.message_dict[index!]
                                    
                                    message_in_dict["status"] = "sent" as AnyObject?
                                    message_in_dict["id"] = sendLocationResult!["id"] as AnyObject?
                                    
                                    self.message_dict[index!] = message_in_dict
                                    
                                    self.collectionView?.reloadData()
                                    playSendSound()
                                    
                                    avatarsCacheDataStack.saveAvatarToCache((sendLocationResult!["attachment"] as! NSDictionary)["id"] as! Int, fileName: imageUrl.lastPathComponent, imageData: imageData)
                                })
                            } else if res.statusCode == 400 {
                                self.messages.remove(at: index)
                                
                                DispatchQueue.main.async(execute: {
                                    TSMessage.showNotification(in: self,
                                        title: "Ошибка",
                                        subtitle: sendLocationResult!["message"] as? String,
                                        type: TSMessageNotificationType.error,
                                        duration: 2.0,
                                        canBeDismissedByUser: true
                                    )
                                    playErrorSound()
                                    
                                    self.collectionView?.deleteItems(at: [IndexPath(row: index, section: 0)])
                                })
                            } else if res.statusCode == 401 {
                                self.messages.remove(at: index)
                                
                                error401Action(self)
                            } else if res.statusCode == 403 {
                                self.messages.remove(at: index)
                                
                                DispatchQueue.main.async(execute: {
                                    
                                    self.collectionView?.deleteItems(at: [IndexPath(row: index, section: 0)])
                                    TSMessage.showNotification(in: self,
                                        title: "Ошибка",
                                        subtitle: "Вы находитесь в черном списке у данного пользователя и не можете отправлять ему сообщения.",
                                        type: TSMessageNotificationType.error,
                                        duration: 2.0,
                                        canBeDismissedByUser: true
                                    )
                                    playErrorSound()
                                })
                            } else if res.statusCode == 500 {
                                self.messages.remove(at: index)
                                
                                DispatchQueue.main.async(execute: {
                                    SwiftSpinner.hide()
                                    
                                    TSMessage.showNotification(in: self,
                                        title: "Ошибка",
                                        subtitle: error500,
                                        type: TSMessageNotificationType.error,
                                        duration: 2.0,
                                        canBeDismissedByUser: true
                                    )
                                    playErrorSound()
                                    
                                    self.collectionView?.deleteItems(at: [IndexPath(row: index, section: 0)])
                                })
                            }
                        }
                    })
                    
                    sendImageTask.resume()
                })
            })
        }
    }
    
    func qb_imagePickerControllerDidCancel(_ imagePickerController: QBImagePickerController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func getAssetImage(_ asset: PHAsset) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var image = UIImage()
        option.isSynchronous = true
        option.isNetworkAccessAllowed = true
        option.deliveryMode = .highQualityFormat
        option.progressHandler = {
            (progress, error, stop, info) in
            
            
        }
        manager.requestImageData(for: asset, options: option) { (data, imageUTI, orientation, info) -> Void in
            
            image = UIImage(data: data!)!
        }
        
        return image
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!) {
        
        if self.message_dict[indexPath.row]["status"] as! String == "errorSending" {
            self.doUnsendedMessagesTasks(indexPath)
        } else {
            // проверим, тапнули ли мы по медиа-сообщению
            if self.messages[indexPath.row].isMediaMessage {
                // да, медиа, проверим тип медиа
                
                if self.messages[indexPath.row].media.isKind(of: JSQLocationMediaItem.self) {
                    
                    // у нас тут геопозиция, надо открыть карту и показать местоположение
                    self.locationToView = (self.messages[indexPath.row].media as! JSQLocationMediaItem).location
                    self.userNameToViewInLocation = self.messages[indexPath.row].senderDisplayName
                    self.performSegue(withIdentifier: "goToLocationView", sender: self)
                } else if self.messages[indexPath.row].media.isKind(of: JSQPhotoMediaItem.self) {
                    
                    // у нас тут фото, надо бы его показать
                    let photo : attachedPhoto = attachedPhoto(image: (self.messages[indexPath.row].media as! JSQPhotoMediaItem).image, attributedCaptionTitle: NSAttributedString(string: ""))
                    
                    let photosViewController : NYTPhotosViewController = NYTPhotosViewController(photos: [photo])
                    
                    self.present(photosViewController, animated: true, completion: nil)
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.notWillDismiss = false
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        self.socket.emit("disconnect")
        self.socket.disconnect()
        if !self.notWillDismiss {
            messagesController = nil
        }
        TSMessage.dismissActiveNotification()
        UIView.setAnimationsEnabled(true)
        super.viewWillDisappear(true)
    }
    
    func loadMessages(_ append: Bool = false) {
        
        if append {
            CATransaction.begin()
            CATransaction.setDisableActions(true)
        }
        
        // загрузка списка сообщений
        let url = getBackendApiPath() + "dialog/\(self.dialogId!.description)/messages?limit=\(self.messagesLimit.description)&offset=\(self.message_dict.count.description)"
        let getMessagesRequest = getURLRequest("GET", url: url, params: Dictionary<String, AnyObject>(), token: getUserToken())
        
        let getMessagesTask = URLSession.shared.dataTask(with: getMessagesRequest, completionHandler: { (data, response, error) in
            
            if (error != nil) {
                // сюда надо добавить обработку ошибок
                DispatchQueue.main.async(execute: {
                    
                    TSMessage.showNotification(in: self,
                        title: "Ошибка",
                        subtitle: error?.localizedDescription,
                        type: TSMessageNotificationType.error,
                        duration: 2.0,
                        canBeDismissedByUser: true
                    )
                    playErrorSound()
                })
            } else {
                let messagesResponse = response as! HTTPURLResponse
                
                print(messagesResponse.statusCode)
                
                if messagesResponse.statusCode == 200 {
                    // все хорошо, сообщения загрузились, откроем сокет для диалога
                    self.setupSocket()
                    
                    let newArray = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [Dictionary<String, AnyObject>]
                    
                    //let newArray = NSMutableArray(array: (try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! NSMutableArray).reverseObjectEnumerator().allObjects)
                    
                    self.currentOffset += newArray.count
                    
                    var i = 0
                    //var indexPaths : [NSIndexPath] = []
                    
                    for pm in newArray {
                        var item : Dictionary<String, AnyObject> = pm as! Dictionary<String, AnyObject>
                        item["status"] = "sent" as AnyObject?
                        self.message_dict.append(item)
                        
                        let sender = (item["user_id"] as! Int).description
                        
                        if !(item["attachment"] is NSNull) && item["attachment"] != nil {
                            // есть аттач, надо проверить тип
                            let attach = item["attachment"] as! Dictionary<String, AnyObject>
                            
                            if attach["type"] as! String == "location" {
                                // отобразим местоположение
                                
                                var username : String = ""
                                
                                if self.members.count > 2 {
                                    for user in self.members {
                                        if user["id"] as! Int == item["user_id"] as! Int {
                                            username = user["username"] as! String
                                            break
                                        }
                                    }
                                } else {
                                    username = sender == getUserId()!.description ? getUserLogin()! : self.userName
                                }
                                
                                let location : JSQLocationMediaItem = JSQLocationMediaItem(maskAsOutgoing: sender == getUserId()!.description)
                                let message = JSQMessage(senderId: sender, senderDisplayName: username, date: Date(timeIntervalSince1970: item["created_at"] as! Double), media: location)
                                
                                self.messages.insert(message!, at: 0)
                                
                                let coordinates = (attach["content"] as! String).characters.split{$0 == ";"}.map(String.init)
                                
                                let locationToShow = CLLocation(latitude: Double(coordinates[0])!, longitude: Double(coordinates[1])!)
                                
                                location.setLocation(locationToShow, region: MKCoordinateRegionMakeWithDistance(locationToShow.coordinate, 500.0, 500.0), withCompletionHandler: { () -> Void in
                                    self.collectionView?.reloadData()
                                })
                            } else {
                                // прислали фотку
                                // проверим, есть ли она в кэше
                                var photo : JSQPhotoMediaItem = JSQPhotoMediaItem()
                                
                                if avatarsCacheDataStack.isAvatarInCache(attach["id"] as! Int) {
                                    let image = avatarsCacheDataStack.getAvatarImageFromCache(attach["id"] as! Int)
                                    
                                    if image == nil {
                                        // картинка недокэширована
                                        photo = JSQPhotoMediaItem(image: nil)
                                    } else {
                                        photo = JSQPhotoMediaItem(image: image)
                                    }
                                    
                                    
                                    photo.appliesMediaViewMaskAsOutgoing = sender == getUserId()!.description
                                    
                                    var username : String = ""
                                    
                                    if self.members.count > 2 {
                                        for user in self.members {
                                            if user["id"] as! Int == item["user_id"] as! Int {
                                                username = user["username"] as! String
                                                break
                                            }
                                        }
                                    } else {
                                        username = sender == getUserId()!.description ? getUserLogin()! : self.userName
                                    }
                                    
                                    let message = JSQMessage(senderId: sender, senderDisplayName: username, date: Date(timeIntervalSince1970: item["created_at"] as! Double), media: photo)
                                    
                                    self.messages.insert(message!, at: 0)
                                    
                                    if image == nil {
                                        avatarsCacheDataStack.safeRemoveAvatarFromCache(attach["id"] as! Int)
                                        
                                        DispatchQueue(label: "loadImg", attributes: []).async(execute: {
                                            
                                            let imageUrl = URL(string: getBackendDomainPath() + (attach["content"] as! String))!
                                            let imageData : Data? = try? Data(contentsOf: imageUrl)
                                            
                                            if imageData != nil {
                                                DispatchQueue.main.async(execute: {
                                                    photo.image = UIImage(data: imageData!)
                                                    
                                                    self.collectionView?.reloadData()
                                                    
                                                    avatarsCacheDataStack.saveAvatarToCache(attach["id"] as! Int, fileName: "\(imageUrl.lastPathComponent)", imageData: imageData!)
                                                })
                                            }
                                        })
                                    }
                                } else {
                                    // фотографии нет в кэше, сохраним ее в кэш
                                    photo = JSQPhotoMediaItem(image: nil)
                                    photo.appliesMediaViewMaskAsOutgoing = sender == getUserId()!.description
                                    
                                    var username : String = ""
                                    
                                    if self.members.count > 2 {
                                        for user in self.members {
                                            if user["id"] as! Int == item["user_id"] as! Int {
                                                username = user["username"] as! String
                                                break
                                            }
                                        }
                                    } else {
                                        username = sender == getUserId()!.description ? getUserLogin()! : self.userName
                                    }
                                    
                                    let message = JSQMessage(senderId: sender, senderDisplayName: username, date: Date(timeIntervalSince1970: item["created_at"] as! Double), media: photo)
                                    
                                    self.messages.insert(message!, at: 0)
                                    
                                    DispatchQueue(label: "loadImg\(attach["id"] as! Int)", attributes: []).async(execute: {
                                        let imageUrl : URL = URL(string: getBackendDomainPath() + (attach["content"] as! String))!
                                        
                                        let imageData : Data? = try? Data(contentsOf: imageUrl)
                                        
                                        if imageData != nil {
                                            DispatchQueue.main.async(execute: {
                                                photo.image = UIImage(data: imageData!)
                                                self.collectionView?.reloadData()
                                                
                                                avatarsCacheDataStack.saveAvatarToCache(attach["id"] as! Int, fileName: imageUrl.lastPathComponent, imageData: imageData!)
                                            })
                                        }
                                    })
                                }
                            }
                        } else {
                            var username : String = ""
                            
                            if self.members.count > 2 {
                                for user in self.members {
                                    if user["id"] as! Int == item["user_id"] as! Int {
                                        username = user["username"] as! String
                                        break
                                    }
                                }
                            } else {
                                username = sender == getUserId()!.description ? getUserLogin()! : self.userName
                            }
                            
                            let message = JSQMessage(senderId: sender, senderDisplayName: username, date: Date(timeIntervalSince1970: (pm as! NSDictionary)["created_at"] as! Double), text: (pm as! NSDictionary)["message"] as! String)
                            self.messages.insert(message!, at: 0)
                        }
                        i += 1
                    }
                    
                    DispatchQueue.main.async(execute: {
                        if !self.isLoaded {
                            self.isLoaded = true
                        }
                        self.collectionView!.reloadData()
                        //self.collectionView?.insertItemsAtIndexPaths(indexPaths)
                        
                        self.collectionView!.collectionViewLayout.invalidateLayout(with: JSQMessagesCollectionViewFlowLayoutInvalidationContext())
                        
                        self.finishReceivingMessage(animated: false)
                        self.collectionView!.layoutIfNeeded()
                        if !append {
                            self.scrollToBottom(animated: false)
                            self.totalCount = Int(messagesResponse.allHeaderFields["X-Pagination-Total-Count"] as! String)!
                        } else {
                            
                            self.collectionView!.scrollToItem(at: IndexPath(item: self.messagesLimit - 1, section: 0), at: .top, animated: false)
                        }
                        
                        if self.totalCount > self.messages.count {
                            self.showLoadEarlierMessagesHeader = true
                        }
                        
                        // обновим данный диалог в списке диалогов
                        // это просто 3.14здец
                        // получим индекс диалога, если список диалогов открыт
                        if dialogsController != nil {
                            let index = dialogsController?.dialogIds.index(of: self.dialogId!)
                            
                            if index != nil {
                                let indexPath = IndexPath(item: index!, section: 0)
                                
                                let message = dialogsController!.dialogs[indexPath.row]["message"] as? Dictionary<String, AnyObject>
                                
                                if message != nil {
                                    if message!["user_id"] as? Int != getUserId() && message!["viewed"] as! Int == 0 {
                                        dialogsController!.tableView.cellForRow(at: indexPath)?.backgroundColor = UIColor.white
                                        
                                        var dialog = dialogsController!.dialogs[indexPath.row]
                                        var new_message : Dictionary<String, AnyObject> = dialog["message"] as! Dictionary<String, AnyObject>
                                        
                                        new_message["viewed"] = 1 as AnyObject?
                                        dialog["message"] = new_message as AnyObject?
                                        
                                        dialogsController!.dialogs[indexPath.row] = dialog
                                    } else if message!["user_id"] as? Int == getUserId() {
                                        dialogsController!.tableView.cellForRow(at: indexPath)?.backgroundColor = UIColor.white
                                    }
                                }
                                
                                
                                UIApplication.shared.applicationIconBadgeNumber -= Int(dialogsController!.dialogs[indexPath.row]["unread"] as! String)!
                                
                                UIApplication.shared.applicationIconBadgeNumber = max (0, UIApplication.shared.applicationIconBadgeNumber)
                                
                                mainContainer?.tabBar.items![1].badgeValue = UIApplication.shared.applicationIconBadgeNumber > 0 ? UIApplication.shared.applicationIconBadgeNumber.description : nil
                                
                                dialogsController!.dialogs[indexPath.row]["unread"] = "0" as AnyObject?
                                (dialogsController!.tableView.cellForRow(at: indexPath) as! DialogCell).unreadCount.text = ""
                                (dialogsController!.tableView.cellForRow(at: indexPath) as! DialogCell).unreadCountBG.isHidden = true
                                
                                dialogsController!.tableView.deselectRow(at: indexPath, animated: true)
                            }
                        }
                    })
                } else if messagesResponse.statusCode == 401 {
                    error401Action(self)
                } else if messagesResponse.statusCode == 500 {
                    TSMessage.showNotification(
                        in: self,
                        title: "Ошибка",
                        subtitle: error500,
                        type: TSMessageNotificationType.error,
                        duration: 2.0,
                        canBeDismissedByUser: true
                    )
                }
            }
        }) 
        
        getMessagesTask.resume()
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, header headerView: JSQMessagesLoadEarlierHeaderView!, didTapLoadEarlierMessagesButton sender: UIButton!) {
        
        // от тут короче мы нажали загрузить исчо)
        self.showLoadEarlierMessagesHeader = false
        loadMessages(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToViewUserProfile" {
            self.notWillDismiss = true
            let destVC : UserProfile = segue.destination as! UserProfile
            
            if self.userId != nil {
                // мы пришли из профиля пользователя, к нему и вернемся
                destVC.userId = self.userId!
                
            } else {
                // мы пришли из списка диалогов, выберем нужный id
                for user in self.members {
                    if user["id"] as! Int != getUserId()! {
                        destVC.userId = user["id"] as! Int
                        break
                    }
                }
            }
            
            destVC.fromDialogs = true
        } else if segue.identifier == "goToLocationView" {
            let destVC : ViewLocation = segue.destination as! ViewLocation
            destVC.location = self.locationToView!
            self.locationToView = nil
            destVC.userName = self.userNameToViewInLocation!
            self.userNameToViewInLocation = nil
        } else if segue.identifier == "goToGroupView" {
            self.notWillDismiss = true
            
            let destVC : ViewGroup = segue.destination as! ViewGroup
            destVC.members = self.members
            destVC.creatorId = self.creatorId!
            
            if self.avatarPath != nil {
                destVC.avatarPath = self.avatarPath
            }
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func goToProfile(_ sender: AnyObject) {
        if self.members.count > 2 {
            self.performSegue(withIdentifier: "goToGroupView", sender: self)
        } else {
            self.performSegue(withIdentifier: "goToViewUserProfile", sender: self)
        }
    }
    
    // PEEK
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        
        guard let indexPath = self.collectionView.indexPathForItem(at: location) else {return nil}
        
        if self.messages[(indexPath as NSIndexPath).row].isMediaMessage {
            
            if self.messages[(indexPath as NSIndexPath).row].media.isKind(of: JSQLocationMediaItem.self) {
                self.previewImageVC = nil
                self.previewMapVC = nil
                
                // у нас тут геопозиция, надо открыть карту и показать местоположение
                self.locationToView = (self.messages[(indexPath as NSIndexPath).row].media as! JSQLocationMediaItem).location
                self.userNameToViewInLocation = self.messages[(indexPath as NSIndexPath).row].senderDisplayName
                
                self.previewMapVC = self.storyboard?.instantiateViewController(withIdentifier: "viewLocationVC") as? ViewLocation
                
                self.previewMapVC!.location = (self.messages[(indexPath as NSIndexPath).row].media as! JSQLocationMediaItem).location
                self.previewMapVC!.userName = self.messages[(indexPath as NSIndexPath).row].senderDisplayName
                
                return self.previewMapVC!
            } else if self.messages[(indexPath as NSIndexPath).row].media.isKind(of: JSQPhotoMediaItem.self) {
                self.previewMapVC = nil
                self.previewImageVC = nil
                
                // у нас тут фото, надо бы его показать
                let photo : attachedPhoto = attachedPhoto(image: (self.messages[(indexPath as NSIndexPath).row].media as! JSQPhotoMediaItem).image, attributedCaptionTitle: NSAttributedString(string: ""))
                
                self.previewImageVC = NYTPhotosViewController(photos: [photo])
                
                return self.previewImageVC
            }
        }
        
        return nil
    }
    
    // POP
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        
        if self.previewMapVC != nil {
            let previewMap = self.previewMapVC!
            show(previewMap, sender: self)
            self.previewMapVC = nil
        } else {
            let previewImage = self.previewImageVC!
            self.present(previewImage, animated: false, completion: nil)
            self.previewImageVC = nil
        }
    }
    
    // ---------------- EMPTY DATASET -----------------------
    func customView(forEmptyDataSet scrollView: UIScrollView!) -> UIView! {
        if self.isLoaded {
            // надо показать заглушку
            let view = EmptyDialog.instanceFromNib()
            
            return view
        } else {
            // надо показать индикатор загрузки
            let activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
            
            activityIndicator.color = UIColor.lightGray
            activityIndicator.startAnimating()
            
            return activityIndicator
        }
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        if !self.isLoaded {
            return true
        } else if self.isLoaded && self.message_dict.count == 0 {
            return true
        }
        
        return false
    }
    
    func emptyDataSetShouldAllowTouch(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func verticalOffset(forEmptyDataSet scrollView: UIScrollView!) -> CGFloat {
        return -100
    }
    // --------------- EMPTY DATASET -------------------------
}

class attachedPhoto: NSObject, NYTPhoto {
    
    var image: UIImage?
    var placeholderImage: UIImage?
    let attributedCaptionTitle: NSAttributedString
    let attributedCaptionSummary = NSAttributedString(string: "", attributes: [NSForegroundColorAttributeName: UIColor.gray])
    let attributedCaptionCredit = NSAttributedString(string: "", attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
    
    init(image: UIImage?, attributedCaptionTitle: NSAttributedString) {
        self.image = image
        self.attributedCaptionTitle = attributedCaptionTitle
        super.init()
    }
    
    convenience init(attributedCaptionTitle: NSAttributedString) {
        self.init(image: nil, attributedCaptionTitle: attributedCaptionTitle)
    }
    
}
