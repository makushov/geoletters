//
//  BackendSettings.swift
//  Letters 1.1
//
//  Created by KingOP on 22.01.16.
//  Copyright © 2016 KingOP. All rights reserved.
//

import Foundation

let isTesting : Bool = false

// production settings
let backendProtocol : String = "https"
let backendDomain : String = "geoletters.com"
let backendApiVersion : String = "3"

// development settings
let devBackendProtocol : String = "http"
let devBackendDomain : String = "5.39.34.132:8081"
let devBackendApiVersion : String = "3"

// globals
let backendApiDirectory : String = "api"
let messageSocketUrl : String = "\(backendProtocol)://\(backendDomain):3000"

// full path to API
func getBackendApiPath() -> String {
    if isTesting {
        return devBackendProtocol + "://" + devBackendDomain + "/" + backendApiDirectory + "/v" + devBackendApiVersion + "/"
    }
    
    return backendProtocol + "://" + backendDomain + "/" + backendApiDirectory + "/v" + backendApiVersion + "/"
}

// full path to attachments
func getBackendDomainPath() -> String {
    if isTesting {
        return devBackendProtocol + "://" + devBackendDomain
    }
    
    return backendProtocol + "://" + backendDomain
}
