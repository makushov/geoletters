//
//  CustomPickerController.swift
//  Letters 1.1
//
//  Created by KingOP on 23.06.16.
//  Copyright © 2016 KingOP. All rights reserved.
//

import UIKit

class CustomPickerController: UIImagePickerController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var shouldAutorotate : Bool {
        if self.sourceType == .camera {
            if DeviceType.IS_IPAD {
                return true
            } else {
                return false
            }
        }
        
        return true
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if self.sourceType == .camera {
            if DeviceType.IS_IPAD {
                return .all
            } else {
                return .portrait
            }
        }
        
        return .all
    }
}
