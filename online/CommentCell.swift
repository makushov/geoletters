//
//  CommentCell.swift
//  Letters 1.1
//
//  Created by KingOP on 24.01.16.
//  Copyright © 2016 KingOP. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {

    @IBOutlet weak var userLogin: UILabel!
    @IBOutlet weak var commentDatetime: UILabel!
    @IBOutlet weak var userAvatar: UIImageView!
    @IBOutlet weak var commentText: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
