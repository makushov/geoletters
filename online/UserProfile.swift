//
//  UserProfile.swift
//  online
//
//  Created by KingOP on 26.10.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit
import Haneke
import SafariServices
import TSMessages

@objc class UserProfile: UITableViewController, UITextViewDelegate, SFSafariViewControllerDelegate {
    
    
    @IBOutlet var avatar: UIImageView!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var userStatus: UITextView!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var emailButton: UIButton!
    
    let heightConstant : CGFloat = 220
    var textViewSized : Bool = false
    
    var currentUser : NSMutableDictionary = NSMutableDictionary()
    
    var phone : String = ""
    var email : String = ""
    
    var userId : Int = 0
    var fromDialogs : Bool = false
    
    var avatarPath : String? = nil
    
    var viewForSpinner : UIView = UIView()
    
    var blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
    var blurEffectView = UIVisualEffectView()
    
    var smallAvatarView : UIImageView = UIImageView()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Загрузка..."
        
        self.userStatus.delegate = self
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        self.avatar.frame = CGRect(x: 0, y: -(self.tableView.frame.size.width / 2), width: self.tableView.frame.size.width, height: (self.tableView.frame.size.width / 2))
        
        self.tableView.addSubview(self.avatar)
        self.tableView.contentInset = UIEdgeInsetsMake(self.heightConstant, 0, 0, 0)
        
        self.updateBlurView()
        
        self.showSpinner("Загрузка данных пользователя...")
        
        var userDataRequest : URLRequest
        
        if isUserAuthenticated() {
            userDataRequest = getURLRequest("GET", url: getBackendApiPath() + "user/authorized/\(self.userId)", params: Dictionary<String, AnyObject>(), token: getUserToken()!)
        } else {
            userDataRequest = getURLRequest("GET", url: getBackendApiPath() + "user/\(self.userId)")
        }
        
        let loadUserDataTask = URLSession.shared.dataTask(with: userDataRequest, completionHandler: { (data, response, error) in
            
            if (error != nil) {
                // сюда надо добавить обработку ошибок
                DispatchQueue.main.async(execute: {
                    self.hideSpinner()
                    
                    playErrorSound()
                    JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                })
            } else {
                let userInfoResponse = response as! HTTPURLResponse
                
                let userInfoResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSMutableDictionary
                
                if userInfoResponse.statusCode == 401 {
                    error401Action(self)
                } else if userInfoResponse.statusCode == 500 {
                    DispatchQueue.main.async(execute: {
                        self.hideSpinner()
                        
                        playErrorSound()
                        JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                    })
                } else {
                    DispatchQueue.main.async(execute: {
                        // обновим данные пользователя в окне
                        self.currentUser = userInfoResult!
                        
                        self.title = userInfoResult!["username"] as? String
                        
                        if (!(userInfoResult!["profileStatus"] is NSNull) && (isUserAuthenticated() && userInfoResult!["areYouBlacklisted"] as! Int == 0)) || !isUserAuthenticated() && (!(userInfoResult!["profileStatus"] is NSNull)) {
                            self.userStatus.text = ((userInfoResult!["profileStatus"] as! NSDictionary)["status"] as! String) + "\n"
                            self.userStatus.sizeToFit()
                        } else if !(userInfoResult!["profileStatus"] is NSNull) && userInfoResult!["profileStatus"] != nil && userInfoResult!["areYouBlacklisted"] as! Int == 1 {
                            self.userStatus.text = "Вы в черном списке у этого пользователя.\n"
                            self.userStatus.textColor = UIColor.red
                        } else {
                            self.userStatus.text = "Статус не задан"
                        }
                        
                        if !(userInfoResult!["phone"] is NSNull) && userInfoResult!["phone"] != nil && (userInfoResult!["phone"] as? String) != "" {
                            self.phone = userInfoResult!["phone"] as! String
                        } else {
                            self.callButton.isEnabled = false
                        }
                        
                        self.email = userInfoResult!["email"] as! String
                        
                        if !isUserAuthenticated() {
                            // не дадим незареганным писать в чат
                            self.chatButton.isEnabled = true
                            self.callButton.isEnabled = true
                            self.emailButton.isEnabled = true
                            self.menuButton.isEnabled = true
                        } else if self.userId == getUserId()! || userInfoResult!["areYouBlacklisted"] as! Int == 1 {
                            // не дадим звонить, чатить или писать себе
                            self.callButton.isEnabled = false
                            self.emailButton.isEnabled = false
                            self.chatButton.isEnabled = false
                            self.menuButton.isEnabled = true
                        } else {
                            self.menuButton.isEnabled = true
                            self.callButton.isEnabled = true
                            self.emailButton.isEnabled = true
                            self.menuButton.isEnabled = true
                            self.chatButton.isEnabled = true
                        }

                        self.tableView.reloadData()
                        
                        self.hideSpinner()
                        
                        // далее идет вечный гемор с аватарками
                        // проверим, есть ли ава у этого профиля
                        if !(userInfoResult!["photo"] is NSNull) && userInfoResult!["photo"] != nil && (userInfoResult!["photo"] as? String) != "" {
                            self.avatarPath = userInfoResult!["photo"] as? String
                            self.avatar.hnk_setImageFromURL(URL(string: getBackendDomainPath() + (userInfoResult!["photo"] as? String)!)!, placeholder: UIImage(named: "nophoto.jpg"))
                            
                            self.smallAvatarView.hnk_setImageFromURL(URL(string: getBackendDomainPath() + (userInfoResult!["photo"] as? String)!)!, placeholder: UIImage(named: "nophoto.jpg"))
                        }
                    })
                }
            }
        }) 
        
        loadUserDataTask.resume()
    }
    
    func updateBlurView() {
        if self.blurEffectView.superview != nil {
            self.blurEffectView.removeFromSuperview()
        }
        
        self.blurEffectView = UIVisualEffectView(effect: self.blurEffect)
        blurEffectView.frame = self.avatar.frame
        self.avatar.addSubview(self.blurEffectView)
        self.avatar.bringSubview(toFront: self.blurEffectView)
        
        let frame = CGRect(x: self.avatar.center.x - 25, y: self.avatar.center.y - 25, width: 50, height: 50)
        self.smallAvatarView = UIImageView(frame: frame)
        smallAvatarView.image = self.avatar.image
        
        smallAvatarView.layer.cornerRadius = 25
        
        smallAvatarView.layer.masksToBounds = true
        smallAvatarView.layer.borderWidth = 2.0
        smallAvatarView.layer.borderColor = UIColor.white.cgColor
        
        self.blurEffectView.addSubview(smallAvatarView)
        self.blurEffectView.bringSubview(toFront: self.smallAvatarView)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let yOffset = scrollView.contentOffset.y
        
        if yOffset < -self.heightConstant {
            var frame : CGRect = self.avatar.frame
            frame.origin.y = yOffset
            frame.size.height = -yOffset
            self.avatar.frame = frame
            
            blurEffectView.frame = frame
            
            let imageframe = CGRect(x: self.avatar.center.x - 25, y: self.avatar.center.y - 25, width: 50, height: 50)
            self.smallAvatarView.frame = imageframe
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isUserAuthenticated() && !(currentUser["areYouBlacklisted"] is NSNull) && currentUser["areYouBlacklisted"] != nil && currentUser["areYouBlacklisted"] as! Int == 1 {
            
            return 2
        }
        
        return 3
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        
        if URL.scheme == "http" || URL.scheme == "https" {
            
            if URL.host! == backendDomain && Int(URL.lastPathComponent)! != 0 {
                
                BridgeToUserProfile.open(Int(URL.lastPathComponent)!)
                return false
            }
            
            let vc = SFSafariViewController(url: URL, entersReaderIfAvailable: true)
            vc.delegate = self
            present(vc, animated: true, completion: nil)
            
            return false
        }
        
        return true
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath as NSIndexPath).section == 0 && (indexPath as NSIndexPath).row == 0 {
            return 84
        } else if (indexPath as NSIndexPath).section == 0 && (indexPath as NSIndexPath).row == 1 {
            var frame : CGRect = self.userStatus.frame
            frame.size.height = self.userStatus.contentSize.height
            self.userStatus.frame = frame
            
            return max(44, self.userStatus.frame.size.height)
        } else {
            return 44
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    @IBAction func callAction(_ sender: AnyObject) {
        let callAlertController : UIAlertController = UIAlertController(title: "Подтверждение", message: "Позвонить пользователю \(self.title!) (\(self.phone))?", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        callAlertController.addAction(UIAlertAction(title: "Позвонить", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            UIApplication.shared.openURL(URL(string: "tel://\(self.phone)")!)
        }))
        
        callAlertController.addAction(UIAlertAction(title: "Отмена", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(callAlertController, animated: true, completion: nil)
    }
    
    @IBAction func emailAction(_ sender: AnyObject) {
        let mailAlertController : UIAlertController = UIAlertController(title: "Подтверждение", message: "Написать email пользователю \(self.title!) (\(self.email))?", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        mailAlertController.addAction(UIAlertAction(title: "Написать", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            UIApplication.shared.openURL(URL(string: "mailto://\(self.email)")!)
        }))
        
        mailAlertController.addAction(UIAlertAction(title: "Отмена", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(mailAlertController, animated: true, completion: nil)
    }
    
    @IBAction func sendMessage(_ sender: AnyObject) {
        if !isUserAuthenticated() {
            let info = JSSAlertView().info(self, title: "Личные сообщения", text: "Обмен личными сообщениями с другими пользователями доступен только авторизованным пользователям", buttonText: "Войти", cancelButtonText: "Отмена")
            
            info.addAction({ () -> Void in
                self.performSegue(withIdentifier: "goToLoginFromUserProfile", sender: self)
            })
        } else {
            if self.fromDialogs == true {
                self.navigationController?.popViewController(animated: true)
            } else {
                self.performSegue(withIdentifier: "goToDialogWithUser", sender: "self")
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToDialogWithUser" {
            let destVC : Messages = segue.destination as! Messages
            
            destVC.userName = self.title!
            destVC.userId = self.userId
            destVC.avatar = self.avatar.image
            destVC.avatarPath = self.avatarPath
        } else if segue.identifier == "goToUserPinsView" {
            let destVC : ViewUsersPins = segue.destination as! ViewUsersPins
            destVC.userId = self.userId
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    
    @IBAction func shareUserAction(_ sender: AnyObject) {
        
        // спросим, что именно нужно сделать с пользователем
        let actionAlertController : UIAlertController = UIAlertController(title: "Выберите действие", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        // пожаловаться на пользователя 
        if self.userId != getUserId()! {
            
            let reportAction : UIAlertAction = UIAlertAction(title: "Пожаловаться", style: .default, handler: { (action) -> Void in
                
                self.reportUser()
            })
            
            actionAlertController.addAction(reportAction)
        }
        
        if isUserAuthenticated() && self.userId != getUserId()! {
            // добавить в избранное / удалить из избранного
            
            let favouriteAction : UIAlertAction = UIAlertAction(title: self.currentUser["isInFavorites"] as! Int == 0 ? "Добавить в избранные" : "Удалить из избранных", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                
                // тут добавление в избранное / удаление из избранного
                let favRequest = self.currentUser["isInFavorites"] as! Int == 0 ? getURLRequest("POST", url: getBackendApiPath() + "user/favorite", params: ["favorited_user_id" : self.userId], token: getUserToken()!) : getURLRequest("DELETE", url: getBackendApiPath() + "user/favorite/\(self.userId)", params: Dictionary<String, AnyObject>(), token: getUserToken()!)
                let favTask = URLSession.shared.dataTask(with: favRequest, completionHandler: {
                    (data, response, error) in
                    
                    if error != nil {
                        DispatchQueue.main.async(execute: {
                            self.hideSpinner()
                            
                            JSSAlertView().danger(self, title: "Ошибка", text: error?.localizedDescription, buttonText: "Закрыть")
                        })
                    } else {
                        let res = response as! HTTPURLResponse
                        
                        if res.statusCode == 401 {
                            error401Action(self)
                        } else if res.statusCode == 422 {
                            DispatchQueue.main.async(execute: {
                                self.hideSpinner()
                                
                                JSSAlertView().danger(self, title: "Ошибка", text: "Данный пользователь уже есть в ваших избранных.", buttonText: "Закрыть")
                            })
                        } else if res.statusCode == 404 {
                            DispatchQueue.main.async(execute: {
                                self.hideSpinner()
                                
                                JSSAlertView().danger(self, title: "Ошибка", text: "Данный пользователь отсутствует в ваших избранных.", buttonText: "Закрыть")
                            })
                        } else if res.statusCode == 201 {
                            // пользователь добавлен в избранное
                            self.currentUser["isInFavorites"] = 1
                            
                            DispatchQueue.main.async(execute: {
                                self.hideSpinner()
                                TSMessage.showNotification(
                                    in: self,//UIApplication.sharedApplication().delegate?.window??.rootViewController,
                                    title: "Успешно",
                                    subtitle: "Пользователь добавлен в избранные.",
                                    type: TSMessageNotificationType.success,
                                    duration: 2.0,
                                    canBeDismissedByUser: true
                                )
                            })
                        } else if res.statusCode == 204 {
                            // пользователь разблокирован
                            self.currentUser["isInFavorites"] = 0
                            
                            DispatchQueue.main.async(execute: {
                                self.hideSpinner()
                                TSMessage.showNotification(
                                    in: self,//UIApplication.sharedApplication().delegate?.window??.rootViewController,
                                    title: "Успешно",
                                    subtitle: "Пользователь удален из избранных.",
                                    type: TSMessageNotificationType.success,
                                    duration: 2.0,
                                    canBeDismissedByUser: true
                                )
                            })
                        } else if res.statusCode == 500 {
                            DispatchQueue.main.async(execute: {
                                self.hideSpinner()
                                
                                JSSAlertView().danger(self.navigationController!, title: "Ошибка", text: error500, buttonText: "Закрыть")
                            })
                        }
                    }
                }) 
                self.showSpinner(self.currentUser["isInFavorites"] as! Int == 0 ? "Добавление в избранное..." : "Удаление из избранных...")
                favTask.resume()
            })
            
            actionAlertController.addAction(favouriteAction)
        }
        
        // скопировать ссылку на профиль пользователя
        let copyLinkAction : UIAlertAction = UIAlertAction(title: "Скопировать ссылку", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
            
            UIPasteboard.general.string = "\(getBackendDomainPath())/user/\(self.userId)"
            TSMessage.showNotification(
                in: self,//UIApplication.sharedApplication().delegate?.window??.rootViewController,
                title: "Успешно",
                subtitle: "Ссылка на профиль пользователя скопирована в буфер обмена",
                type: TSMessageNotificationType.success,
                duration: 2.0,
                canBeDismissedByUser: true
            )
        }
        
        actionAlertController.addAction(copyLinkAction)
        
        if !isUserAuthenticated() {
            
            let blockAction : UIAlertAction = UIAlertAction(title: "Заблокировать", style: .destructive, handler: { (action) -> Void in
                
                let info = JSSAlertView().info(self, title: "Блокировка", text: "Для того, чтобы заблокировать пользователя, пожалуйста, выполните вход под Вашей учетной записью.", buttonText: "Войти", cancelButtonText: "Отмена")
                
                info.addAction({ () -> Void in
                    self.performSegue(withIdentifier: "goToLoginFromUserProfile", sender: self)
                })
            })
            
            actionAlertController.addAction(blockAction)
            
        } else if isUserAuthenticated() && self.userId != getUserId()!{
            // заблокировать / разблокировать пользователя
            
            let blockAction : UIAlertAction = UIAlertAction(title: self.currentUser["isInBlacklist"] as! Int == 0 ? "Заблокировать" : "Разблокировать", style: UIAlertActionStyle.destructive, handler: { (UIAlertAction) -> Void in
                
                // блокировка / разблокировка пользователя
                let blockUserRequest = self.currentUser["isInBlacklist"] as! Int == 0 ? getURLRequest("POST", url: getBackendApiPath() + "user/block", params: ["blacklisted_user_id" : self.userId], token: getUserToken()!) : getURLRequest("DELETE", url: getBackendApiPath() + "user/block/\(self.userId)", params: Dictionary<String, AnyObject>(), token: getUserToken()!)
                
                let blockUserTask = URLSession.shared.dataTask(with: blockUserRequest, completionHandler: {
                    (data, response, error) in
                    
                    if error != nil {
                        DispatchQueue.main.async(execute: {
                            self.hideSpinner()
                            
                            JSSAlertView().danger(self, title: "Ошибка", text: error?.localizedDescription, buttonText: "Закрыть")
                        })
                    } else {
                        let res = response as! HTTPURLResponse
                        
                        if res.statusCode == 401 {
                            error401Action(self)
                        } else if res.statusCode == 422 {
                            DispatchQueue.main.async(execute: {
                                self.hideSpinner()
                                
                                JSSAlertView().danger(self, title: "Ошибка", text: "Данный пользователь уже находится в Вашем черном списке.", buttonText: "Закрыть")
                            })
                        } else if res.statusCode == 404 {
                            DispatchQueue.main.async(execute: {
                                self.hideSpinner()
                                
                                JSSAlertView().danger(self, title: "Ошибка", text: "Данный пользователь отсутствует в вашем черном списке.", buttonText: "Закрыть")
                            })
                        } else if res.statusCode == 201 {
                            // пользователь заблокирован
                            self.currentUser["isInBlacklist"] = 1
                            
                            DispatchQueue.main.async(execute: {
                                self.hideSpinner()
                                
                                TSMessage.showNotification(
                                    in: self,//UIApplication.sharedApplication().delegate?.window??.rootViewController,
                                    title: "Успешно",
                                    subtitle: "Пользователь заблокирован",
                                    type: TSMessageNotificationType.success,
                                    duration: 2.0,
                                    canBeDismissedByUser: true
                                )
                            })
                        } else if res.statusCode == 204 {
                            // пользователь разблокирован
                            self.currentUser["isInBlacklist"] = 0
                            
                            DispatchQueue.main.async(execute: {
                                self.hideSpinner()
                                TSMessage.showNotification(
                                    in: self,//UIApplication.sharedApplication().delegate?.window??.rootViewController,
                                    title: "Успешно",
                                    subtitle: "Пользователь разблокирован",
                                    type: TSMessageNotificationType.success,
                                    duration: 2.0,
                                    canBeDismissedByUser: true
                                )
                            })
                        } else if res.statusCode == 500 {
                            DispatchQueue.main.async(execute: {
                                self.hideSpinner()
                                
                                JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                            })
                        }
                    }
                }) 
                
                self.showSpinner(self.currentUser["isInBlacklist"] as! Int == 0 ? "Блокировка пользователя..." : "Разблокировка пользователя...")
                blockUserTask.resume()
            })
            
            actionAlertController.addAction(blockAction)
        }
        
        // отмена
        let cancelAction : UIAlertAction = UIAlertAction(title: "Отмена", style: UIAlertActionStyle.cancel, handler: nil)
        
        actionAlertController.addAction(cancelAction)
        
        self.present(actionAlertController, animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        TSMessage.dismissActiveNotification()
        UIView.setAnimationsEnabled(true)
        super.viewWillDisappear(true)
    }
    
    func showSpinner(_ title: String) {
        UIView.setAnimationsEnabled(true)
        self.viewForSpinner = UIView(frame: CGRect(x: 0, y: self.view.frame.minY - self.heightConstant, width: self.view.frame.size.width, height: self.view.frame.size.height))
        self.view.addSubview(viewForSpinner)
        SwiftSpinner.show(title, animated: true, viewToDisplay: self.viewForSpinner)
    }
    
    func hideSpinner() {
        SwiftSpinner.hide()
        self.viewForSpinner.removeFromSuperview()
        self.viewForSpinner = UIView()
    }
    
    func reportUser(_ controller: UIViewController? = nil) {
        if !isUserAuthenticated() {
            let info = JSSAlertView().info(self, title: "Отправить жалобу", text: "Для того, чтобы отправить жалобу, пожалуйста, выполните вход под Вашей учетной записью.", buttonText: "Войти", cancelButtonText: "Отмена")
            
            info.addAction({ () -> Void in
                self.performSegue(withIdentifier: "goToLoginFromUserProfile", sender: self)
            })
        } else {
            let alertController = UIAlertController(title: "Пожаловаться", message: "Укажите, пожалуйста, причину жалобы (до 200 символов)", preferredStyle: UIAlertControllerStyle.alert)
            
            let reportAction = UIAlertAction(title: "Отправить жалобу", style: UIAlertActionStyle.destructive, handler: {
                alert -> Void in
                
                self.showSpinner("Отправка жалобы...")
                
                let firstTextField = alertController.textFields![0] as UITextField
                
                let reportText : String = firstTextField.text!.trimmingCharacters(in: CharacterSet.whitespaces)
                
                // вот тут отправка жалобы
                let sendReportRequest = getURLRequest("POST", url: "\(getBackendApiPath())report", params: ["description" : reportText, "target" : "user", "target_id" : self.userId], token: getUserToken()!)
                
                let sendReportTask = URLSession.shared.dataTask(with: sendReportRequest, completionHandler: {
                    (data, response, error) in
                    
                    if error != nil {
                        DispatchQueue.main.async(execute: {
                            self.hideSpinner()
                            
                            let errorView = JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                            
                            errorView.addAction({
                                self.navigationController?.popViewController(animated: true)
                            })
                        })
                    } else {
                        let res = response as! HTTPURLResponse
                        
                        if res.statusCode == 201 {
                            DispatchQueue.main.async(execute: {
                                self.hideSpinner()
                                
                                JSSAlertView().info(self, title: "Спасибо!", text: "Ваша жалоба принята к рассмотрению.", buttonText: "Продолжить")
                            })
                        } else if res.statusCode == 403 {
                            let json = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                            
                            DispatchQueue.main.async(execute: {
                                self.hideSpinner()
                                TSMessage.showNotification(
                                    in: UIApplication.shared.delegate?.window??.rootViewController,
                                    title: "Ошибка",
                                    subtitle: json!["message"] as! String,
                                    type: TSMessageNotificationType.error,
                                    duration: 2.0,
                                    canBeDismissedByUser: true
                                )
                                playErrorSound()
                            })
                        } else if res.statusCode == 422 {
                            let jsonResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSMutableArray
                            
                            if let parseJson = jsonResult {
                                var errorText = ""
                                
                                for error in parseJson {
                                    errorText += (error as! NSDictionary)["message"] as! String + "\n"
                                }
                                
                                DispatchQueue.main.async(execute: {
                                    self.hideSpinner()
                                    TSMessage.showNotification(
                                        in: UIApplication.shared.delegate?.window??.rootViewController,
                                        title: "Ошибка",
                                        subtitle: errorText,
                                        type: TSMessageNotificationType.error,
                                        duration: 2.0,
                                        canBeDismissedByUser: true
                                    )
                                    playErrorSound()
                                })
                            }
                        } else if res.statusCode == 401 {
                            error401Action(self)
                        } else if res.statusCode == 500 {
                            DispatchQueue.main.async(execute: {
                                self.hideSpinner()
                                
                                JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                            })
                        }
                    }
                }) 
                
                sendReportTask.resume()
            })
            
            let cancelAction = UIAlertAction(title: "Отмена", style: UIAlertActionStyle.cancel, handler: nil)
            
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "Причина жалобы..."
            }
            
            alertController.addAction(reportAction)
            alertController.addAction(cancelAction)
            
            if controller != nil {
                controller!.present(alertController, animated: true, completion: nil)
            } else {
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    override var previewActionItems : [UIPreviewActionItem] {
        
        var actions : [UIPreviewActionItem] = []
        
        // пожаловаться на пользователя
        if self.userId != getUserId()! {
            
            let reportAction : UIPreviewAction = UIPreviewAction(title: "Пожаловаться", style: .default, handler: { (action, vc) in
                
                self.reportUser(UIApplication.shared.delegate?.window??.rootViewController)
            })

            actions.append(reportAction)
        }
        
        if isUserAuthenticated() && self.userId != getUserId()! && self.currentUser["isInFavorites"] != nil {
            // добавить в избранное / удалить из избранного
            
            
            let favouriteAction : UIPreviewAction = UIPreviewAction(title: self.currentUser["isInFavorites"] as! Int == 0 ? "Добавить в избранные" : "Удалить из избранных", style: .default, handler: { (action, vc) -> Void in
                
                // тут добавление в избранное / удаление из избранного
                let favRequest = self.currentUser["isInFavorites"] as! Int == 0 ? getURLRequest("POST", url: getBackendApiPath() + "user/favorite", params: ["favorited_user_id" : self.userId], token: getUserToken()!) : getURLRequest("DELETE", url: getBackendApiPath() + "user/favorite/\(self.userId)", params: Dictionary<String, AnyObject>(), token: getUserToken()!)
                let favTask = URLSession.shared.dataTask(with: favRequest, completionHandler: {
                    (data, response, error) in
                    
                    if error != nil {
                        DispatchQueue.main.async(execute: {
                            self.hideSpinner()
                            
                            JSSAlertView().danger(self, title: "Ошибка", text: error?.localizedDescription, buttonText: "Закрыть")
                        })
                    } else {
                        let res = response as! HTTPURLResponse
                        
                        if res.statusCode == 401 {
                            error401Action(self)
                        } else if res.statusCode == 422 {
                            DispatchQueue.main.async(execute: {
                                self.hideSpinner()
                                
                                JSSAlertView().danger(self, title: "Ошибка", text: "Данный пользователь уже есть в ваших избранных.", buttonText: "Закрыть")
                            })
                        } else if res.statusCode == 404 {
                            DispatchQueue.main.async(execute: {
                                self.hideSpinner()
                                
                                JSSAlertView().danger(self, title: "Ошибка", text: "Данный пользователь отсутствует в ваших избранных.", buttonText: "Закрыть")
                            })
                        } else if res.statusCode == 201 {
                            // пользователь добавлен в избранное
                            self.currentUser["isInFavorites"] = 1
                            
                            DispatchQueue.main.async(execute: {
                                self.hideSpinner()
                                TSMessage.showNotification(
                                    in: UIApplication.shared.delegate?.window??.rootViewController,
                                    title: "Успешно",
                                    subtitle: "Пользователь добавлен в избранные.",
                                    type: TSMessageNotificationType.success,
                                    duration: 2.0,
                                    canBeDismissedByUser: true
                                )
                            })
                        } else if res.statusCode == 204 {
                            // пользователь разблокирован
                            self.currentUser["isInFavorites"] = 0
                            
                            DispatchQueue.main.async(execute: {
                                self.hideSpinner()
                                TSMessage.showNotification(
                                    in: UIApplication.shared.delegate?.window??.rootViewController,
                                    title: "Успешно",
                                    subtitle: "Пользователь удален из избранных.",
                                    type: TSMessageNotificationType.success,
                                    duration: 2.0,
                                    canBeDismissedByUser: true
                                )
                            })
                        } else if res.statusCode == 500 {
                            DispatchQueue.main.async(execute: {
                                self.hideSpinner()
                                
                                JSSAlertView().danger(self.navigationController!, title: "Ошибка", text: error500, buttonText: "Закрыть")
                            })
                        }
                    }
                }) 
                self.showSpinner(self.currentUser["isInFavorites"] as! Int == 0 ? "Добавление в избранное..." : "Удаление из избранных...")
                favTask.resume()
            })
            
            actions.append(favouriteAction)
        }
        
        // скопировать ссылку на профиль пользователя
        let copyLinkAction : UIPreviewAction = UIPreviewAction(title: "Скопировать ссылку", style: .default) { (action, vc) -> Void in
            
            UIPasteboard.general.string = "\(getBackendDomainPath())/user/\(self.userId)"
            TSMessage.showNotification(
                in: UIApplication.shared.delegate?.window??.rootViewController,
                title: "Успешно",
                subtitle: "Ссылка на профиль пользователя скопирована в буфер обмена",
                type: TSMessageNotificationType.success,
                duration: 2.0,
                canBeDismissedByUser: true
            )
        }
        
        actions.append(copyLinkAction)
        
        if !isUserAuthenticated() {
            
            let blockAction : UIPreviewAction = UIPreviewAction(title: "Заблокировать", style: .destructive, handler: { (action, vc) -> Void in
                
                let info = JSSAlertView().info(self, title: "Блокировка", text: "Для того, чтобы заблокировать пользователя, пожалуйста, выполните вход под Вашей учетной записью.", buttonText: "Войти", cancelButtonText: "Отмена")
                
                info.addAction({ () -> Void in
                    self.performSegue(withIdentifier: "goToLoginFromUserProfile", sender: self)
                })
            })
            
            actions.append(blockAction)
            
        } else if isUserAuthenticated() && self.userId != getUserId()! && self.currentUser["isInBlacklist"] != nil {
            // заблокировать / разблокировать пользователя
            
            let blockAction : UIPreviewAction = UIPreviewAction(title: self.currentUser["isInBlacklist"] as! Int == 0 ? "Заблокировать" : "Разблокировать", style: .destructive, handler: { (action, vc) -> Void in
                
                // блокировка / разблокировка пользователя
                let blockUserRequest = self.currentUser["isInBlacklist"] as! Int == 0 ? getURLRequest("POST", url: getBackendApiPath() + "user/block", params: ["blacklisted_user_id" : self.userId], token: getUserToken()!) : getURLRequest("DELETE", url: getBackendApiPath() + "user/block/\(self.userId)", params: Dictionary<String, AnyObject>(), token: getUserToken()!)
                
                let blockUserTask = URLSession.shared.dataTask(with: blockUserRequest, completionHandler: {
                    (data, response, error) in
                    
                    if error != nil {
                        DispatchQueue.main.async(execute: {
                            self.hideSpinner()
                            
                            JSSAlertView().danger(self, title: "Ошибка", text: error?.localizedDescription, buttonText: "Закрыть")
                        })
                    } else {
                        let res = response as! HTTPURLResponse
                        
                        if res.statusCode == 401 {
                            error401Action(self)
                        } else if res.statusCode == 422 {
                            DispatchQueue.main.async(execute: {
                                self.hideSpinner()
                                
                                JSSAlertView().danger(self, title: "Ошибка", text: "Данный пользователь уже находится в Вашем черном списке.", buttonText: "Закрыть")
                            })
                        } else if res.statusCode == 404 {
                            DispatchQueue.main.async(execute: {
                                self.hideSpinner()
                                
                                JSSAlertView().danger(self, title: "Ошибка", text: "Данный пользователь отсутствует в вашем черном списке.", buttonText: "Закрыть")
                            })
                        } else if res.statusCode == 201 {
                            // пользователь заблокирован
                            self.currentUser["isInBlacklist"] = 1
                            
                            DispatchQueue.main.async(execute: {
                                self.hideSpinner()
                                
                                TSMessage.showNotification(
                                    in: UIApplication.shared.delegate?.window??.rootViewController,
                                    title: "Успешно",
                                    subtitle: "Пользователь заблокирован",
                                    type: TSMessageNotificationType.success,
                                    duration: 2.0,
                                    canBeDismissedByUser: true
                                )
                            })
                        } else if res.statusCode == 204 {
                            // пользователь разблокирован
                            self.currentUser["isInBlacklist"] = 0
                            
                            DispatchQueue.main.async(execute: {
                                self.hideSpinner()
                                TSMessage.showNotification(
                                    in: UIApplication.shared.delegate?.window??.rootViewController,
                                    title: "Успешно",
                                    subtitle: "Пользователь разблокирован",
                                    type: TSMessageNotificationType.success,
                                    duration: 2.0,
                                    canBeDismissedByUser: true
                                )
                            })
                        } else if res.statusCode == 500 {
                            DispatchQueue.main.async(execute: {
                                self.hideSpinner()
                                
                                JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                            })
                        }
                    }
                }) 
                
                self.showSpinner(self.currentUser["isInBlacklist"] as! Int == 0 ? "Блокировка пользователя..." : "Разблокировка пользователя...")
                blockUserTask.resume()
            })
            
            actions.append(blockAction)
        }
        
        return actions
    }
}
