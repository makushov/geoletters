//
//  BridgeToPin.h
//  Letters 1.1
//
//  Created by KingOP on 05.02.16.
//  Copyright © 2016 KingOP. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BridgeToPin : NSObject
+(void)openPin:(NSInteger)pinId;
@end
