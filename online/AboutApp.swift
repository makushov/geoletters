//
//  AboutApp.swift
//  Letters
//
//  Created by KingOP on 09.12.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit

class AboutApp: UIViewController {

    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var appName: UILabel!
    @IBOutlet weak var copyright: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.appName.text = Bundle.applicationBundleDisplayName
        
        let version : String = "Версия: " + Bundle.applicationVersionNumber
        self.versionLabel.text = version
        
        let currentYear : Int = (Calendar.current as NSCalendar).component(NSCalendar.Unit.year, from: Date())
        
        self.copyright.text = "GeoLetters © \(currentYear)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func contactDeveloper(_ sender: AnyObject) {
    }
    
    @IBAction func dismissAction(_ sender: AnyObject) {
        
        self.dismiss(animated: true, completion: nil)
    }
}
