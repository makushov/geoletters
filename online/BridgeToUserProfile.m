//
//  BridgeToUserProfile.m
//  Letters
//
//  Created by KingOP on 29.11.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "online-Swift.h"
#import "BridgeToUserProfile.h"

@implementation BridgeToUserProfile

+(void)openUserProfile:(NSInteger)userId {
    
    UserProfile *userProfile = (UserProfile *)[[[[UIApplication sharedApplication] delegate] window].rootViewController.storyboard instantiateViewControllerWithIdentifier:@"userProfile"];
    
    userProfile.userId = userId;
    
    [(UINavigationController*)[(MainContainer*)[[[UIApplication sharedApplication] delegate] window].rootViewController selectedViewController] pushViewController:userProfile animated:YES];
}

// Assumes input like "#00FF00" (#RRGGBB).
+ (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

@end