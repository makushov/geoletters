//
//  EditProfile.swift
//  online
//
//  Created by KingOP on 28.10.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit
import Haneke
import RSKImageCropper

class EditProfile: UITableViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, RSKImageCropViewControllerDelegate {

    @IBOutlet weak var userLogin: MKTextField!
    @IBOutlet weak var userPassword: MKTextField!
    @IBOutlet weak var userPasswordConfirm: MKTextField!
    @IBOutlet weak var userEmail: MKTextField!
    @IBOutlet weak var userPhone: MKTextField!
    @IBOutlet weak var userSexSwitcher: UISegmentedControl!
    @IBOutlet weak var userAvatar: UIImageView!
    @IBOutlet weak var uploadPhotoButton: UIButton!
    @IBOutlet weak var deletePhotoButton: UIButton!
    
    var currentUserInfo : NSMutableDictionary = NSMutableDictionary()
    
    let imagePicker = CustomPickerController()
    
    var avatarImage : UIImage? = nil
    
    var isLoaded : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.userLogin.layer.borderColor = UIColor.clear.cgColor
        self.userLogin.floatingPlaceholderEnabled = true
        self.userLogin.placeholder = "Логин"
        self.userLogin.rippleLayerColor = UIColor.white
        self.userLogin.tintColor = UIColor.MKColor.Blue
        self.userLogin.text = self.currentUserInfo["username"] as? String
        
        self.userLogin.delegate = self
        
        self.userPassword.layer.borderColor = UIColor.clear.cgColor
        self.userPassword.floatingPlaceholderEnabled = true
        self.userPassword.placeholder = "Пароль"
        self.userPassword.rippleLayerColor = UIColor.white
        self.userPassword.tintColor = UIColor.MKColor.Blue
        
        self.userPassword.delegate = self
        
        self.userPasswordConfirm.layer.borderColor = UIColor.clear.cgColor
        self.userPasswordConfirm.floatingPlaceholderEnabled = true
        self.userPasswordConfirm.placeholder = "Подтверждение пароля"
        self.userPasswordConfirm.rippleLayerColor = UIColor.white
        self.userPasswordConfirm.tintColor = UIColor.MKColor.Blue
        
        self.userPasswordConfirm.delegate = self
        
        self.userPhone.layer.borderColor = UIColor.clear.cgColor
        self.userPhone.floatingPlaceholderEnabled = true
        self.userPhone.placeholder = "Телефон"
        self.userPhone.rippleLayerColor = UIColor.white
        self.userPhone.tintColor = UIColor.MKColor.Blue
        self.userPhone.text = (self.currentUserInfo["phone"] is NSNull || self.currentUserInfo["phone"] == nil || (self.currentUserInfo["phone"] as! String) == "") ? "" : self.currentUserInfo["phone"] as? String
        
        self.userPasswordConfirm.delegate = self
        
        self.userEmail.layer.borderColor = UIColor.clear.cgColor
        self.userEmail.floatingPlaceholderEnabled = true
        self.userEmail.placeholder = "Email"
        self.userEmail.rippleLayerColor = UIColor.white
        self.userEmail.tintColor = UIColor.MKColor.Blue
        self.userEmail.text = self.currentUserInfo["email"] as? String
        
        self.userEmail.delegate = self
        
        if !(self.currentUserInfo["gender"] is NSNull) && self.currentUserInfo["gender"] != nil && (self.currentUserInfo["gender"] as! String) != "" {
            self.userSexSwitcher.selectedSegmentIndex = (self.currentUserInfo["gender"] as! String) == "m" ? 1 : 2
        }
        
        self.imagePicker.delegate = self
        
        // вот начинается мутня с синхронизацией пользовательской аватарки
        // для начала проверим, есть ли ава у полученного пользователя
        if !(currentUserInfo["photo"] is NSNull) && currentUserInfo["photo"] != nil && (currentUserInfo["photo"] as? String) != "" {
            
            // аватарка есть
            let cache = Shared.imageCache
            
            let iconFormat = Format<UIImage>(name: "icons", diskCapacity: 10 * 1024 * 1024) { image in
                return maskedAvatarFromImage(image)
            }
            cache.addFormat(iconFormat)
            
            self.userAvatar.hnk_setImageFromURL(URL(string: getBackendDomainPath() + (currentUserInfo["photo"] as? String)!)!, placeholder: UIImage(named: "nophoto.jpg"), format: iconFormat)
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count: Int = 0
        
        if section == 0 {
            count = 4
        } else if section == 1 {
            if !(currentUserInfo["photo"] is NSNull) && currentUserInfo["photo"] != nil && (currentUserInfo["photo"] as? String) != "" {
                count = 4
            } else if self.avatarImage != nil {
                count = 4
            } else {
                count = 3
            }
        }
        
        return count
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    @IBAction func uploadPhoto(_ sender: AnyObject) {
        self.view.endEditing(true)
        
        self.imagePicker.allowsEditing = false
        
        let imagePickVariantController : UIAlertController = UIAlertController(title: "Сделайте выбор", message: "Что использовать для выбора изображения?", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        imagePickVariantController.addAction(UIAlertAction(title: "Камера", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            self.imagePicker.sourceType = .camera
            
            self.present(self.imagePicker, animated: true, completion: nil)
        }))
        
        imagePickVariantController.addAction(UIAlertAction(title: "Галерея", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            self.imagePicker.sourceType = .photoLibrary
            
            self.present(self.imagePicker, animated: true, completion: nil)
        }))
        
        imagePickVariantController.addAction(UIAlertAction(title: "Отмена", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(imagePickVariantController, animated: true, completion: nil)
    }
    
    @IBAction func saveChanges(_ sender: AnyObject) {
        self.view.endEditing(true)
        
        let login = self.userLogin.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        let password = self.userPassword.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        let passwordConfirm = self.userPasswordConfirm.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        let email = self.userEmail.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        
        let phone = self.userPhone.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        
        var error : String = ""
        
        if login == "" {
            error += "Вы не указали логин.\n"
        } else if login.length > 20 {
            error += "Длина логина не должна превышать 20 символов.\n"
        }
        if password != "" && passwordConfirm != "" && password != passwordConfirm {
            error += "Введенные пароли не совпадают.\n"
        }
        if email == "" {
            error += "Вы не указали email.\n"
        } else if email != "" && !isEmailCorrect(email) {
            error += "Вы указали некорректный email.\n"
        }
        if phone != "" && !isPhoneNumberCorrect(phone) {
            error += "Вы указали некорректный номер телефона.\n"
        }
        
        if error != "" {
            JSSAlertView().danger(self, title: "Ошибка", text: error, buttonText: "Закрыть")
        } else {
            SwiftSpinner.show("Сохранение изменений...", animated: true, viewToDisplay: self.view)
            
            var sex = ""
            
            if self.userSexSwitcher.selectedSegmentIndex == 1 {
                sex = "m"
            } else if self.userSexSwitcher.selectedSegmentIndex == 2 {
                sex = "f"
            }
            
            var params : Dictionary<String, AnyObject>
            
            if self.avatarImage != nil {
                
                let data = UIImageJPEGRepresentation(self.avatarImage!, 0.5)
                let encodedImage = data!.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters)
                
                params = [
                    "username"          : login as AnyObject,
                    "password"          : password as AnyObject,
                    "email"             : email as AnyObject,
                    "phone"             : phone as AnyObject,
                    "gender"            : sex as AnyObject,
                    "photo"             : encodedImage as AnyObject
                ]
            } else {
                params = [
                    "username"          : login as AnyObject,
                    "password"          : password as AnyObject,
                    "email"             : email as AnyObject,
                    "phone"             : phone as AnyObject,
                    "gender"            : sex as AnyObject,
                    "photo"             : "" as AnyObject
                ]
            }
            
            let updateUserRequest = getURLRequest("PATCH", url: getBackendApiPath() + "user/\(getUserId()!)", params: params, token: getUserToken())
            
            print(getUserId())
            
            let updateUserTask = URLSession.shared.dataTask(with: updateUserRequest, completionHandler: { (data, response, error) in
                
                if (error != nil) {
                    // сюда надо добавить обработку ошибок
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        
                        JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                    })
                } else {
                    let updateUserResponse = response as! HTTPURLResponse
                    print(updateUserResponse.statusCode)
                    
                    if updateUserResponse.statusCode ==  200 {
                        DispatchQueue.main.async(execute: {
                            
                            if login != getUserLogin() {
                                setUserLogin(login)
                            }
                            
                            SwiftSpinner.hide()
                            
                            let success = JSSAlertView().info(self, title: "Успешно", text: "Изменения успешно сохранены.", buttonText: "Продолжить")
                            
                            success.addAction({
                                self.navigationController?.dismiss(animated: true, completion: nil)
                            })
                        })
                    } else if updateUserResponse.statusCode == 401 {
                        error401Action(self)
                    } else if updateUserResponse.statusCode == 500 {
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            
                            JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                        })
                    } else if updateUserResponse.statusCode == 422 {
                        let jsonResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [Dictionary<String, AnyObject>]
                        
                        if let parseJson = jsonResult {
                            var errorText = ""
                            
                            for error in parseJson {
                                errorText += error["message"] as! String + "\n"
                            }
                            
                            DispatchQueue.main.async(execute: {
                                SwiftSpinner.hide()
                                
                                JSSAlertView().danger(self, title: "Ошибка", text: errorText, buttonText: "Закрыть")
                            })
                        }
                    }
                }
            }) 
            
            updateUserTask.resume()
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.avatarImage = pickedImage
        }
        
        self.dismiss(animated: true, completion: nil)
        
        let imageCropVC : RSKImageCropViewController = RSKImageCropViewController(image: self.avatarImage)
        imageCropVC.delegate = self
        self.navigationController?.pushViewController(imageCropVC, animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController!) {
        self.avatarImage = nil
        self.navigationController?.popViewController(animated: true)
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController!, didCropImage croppedImage: UIImage!, usingCropRect cropRect: CGRect) {
        SwiftSpinner.hide()
        
        self.userAvatar.image = maskedAvatarFromImage(croppedImage)
        self.avatarImage = croppedImage
        self.navigationController?.popViewController(animated: true)
        self.tableView.reloadData()
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController!, willCropImage originalImage: UIImage!) {
        SwiftSpinner.show("Сохранение...", animated: true, viewToDisplay: controller.view)
    }
    
    @IBAction func deletePhotoAction(_ sender: AnyObject) {
        if !(currentUserInfo["photo"] is NSNull) && currentUserInfo["photo"] != nil && (currentUserInfo["photo"] as? String) != "" {
            // нужно удалять фотку с сервака
            // а поэтому нужно спросить подтверждения
            let confirmAlertController : UIAlertController = UIAlertController(title: "Подтверждение", message: "Вы уверены, что хотите удалить фото профиля?", preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let deleteAction : UIAlertAction = UIAlertAction(title: "Удалить", style: UIAlertActionStyle.destructive, handler: { (UIAlertAction) -> Void in
                
                let deletePhotoRequest = getURLRequest("POST", url: getBackendApiPath() + "user/reset-photo", params: Dictionary<String, AnyObject>(), token: getUserToken())
                let deletePhotoTask = URLSession.shared.dataTask(with: deletePhotoRequest, completionHandler: { (data, response, error) -> Void in
                    
                    if (error != nil) {
                        // сюда надо добавить обработку ошибок
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                        })
                    } else {
                        let deletePhotoResponse = response as! HTTPURLResponse
                        
                        if deletePhotoResponse.statusCode == 401 {
                            // опачки, 401 ошибочка
                            error401Action(self)
                        } else if deletePhotoResponse.statusCode == 204 {
                            // фотка успешно удалена, удаляем ее из кэша
                            self.avatarImage = nil
                            self.currentUserInfo.setValue("", forKey: "photo")
                            
                            DispatchQueue.main.async(execute: {
                                self.userAvatar.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!)
                                self.tableView.reloadData()
                                SwiftSpinner.hide()
                            })
                        } else if deletePhotoResponse.statusCode == 500 {
                            DispatchQueue.main.async(execute: {
                                SwiftSpinner.hide()
                                
                                JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                            })
                        }
                    }
                })
                
                SwiftSpinner.show("Удаление фотографии...", animated: true, viewToDisplay: self.view)
                deletePhotoTask.resume()
            })
            
            let cancelAction : UIAlertAction = UIAlertAction(title: "Отмена", style: UIAlertActionStyle.cancel, handler: nil)
            
            confirmAlertController.addAction(deleteAction)
            confirmAlertController.addAction(cancelAction)
            
            self.present(confirmAlertController, animated: true, completion: nil)
        } else {
            // нужно удалить фотку с imageView
            self.avatarImage = nil
            self.userAvatar.image = maskedAvatarFromImage(UIImage(named: "nophoto_60.jpg")!)
            self.tableView.reloadData()
        }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
}
