//
//  Settings.swift
//  online
//
//  Created by KingOP on 26.10.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import Foundation

/* 
Поля пользователя, которые хранятся в нстройках:
 - id
 - token
 - login
*/

func isUserAuthenticated() -> Bool {
    return UserDefaults.standard.bool(forKey: "isAuthenticated")
}

func authenticateUser(_ id : Int, token: String, login: String) {
    // сохраним все пользовательские поля
    setUserId(id)
    setUserToken(token)
    setUserLogin(login)
    
    // отметим авторизацию в настройках
    UserDefaults.standard.set(true, forKey: "isAuthenticated")
}

func getUserDataForWatchKit() -> [String : Any] {
    return [
        "auth_token" : getUserToken()!,
        "user_id" : getUserId()!,
        "user_login" : getUserLogin()!
    ]
}

func getUserToken() -> String? {
    return UserDefaults.standard.string(forKey: "auth_token")
}

func setUserToken(_ token: String) {
    UserDefaults.standard.setValue(token, forKey: "auth_token")
}

func getUserId() -> Int? {
    return UserDefaults.standard.integer(forKey: "user_id")
}

func setUserId(_ id: Int) {
    UserDefaults.standard.set(id, forKey: "user_id")
}

func getUserLogin() -> String? {
    return UserDefaults.standard.string(forKey: "user_login")
}

func setUserLogin(_ login: String) {
    UserDefaults.standard.setValue(login, forKey: "user_login")
}

func isTutorialShown() -> Bool {
    return UserDefaults.standard.bool(forKey: "tutorialShown")
}

func setTutorialShown() {
    UserDefaults.standard.set(true, forKey: "tutorialShown")
}

func isTOSShown() -> Bool {
    return UserDefaults.standard.bool(forKey: "isTOSShown")
}

func setTOSShown() {
    UserDefaults.standard.set(true, forKey: "isTOSShown")
}

func isFirstSuggestionShown() -> Bool {
    return UserDefaults.standard.bool(forKey: "isFirstSuggestionShown")
}

func setFirstSuggestionShown() {
    UserDefaults.standard.set(true, forKey: "isFirstSuggestionShown")
}

func logout() {
    // вот тут еще надо аватарку удалить
    //unsubscribeFromPushes()
    
    UserDefaults.standard.set(0, forKey: "user_id")
    UserDefaults.standard.setValue("", forKey: "user_login")
    UserDefaults.standard.setValue("", forKey: "auth_token")
    UserDefaults.standard.set(false, forKey: "isAuthenticated")
    UserDefaults.standard.set(false, forKey: "isRegisteredForPushes")
    
    avatarsCacheDataStack.removeAllAvatarsFromCache()
    clearDeadCache()
    
    // удалим ярлык для быстрого создания метки
    UIApplication.shared.shortcutItems = []
    
    UIApplication.shared.applicationIconBadgeNumber = 0
    
    if mainContainer != nil {
        mainContainer?.tabBar.items![1].badgeValue = nil
    }
}

func isAppInstalled() -> Bool {
    return UserDefaults.standard.bool(forKey: "installed")
}

func setAppInstalled() {
    UserDefaults.standard.set(true, forKey: "installed")
}

func isRegisteredForPushes() -> Bool {
    return UserDefaults.standard.bool(forKey: "isRegisteredForPushes")
}

func getPushToken() -> String {
    return UserDefaults.standard.string(forKey: "pushToken") == nil ? "" : UserDefaults.standard.string(forKey: "pushToken")!
}

func savePushToken(_ token: String) {
    UserDefaults.standard.setValue(token, forKey: "pushToken")
}

func registerForPushes() {
    if !isRegisteredForPushes() && getPushToken() != "" {
        let registerForPushesRequest = getURLRequest("POST", url: getBackendApiPath() + "push/subscribe", params: ["device_token" : pushToken, "device_id" : deviceUDID, "mobile" : "ios"], token: getUserToken()!)
        
        let registerForPushesTask = URLSession.shared.dataTask(with: registerForPushesRequest, completionHandler: {
            (data, response, error) in
            
            if error == nil {
                let pushRegisterResponse = response as! HTTPURLResponse
                
                if pushRegisterResponse.statusCode == 201 {
                    UserDefaults.standard.set(true, forKey: "isRegisteredForPushes")
                    print("registered for pushes")
                    
                    setAppInstalled()
                }
            }
        }) 
        
        registerForPushesTask.resume()
    }
}

func unsubscribeFromPushes() {
    let unsubscribeFromPushesRequest = getURLRequest("POST", url: getBackendApiPath() + "push/unsubscribe", params: ["device_id" : deviceUDID], token: getUserToken()!)
    
    let unsubscribeFromPushesTask = URLSession.shared.dataTask(with: unsubscribeFromPushesRequest, completionHandler: {
        (data, response, error) in
        
        if error == nil {
            let pushRegisterResponse = response as! HTTPURLResponse
            
            print(pushRegisterResponse.statusCode)
            
            if pushRegisterResponse.statusCode == 204 {
                UserDefaults.standard.set(false, forKey: "isRegisteredForPushes")
            }
        }
    }) 
    
    unsubscribeFromPushesTask.resume()
}

// ----------------- new push settings ---------------------
func isFirstRegisteredForPushes() -> Bool {
    return UserDefaults.standard.bool(forKey: "isFirstRegisteredForPushes")
}

func firstRegisterForPushes() {
    if !isFirstRegisteredForPushes() && getPushToken() != "" {
        let registerForPushesRequest = getURLRequest("POST", url: getBackendApiPath() + "push/subscribe", params: ["device_token" : pushToken, "device_id" : deviceUDID, "mobile" : "ios", "on_message" : 1, "on_like" : 1, "on_friend_status" : 1, "on_comment" : 1], token: getUserToken()!)
        
        let registerForPushesTask = URLSession.shared.dataTask(with: registerForPushesRequest, completionHandler: {
            (data, response, error) in
            
            if error == nil {
                let pushRegisterResponse = response as! HTTPURLResponse
                
                if pushRegisterResponse.statusCode == 201 {
                    UserDefaults.standard.set(true, forKey: "isFirstRegisteredForPushes")
                    print("first registered for pushes")
                }
            }
        }) 
        
        registerForPushesTask.resume()
    }
}
