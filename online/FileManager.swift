//
//  FileManager.swift
//  online
//
//  Created by KingOP on 28.10.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import Foundation

func saveImageFile(_ name: String, image: Data) -> String? {
    let paths : NSArray = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
    let documentsDirectory : NSString = paths.object(at: 0)  as! NSString
    let filePath : String = documentsDirectory.appendingPathComponent(name)

    if (try? image.write(to: URL(fileURLWithPath: filePath as String), options: [.atomic])) != nil {
        return name
    }
    
    return nil
}

func deleteImageFile(_ filePath: String) -> Bool {
    let fileManager : FileManager = FileManager.default
    let paths : NSArray = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
    let documentsDirectory : NSString = paths.object(at: 0)  as! NSString
    
    let pathToFile : String = documentsDirectory.appendingPathComponent(filePath)
    var success : Bool = false
    
    do {
        try fileManager.removeItem(atPath: pathToFile)
        success = true
    } catch {
        success = false
    }
    
    return success
}

func getImageNSURL(_ fileName: String) -> URL? {
    let paths : NSArray = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
    let documentsDirectory : NSString = paths.object(at: 0)  as! NSString
    
    let fullPath : NSString = documentsDirectory.appendingPathComponent(fileName) as NSString
    
    if FileManager.default.fileExists(atPath: fullPath as String) {
        return URL(fileURLWithPath: fullPath as String, isDirectory: false)
    } else {
        return nil
    }
}

func getDocumentsFolderSize() -> String {
    // получим список файлов
    let paths : NSArray = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
    let documentsDirectory : NSString = paths.object(at: 0)  as! NSString
    
    do {
        let files : [String] = try FileManager.default.contentsOfDirectory(atPath: documentsDirectory as String)
        
        var size : Int = 0
        var sizeIndex : Int = 0
        
        let sizeMetrick : [String] = ["Б", "KБ", "MБ", "ГБ", "TБ"]
        
        for file in files {
            
            // тут нужно будет сделать проверку, чтобы файлы базы данных не считать за кэш
            if (file as NSString).pathExtension == "jpg" || (file as NSString).pathExtension == "png" || (file as NSString).pathExtension == "jpeg" || (file as NSString).pathExtension == "gif" {
                let fullPath : String = documentsDirectory.appendingPathComponent(file)
                
                do {
                    let fileAttributes : NSDictionary = try FileManager.default.attributesOfItem(atPath: fullPath) as NSDictionary
                    size += Int(fileAttributes.fileSize())
                } catch {
                    return "недоступно"
                }
            }
        }
        
        while size > 1024 {
            size /= 1024
            sizeIndex += 1
        }
        
        return "\(size) \(sizeMetrick[sizeIndex])"
    } catch {
        return "недоступно"
    }
}

func clearDeadCache() {
    let fileManager : FileManager = FileManager.default
    let paths : NSArray = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
    let documentsDirectory : NSString = paths.object(at: 0)  as! NSString
    
    do {
        let files : [String] = try FileManager.default.contentsOfDirectory(atPath: documentsDirectory as String)
        
        for file in files {
            
            // тут нужно будет сделать проверку, чтобы файлы базы данных не считать за кэш
            if (file as NSString).pathExtension == "jpg" || (file as NSString).pathExtension == "png" || (file as NSString).pathExtension == "jpeg" || (file as NSString).pathExtension == "gif" {
                let fullPath : String = documentsDirectory.appendingPathComponent(file)
                
                do {
                    try fileManager.removeItem(atPath: fullPath)
                } catch {

                }
            }
        }
    } catch {

    }
}
