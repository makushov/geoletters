//
//  PostMessage.swift
//  online
//
//  Created by KingOP on 23.10.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit
import MapKit
import SZTextView

class PostMessage: UITableViewController, UITextViewDelegate, UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var postText: SZTextView!
    @IBOutlet weak var userName: MKTextField!
    var mapController : AppleMap = AppleMap()
    var locationManager = CLLocationManager()
    @IBOutlet weak var limitLabel: UILabel!
    @IBOutlet weak var currentLocationLabel: UILabel!
    @IBOutlet weak var postFullText: SZTextView!
    
    @IBOutlet weak var selectedCatTitle: UILabel!
    @IBOutlet weak var isUrgently: UISwitch!
    @IBOutlet weak var attachedImage: UIImageView!
    
    @IBOutlet weak var selectedCatImage: UIImageView!
    
    var selectedLongitude : Double? = nil
    var selectedLatitude : Double? = nil
    
    var selectedCatId : Int = 0
    
    let imagePicker = CustomPickerController()
    
    var attachment : UIImage? = nil
    
    var mode : String = "add"
    
    var pinId : Int? = nil
    var pinToEdit : NSDictionary? = nil
    
    var myPinsController : MyPins? = nil
    var indexToUpdate : Int? = nil
    
    var photoChanged : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        self.postText.delegate = self
        self.postFullText.delegate = self
        
        self.userName.layer.borderColor = UIColor.clear.cgColor
        self.userName.floatingPlaceholderEnabled = true
        self.userName.placeholder = "Как вас зовут?"
        self.userName.rippleLayerColor = UIColor.white
        self.userName.tintColor = UIColor.MKColor.Blue
        
        self.userName.delegate = self
        
        self.imagePicker.delegate = self
        
        if self.mode == "edit" {
            // загрузим метку
            self.loadPinDefaults()
            //self.navigationItem.leftBarButtonItem = nil//UIBarButtonItem(title: "Назад", style: .Plain, target: self, action: Selector("goBack"))
        }
        
        if CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedWhenInUse {
            self.currentLocationLabel.text = "Выберите местоположение для вашего сообщения"
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView == self.postText {
            self.limitLabel.text = "Осталось знаков: " + (75 - ((textView.text.length - range.length) + text.length)).description
            
            return textView.text.length + (text.length - range.length) < 75
        }
        
        return true
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }

    @IBAction func saveAction(_ sender: AnyObject) {

        self.view.endEditing(true)
        
        var name : String? = nil
        
        let text = self.postText.text.trimmingCharacters(in: CharacterSet.whitespaces)
        
        let fullText = self.postFullText.text.trimmingCharacters(in: CharacterSet.whitespaces)
        
        if !isUserAuthenticated() {
            name = self.userName.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        }
        
        var error = ""
        
        if !isUserAuthenticated() && name == "" {
            error += "Вы не указали имя.\n"
        }
        
        if text == "" {
            error += "Вы не указали заголовок метки.\n"
        }
        
        if CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedWhenInUse && (self.selectedLatitude == nil || self.selectedLatitude == nil) {
            error += "Вы не указали местоположение для вашего сообщения.\n"
        }
        
        if error != "" {
            JSSAlertView().danger(self, title: "Ошибка", text: error, buttonText: "Закрыть")
        } else {
            var coordinates : CLLocationCoordinate2D? = CLLocationCoordinate2D()
            
            if self.selectedLatitude != nil && self.selectedLongitude != nil {
                coordinates = CLLocationCoordinate2D(latitude: self.selectedLatitude!, longitude: self.selectedLongitude!)
            } else {
                coordinates = self.locationManager.location?.coordinate
            }
            
            if coordinates == nil {
                let errorAlert = JSSAlertView().danger(self, title: "Ошибка", text: "Не удалось определить Ваше текущее местоположение. Пожалуйста, укажите его вручную. Приносим извинения за временные неудобства.\n", buttonText: "Закрыть")
                
                errorAlert.addAction({
                    self.currentLocationLabel.text = "Выберите местоположение для вашего сообщения"
                })
            } else {
                var params : Dictionary<String, String>
                var request : URLRequest
                
                
                if self.mode == "edit" {
                    if self.attachment != nil && self.photoChanged {
                        let data = UIImageJPEGRepresentation(self.attachment!, 0.5)
                        let encodedImage = data!.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters)
                        
                        params = [
                            "status"        : text,
                            "latitude"      : coordinates!.latitude.description,
                            "longitude"     : coordinates!.longitude.description,
                            "text"          : fullText,
                            "is_special"    : self.isUrgently.isOn == true ? "1" : "0",
                            "category_id"   : self.selectedCatId.description,
                            "photo"         : encodedImage
                        ]
                    } else {
                        params = [
                            "status"        : text,
                            "latitude"      : coordinates!.latitude.description,
                            "longitude"     : coordinates!.longitude.description,
                            "text"          : fullText,
                            "is_special"    : self.isUrgently.isOn == true ? "1" : "0",
                            "category_id"   : self.selectedCatId.description
                        ]
                    }
                    
                    request = getURLRequest("PATCH", url: getBackendApiPath() + "temprorary-status/\(self.pinId!)", params: params, token: getUserToken()!)
                    
                    let updatePinTask = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                        
                        if (error != nil) {
                            // сюда надо добавить обработку ошибок
                            DispatchQueue.main.async(execute: {
                                SwiftSpinner.hide()
                                JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                            })
                        } else {
                            let res = response as! HTTPURLResponse
                            
                            print(res.statusCode)
                            
                            if res.statusCode == 204 {
                                
                                (self.myPinsController?.pins[self.indexToUpdate!] as! NSMutableDictionary)["status"] = text
                                (self.myPinsController?.pins[self.indexToUpdate!] as! NSMutableDictionary)["category_id"] = self.selectedCatId
                                
                                DispatchQueue.main.async(execute: {
                                    self.myPinsController?.tableView.reloadData()
                                    
                                    SwiftSpinner.hide()
                                    
                                    let infoView = JSSAlertView().info(self, title: "Успешно!", text: "Изменения успешно сохранены.\n", buttonText: "Продолжить")
                                    
                                    infoView.addAction({
                                        self.dismiss(animated: true, completion: nil)
                                    })
                                })
                            } else if res.statusCode == 422 {
                                let jsonResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                                
                                if let parseJson = jsonResult {
                                    var errorText = ""
                                    
                                    for error in parseJson {
                                        errorText += (error as! NSDictionary)["message"] as! String + "\n"
                                    }
                                    
                                    DispatchQueue.main.async(execute: {
                                        SwiftSpinner.hide()
                                        
                                        JSSAlertView().danger(self, title: "Ошибка", text: errorText, buttonText: "Закрыть")
                                    })
                                }
                            } else if res.statusCode == 500 {
                                DispatchQueue.main.async(execute: {
                                    SwiftSpinner.hide()
                                    
                                    JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                                })
                            } else if res.statusCode == 401 {
                                error401Action(self)
                            } else if res.statusCode == 403 {
                                let jsonResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                                
                                if let parseJson = jsonResult {
                                    DispatchQueue.main.async(execute: {
                                        SwiftSpinner.hide()
                                        
                                        JSSAlertView().danger(self, title: "Ошибка", text: parseJson["message"] as! String + "\n", buttonText: "Закрыть")
                                    })
                                }
                            }
                        }
                    })
                    
                    SwiftSpinner.show("Сохранение...", animated: true, viewToDisplay: self.view)
                    
                    updatePinTask.resume()
                } else if self.mode == "add" {
                    if self.attachment != nil {
                        let data = UIImageJPEGRepresentation(self.attachment!, 0.5)
                        let encodedImage = data!.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters)
                        
                        if isUserAuthenticated() {
                            params = [
                                "status"        : text,
                                "latitude"      : coordinates!.latitude.description,
                                "longitude"     : coordinates!.longitude.description,
                                "text"          : fullText,
                                "is_special"    : self.isUrgently.isOn == true ? "1" : "0",
                                "category_id"   : self.selectedCatId.description,
                                "photo"         : encodedImage
                            ]
                            
                            request = getURLRequest("POST", url: getBackendApiPath() + "temprorary-status", params: params, token: getUserToken()!)
                        } else {
                            params = [
                                "name"          : name!,
                                "status"        : text,
                                "latitude"      : coordinates!.latitude.description,
                                "longitude"     : coordinates!.longitude.description,
                                "text"          : fullText,
                                "is_special"    : self.isUrgently.isOn == true ? "1" : "0",
                                "category_id"   : self.selectedCatId.description,
                                "photo"         : encodedImage
                            ]
                            
                            request = getURLRequest("POST", url: getBackendApiPath() + "temprorary-status/guest", params: params)
                        }
                    } else {
                        if isUserAuthenticated() {
                            params = [
                                "status"        : text,
                                "latitude"      : coordinates!.latitude.description,
                                "longitude"     : coordinates!.longitude.description,
                                "text"          : fullText,
                                "is_special"    : self.isUrgently.isOn == true ? "1" : "0",
                                "category_id"   : self.selectedCatId.description
                            ]
                            
                            request = getURLRequest("POST", url: getBackendApiPath() + "temprorary-status", params: params, token: getUserToken()!)
                        } else {
                            params = [
                                "name"          : name!,
                                "status"        : text,
                                "latitude"      : coordinates!.latitude.description,
                                "longitude"     : coordinates!.longitude.description,
                                "text"          : fullText,
                                "is_special"    : self.isUrgently.isOn == true ? "1" : "0",
                                "category_id"   : self.selectedCatId.description
                            ]
                            
                            request = getURLRequest("POST", url: getBackendApiPath() + "temprorary-status/guest", params: params)
                        }
                    }
                    
                    let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                        
                        if (error != nil) {
                            // сюда надо добавить обработку ошибок
                            DispatchQueue.main.async(execute: {
                                SwiftSpinner.hide()
                                JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                            })
                        } else {
                            let res = response as! HTTPURLResponse
                            
                            print(res.statusCode)
                            
                            if res.statusCode == 201 {
                                
                                let jsonResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                                
                                DispatchQueue.main.async(execute: {
                                    SwiftSpinner.hide()
                                    
                                    if isUserAuthenticated() {
                                        let pinMarker = StatusPin(coordinate: coordinates!, title: getUserLogin()!, subtitle: text, id: jsonResult!["id"] as? Int, catId: jsonResult!["category_id"] as! Int, isUrgency: (jsonResult!["is_special"] as! Int) == 0 ? false : true)
                                        pinMarker.userId = getUserId()
                                        
                                        self.mapController.clusteringManager.addAnnotations([pinMarker])
                                        
                                        let center = CLLocationCoordinate2D(latitude: (coordinates?.latitude)!, longitude: coordinates!.longitude)
                                        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
                                        
                                        self.mapController.mapView.setRegion(region, animated: true)
                                        self.mapController.mapView.selectAnnotation(pinMarker, animated: true)
                                    } else {
                                        let pinMarker = StatusPin(coordinate: coordinates!, title: name!, subtitle: text, id: jsonResult!["id"] as? Int, catId: jsonResult!["category_id"] as! Int, isUrgency: (jsonResult!["is_special"] as! Int) == 0 ? false : true)
                                        
                                        self.mapController.clusteringManager.addAnnotations([pinMarker])
                                        
                                        let center = CLLocationCoordinate2D(latitude: (coordinates?.latitude)!, longitude: coordinates!.longitude)
                                        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
                                        
                                        self.mapController.mapView.setRegion(region, animated: true)
                                        self.mapController.mapView.selectAnnotation(pinMarker, animated: true)
                                    }
                                    
                                    self.dismiss(animated: true, completion: nil)
                                })
                            } else if res.statusCode == 422 {
                                let jsonResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [Dictionary<String, AnyObject>]
                                
                                if let parseJson = jsonResult {
                                    var errorText = ""
                                    
                                    for error in parseJson {
                                        errorText += error["message"] as! String + "\n"
                                    }
                                    
                                    DispatchQueue.main.async(execute: {
                                        SwiftSpinner.hide()
                                        
                                        JSSAlertView().danger(self, title: "Ошибка", text: errorText, buttonText: "Закрыть")
                                    })
                                }
                            } else if res.statusCode == 500 {
                                DispatchQueue.main.async(execute: {
                                    SwiftSpinner.hide()
                                    
                                    JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                                })
                            } else if res.statusCode == 401 {
                                error401Action(self)
                            } else if res.statusCode == 403 {
                                let jsonResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary<String, AnyObject>
                                
                                if let parseJson = jsonResult {
                                    DispatchQueue.main.async(execute: {
                                        SwiftSpinner.hide()
                                        
                                        JSSAlertView().danger(self, title: "Ошибка", text: parseJson["message"] as! String + "\n", buttonText: "Закрыть")
                                    })
                                }
                            }
                        }
                    }) 
                    SwiftSpinner.show("Сохранение...", animated: true, viewToDisplay: self.view)
                    
                    task.resume()
                }
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isUserAuthenticated() {
            return 7
        } else {
            return 8
        }
    }
    
    @IBAction func cancelAction(_ sender: AnyObject) {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToLocationSelect" {
            let destVC : SelectLocation = segue.destination as! SelectLocation
            destVC.postMessageController = self
            
            if self.selectedLatitude != nil && self.selectedLongitude != nil {
                destVC.latitude = self.selectedLatitude
                destVC.longitude = self.selectedLongitude
                destVC.locationText = self.currentLocationLabel.text
            }
        } else if segue.identifier == "goToPinCategorySelect" {
            let destVC : SelectPinCategory = segue.destination as! SelectPinCategory
            destVC.postMessageController = self
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView == self.postFullText {
            let fixedWidth = textView.frame.size.width
            textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = textView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            textView.frame = newFrame
            
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath as NSIndexPath).section == 0 && (indexPath as NSIndexPath).row == 1 {
            let fixedWidth = self.postFullText.frame.size.width
            self.postFullText.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = self.postFullText.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            
            return max(newSize.height + 10, 85)
        } else if (indexPath as NSIndexPath).section == 0 && (indexPath as NSIndexPath).row == 0 {
            return 85
        } else if (indexPath as NSIndexPath).section == 0 && (indexPath as NSIndexPath).row == 2 {
            return 58
        } else if (indexPath as NSIndexPath).section == 0 && (indexPath as NSIndexPath).row == 6 {
            return self.attachment == nil ? CGFloat.leastNormalMagnitude : 58
        } else {
            return 44
        }
    }
    
    @IBAction func attachImage(_ sender: AnyObject) {
        let imagePickVariantController : UIAlertController = UIAlertController(title: "Сделайте выбор", message: "Что использовать для выбора изображения?", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        imagePickVariantController.addAction(UIAlertAction(title: "Камера", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            self.imagePicker.sourceType = .camera
            
            self.present(self.imagePicker, animated: true, completion: nil)
        }))
        
        imagePickVariantController.addAction(UIAlertAction(title: "Галерея", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            self.imagePicker.sourceType = .photoLibrary
            
            self.present(self.imagePicker, animated: true, completion: nil)
        }))
        
        imagePickVariantController.addAction(UIAlertAction(title: "Отмена", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(imagePickVariantController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.attachment = pickedImage
            self.attachedImage.image = attachment
            self.attachedImage.contentMode = .scaleAspectFit
            self.tableView.reloadData()
            
            if self.mode == "edit" {
                self.photoChanged = true
            }
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func deleteAttachedPhoto(_ sender: AnyObject) {
        
        let confirmController : UIAlertController = UIAlertController(title: "Подтверждение", message: "Удалить выбранное изображение?", preferredStyle: .actionSheet)
        
        confirmController.addAction(UIAlertAction(title: "Удалить", style: .destructive, handler: { (action) -> Void in
            
            self.attachedImage.image = UIImage(named: "no_image.png")
            self.attachment = nil
            self.tableView.reloadData()
        }))
        
        confirmController.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        
        self.present(confirmController, animated: true, completion: nil)
    }
    
    func putToolbarOnTopOfKeyboard(_ textView: UITextView) {
        let toolbar : KeyboardToolbar = Bundle.main.loadNibNamed("KeyboardToolbar", owner: self, options: nil)![0] as! KeyboardToolbar
        
        toolbar.doneButton.action = #selector(PostMessage.hideKeyboard(_:))
        textView.inputAccessoryView = toolbar
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        putToolbarOnTopOfKeyboard(textView)
        return true
    }
    
    func hideKeyboard(_ sender: AnyObject) {
        self.view.endEditing(true)
    }
    
    
    func loadPinDefaults() {
        let request = getURLRequest("GET", url: "\(getBackendApiPath())temprorary-status/authorized/\(self.pinId!)", params: Dictionary<String, AnyObject>(), token: getUserToken()!)
        
        let loadPinTask = URLSession.shared.dataTask(with: request, completionHandler: {
            (data, response, error) in
            
            if error != nil {
                DispatchQueue.main.async(execute: {
                    SwiftSpinner.hide()
                    
                    let errorView = JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                    
                    errorView.addAction({
                        self.dismiss(animated: true, completion: nil)
                    })
                })
            } else {
                let res = response as! HTTPURLResponse
                
                if res.statusCode == 200 {
                    
                    self.pinToEdit = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSMutableDictionary
                    
                    self.selectedLongitude = (self.pinToEdit!["longitude"] as! String).doubleValue
                    self.selectedLatitude = (self.pinToEdit!["latitude"] as! String).doubleValue
                    
                    let coordinates = CLLocationCoordinate2DMake(self.selectedLatitude!, self.selectedLongitude!)
                    
                    DispatchQueue.main.async(execute: {
                        
                        self.currentLocationLabel.text = "Загрузка..."
                        
                        if !(self.pinToEdit!["text"] is NSNull) && self.pinToEdit!["text"] != nil && self.pinToEdit!["text"] as! String != "" {
                            
                            self.postFullText.text = "\(self.pinToEdit!["text"] as! String)\n"
                        } else {
                            self.postFullText.text = ""
                        }
                        
                        self.postFullText.sizeToFit()
                        self.postText.text = self.pinToEdit!["status"] as? String
                        
                        self.isUrgently.isOn = (self.pinToEdit!["is_special"] as! Int) == 1
                        
                        
                        if (self.pinToEdit!["category_id"] as! Int) != 0 && (self.pinToEdit!["category_id"] as! Int) < pinsCategories.count {
                            self.selectedCatTitle.text = pinsCategories[self.pinToEdit!["category_id"] as! Int].title
                            self.selectedCatImage.image = UIImage(named: pinsCategories[self.pinToEdit!["category_id"] as! Int].image)
                            self.selectedCatId = self.pinToEdit!["category_id"] as! Int
                        }
                        
                        self.tableView.reloadData()
                        
                        var address : String = ""
                        
                        let geoCoder = CLGeocoder()
                        geoCoder.reverseGeocodeLocation(CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude)) {
                            (placemarks, error) -> Void in
                            
                            let placeArray = placemarks as [CLPlacemark]!
                            
                            // Place details
                            var placeMark: CLPlacemark!
                            placeMark = placeArray?[0]
                            
                            // Location name
                            if let locationName = placeMark.addressDictionary?["Name"] as? String {
                                address += locationName + ", "
                            }
                            
                            // Street address
                            if let street = placeMark.addressDictionary?["Thoroughfare"] as? String {
                                if !address.contains(street) {
                                    address += street + ", "
                                }
                            }
                            
                            // City
                            if let city = placeMark.addressDictionary?["City"] as? String {
                                if !address.contains(city) {
                                    address += city + ", "
                                }
                            }
                            
                            // Zip code
                            if let zip = placeMark.addressDictionary?["ZIP"] as? String {
                                address += zip + ", "
                            }
                            
                            // Country
                            if let country = placeMark.addressDictionary?["Country"] as? String {
                                address += country + ", "
                            }
                            
                            address = address.truncate(address.length - 2)
                            
                            self.currentLocationLabel.text = address == "" ? "Выберите местоположение" : address
                        }
                        
                        if !(self.pinToEdit!["photo"] is NSNull) && self.pinToEdit!["photo"] != nil && self.pinToEdit!["photo"] as! String != "" {
                            
                            // загрузим изображение
                            DispatchQueue(label: "loadAttach", attributes: []).async(execute: {
                                let imageData : Data? = try? Data(contentsOf: URL(string: "\(getBackendDomainPath())\(self.pinToEdit!["photo"] as! String)")!)
                                
                                if imageData != nil {
                                    self.attachment = UIImage(data: imageData!)
                                    
                                    DispatchQueue.main.async(execute: {
                                        self.attachedImage.image = self.attachment!
                                        self.tableView.reloadData()
                                    })
                                }
                            })
                        }
                        
                        self.tableView.reloadData()
                        SwiftSpinner.hide()
                    })
                    
                } else if res.statusCode == 404 {
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        
                        let errorView = JSSAlertView().danger(self, title: "Ошибка", text: "Выбранной метки не существует.", buttonText: "Закрыть")
                        
                        errorView.addAction({
                            self.dismiss(animated: true, completion: nil)
                        })
                    })
                } else if res.statusCode == 500 {
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        
                        let errorView = JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                        
                        errorView.addAction({
                            self.dismiss(animated: true, completion: nil)
                        })
                    })
                }
            }
        }) 
        
        SwiftSpinner.show("Загрузка...", animated: true, viewToDisplay: self.view)
        
        loadPinTask.resume()
    }
    
    func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
}
