//
//  StatusPinView.swift
//  Letters
//
//  Created by KingOP on 21.11.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit

class StatusView: UIView {

    @IBOutlet weak var status: UILabel!
    //@IBOutlet weak var likes: UILabel!
    //@IBOutlet weak var comments: UILabel!
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "StatusView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
}
