//
//  OfflineAvatars+CoreDataProperties.swift
//  
//
//  Created by KingOP on 30.10.15.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension OfflineAvatars {

    @NSManaged var id: String?
    @NSManaged var fileName: String?

}
