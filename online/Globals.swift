//
//  Globals.swift
//  online
//
//  Created by KingOP on 26.10.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import Foundation
import UIKit

let googleApiKey : String = "AIzaSyD6-GzXIQXOgmRJ7vPIgnWnRg5tN0OCJHY"

// UDID устройства
let deviceUDID : String = (UIDevice.current.identifierForVendor?.uuidString)!

var resetPasswordToken : String = ""

let avatarsCacheDataStack : AvatarsCacheDataStack = AvatarsCacheDataStack()

var pushToken : String = ""

var messagesController : Messages? = nil

var pushUserInfo : Dictionary<String, AnyObject>? = nil

var mainContainer : MainContainer? = nil

var dialogsController : Dialogs? = nil

var viewPinController : PinDetailView? = nil
var viewPinCommentsController : PinComments? = nil

var isDialogsListShown : Bool = false

var filterString : String? = nil


// далее идут тексты ошибок
let error404 : String = "Запрашиваемый адрес не найден на сервере."
let error500 : String = "На сервере произошла ошибка. Пожалуйста, повторите ваш запрос позже."
let error403 : String = "Ошибка доступа. Вам не разрешен доступ к данному содержимому."
let error401 : String = "Ваша сессия была сброшена. Для продолжения Вам необходима повторная авторизация."

var avatarCache = [String:UIImage]()
