//
//  SendTextMessagesExtension.swift
//  Letters 1.1
//
//  Created by KingOP on 26.06.16.
//  Copyright © 2016 KingOP. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import TSMessages
import SocketIO

extension Messages {
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        self.stopTyping()
        
        if self.socket.status == .connected {
            self.socket.emit("send", text)
        } else {
            // добавим сообщение в диалог
            let new_message : JSQMessage = JSQMessage(senderId: getUserId()!.description, senderDisplayName: self.senderDisplayName, date: Date(timeIntervalSince1970: getCurrentTimestamp()), text: text)
            
            let dictionary : Dictionary<String, AnyObject> = [
                "user_id"       : getUserId()! as AnyObject,
                "created_at"    : getCurrentTimestamp() as AnyObject,
                "message"       : text as AnyObject,
                "status"        : "sending" as AnyObject,
                "id"            : -1 as AnyObject
            ]
            
            
            self.message_dict.append(dictionary)
            
            self.messages.append(new_message)
            self.finishSendingMessage()
            
            // проверим, отправляем ли мы сообщение в существующий диалог, или создаем новый
            if self.dialogId == nil {
                
                // мы создаем новый диалог
                let params = [
                    "users"     : self.userId!.description,
                    ]
                
                let createDialogRequest : URLRequest = getURLRequest("POST", url: getBackendApiPath() + "dialog", params: params, token: getUserToken())
                
                let createDialogTask = URLSession.shared.dataTask(with: createDialogRequest, completionHandler: { (data, response, error) in
                    
                    if (error != nil) {
                        // сюда надо добавить обработку ошибок
                        DispatchQueue.main.async(execute: {
                            
                            self.view.endEditing(true)
                            
                            TSMessage.showNotification(in: self,
                                                       title: "Ошибка",
                                                       subtitle: error?.localizedDescription,
                                                       type: TSMessageNotificationType.error,
                                                       duration: 2.0,
                                                       canBeDismissedByUser: true
                            )
                            playErrorSound()
                        })
                    } else {
                        let infoResponse = response as! HTTPURLResponse
                        
                        let infoResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                        
                        if infoResponse.statusCode == 401 {
                            error401Action(self)
                        } else if infoResponse.statusCode == 422 {
                            DispatchQueue.main.async(execute: {
                                
                                self.view.endEditing(true)
                                
                                TSMessage.showNotification(in: self,
                                                           title: "Ошибка",
                                                           subtitle: infoResult!["message"] as? String,
                                                           type: TSMessageNotificationType.error,
                                                           duration: 2.0,
                                                           canBeDismissedByUser: true
                                )
                                playErrorSound()
                            })
                        } else if infoResponse.statusCode == 500 {
                            DispatchQueue.main.async(execute: {
                                SwiftSpinner.hide()
                                
                                self.view.endEditing(true)
                                
                                TSMessage.showNotification(in: self,
                                                           title: "Ошибка",
                                                           subtitle: error500,
                                                           type: TSMessageNotificationType.error,
                                                           duration: 2.0,
                                                           canBeDismissedByUser: true
                                )
                                playErrorSound()
                            })
                        } else if infoResponse.statusCode == 403 {
                            DispatchQueue.main.async(execute: {
                                
                                self.view.endEditing(true)
                                
                                TSMessage.showNotification(in: self,
                                                           title: "Ошибка",
                                                           subtitle: "Вы находитесь в черном списке у этого пользователя и не можете отправлять ему сообщения.",
                                                           type: TSMessageNotificationType.error,
                                                           duration: 2.0,
                                                           canBeDismissedByUser: true
                                )
                                playErrorSound()
                            })
                        } else {
                            // диалог успешно создан
                            // отправим в него сообщение
                            self.dialogId = infoResult!["id"] as? Int
                            self.members = infoResult!["members"] as! [Dictionary<String, AnyObject>]
                            
                            self.processSendingTextMessage(text, chatId: self.dialogId!, dictionary: dictionary)
                        }
                    }
                }) 
                
                createDialogTask.resume()
            } else {
                // диалог существует
                // отправляем сообщение в существующий
                // но сначала проверим состояние сокета
                
                if self.socket.status == SocketIOClientStatus.connected {
                    // соединение по сокету установлено, отправим сообщение по нему
                    //self.socket.emit("send", [text])
                    self.processSendingTextMessage(text, chatId: self.dialogId!, dictionary: dictionary)
                } else {
                    // соединения по сокету нет, отправим просто через API
                    self.processSendingTextMessage(text, chatId: self.dialogId!, dictionary: dictionary)
                }
            }
        }
    }
    
    func processSendingTextMessage(_ text: String, chatId: Int, dictionary: Dictionary<String, AnyObject>) {
        // отправим сообщение
        self.sendMessage(self.dialogId!, text: text, completionHandler: { (message: Dictionary<String, AnyObject>?, statusCode: Int, errorText: String) in
            
            if statusCode == 0 {
                // ошибка при отправке
                TSMessage.showNotification(in: self,
                    title: "Ошибка",
                    subtitle: errorText,
                    type: TSMessageNotificationType.error,
                    duration: 2.0,
                    canBeDismissedByUser: true
                )
                
                let index = self.message_dict.index(where: {$0["created_at"] as! Double == dictionary["created_at"] as! Double})
                var message = self.message_dict[index!]
                
                message["status"] = "errorSending" as AnyObject?
                
                self.message_dict[index!] = message
                
                self.collectionView.reloadData()
                
                playErrorSound()
            } else {
                if statusCode == 201 {
                    // успешно отправлено
                    playSendSound()
                    
                    let index = self.message_dict.index(where: {$0["created_at"] as! Double == dictionary["created_at"] as! Double})
                    var message_in_dict = self.message_dict[index!]
                    
                    message_in_dict["status"] = "sent" as AnyObject?
                    message_in_dict["id"] = message!["id"]
                    
                    self.message_dict[index!] = message_in_dict
                    
                    self.collectionView.reloadData()
                    
                    // тут надо обновить список диалогов, если он был открыт
                    if dialogsController != nil {
                        // получим индекс
                        let index = dialogsController?.dialogIds.index(of: self.dialogId!)
                        
                        if index != nil {
                            // текущий диалог есть в списке диалогов, обновим его
                            var dialog : Dictionary<String, AnyObject> = dialogsController!.dialogs[index!]
                            
                            if dialog["message"] == nil {
                                let new_message : Dictionary<String, AnyObject> = [
                                    "created_at"    : message!["created_at"]!,
                                    "user_id"       : getUserId()! as AnyObject,
                                    "viewed"        : 0 as AnyObject,
                                    "message"       : message!["message"]!
                                ]
                                
                                dialog["message"] = new_message as AnyObject?
                            } else {
                                var new_message : Dictionary<String, AnyObject> = dialog["message"] as! Dictionary<String, AnyObject>
                                
                                new_message["user_id"] = getUserId()! as AnyObject?
                                new_message["created_at"] = message!["created_at"] as! Double as AnyObject?
                                new_message["viewed"] = 0 as AnyObject?
                                new_message["message"] = message!["message"]!
                                
                                dialog["message"] = new_message as AnyObject?
                            }
                            
                            dialog["unread"] = "0" as AnyObject?
                            
                            // сделаем перемещение
                            dialogsController!.dialogs.remove(at: index!)
                            dialogsController!.dialogIds.remove(at: index!)
                            
                            dialogsController!.dialogs.insert(dialog, at: 0)
                            dialogsController!.dialogIds.insert(self.dialogId!, at: 0)
                        } else {
                            // диалога не было, его надо создать и впихнуть
                            
                            let new_message : Dictionary<String, AnyObject> = [
                                "user_id"       : getUserId()! as AnyObject,
                                "created_at"    : message!["created_at"]!,
                                "message"       : message!["message"]!,
                                "attachment"    : NSNull()
                            ]
                            
                            let dialog : Dictionary<String, AnyObject> = [
                                "message"           : new_message as AnyObject,
                                "id"                : self.dialogId as AnyObject,
                                "unread"            : 0 as AnyObject,
                                "master_user_id"    : getUserId()! as AnyObject,
                                "members"           : self.members as AnyObject
                            ]
                            
                            // сделаем перемещение
                            dialogsController!.dialogs.insert(dialog, at: 0)
                            dialogsController!.dialogIds.insert(self.dialogId!, at: 0)
                            dialogsController!.currentOffset += 1
                        }
                    }
                } else if statusCode == 401 {
                    error401Action(self)
                } else if statusCode == 403 {
                    self.view.endEditing(true)
                    
                    TSMessage.showNotification(in: self,
                        title: "Ошибка",
                        subtitle: "Вы находитесь в черном списке у этого пользователя и не можете отправлять ему сообщения.",
                        type: TSMessageNotificationType.error,
                        duration: 2.0,
                        canBeDismissedByUser: true
                    )
                    let index = self.message_dict.index(where: {$0["created_at"] as! Double == dictionary["created_at"] as! Double})
                    var message = self.message_dict[index!]
                    
                    message["status"] = "errorSending" as AnyObject?
                    
                    self.message_dict[index!] = message
                    self.collectionView.reloadData()
                    playErrorSound()
                } else if statusCode == 500 {
                    self.view.endEditing(true)
                    
                    TSMessage.showNotification(in: self,
                        title: "Ошибка",
                        subtitle: error500,
                        type: TSMessageNotificationType.error,
                        duration: 2.0,
                        canBeDismissedByUser: true
                    )
                    let index = self.message_dict.index(where: {$0["created_at"] as! Double == dictionary["created_at"] as! Double})
                    var message = self.message_dict[index!]
                    
                    message["status"] = "errorSending" as AnyObject?
                    
                    self.message_dict[index!] = message
                    self.collectionView.reloadData()
                    playErrorSound()
                }
            }
        })
    }
    
    func sendMessage(_ chatId: Int, text: String, completionHandler: @escaping (_ message : Dictionary<String, AnyObject>?, _ statusCode: Int, _ errorText: String) -> ()) {
        let sendMessageRequest = getURLRequest("POST", url: getBackendApiPath() + "dialog/\(chatId)/messages", params: ["message" : text], token: getUserToken())
        
        let sendMessageTask = URLSession.shared.dataTask(with: sendMessageRequest, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                DispatchQueue.main.async(execute: {
                    completionHandler(nil, 0, (error?.localizedDescription)!)
                })
            } else {
                let res = response as! HTTPURLResponse
                
                print(res.statusCode)
                
                if res.statusCode == 201 {
                    let message = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, AnyObject>
                    
                    DispatchQueue.main.async(execute: {
                        completionHandler(message, 201, "")
                    })
                } else if res.statusCode == 403 {
                    // нет комнаты диалога
                    completionHandler(nil, 403, "")
                } else if res.statusCode == 500 {
                    completionHandler(nil, 500, "")
                }
            }
        }) 
        
        sendMessageTask.resume()
    }
    
    func doUnsendedMessagesTasks(_ indexPath: IndexPath) {
        let index = (indexPath as NSIndexPath).row
        
        let retryAlert : UIAlertController = UIAlertController(title: "Сообщение не отправлено", message: "При отправке сообщения произошла ошибка", preferredStyle: .actionSheet)
        
        retryAlert.addAction(UIAlertAction(title: "Повторить отправку", style: .default, handler: { (action:UIAlertAction) in
            
            // попробуем отправить сообщение повторно
            let text : String = self.message_dict[index]["message"] as! String
            //let summ : Int =
            
            // удалим текущее сообщение из списка
            self.deleteMessage(indexPath)
            
            // создадим новое сообщение
            let message : JSQMessage = JSQMessage(senderId: getUserId()!.description, senderDisplayName: self.senderDisplayName, date: Date(timeIntervalSince1970: getCurrentTimestamp()), text: text)
            
            let dictionary : Dictionary<String, AnyObject> = [
                "user_id"   : getUserId()! as AnyObject,
                "created_at"    : getCurrentTimestamp() as AnyObject,
                "message"       : text as AnyObject,
                "status"        : "sending" as AnyObject,
                "id"            : -1 as AnyObject
            ]
            
            self.message_dict.append(dictionary)
            
            self.messages += [message]
            self.finishSendingMessage()
            
            self.processSendingTextMessage(text, chatId: self.dialogId!, dictionary: dictionary)
        }))
        
        retryAlert.addAction(UIAlertAction(title: "Удалить сообщение", style: .destructive, handler: { (action:UIAlertAction) in
            
            // отменим отправку сообщения
            self.deleteMessage(indexPath)
        }))
        
        self.present(retryAlert, animated: true, completion: nil)
    }
    
    // удаление сообщения из диалога
    func deleteMessage(_ indexPath: IndexPath) {
        self.message_dict.remove(at: indexPath.row)
        self.messages.remove(at: indexPath.row)
        self.collectionView!.deleteItems(at: [indexPath])
    }
}
