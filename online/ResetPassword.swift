//
//  ResetPassword.swift
//  online
//
//  Created by KingOP on 28.10.15.
//  Copyright © 2015 KingOP. All rights reserved.
//

import UIKit

class ResetPassword: UITableViewController, UITextFieldDelegate {

    @IBOutlet weak var userEmail: MKTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.userEmail.layer.borderColor = UIColor.clear.cgColor
        self.userEmail.floatingPlaceholderEnabled = true
        self.userEmail.placeholder = "Email"
        self.userEmail.rippleLayerColor = UIColor.white
        self.userEmail.tintColor = UIColor.MKColor.Blue
        
        self.userEmail.delegate = self
        
        self.userEmail.becomeFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    @IBAction func resetPassword(_ sender: AnyObject) {
        self.view.endEditing(true)
        
        let email : String = self.userEmail.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        
        if email == "" {
            JSSAlertView().danger(self, title: "Ошибка", text: "Вы не указали Ваш email", buttonText: "Закрыть")
        } else if !isEmailCorrect(email) {
            JSSAlertView().danger(self, title: "Ошибка", text: "Вы указали некорректный email", buttonText: "Закрыть")
        } else {
            SwiftSpinner.show("Отправка данных...", animated: true, viewToDisplay: self.view)
            
            let params : Dictionary<String, AnyObject> = [
                "email" : email as AnyObject
            ]
            
            let request : URLRequest = getURLRequest("POST", url: getBackendApiPath() + "user/request-password-reset", params: params)
            
            let resetPasswordTask = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                
                if (error != nil) {
                    // сюда надо добавить обработку ошибок
                    DispatchQueue.main.async(execute: {
                        SwiftSpinner.hide()
                        
                        JSSAlertView().danger(self, title: "Ошибка", text: error!.localizedDescription, buttonText: "Закрыть")
                    })
                } else {
                    let resetPasswordResponse = response as! HTTPURLResponse
                    print(resetPasswordResponse.statusCode)
                    
                    if resetPasswordResponse.statusCode ==  204 {
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            
                            let success = JSSAlertView().info(self, title: "Успешно", text: "Инструкции по установке нового пароля отправлены Вам на email.", buttonText: "Продолжить")
                            
                            success.addAction({
                                self.navigationController?.dismiss(animated: true, completion: nil)
                            })
                        })
                    } else if resetPasswordResponse.statusCode == 422 {
                        let tokenResult = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                        
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            
                            JSSAlertView().danger(self, title: "Ошибка", text: tokenResult!["message"] as? String, buttonText: "Закрыть")
                        })
                    } else if resetPasswordResponse.statusCode == 500 {
                        DispatchQueue.main.async(execute: {
                            SwiftSpinner.hide()
                            
                            JSSAlertView().danger(self, title: "Ошибка", text: error500, buttonText: "Закрыть")
                        })
                    }
                }
            }) 
            
            resetPasswordTask.resume()
        }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
}
