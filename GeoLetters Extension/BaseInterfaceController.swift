//
//  BaseInterfaceController.swift
//  Letters 1.1
//
//  Created by Stanislav Makushov on 4/6/17.
//  Copyright © 2017 KingOP. All rights reserved.
//

import WatchKit
import Foundation

class BaseInterfaceController: WKInterfaceController {
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}

//MARK: actions
extension BaseInterfaceController {
    
    func showNeedLoginAlert(withRetryAction action: (() -> Void)?) {
        if let repeatAction = action {
            let retryAction = WKAlertAction(title: "Повторить", style: .default, handler: {
                repeatAction()
            })
            let cancelAction = WKAlertAction(title: "Отмена", style: .cancel, handler: {})
            
            self.presentAlert(withTitle: "Ошибка", message: "Вы не авторизованы", preferredStyle: .alert, actions: [retryAction, cancelAction])
        } else {
            
        }
    }
}
