//
//  DialogRowController.swift
//  Letters 1.1
//
//  Created by Stanislav Makushov on 4/5/17.
//  Copyright © 2017 KingOP. All rights reserved.
//

import WatchKit

class DialogRowController: NSObject {
    
    @IBOutlet var avatarImageView: WKInterfaceImage!
    @IBOutlet var usernameLabel: WKInterfaceLabel!
    @IBOutlet var messageLabel: WKInterfaceLabel!
    @IBOutlet var datetimeLabel: WKInterfaceLabel!
    
    @IBOutlet var group: WKInterfaceGroup!
    
    var dialog: Dictionary<String, AnyObject>? {
        didSet {
            guard let dialog = dialog else { return }
            
            var username : String = ""
            var avatar : Dictionary<String, AnyObject>? = nil
            
            let members : [Dictionary<String, AnyObject>] = dialog["members"] as! [Dictionary<String, AnyObject>]
            
            if members.count > 2 {
                if !(dialog["title"] is NSNull) && dialog["title"] != nil && dialog["title"] as! String != "" {
                    username = dialog["title"] as! String
                } else {
                    for user in members {
                        username += user["username"] as! String + ", "
                    }
                    
                    username = username.truncate(username.length - 2)
                }
            } else {
                for user in members {
                    if user["id"] as! Int != getUserId()! {
                        username = user["username"] as! String
                        
                        if !(user["photo"] is NSNull) && user["photo"] != nil && user["photo"] as! String != "" {
                            // у пользователя есть аватарка, сохраним ее во временную переменную
                            avatar = [
                                "id"    : user["id"] as! Int as AnyObject,
                                "photo" : user["photo"] as! String as AnyObject
                            ]
                        } else {
                            avatar = [
                                "id"    : user["id"] as! Int as AnyObject
                            ]
                        }
                        
                        break
                    }
                }
            }
            
            var messageText : String = "Нет сообщений"
            self.datetimeLabel.setText("")
            
            if !(dialog["message"] is NSNull) && dialog["message"] != nil {
                messageText = ((dialog["message"] as! NSDictionary)["message"] as? String)!
                self.datetimeLabel.setText(stringDateFromTimestamp((dialog["message"] as! NSDictionary)["created_at"] as! Double))
                
                if !((dialog["message"] as! NSDictionary)["attachment"] is NSNull) && (dialog["message"] as! NSDictionary)["attachment"] != nil {
                    if ((dialog["message"] as! NSDictionary)["attachment"] as! NSDictionary)["type"] as! String == "location" {
                        messageText = "Местоположение"
                    } else {
                        messageText = "Изображение"
                    }
                }
            }
            
            self.messageLabel.setText(messageText)
            self.usernameLabel.setText(username)
            
            avatarImageView.setImage(maskedAvatarFromImage(#imageLiteral(resourceName: "nophoto.jpg")))
            
            if members.count > 2 {
                if !(dialog["photo"] is NSNull) && dialog["photo"] != nil && dialog["photo"] as! String != "" {
                    
                    let path = "\(getBackendDomainPath())\(dialog["photo"] as! String)"
                    
                    ImageLoader.sharedLoader.imageForUrl(urlString: path, completionHandler:{(image: UIImage?, url: String) in
                        if let image = image {
                            self.avatarImageView.setImage(maskedAvatarFromImage(image))
                        }
                    })
                }
            } else {
                if (avatar?.count)! > 1 {
                    let path = "\(getBackendDomainPath())\(avatar!["photo"] as! String)"
                    
                    ImageLoader.sharedLoader.imageForUrl(urlString: path, completionHandler:{(image: UIImage?, url: String) in
                        if let image = image {
                            self.avatarImageView.setImage(maskedAvatarFromImage(image))
                        }
                    })
                }
            }
            
            if Int(dialog["unread"] as! String)! > 0 {
                self.group.setBackgroundColor(UIColor(netHex: 0x5C5E66))
            }
        }
    }
}
