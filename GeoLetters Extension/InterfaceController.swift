//
//  InterfaceController.swift
//  GeoLetters Extension
//
//  Created by Stanislav Makushov on 04.04.17.
//  Copyright © 2017 KingOP. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: BaseInterfaceController {

    @IBOutlet var table: WKInterfaceTable!
    var dialogs: [Dictionary<String, AnyObject>] = []
    
    var dialogIds : [Int] = []
    
    var isLoaded : Bool = false
    
    var dialogsLimit : Int = 15
    var currentOffset : Int = 0
    
    @IBOutlet var refreshButton: WKInterfaceButton!
    @IBOutlet var loadingIndicator: WKInterfaceImage!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        if !self.isLoaded {
            self.table.setNumberOfRows(0, withRowType: "WKDialogCell")
            self.loadDialogs()
        }
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
}

//MARK: table
extension InterfaceController {
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        // perform segue to messages
    }
}

//MARK: Actions
extension InterfaceController {
    
    @IBAction func reloadAction() {
        self.currentOffset = 0
        self.table.setNumberOfRows(0, withRowType: "WKDialogCell")
        self.loadDialogs()
    }
    
    func startAnimating() {
        self.refreshButton.setHidden(true)
        self.table.setHidden(true)
        self.loadingIndicator.setHidden(false)
        self.loadingIndicator.setImageNamed("Activity")
        self.loadingIndicator.startAnimatingWithImages(in: NSRange(location: 0, length: 15), duration: 1.0, repeatCount: 0)
    }
    
    func stopAnimating() {
        self.loadingIndicator.stopAnimating()
        self.loadingIndicator.setImage(nil)
        self.loadingIndicator.setHidden(true)
        self.refreshButton.setHidden(false)
        self.table.setHidden(false)
    }
    
    @IBAction func markAllRead() {
        // удаляем диалог
        let markAllDialogsRequest = getURLRequest("POST", url: getBackendApiPath() + "dialog/read-all", params: Dictionary<String, AnyObject>(), token: getUserToken())
        
        let markAllDialogsTask = URLSession.shared.dataTask(with: markAllDialogsRequest, completionHandler: {
            (data, response, error) in
            
            if (error != nil) {
                // сюда надо добавить обработку ошибок
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                    
                    let retryAction = WKAlertAction(title: "Повторить", style: .default, handler: {
                        self.markAllRead()
                    })
                    let cancelAction = WKAlertAction(title: "Отмена", style: .cancel, handler: {})
                    
                    self.presentAlert(withTitle: "Ошибка", message: error?.localizedDescription, preferredStyle: .alert, actions: [retryAction, cancelAction])
                })
            } else {
                let res = response as! HTTPURLResponse
                
                print(res.statusCode)
                
                if res.statusCode == 204 {
                    // отметка прошла успешно
                    DispatchQueue.main.async(execute: {
                        self.currentOffset = 0
                        self.loadDialogs(true, append: false, withCallback: {
                            self.stopAnimating()
                            
                            let action = WKAlertAction(title: "OK", style: .cancel, handler: {})
                            
                            self.presentAlert(withTitle: "Успешно", message: "Все диалоги отмечены прочитанными.", preferredStyle: .alert, actions: [action])
                        })
                    })
                } else if res.statusCode == 401 {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                        
                        let retryAction = WKAlertAction(title: "Повторить", style: .default, handler: {
                            self.markAllRead()
                        })
                        let cancelAction = WKAlertAction(title: "Отмена", style: .cancel, handler: {})
                        
                        self.presentAlert(withTitle: "Ошибка", message: "Вы не авторизованы", preferredStyle: .alert, actions: [retryAction, cancelAction])
                    })
                } else if res.statusCode == 500 {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                        
                        let retryAction = WKAlertAction(title: "Повторить", style: .default, handler: {
                            self.markAllRead()
                        })
                        let cancelAction = WKAlertAction(title: "Отмена", style: .cancel, handler: {})
                        
                        self.presentAlert(withTitle: "Ошибка", message: "На сервере произошла ошибка. Пожалуйста, повторите ваш запрос позже.", preferredStyle: .alert, actions: [retryAction, cancelAction])
                    })
                }
            }
        })
        
        self.startAnimating()
        
        markAllDialogsTask.resume()
    }
    
    func loadDialogs(_ noAnimation : Bool = false, append: Bool = false, withCallback: (() -> Void)? = nil) {
        let getDialogsRequest = getURLRequest("GET", url: getBackendApiPath() + "dialog?limit=\(self.dialogsLimit)&offset=\(self.currentOffset)", params: Dictionary<String, AnyObject>(), token: getUserToken())
        
        let getDialogDataTask = URLSession.shared.dataTask(with: getDialogsRequest) { (data, response, error) in
            
            self.isLoaded = true
            
            if (error != nil) {
                // сюда надо добавить обработку ошибок
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                    
                    let retryAction = WKAlertAction(title: "Повторить", style: .default, handler: {
                        self.loadDialogs(noAnimation, append: append)
                    })
                    let cancelAction = WKAlertAction(title: "Отмена", style: .cancel, handler: {})
                    
                    self.presentAlert(withTitle: "Ошибка", message: error?.localizedDescription, preferredStyle: .alert, actions: [retryAction, cancelAction])
                })
            } else {
                let dialogsResponse = response as! HTTPURLResponse

                if dialogsResponse.statusCode == 200 {
                    let dialogList = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [Dictionary<String, AnyObject>]
                    
                    if dialogList.count == 0 && !append {
                        self.dialogs = dialogList
                    } else if dialogList.count == 0 && append {
                        self.currentOffset = self.dialogs.count
                    } else {
                        
                        if !append {
                            self.dialogs.removeAll()
                            self.dialogIds.removeAll()
                            self.dialogs = dialogList
                        } else {
                            if dialogList.count >= self.dialogsLimit {
                                self.dialogs.append(contentsOf: dialogList)
                            } else if dialogList.count > 0 && dialogList.count < self.dialogsLimit {
                                self.dialogs.append(contentsOf: dialogList)
                            } else if dialogList.count == 0 {
                            }
                        }
                        
                        self.currentOffset = self.dialogs.count
                    }
                    
                    self.isLoaded = true
                    
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                        
                        self.table.setNumberOfRows(self.dialogs.count, withRowType: "WKDialogCell")
                        
                        for index in 0..<self.dialogs.count {
                            guard let controller = self.table.rowController(at: index) as? DialogRowController else { continue }
                            
                            controller.dialog = self.dialogs[index]
                        }
                        
                        if let callback = withCallback {
                            callback()
                        }
                    })
                } else if dialogsResponse.statusCode == 500 {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                        
                        let retryAction = WKAlertAction(title: "Повторить", style: .default, handler: {
                            self.loadDialogs(noAnimation, append: append)
                        })
                        let cancelAction = WKAlertAction(title: "Отмена", style: .cancel, handler: {})
                        
                        self.presentAlert(withTitle: "Ошибка", message: "На сервере произошла ошибка. Пожалуйста, повторите ваш запрос позже.", preferredStyle: .alert, actions: [retryAction, cancelAction])
                    })
                } else if dialogsResponse.statusCode == 401 {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                        
                        let retryAction = WKAlertAction(title: "Повторить", style: .default, handler: {
                            self.loadDialogs(noAnimation, append: append)
                        })
                        let cancelAction = WKAlertAction(title: "Отмена", style: .cancel, handler: {})
                        
                        self.presentAlert(withTitle: "Ошибка", message: "Вы не авторизованы", preferredStyle: .alert, actions: [retryAction, cancelAction])
                    })
                }
            }
        }
        
        if !noAnimation {
            self.startAnimating()
        }
        
        getDialogDataTask.resume()
    }
}
