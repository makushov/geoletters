//
//  Ext.swift
//  Letters 1.1
//
//  Created by Stanislav Makushov on 4/5/17.
//  Copyright © 2017 KingOP. All rights reserved.
//

import Foundation
import WatchKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}

extension String {
    /// Truncates the string to length number of characters and
    /// appends optional trailing string if longer
    func truncate(_ length: Int, trailing: String? = nil) -> String {
        if self.length > length {
            return self.substring(to: self.characters.index(self.startIndex, offsetBy: length)) + (trailing ?? "")
        } else {
            return self
        }
    }
    
    var length: Int {
        return self.characters.count
    }
}

func isAuthenticated() -> Bool {
    return getUserToken() != nil && getUserLogin() != nil && getUserId() != nil
}

func stringDateFromTimestamp(_ timestamp: TimeInterval) -> String {
    let date : Date = Date(timeIntervalSince1970: timestamp)
    
    let dateFormatter = DateFormatter()
    
    if Calendar.current.isDateInToday(date) {
        dateFormatter.dateStyle = DateFormatter.Style.none//ShortStyle
        dateFormatter.timeStyle = DateFormatter.Style.short//.ShortStyle
    } else {
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.none//.ShortStyle
    }
    
    return dateFormatter.string(from: date)
}

func getUserToken() -> String? {
    return UserDefaults.standard.string(forKey: "auth_token")
}

func setUserToken(_ token: String) {
    UserDefaults.standard.setValue(token, forKey: "auth_token")
}

func getUserId() -> Int? {
    return UserDefaults.standard.integer(forKey: "user_id")
}

func setUserId(_ id: Int) {
    UserDefaults.standard.set(id, forKey: "user_id")
}

func getUserLogin() -> String? {
    return UserDefaults.standard.string(forKey: "user_login")
}

func setUserLogin(_ login: String) {
    UserDefaults.standard.setValue(login, forKey: "user_login")
}

func getURLRequest(_ method: String, url: String, params: Dictionary<String, Any> = Dictionary(), token: String? = nil) -> URLRequest {
    let urlValue: URL = URL(string: url)!
    
    let request = NSMutableURLRequest(url: urlValue)
    request.httpMethod = method
    request.addValue("application/json", forHTTPHeaderField: "Accept")
    request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
    request.addValue("keep-alive", forHTTPHeaderField: "Connection")
    
    if token != nil {
        request.addValue("Bearer " + token!, forHTTPHeaderField: "Authorization")
    }
    
    if params.count > 0 {
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions())
        } catch let error as NSError {
            print(error.localizedDescription)
            request.httpBody = nil
        }
        
        request.addValue((request.httpBody?.count.description)!, forHTTPHeaderField: "Content-Length")
    }
    
    return request as URLRequest
}

func maskedAvatarFromImage(_ image: UIImage, original: Bool = false) -> UIImage {
    var width : CGFloat = 0
    var height: CGFloat = 0
    
    if !original {
        width = 60
        height = 60
    } else {
        width = image.size.width
        height = image.size.height
    }
    let imageRect : CGRect = CGRect(x: 0, y: 0, width: width, height: height)
    UIGraphicsBeginImageContextWithOptions(imageRect.size, false, 0)
    
    let circlePath : UIBezierPath = UIBezierPath(ovalIn: imageRect)
    circlePath.addClip()
    
    image.draw(in: imageRect)
    let maskedImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return maskedImage!
}
