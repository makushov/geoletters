//
//  MessagesInterfaceController.swift
//  Letters 1.1
//
//  Created by Stanislav Makushov on 08.04.17.
//  Copyright © 2017 KingOP. All rights reserved.
//

import WatchKit

class MessagesInterfaceController: BaseInterfaceController {
    
    let identifiers = ["outgoingCell", "incomingCell"/*, "incomingImageCell", "outgoingImageCell"*/]
    
    @IBOutlet var indicatorImage: WKInterfaceImage!
    @IBOutlet var table: WKInterfaceTable!
    
    var dialogId = 0
    
    var isLoaded = false
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        if !self.isLoaded {
            self.loadMessages()
        }
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
}

//MARK: actions
extension MessagesInterfaceController {
    
    func startAnimating() {
        self.table.setHidden(true)
        self.indicatorImage.setHidden(false)
        self.indicatorImage.setImageNamed("Activity")
        self.indicatorImage.startAnimatingWithImages(in: NSRange(location: 0, length: 15), duration: 1.0, repeatCount: 0)
    }
    
    func stopAnimating() {
        self.indicatorImage.stopAnimating()
        self.indicatorImage.setImage(nil)
        self.indicatorImage.setHidden(true)
        self.table.setHidden(false)
    }
    
    func loadMessages() {
        // загрузка списка сообщений
        let url = getBackendApiPath() + "dialog/\(self.dialogId.description)/messages?limit=30&offset=0"
        let getMessagesRequest = getURLRequest("GET", url: url, params: Dictionary<String, AnyObject>(), token: getUserToken())
        
        let getMessagesTask = URLSession.shared.dataTask(with: getMessagesRequest, completionHandler: { (data, response, error) in
            
            if (error != nil) {
                // сюда надо добавить обработку ошибок
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                    
                    let retryAction = WKAlertAction(title: "Повторить", style: .default, handler: {
                        self.loadMessages()
                    })
                    let cancelAction = WKAlertAction(title: "Отмена", style: .cancel, handler: {})
                    
                    self.presentAlert(withTitle: "Ошибка", message: error?.localizedDescription, preferredStyle: .alert, actions: [retryAction, cancelAction])
                    //playErrorSound()
                })
            } else {
                let messagesResponse = response as! HTTPURLResponse
                
                print(messagesResponse.statusCode)
                
                if messagesResponse.statusCode == 200 {
                    // все хорошо, сообщения загрузились, откроем сокет для диалога
                    let newArray = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [Dictionary<String, AnyObject>]
                    
                    var i = 0
                    //var indexPaths : [NSIndexPath] = []
                    
                    for pm in newArray {
                        var item : Dictionary<String, AnyObject> = pm 
                        item["status"] = "sent" as AnyObject?
                        //self.message_dict.append(item)
                        
                        let sender = (item["user_id"] as! Int).description
                        
                        if !(item["attachment"] is NSNull) && item["attachment"] != nil {
                            // есть аттач, надо проверить тип
                            /*let attach = item["attachment"] as! Dictionary<String, AnyObject>
                            
                            if attach["type"] as! String == "location" {
                                // отобразим местоположение
                                
                                var username : String = ""
                                
                                if self.members.count > 2 {
                                    for user in self.members {
                                        if user["id"] as! Int == item["user_id"] as! Int {
                                            username = user["username"] as! String
                                            break
                                        }
                                    }
                                } else {
                                    username = sender == getUserId()!.description ? getUserLogin()! : self.userName
                                }
                                
                                let location : JSQLocationMediaItem = JSQLocationMediaItem(maskAsOutgoing: sender == getUserId()!.description)
                                let message = JSQMessage(senderId: sender, senderDisplayName: username, date: Date(timeIntervalSince1970: item["created_at"] as! Double), media: location)
                                
                                self.messages.insert(message!, at: 0)
                                
                                let coordinates = (attach["content"] as! String).characters.split{$0 == ";"}.map(String.init)
                                
                                let locationToShow = CLLocation(latitude: Double(coordinates[0])!, longitude: Double(coordinates[1])!)
                                
                                location.setLocation(locationToShow, region: MKCoordinateRegionMakeWithDistance(locationToShow.coordinate, 500.0, 500.0), withCompletionHandler: { () -> Void in
                                    self.collectionView?.reloadData()
                                })
                            } else {
                                // прислали фотку
                                // проверим, есть ли она в кэше
                                var photo : JSQPhotoMediaItem = JSQPhotoMediaItem()
                                
                                if avatarsCacheDataStack.isAvatarInCache(attach["id"] as! Int) {
                                    let image = avatarsCacheDataStack.getAvatarImageFromCache(attach["id"] as! Int)
                                    
                                    if image == nil {
                                        // картинка недокэширована
                                        photo = JSQPhotoMediaItem(image: nil)
                                    } else {
                                        photo = JSQPhotoMediaItem(image: image)
                                    }
                                    
                                    
                                    photo.appliesMediaViewMaskAsOutgoing = sender == getUserId()!.description
                                    
                                    var username : String = ""
                                    
                                    if self.members.count > 2 {
                                        for user in self.members {
                                            if user["id"] as! Int == item["user_id"] as! Int {
                                                username = user["username"] as! String
                                                break
                                            }
                                        }
                                    } else {
                                        username = sender == getUserId()!.description ? getUserLogin()! : self.userName
                                    }
                                    
                                    let message = JSQMessage(senderId: sender, senderDisplayName: username, date: Date(timeIntervalSince1970: item["created_at"] as! Double), media: photo)
                                    
                                    self.messages.insert(message!, at: 0)
                                    
                                    if image == nil {
                                        avatarsCacheDataStack.safeRemoveAvatarFromCache(attach["id"] as! Int)
                                        
                                        DispatchQueue(label: "loadImg", attributes: []).async(execute: {
                                            
                                            let imageUrl = URL(string: getBackendDomainPath() + (attach["content"] as! String))!
                                            let imageData : Data? = try? Data(contentsOf: imageUrl)
                                            
                                            if imageData != nil {
                                                DispatchQueue.main.async(execute: {
                                                    photo.image = UIImage(data: imageData!)
                                                    
                                                    self.collectionView?.reloadData()
                                                    
                                                    avatarsCacheDataStack.saveAvatarToCache(attach["id"] as! Int, fileName: "\(imageUrl.lastPathComponent)", imageData: imageData!)
                                                })
                                            }
                                        })
                                    }
                                } else {
                                    // фотографии нет в кэше, сохраним ее в кэш
                                    photo = JSQPhotoMediaItem(image: nil)
                                    photo.appliesMediaViewMaskAsOutgoing = sender == getUserId()!.description
                                    
                                    var username : String = ""
                                    
                                    if self.members.count > 2 {
                                        for user in self.members {
                                            if user["id"] as! Int == item["user_id"] as! Int {
                                                username = user["username"] as! String
                                                break
                                            }
                                        }
                                    } else {
                                        username = sender == getUserId()!.description ? getUserLogin()! : self.userName
                                    }
                                    
                                    let message = JSQMessage(senderId: sender, senderDisplayName: username, date: Date(timeIntervalSince1970: item["created_at"] as! Double), media: photo)
                                    
                                    self.messages.insert(message!, at: 0)
                                    
                                    DispatchQueue(label: "loadImg\(attach["id"] as! Int)", attributes: []).async(execute: {
                                        let imageUrl : URL = URL(string: getBackendDomainPath() + (attach["content"] as! String))!
                                        
                                        let imageData : Data? = try? Data(contentsOf: imageUrl)
                                        
                                        if imageData != nil {
                                            DispatchQueue.main.async(execute: {
                                                photo.image = UIImage(data: imageData!)
                                                self.collectionView?.reloadData()
                                                
                                                avatarsCacheDataStack.saveAvatarToCache(attach["id"] as! Int, fileName: imageUrl.lastPathComponent, imageData: imageData!)
                                            })
                                        }
                                    })
                                }
                            }*/
                        } else {
                            var username : String = ""
                            
                            if self.members.count > 2 {
                                for user in self.members {
                                    if user["id"] as! Int == item["user_id"] as! Int {
                                        username = user["username"] as! String
                                        break
                                    }
                                }
                            } else {
                                username = sender == getUserId()!.description ? getUserLogin()! : self.userName
                            }
                            
                            let message = JSQMessage(senderId: sender, senderDisplayName: username, date: Date(timeIntervalSince1970: (pm as! NSDictionary)["created_at"] as! Double), text: (pm as! NSDictionary)["message"] as! String)
                            self.messages.insert(message!, at: 0)
                        }
                        i += 1
                    }
                    
                    DispatchQueue.main.async(execute: {
                        if !self.isLoaded {
                            self.isLoaded = true
                        }
                        self.collectionView!.reloadData()
                        //self.collectionView?.insertItemsAtIndexPaths(indexPaths)
                        
                        self.collectionView!.collectionViewLayout.invalidateLayout(with: JSQMessagesCollectionViewFlowLayoutInvalidationContext())
                        
                        self.finishReceivingMessage(animated: false)
                        self.collectionView!.layoutIfNeeded()
                        if !append {
                            self.scrollToBottom(animated: false)
                            self.totalCount = Int(messagesResponse.allHeaderFields["X-Pagination-Total-Count"] as! String)!
                        } else {
                            
                            self.collectionView!.scrollToItem(at: IndexPath(item: self.messagesLimit - 1, section: 0), at: .top, animated: false)
                        }
                        
                        if self.totalCount > self.messages.count {
                            self.showLoadEarlierMessagesHeader = true
                        }
                        
                        // обновим данный диалог в списке диалогов
                        // это просто 3.14здец
                        // получим индекс диалога, если список диалогов открыт
                        if dialogsController != nil {
                            let index = dialogsController?.dialogIds.index(of: self.dialogId!)
                            
                            if index != nil {
                                let indexPath = IndexPath(item: index!, section: 0)
                                
                                let message = dialogsController!.dialogs[indexPath.row]["message"] as? Dictionary<String, AnyObject>
                                
                                if message != nil {
                                    if message!["user_id"] as? Int != getUserId() && message!["viewed"] as! Int == 0 {
                                        dialogsController!.tableView.cellForRow(at: indexPath)?.backgroundColor = UIColor.white
                                        
                                        var dialog = dialogsController!.dialogs[indexPath.row]
                                        var new_message : Dictionary<String, AnyObject> = dialog["message"] as! Dictionary<String, AnyObject>
                                        
                                        new_message["viewed"] = 1 as AnyObject?
                                        dialog["message"] = new_message as AnyObject?
                                        
                                        dialogsController!.dialogs[indexPath.row] = dialog
                                    } else if message!["user_id"] as? Int == getUserId() {
                                        dialogsController!.tableView.cellForRow(at: indexPath)?.backgroundColor = UIColor.white
                                    }
                                }
                                
                                
                                UIApplication.shared.applicationIconBadgeNumber -= Int(dialogsController!.dialogs[indexPath.row]["unread"] as! String)!
                                
                                UIApplication.shared.applicationIconBadgeNumber = max (0, UIApplication.shared.applicationIconBadgeNumber)
                                
                                mainContainer?.tabBar.items![1].badgeValue = UIApplication.shared.applicationIconBadgeNumber > 0 ? UIApplication.shared.applicationIconBadgeNumber.description : nil
                                
                                dialogsController!.dialogs[indexPath.row]["unread"] = "0" as AnyObject?
                                (dialogsController!.tableView.cellForRow(at: indexPath) as! DialogCell).unreadCount.text = ""
                                (dialogsController!.tableView.cellForRow(at: indexPath) as! DialogCell).unreadCountBG.isHidden = true
                                
                                dialogsController!.tableView.deselectRow(at: indexPath, animated: true)
                            }
                        }
                    })
                } else if messagesResponse.statusCode == 401 {
                    error401Action(self)
                } else if messagesResponse.statusCode == 500 {
                    TSMessage.showNotification(
                        in: self,
                        title: "Ошибка",
                        subtitle: error500,
                        type: TSMessageNotificationType.error,
                        duration: 2.0,
                        canBeDismissedByUser: true
                    )
                }
            }
        }) 
        
        getMessagesTask.resume()
    }
    
    @IBAction func refreshAction() {
    }
    @IBAction func replyAction() {
        self.showInput()
    }
    
    func showInput() {
        self.presentTextInputController(withSuggestions: ["OK", "Спасибо", "Конечно", "Нет"], allowedInputMode: .allowEmoji) { (array) in
            if let results = array {
                print(results)
            }
        }
    }
}
