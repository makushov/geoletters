//
//  IncomingRowController.swift
//  Letters 1.1
//
//  Created by Stanislav Makushov on 08.04.17.
//  Copyright © 2017 KingOP. All rights reserved.
//

import WatchKit

class IncomingRowController: NSObject {

    @IBOutlet var group: WKInterfaceGroup!
    @IBOutlet var authorLabel: WKInterfaceLabel!
    @IBOutlet var textLabel: WKInterfaceLabel!
    @IBOutlet var dateLabel: WKInterfaceLabel!
    
}
