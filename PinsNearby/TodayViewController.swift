//
//  TodayViewController.swift
//  PinsNearby
//
//  Created by Stanislav Makushov on 08.10.16.
//  Copyright © 2016 KingOP. All rights reserved.
//

import UIKit
import NotificationCenter
import MapKit

class TodayViewController: UIViewController, NCWidgetProviding, MKMapViewDelegate, CLLocationManagerDelegate {
    
    lazy var locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        return manager
    }()
    
    @IBOutlet weak var snapshotView: UIImageView!
    var snapshottedImage : UIImage = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.locationManager.startUpdatingLocation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdate(completionHandler: ((NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        
        self.locationManager.startUpdatingLocation()
        
        completionHandler(NCUpdateResult.newData)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.createSnapshotWithPins(withLocation: locations[0].coordinate)
    }
    
    func createSnapshotWithPins(withLocation: CLLocationCoordinate2D) {
        // сделаем снэпшот
        
        let options : MKMapSnapshotOptions = MKMapSnapshotOptions()
        options.region = MKCoordinateRegion(center: withLocation, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        options.size = CGSize(width: self.snapshotView.frame.width, height: self.snapshotView.frame.height)
        options.scale = UIScreen.main.scale
        
        let snapshotter : MKMapSnapshotter = MKMapSnapshotter(options: options)
        snapshotter.start(with: DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default)) { (snapshot, error) -> Void in
            
            if error == nil {
                let image : UIImage = (snapshot?.image)!
                
                /*let new_pin : MKAnnotationView = MKAnnotationView(annotation: nil, reuseIdentifier: nil)
                var coordinatePoint : CGPoint = (snapshot?.point(for: self.pin.coordinate))!
                
                coordinatePoint.x += new_pin.centerOffset.x - (new_pin.bounds.width / 2)
                coordinatePoint.y += new_pin.centerOffset.y - (new_pin.bounds.height / 2)*/
                
                UIGraphicsBeginImageContextWithOptions(image.size, true, image.scale)
                
                /*image.draw(at: CGPoint.zero)
                
                //let index : Int = pinInfoResult!["category_id"] as! Int >= pinsCategories.count ? 0 : pinInfoResult!["category_id"] as! Int
                
                new_pin.image = UIImage(named: /*pinsCategories[index].image*/"pinIcon")
                new_pin.image?.draw(at: coordinatePoint)*/
                
                self.snapshottedImage = UIGraphicsGetImageFromCurrentImageContext()!
                
                UIGraphicsEndImageContext()
                
                DispatchQueue.main.async(execute: {
                    self.snapshotView.image = self.snapshottedImage
                    //self.hideSpinner()
                })
            } else {
                DispatchQueue.main.async(execute: {
                    //self.hideSpinner()
                })
            }
        }
    }
}
